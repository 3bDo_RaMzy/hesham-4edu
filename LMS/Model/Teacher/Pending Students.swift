//
//  Pinding Students.swift
//  LMS
//
//  Created by MacBook on 6/23/20.
//  Copyright © 2020 eaglez-group. All rights reserved.
//

import UIKit

struct PendingStudents {
 
  
    var Id:           Int       = 0
    var FullName:     String    = ""
    var EmailAddress: String    = ""
    var Gender:       Int       = 0
    var CreatedTime:  String    = ""
    var Mobile:       String    = ""
    var GroupName:    String    = ""
    

    init(Id: Int, FullName: String, EmailAddress: String, Gender: Int,
         CreatedTime: String, Mobile: String, GroupName: String ){
        
        self.Id           = Id
        self.FullName     = FullName
        self.EmailAddress = EmailAddress
        self.Gender       = Gender
        self.CreatedTime  = CreatedTime
        self.Mobile       = Mobile
        self.GroupName    = GroupName
        
    }
    
    init(student: PendingStudents) {
        
        self.Id           = student.Id
        self.FullName     = student.FullName
        self.EmailAddress = student.EmailAddress
        self.Gender       = student.Gender
        self.CreatedTime  = student.CreatedTime
        self.Mobile       = student.Mobile
        self.GroupName    = student.GroupName
        
    }
}
