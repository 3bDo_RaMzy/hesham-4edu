//
//  PendingStudentsCell.swift
//  LMS
//
//  Created by MacBook on 6/23/20.
//  Copyright © 2020 eaglez-group. All rights reserved.
//

import UIKit

class PendingStudentsCell: UITableViewCell {

    
    var PendingStudentsViewModel: PendingStudents! {
         
         
           didSet {
               
             ST_Name.text  = "Name: " + PendingStudentsViewModel.FullName
             ST_Group.text = "Group: " + PendingStudentsViewModel.GroupName
             ST_Phone.text = PendingStudentsViewModel.Mobile
             ST_Date.text  = PendingStudentsViewModel.CreatedTime.components(separatedBy: "T").first
             
       }
    }
    
    @IBOutlet weak var ST_Name:  UILabel!
    @IBOutlet weak var ST_Group: UILabel!
    @IBOutlet weak var ST_Phone: UILabel!
    @IBOutlet weak var ST_Date:  UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
