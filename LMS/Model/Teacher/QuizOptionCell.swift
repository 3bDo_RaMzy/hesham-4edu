//
//  QuizOptionCell.swift
//  LMS
//
//  Created by MacBook on 6/21/20.
//  Copyright © 2020 eaglez-group. All rights reserved.
//

import UIKit

class QuizOptionCell: UITableViewCell {
 
        
    @IBOutlet weak var option_name: UILabel!
    @IBOutlet weak var option_view: UIView!
        
        
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
            
        option_name.sizeToFit()
        option_name.numberOfLines = 0
    }

    //For single choice selection
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        selectionStyle = .none
    }
        
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
