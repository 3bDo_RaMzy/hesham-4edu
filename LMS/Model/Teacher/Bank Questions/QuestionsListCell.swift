//
//  QuestionsListCell.swift
//  LMS
//
//  Created by MacBook on 7/13/20.
//  Copyright © 2020 eaglez-group. All rights reserved.
//

import UIKit

class QuestionsListCell: UITableViewCell {

 
    var BankQuestionsViewModel: BankQuestionsModel! {
         
         
           didSet {
               
            QuestionName.text = BankQuestionsViewModel.Value
             
       }
    }
    
    @IBOutlet weak var QuestionName: UILabel!
    
        override func awakeFromNib() {
            super.awakeFromNib()
            // Initialization code
            
            QuestionName.sizeToFit()
            QuestionName.numberOfLines = 0
        }

        //For single choice selection
        required init(coder aDecoder: NSCoder) {
            super.init(coder: aDecoder)!
            selectionStyle = .none
        }
        
        override func setSelected(_ selected: Bool, animated: Bool) {
            super.setSelected(selected, animated: animated)

            // Configure the view for the selected state
        }

    }
