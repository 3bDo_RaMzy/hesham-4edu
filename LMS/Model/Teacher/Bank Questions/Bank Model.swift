//
//  Bank Model.swift
//  LMS
//
//  Created by Abdo on 7/9/20.
//  Copyright © 2020 eaglez-group. All rights reserved.
//

import UIKit
import ObjectMapper


struct BankModel: Decodable {
     
    let Id:     Int?
    let Name:   String?
    
    init( Id: Int, Name: String ) {
       
        self.Id    = Id
        self.Name  = Name
        
    }
    
    init(bank: BankModel) {
        
        self.Id    = bank.Id
        self.Name  = bank.Name
    }
    
}


//-----------------------------------------------------
struct BankQuestionsModel: Decodable {
     
//    "Id": 34,
//    "Value": "question one true false",
//    "Type": 1, --------------------------> Cannot parse Variable: Type
//    "QuestionBankId": 27,
//    "QuestionsBank": null,
//    "Answers": null,
//    "QuestionsAttatchments": null,
//    "IsDeleted": false
    
    let Id:             Int?
    let Value:          String?
    let QuestionBankId: Int?
    let IsDeleted:      Bool?
    
    init( Id: Int, Value: String, QuestionBankId: Int, IsDeleted: Bool ) {
       
        self.Id             = Id
        self.Value          = Value
        self.QuestionBankId = QuestionBankId
        self.IsDeleted      = IsDeleted
        
    }
    
    init(bank: BankQuestionsModel) {
        
        self.Id             = bank.Id
        self.Value          = bank.Value
        self.QuestionBankId = bank.QuestionBankId
        self.IsDeleted      = bank.IsDeleted
    }
    
}


//-----------------------------------------------------
class OptionModel: NSObject, Decodable, Mappable {


    required init?(from: Any){

    }

    required init?(map: Map){

    }

    override init() {

    }
 
    var Value:          String = ""
    var IsCorectAnswer: Bool   = false

     init( Value: String, IsCorectAnswer: Bool ) {
        self.Value          = Value
        self.IsCorectAnswer = IsCorectAnswer
    }

    func mapping(map: Map) {

        Value  <- map["Value"]
        IsCorectAnswer   <- map["IsCorectAnswer"]

    }

}
