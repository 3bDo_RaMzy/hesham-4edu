//
//  visitVideoCell.swift
//  LMS
//
//  Created by Abdo on 11/29/19.
//  Copyright © 2019 eaglez-group. All rights reserved.
//

import UIKit

class visitVideoCell: UITableViewCell {

    
    var activityIndicator = UIActivityIndicatorView()
    
    
    var chanalViewModel: ChanalViewModel! {
        
        didSet {
            ChanalName?.text = String(chanalViewModel.Term) + "  play list"
            
        }
    }
    
    var videoViewModel: VideoViewModel! {
        
        didSet {
            ChanalName?.text = videoViewModel.title
            
            ChanalImage.addSubview(self.activityIndicator)
            activityIndicator.startAnimating()
            
            let url = NSURL(string: videoViewModel.thumbnailsURL)!
            let task = URLSession.shared.dataTask(with: url as URL) { (responseData, responseUrl, error) -> Void in
                if (responseData != nil && responseUrl != nil && error == nil)
                {
                    if let data = responseData{
                        DispatchQueue.main.async {
                            self.ChanalImage.image = UIImage(data: data)
                            self.activityIndicator.stopAnimating()
                        }
                    }
                } else {
                    DispatchQueue.main.async { // Make sure you're on the main thread here
                        self.ChanalImage.image = UIImage(named: "video show")
                        self.activityIndicator.stopAnimating()
                    }
                }
                
            }
            task.resume()
        }
    }
    
    @IBOutlet weak var ChanalName: UILabel!
    @IBOutlet weak var ChanalBtn: UIButton!
    @IBOutlet weak var ChanalImage: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.activityIndicator = UIActivityIndicatorView(style: .gray)
        self.activityIndicator.frame = CGRect(x: 0, y: 0, width: 46, height: 46)
        self.activityIndicator.hidesWhenStopped = true
        //self.activityIndicator.center = BlogImage.center
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
