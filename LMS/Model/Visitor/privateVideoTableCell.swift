//
//  privateVideoTableCell.swift
//  LMS
//
//  Created by MacBook on 11/19/20.
//  Copyright © 2020 eaglez-group. All rights reserved.
//

import UIKit

class privateVideoTableCell: UITableViewCell {

    var privateVideoViewModel: PrivateVideoViewModel! {
        
        didSet {
            if privateVideoViewModel.Marials.count != 1 {
                
                FolderName?.text = privateVideoViewModel.FileName + "\n \(privateVideoViewModel.Marials.count) files"
                
            } else {
                
                FolderName?.text = privateVideoViewModel.FileName + "\n \(privateVideoViewModel.Marials.count) file"
            }
        }
    }
    
    
    var materialsViewModel: PrivateVideosMaterials! {
        
        didSet {
            
            FolderName?.text = materialsViewModel.Name 
        }
    }
    
    
    @IBOutlet weak var FolderName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
