//
//  Files.swift
//  LMS
//
//  Created by Abdo on 11/30/19.
//  Copyright © 2019 eaglez-group. All rights reserved.
//

import UIKit

//---------------------------------
//---> Model
struct Files: Decodable {

    let Id:                 Int?
    let Path:               String?
    
    init(Id: Int, Path: String) {
            
        self.Id                 = Id
        self.Path               = Path
    }
        
}

//--------------------------------------------
//---> View Model

struct FileViewModel {

    var Id:                 Int    = 0
    var Path:               String = ""
    
    init(file: Files) {
        
        self.Id                 = file.Id    ?? 0
        self.Path               = file.Path  ?? ""
    }
    
}
