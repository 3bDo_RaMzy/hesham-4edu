//
//  visitPDFCell.swift
//  LMS
//
//  Created by Abdo on 11/29/19.
//  Copyright © 2019 eaglez-group. All rights reserved.
//

import UIKit

class visitPDFCell: UITableViewCell {

    var unitViewModel: UnitViewModel! {
        
        didSet {
            UnitName?.text = unitViewModel.Name
            
        }
    }
    
    @IBOutlet weak var UnitName: UILabel!
    @IBOutlet weak var UnitBtn: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
