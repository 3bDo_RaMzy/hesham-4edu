//
//  Private Videos.swift
//  LMS
//
//  Created by MacBook on 11/19/20.
//  Copyright © 2020 eaglez-group. All rights reserved.
//

import UIKit


//--------------------------------- Model

struct PrivateVideos: Decodable {

    let FileName:        String?
    let Marials:         [PrivateVideosMaterials]?
    
    init( FileName: String,  Marials: [PrivateVideosMaterials]) {
            
        self.FileName       = FileName
        self.Marials        = Marials
    }
        
}
//-------
struct PrivateVideosMaterials: Decodable {

    let Path:    String?
    let Name:    String?
    
    init( Path: String,  Name: String) {
            
        self.Path    = Path
        self.Name    = Name
    }
        
}

 
//--------------------------------------------
//---> View Model
struct PrivateVideoViewModel {

    var FileName:  String = ""
    var Marials: [PrivateVideosMaterials] = []
     
    
    init(video: PrivateVideos) {
        
        self.FileName   = video.FileName ?? ""
        self.Marials    = video.Marials  ?? []
    }
    
}

 
