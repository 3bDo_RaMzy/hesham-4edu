//
//  Unites.swift
//  LMS
//
//  Created by Abdo on 11/29/19.
//  Copyright © 2019 eaglez-group. All rights reserved.
//

import UIKit

//---------------------------------
//---> Model
struct Units: Decodable {

    let Id:                 Int?
    let Name:               String?
    let TermSystem:         String?
    let FirstTermSystem:    String?
    let SecondTermSystem:   String?
    let file:               String?
    let Other:              String?
    let gradetype:          String?
    
    init(Id: Int, Name: String, TermSystem: String, FirstTermSystem: String,
         SecondTermSystem: String, file: String, Other: String, gradetype: String) {
            
        self.Id                 = Id
        self.Name               = Name
        self.TermSystem         = TermSystem
        self.FirstTermSystem    = FirstTermSystem
        self.SecondTermSystem   = SecondTermSystem
        self.file               = file
        self.Other              = Other
        self.gradetype          = gradetype
    }
        
}

//--------------------------------------------
//---> View Model

struct UnitViewModel {

    var Id:                 Int    = 0
    var Name:               String = ""
    var TermSystem:         String = ""
    var FirstTermSystem:    String = ""
    var SecondTermSystem:   String = ""
    var file:               String = ""
    var Other:              String = ""
    var gradetype:          String = ""
     
    
    init(unit: Units) {
        
        self.Id                 = unit.Id               ?? 0
        self.Name               = unit.Name             ?? ""
        self.TermSystem         = unit.TermSystem       ?? ""
        self.FirstTermSystem    = unit.FirstTermSystem  ?? ""
        self.SecondTermSystem   = unit.SecondTermSystem ?? ""
        self.file               = unit.file             ?? ""
        self.Other              = unit.Other            ?? ""
        self.gradetype          = unit.gradetype        ?? ""
    }
    
}


