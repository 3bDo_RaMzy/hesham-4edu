//
//  AboutUS.swift
//  LMS
//
//  Created by Abdo on 12/2/19.
//  Copyright © 2019 eaglez-group. All rights reserved.
//

import UIKit


//---------------------------------
//---> Model
struct AboutUS: Decodable {

    let Id:     Int?
    let Key:    String?
    let Value:  String?
    
    init( Id: Int, Key: String, Value: String) {
            
        self.Id     = Id
        self.Key    = Key
        self.Value  = Value
    }
        
}

//--------------------------------------------
//---> View Model
struct AboutUSViewModel {

    var Id:       Int    = 0
    var Key:      String = ""
    var Value:    String = ""
     
    
    init(about: AboutUS) {
        
        self.Id       = about.Id     ?? 0
        self.Key      = about.Key    ?? ""
        self.Value    = about.Value  ?? ""
    }
    
}
