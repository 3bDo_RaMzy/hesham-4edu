//
//  visitAboutUSCell.swift
//  LMS
//
//  Created by Abdo on 12/2/19.
//  Copyright © 2019 eaglez-group. All rights reserved.
//

import UIKit

class visitAboutUSCell: UITableViewCell {

    var aboutUSViewModel: AboutUSViewModel! {
        
        didSet {
            aboutKey?.text   = aboutUSViewModel.Key
            aboutValue?.text = aboutUSViewModel.Value
        }
    }
    
    @IBOutlet weak var aboutKey: UILabel!
    @IBOutlet weak var aboutValue: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
