//
//  Chanals.swift
//  LMS
//
//  Created by Abdo on 12/1/19.
//  Copyright © 2019 eaglez-group. All rights reserved.
//

import UIKit


//---------------------------------
//---> Model
struct Chanals: Decodable {

    let ChanalId:       Int?
    let Path:           String?
    let Term:           String?
    let YouTubeListId:  String?
    
    init( ChanalId: Int, Path: String, Term: String, YouTubeListId: String ) {
            
        self.ChanalId       = ChanalId
        self.Path           = Path
        self.Term           = Term
        self.YouTubeListId  = YouTubeListId
    }
        
}

//--------------------------------------------
//---> View Model

struct ChanalViewModel {

    var ChanalId:       Int    = 0
    var Path:           String = ""
    var Term:           String = ""
    var YouTubeListId:  String = ""
     
    
    init(chanal: Chanals) {
        
        self.ChanalId        = chanal.ChanalId      ?? 0
        self.Path            = chanal.Path          ?? ""
        self.Term            = chanal.Term          ?? ""
        self.YouTubeListId   = chanal.YouTubeListId ?? ""
    }
    
}
