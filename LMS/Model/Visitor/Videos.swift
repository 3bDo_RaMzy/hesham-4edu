//
//  Videos.swift
//  LMS
//
//  Created by Abdo on 12/1/19.
//  Copyright © 2019 eaglez-group. All rights reserved.
//

import UIKit


//---------------------------------
//---> Model
struct Videos: Decodable {

    let title:           String?
    let thumbnailsURL:   String?
    let videoId:         String?
    
    init( title: String, thumbnailsURL: String, videoId: String) {
            
        self.title          = title
        self.thumbnailsURL  = thumbnailsURL
        self.videoId        = videoId
    }
        
}

//--------------------------------------------
//---> View Model
struct VideoViewModel {

    var title:           String = ""
    var thumbnailsURL:   String = ""
    var videoId:         String = ""
     
    
    init(video: Videos) {
        
        self.title           = video.title         ?? ""
        self.thumbnailsURL   = video.thumbnailsURL ?? ""
        self.videoId         = video.videoId       ?? ""
    }
    
}
