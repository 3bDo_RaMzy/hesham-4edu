//
//  ParentStudentsCell.swift
//  LMS
//
//  Created by Abdo on 12/12/19.
//  Copyright © 2019 eaglez-group. All rights reserved.
//

import UIKit

class ParentStudentsCell: UITableViewCell  {
    
    
    
    var activityIndicator = UIActivityIndicatorView()
    
    
    var parentStudentsViewModel: ParentStudentsViewModel! {
        
        didSet {
            
            StudentName?.text = parentStudentsViewModel.StundetName
            StudentGroupName?.text = parentStudentsViewModel.GroupName
            
            
            StudentImage.addSubview(self.activityIndicator)
            activityIndicator.startAnimating()
            if parentStudentsViewModel  == nil {
                
                self.StudentImage.image = UIImage(named: "student.png")
            } else if parentStudentsViewModel?.ImageUrl == "" {
                
                self.StudentImage.image = UIImage(named: "student.png")
            } else {
                
                self.StudentImage.sd_setImage(with: URL(string:  URLs.ImageDomain + parentStudentsViewModel.ImageUrl.dropFirst(1)), placeholderImage: UIImage(named: "student.png"))
            }
            self.activityIndicator.stopAnimating()
            
            
        }
    }
    
    
    @IBOutlet weak var StudentImage:      UIImageView!
    @IBOutlet weak var StudentName:       UILabel!
    @IBOutlet weak var StudentGroupName:  UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        //PriceView.backgroundColor = UIColor(patternImage: UIImage(named: "offerBag")!)
        
        self.activityIndicator = UIActivityIndicatorView(style: .gray)
        self.activityIndicator.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        self.activityIndicator.hidesWhenStopped = true
        //self.activityIndicator.center = BlogImage.center
 
//        self.layer.cornerRadius = 10
//        self.layer.borderWidth = 1.0
//        self.layer.borderColor = UIColor.lightGray.cgColor
//
//        self.layer.backgroundColor = UIColor.white.cgColor
//        self.layer.shadowColor = UIColor.gray.cgColor
//        self.layer.shadowOffset = CGSize(width: 2.0, height: 4.0)
//        self.layer.shadowRadius = 2.0
//        self.layer.shadowOpacity = 1.0
//        self.layer.masksToBounds = false
    
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
