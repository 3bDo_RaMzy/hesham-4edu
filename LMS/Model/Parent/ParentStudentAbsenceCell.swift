//
//  ParentStudentAbsenceCell.swift
//  LMS
//
//  Created by Abdo on 12/16/19.
//  Copyright © 2019 eaglez-group. All rights reserved.
//

import UIKit

class ParentStudentAbsenceCell: UITableViewCell {
    
    
    var absenceReportViewModel: QuizResultViewModel! {
        didSet {
            
            quizDate?.text   = absenceReportViewModel.date
            if (absenceReportViewModel.degree == -1 ) {
                
                quizLogo.image = UIImage(named: "absence")
                quizDegree?.text    = " No Degree "
                quizMaxDegree?.text = "From: " + String(absenceReportViewModel.maxdegree)
                
            } else {
                
                quizLogo.image = UIImage(named: "attend")
                quizDegree?.text    = "Degree: " + String(absenceReportViewModel.degree)
                quizMaxDegree?.text = "From: " + String(absenceReportViewModel.maxdegree)
                
            }
            
            
            
        }
    }
    
    
    @IBOutlet weak var quizLogo:      UIImageView!
    @IBOutlet weak var quizDate:      UILabel!
    @IBOutlet weak var quizDegree:    UILabel!
    @IBOutlet weak var quizMaxDegree: UILabel!
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
