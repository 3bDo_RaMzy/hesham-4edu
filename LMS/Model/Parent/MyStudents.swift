//
//  MyStudents.swift
//  LMS
//
//  Created by Abdo on 12/12/19.
//  Copyright © 2019 eaglez-group. All rights reserved.
//


import UIKit


//---------------------------------
//---> Model

/*
{
    "StudentId": 3236,
    "GroupName": "Shebien Elkom 2nd Sec at Hesham Allam Academy ",
    "Gender": "Male",
    "ImageUrl": "~/attachments/UserImages/b30cb755-b95e-4578-a8e9-287b4eadfb18.jpeg",
    "StundetName": "rami",
    "Grade": "2nd Secondary",
    "AppointmentsTimeingGroups": [
        {
            "Day": "Monday",
            "From": "18:45:00",
            "To": "20:00:00"
        },
        {
            "Day": "Thursday",
            "From": "18:45:00",
            "To": "20:00:00"
        }
    ]
}
*/

struct ParentStudents: Decodable {
     
    let StudentId:     Int?
    let GroupName:     String?
    let Gender:        String?
    let ImageUrl:      String?
    let StundetName:   String?
    let Grade:         String?
    let AppointmentsTimeingGroups:    [StudentGroupTimes]?
    
    init( StudentId: Int, GroupName: String, Gender: String, ImageUrl: String, StundetName: String, Grade: String, AppointmentsTimeingGroups:  [StudentGroupTimes]) {
        self.StudentId     = StudentId
        self.GroupName     = GroupName
        self.Gender        = Gender
        self.ImageUrl      = ImageUrl
        self.StundetName   = StundetName
        self.Grade         = Grade
        self.AppointmentsTimeingGroups = AppointmentsTimeingGroups
        
    }
}

struct StudentGroupTimes: Decodable {
     
    let Day:     String?
    let From:    String?
    let To:      String?
    
    init(  Day: String, From: String, To: String ) {
        self.Day     = Day
        self.From    = From
        self.To      = To
        
    }
}
//---------------------------------------------------------
//---> View Model
struct ParentStudentsViewModel {

    var StudentId:       Int        = 0
    var GroupName:       String     = ""
    var Gender:          String     = ""
    var ImageUrl:        String     = ""
    var StundetName:     String     = ""
    var Grade:           String     = ""
    var AppointmentsTimeingGroups: [StudentGroupTimes] = []

    init() {

    }
    
    init(parentST: ParentStudents) {
        self.StudentId    = parentST.StudentId      ?? 0
        self.GroupName    = parentST.GroupName      ?? ""
        self.Gender       = parentST.Gender         ?? ""
        self.ImageUrl     = parentST.ImageUrl       ?? ""
        self.StundetName  = parentST.StundetName    ?? ""
        self.Grade        = parentST.Grade          ?? ""
        self.AppointmentsTimeingGroups = parentST.AppointmentsTimeingGroups     ?? []
    }
}
