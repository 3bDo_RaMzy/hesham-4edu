//
//  RequestCell.swift
//  LMS
//
//  Created by Abdo on 1/2/20.
//  Copyright © 2020 eaglez-group. All rights reserved.
//

import UIKit

class RequestCell: UITableViewCell {

    var requestViewModel: Request! {
          
          didSet {
              
            RequestData?.text = requestViewModel.Detalis
            let token = requestViewModel.Time.components(separatedBy: "T")
            RequestTime.setTitle(token[0], for: .normal)
                      
                if requestViewModel.Comments.count > 0 {
                              
                    RequestReplayCount?.setTitle(String(requestViewModel.Comments.count) + "   Replays", for: .normal)
                          
                } else { RequestReplayCount?.setTitle("0  Replays", for: .normal)   }
              
            }
      }
    
    var commentViewModel: Comment! {
             
         didSet {
                 
                RequestData?.text = commentViewModel.Message
                let token = commentViewModel.Time.components(separatedBy: "T")
                RequestTime.setTitle(token[0], for: .normal)
                            
            }
    }
    
    @IBOutlet weak var RequestData: UILabel!
    @IBOutlet weak var RequestTime: UIButton!
    @IBOutlet weak var RequestReplayCount: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
