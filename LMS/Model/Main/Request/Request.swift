//
//  Request.swift
//  LMS
//
//  Created by Abdo on 1/2/20.
//  Copyright © 2020 eaglez-group. All rights reserved.
//

import UIKit



//-----------------------------------------------------------------------------------
//---> View Model
struct Request {
 
    var Id:         Int       = 0
    var Time:       String    = ""
    var Status:     String    = ""
    var Title:      String    = ""
    var Detalis:    String    = ""
    var Comments:   [Comment] = []

    init(Id: Int, Time: String, Status: String,Title: String, Detalis: String, Comments: [Comment] ){
        
        self.Id = Id
        self.Time = Time
        self.Status = Status
        self.Title = Title
        self.Detalis = Detalis
        self.Comments = Comments
    }
    
    init(request: Request) {
        self.Id          = request.Id
        self.Time        = request.Time
        self.Status      = request.Status
        self.Title       = request.Title
        self.Detalis     = request.Detalis
        self.Comments    = request.Comments
    }
}

struct Comment {
     
    var StudentId:  Int       = 0
    var Time:       String    = ""
    var Message:    String    = ""

    init( Time: String, Message: String, StudentId: Int ){
        self.Time    = Time
        self.Message = Message
        self.StudentId = StudentId
    }
    
    init(comment: Comment) {
        self.Time     = comment.Time
        self.Message  = comment.Message
        self.StudentId = comment.StudentId
    }
}
