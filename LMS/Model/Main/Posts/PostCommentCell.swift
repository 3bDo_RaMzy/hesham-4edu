//
//  PostCommentCell.swift
//  LMS
//
//  Created by Abdo on 1/31/20.
//  Copyright © 2020 eaglez-group. All rights reserved.
//

import UIKit

class PostCommentCell: UITableViewCell {

    
    var activityIndicator = UIActivityIndicatorView()
    
    var postCommentViewModel: CommentPost! {
        
        
          didSet {
              
            CommentUserName.text = postCommentViewModel.UserName
            let token = postCommentViewModel.CommentDate.components(separatedBy: "T")
            CommentDate.text = token[0]
            
            CommentUserImage.addSubview(self.activityIndicator)
            self.activityIndicator.startAnimating()
            let url = NSURL(string: URLs.BaseUrl + postCommentViewModel.UserImage.dropFirst() )!
            let task = URLSession.shared.dataTask(with: url as URL) { (responseData, responseUrl, error) -> Void in
                        if (responseData != nil && responseUrl != nil && error == nil)
                            {
                                if let data = responseData{
                                DispatchQueue.main.async {
                                                self.CommentUserImage.image = UIImage(data: data)
                                                self.activityIndicator.stopAnimating()
                                    }
                                }
                         } else {
                                DispatchQueue.main.async { // Make sure you're on the main thread here
                                    self.CommentUserImage.image = UIImage(named: "student")
                                    self.activityIndicator.stopAnimating()
                                }
                            }
                                             
                        }
            task.resume()
            
            
            CommentText.text = postCommentViewModel.Comment
            //CommentReplayCount?.setTitle(String(postViewModel.CommentCount) + "   Comments", for: .normal)
            }
      }
    
    
    
    
    
    @IBOutlet weak var CommentUserImage:     UIImageView!
    @IBOutlet weak var CommentDate:          UILabel!
    @IBOutlet weak var CommentUserName:      UILabel!
    @IBOutlet weak var CommentText:          UILabel!
    @IBOutlet weak var CommentReplayCount: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
