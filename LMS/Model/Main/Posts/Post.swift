//
//  Post.swift
//  LMS
//
//  Created by Abdo on 1/25/20.
//  Copyright © 2020 eaglez-group. All rights reserved.
//

import UIKit

struct Post {
 
    var Id:           Int       = 0
    var Post:         String    = ""
    var PostDate:     String    = ""
    var UserName:     String    = ""
    var UserImage:    String    = ""
    var UserId:       Int       = 0
    var CommentCount: Int       = 0

    init(Id: Int, Post: String, PostDate: String,UserName: String, UserImage: String, UserId: Int, CommentCount: Int){
        
        self.Id           = Id
        self.Post         = Post
        self.PostDate     = PostDate
        self.UserName     = UserName
        self.UserImage    = UserImage
        self.UserId       = UserId
        self.CommentCount = CommentCount
    }
    
    init(post: Post) {
        
        self.Id           = post.Id
        self.Post         = post.Post
        self.PostDate     = post.PostDate
        self.UserName     = post.UserName
        self.UserImage    = post.UserImage
        self.UserId       = post.UserId
        self.CommentCount = post.CommentCount
    }
}

//-------------------------------------------------------------------
struct CommentPost {
    
    var CommentId:    Int       = 0
    var Comment:      String    = ""
    var CommentDate:  String    = ""
    var UserName:     String    = ""
    var UserImage:    String    = ""
    var UserId:       Int       = 0
    var ReplayCount:  Int       = 0

    init(CommentId: Int, Comment: String, CommentDate: String, UserName: String, UserImage: String, UserId: Int, ReplayCount: Int){
        
        self.CommentId     = CommentId
        self.Comment       = Comment
        self.CommentDate   = CommentDate
        self.UserName      = UserName
        self.UserImage     = UserImage
        self.UserId        = UserId
        self.ReplayCount   = ReplayCount
    }
    
    init(comment: CommentPost) {
        
        self.CommentId     = comment.CommentId
        self.Comment       = comment.Comment
        self.CommentDate   = comment.CommentDate
        self.UserName      = comment.UserName
        self.UserImage     = comment.UserImage
        self.UserId        = comment.UserId
        self.ReplayCount   = comment.ReplayCount
    }
}

//-------------------------------------------------------------------
struct ReplayCommentPost {
    
    var ReplayId:     Int       = 0
    var Replay:       String    = ""
    var ReplayDate:   String    = ""
    var UserName:     String    = ""
    var UserImage:    String    = ""
    var UserId:       Int       = 0

    init(ReplayId: Int, Replay: String, ReplayDate: String, UserName: String, UserImage: String, UserId: Int ){
        
        self.ReplayId      = ReplayId
        self.Replay        = Replay
        self.ReplayDate    = ReplayDate
        self.UserName      = UserName
        self.UserImage     = UserImage
        self.UserId        = UserId
    }
    
    init(replay: ReplayCommentPost) {
        
        self.ReplayId      = replay.ReplayId
        self.Replay        = replay.Replay
        self.ReplayDate    = replay.ReplayDate
        self.UserName      = replay.UserName
        self.UserImage     = replay.UserImage
        self.UserId        = replay.UserId
    }
}
