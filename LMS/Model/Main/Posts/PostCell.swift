//
//  PostCell.swift
//  LMS
//
//  Created by Abdo on 1/25/20.
//  Copyright © 2020 eaglez-group. All rights reserved.
//

import UIKit

class PostCell: UITableViewCell {


    var activityIndicator = UIActivityIndicatorView()
    
    var postViewModel: Post! {
        
        
          didSet {
              
            PostUserName.text = postViewModel.UserName
            let token = postViewModel.PostDate.components(separatedBy: "T")
            PostDate.text = token[0]
            
            PostUserImage.addSubview(self.activityIndicator)
            self.activityIndicator.startAnimating()
            let url = NSURL(string: URLs.BaseUrl + postViewModel.UserImage.dropFirst() )!
            let task = URLSession.shared.dataTask(with: url as URL) { (responseData, responseUrl, error) -> Void in
                        if (responseData != nil && responseUrl != nil && error == nil)
                            {
                                if let data = responseData{
                                DispatchQueue.main.async {
                                                self.PostUserImage.image = UIImage(data: data)
                                                self.activityIndicator.stopAnimating()
                                    }
                                }
                         } else {
                                DispatchQueue.main.async { // Make sure you're on the main thread here
                                    self.PostUserImage.image = UIImage(named: "student")
                                    self.activityIndicator.stopAnimating()
                                }
                            }
                                             
                        }
            task.resume()
            
            
            PostText.text = postViewModel.Post
            PostCommentsCount?.setTitle(String(postViewModel.CommentCount) + "   Comments", for: .normal)
            }
      }
    
    
    @IBOutlet weak var PostUserImage:     UIImageView!
    @IBOutlet weak var PostDate:          UILabel!
    @IBOutlet weak var PostUserName:      UILabel!
    @IBOutlet weak var PostText:          UILabel!
    @IBOutlet weak var PostCommentsCount: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
