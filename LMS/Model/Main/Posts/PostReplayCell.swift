//
//  PostReplayCell.swift
//  LMS
//
//  Created by Abdo on 1/31/20.
//  Copyright © 2020 eaglez-group. All rights reserved.
//

import UIKit

class PostReplayCell: UITableViewCell {

    
        var activityIndicator = UIActivityIndicatorView()
        
        var replayCommentPostViewModel: ReplayCommentPost! {
            
              didSet {
                  
                ReplayUserName.text = replayCommentPostViewModel.UserName
                let token = replayCommentPostViewModel.ReplayDate.components(separatedBy: "T")
                ReplayDate.text = token[0]
                
                ReplayUserImage.addSubview(self.activityIndicator)
                self.activityIndicator.startAnimating()
                let url = NSURL(string: URLs.BaseUrl + replayCommentPostViewModel.UserImage.dropFirst() )!
                let task = URLSession.shared.dataTask(with: url as URL) { (responseData, responseUrl, error) -> Void in
                            if (responseData != nil && responseUrl != nil && error == nil)
                                {
                                    if let data = responseData{
                                    DispatchQueue.main.async {
                                                self.ReplayUserImage.image = UIImage(data: data)
                                                self.activityIndicator.stopAnimating()
                                        }
                                    }
                             } else {
                                    DispatchQueue.main.async { // Make sure you're on the main thread here
                                                self.ReplayUserImage.image = UIImage(named: "student")
                                                self.activityIndicator.stopAnimating()
                                    }
                                }
                                                 
                            }
                task.resume()
                
                
                ReplayText.text = replayCommentPostViewModel.Replay
                //CommentReplayCount?.setTitle(String(postViewModel.CommentCount) + "   Comments", for: .normal)
                }
          }
        
        
        @IBOutlet weak var ReplayUserImage:     UIImageView!
        @IBOutlet weak var ReplayDate:          UILabel!
        @IBOutlet weak var ReplayUserName:      UILabel!
        @IBOutlet weak var ReplayText:          UILabel!
        
        override func awakeFromNib() {
            super.awakeFromNib()
            // Initialization code
        }

        override func setSelected(_ selected: Bool, animated: Bool) {
            super.setSelected(selected, animated: animated)

            // Configure the view for the selected state
        }

    }
