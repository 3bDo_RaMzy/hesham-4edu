//
//  BlogCell.swift
//  LMS
//
//  Created by Abdo on 11/20/19.
//  Copyright © 2019 eaglez-group. All rights reserved.
//

import UIKit

class BlogCell: UITableViewCell {

    var blogViewModel: BlogViewModel! {
        
        didSet {
            BlogText?.text = blogViewModel.Text
            
            let token = blogViewModel.Date.components(separatedBy: "T")
            print (token[0])
            BlogDate?.text = token[0]
            
            
            
            
        }
    }
    
    @IBOutlet weak var BlogDate: UILabel!
    @IBOutlet weak var BlogText: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
