//
//  Blog.swift
//  LMS
//
//  Created by Abdo on 12/18/19.
//  Copyright © 2019 eaglez-group. All rights reserved.
//

import UIKit


class Blogs: Decodable {
    
    var ID   : Int       = 0
    var Date : String    = ""
    var Text : String    = ""
    
    init( ID: Int, Date: String, Text: String ) {

        self.ID        = ID
        self.Date      = Date
        self.Text      = Text
    }
}

//-----------------------------------------------------------------------------------
//---> View Model
struct BlogViewModel {

    var ID:      Int     = 0
    var Date:    String  = ""
    var Text:    String  = ""

    init(blog: Blogs) {
        self.ID      = blog.ID
        self.Date    = blog.Date   
        self.Text    = blog.Text
    }
}
