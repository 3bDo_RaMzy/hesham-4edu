//
//  PaymentCell.swift
//  LMS
//
//  Created by MacBook on 8/31/20.
//  Copyright © 2020 eaglez-group. All rights reserved.
//

import UIKit

class PaymentCell: UICollectionViewCell {

    
        var paymentViewModel: Payment! {
            didSet {
                
                PaymentName.text = paymentViewModel.title
                PaymentLogo.image = UIImage(named: paymentViewModel.logo ?? "Blog")
            }
        }
    
    
    
    
       @IBOutlet weak var PaymentBackView: CardView!
       @IBOutlet weak var PaymentName: UILabel!
       @IBOutlet weak var PaymentLogo: UIImageView!
    
       override func awakeFromNib() {
           super.awakeFromNib()
           // Initialization code
       }

       
       override var isSelected: Bool {
           didSet {
                       
                   if isSelected {
                       
                       PaymentBackView.backgroundColor = UIColor(hexString: "00B4AB")
                           
                   } else {
                           
                       PaymentBackView.backgroundColor = UIColor(hexString: "F2F2F7")
                       
                   }
           }
       }
}
