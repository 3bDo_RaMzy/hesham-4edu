//
//  Payment.swift
//  LMS
//
//  Created by MacBook on 8/31/20.
//  Copyright © 2020 eaglez-group. All rights reserved.
//

import UIKit
import ObjectMapper


//---------------------------------
//---> Model
struct Payment: Decodable {
    let id:             Int?
    let title:          String?
    let instructions:   String?
    let logo:           String?
    
    
    init( id: Int, title: String, instructions: String, logo: String ) {
        self.id           = id
        self.title        = title
        self.instructions = instructions
        self.logo         = logo
    }
    
    
    init(payment: Payment) {
           
           self.id            = payment.id
           self.title         = payment.title
           self.instructions  = payment.instructions
           self.logo          = payment.logo
       }
    
    
    
}
