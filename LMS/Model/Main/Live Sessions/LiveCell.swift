//
//  LiveCell.swift
//  LMS
//
//  Created by Abdo on 4/5/20.
//  Copyright © 2020 eaglez-group. All rights reserved.
//

import UIKit

class LiveCell: UITableViewCell {

    
    var liveViewModel: Live! {
        
        
          didSet {
              
            LiveName.text = liveViewModel.PublicName
            
      }
   }
    

    var QuestionBankViewModel: BankModel! {
         
         
           didSet {
               
            LiveName.text = QuestionBankViewModel.Name
             
       }
    }
    
    
    var BankQuestionsViewModel: BankQuestionsModel! {
         
         
           didSet {
               
            LiveName.text = BankQuestionsViewModel.Value
             
       }
    }
    
    @IBOutlet weak var LiveName: UILabel!
    @IBOutlet weak var LiveJoin: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
