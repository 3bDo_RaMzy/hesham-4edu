//
//  Live.swift
//  LMS
//
//  Created by Abdo on 4/5/20.
//  Copyright © 2020 eaglez-group. All rights reserved.
//

import UIKit

struct Live {
 
 
    var Id:           Int       = 0
    var Name:         String    = ""
    var PublicName:   String    = ""
    

    init(Id: Int, Name: String, PublicName: String ){
        
        self.Id           = Id
        self.Name         = Name
        self.PublicName     = PublicName
    }
    
    init(live: Live) {
        
        self.Id           = live.Id
        self.Name         = live.Name
        self.PublicName   = live.PublicName
    }
}
