//
//  QuizResultCell.swift
//  LMS
//
//  Created by Abdo on 12/11/19.
//  Copyright © 2019 eaglez-group. All rights reserved.
//

import UIKit

class QuizResultCell: UITableViewCell {


    
    
    var quizResultViewModel: QuizResultViewModel! {
        didSet {
            quizName?.text      = quizResultViewModel.quizTitle
            quizDate?.text      = quizResultViewModel.date
            quizDegree?.text    = String(quizResultViewModel.degree)
            quizMaxDegree?.text = String(quizResultViewModel.maxdegree)
            
        }
    }
    
    

    @IBOutlet weak var quizName:          UILabel!
    @IBOutlet weak var quizDate:          UILabel!
    @IBOutlet weak var quizDegree:        UILabel!
    @IBOutlet weak var quizMaxDegree:     UILabel!
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
