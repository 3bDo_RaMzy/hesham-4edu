//
//  QuizCorrection.swift
//  LMS
//
//  Created by MacBook on 6/11/20.
//  Copyright © 2020 eaglez-group. All rights reserved.
//

import UIKit
import ObjectMapper

    struct QuizCorrection: Decodable {
 
        init?(from: Any){
               
        }

        init?(map: Map){
               
        }
           
        init() {
                   
        }
        
        var Title:                String?
        var Duration:             Double?
        var ArticleNumber:        Int?
        var MultipleChoiceNumber: Int?
        var TrueOrFalseNumber:    Int?
        var ArticleDegree:        Double?
        var MultipleChoiceDegree: Double?
        var TrueOrFalseDegree:    Double?
        var TotalDegree:          Double?
        var QuestionList:         [CorrectionList]?
        
        init( Duration: Double, Title: String, TotalDegree: Double, ArticleNumber: Int,MultipleChoiceNumber: Int, TrueOrFalseNumber: Int,
              ArticleDegree: Double, MultipleChoiceDegree: Double, TrueOrFalseDegree: Double,  QuestionList: [CorrectionList]) {
            
            self.Duration              = Duration
            self.Title                 = Title
            self.TotalDegree           = TotalDegree
            self.ArticleNumber         = ArticleNumber
            self.MultipleChoiceNumber  = MultipleChoiceNumber
            self.TrueOrFalseNumber     = TrueOrFalseNumber
            self.ArticleDegree         = ArticleDegree
            self.MultipleChoiceDegree  = MultipleChoiceDegree
            self.TrueOrFalseDegree     = TrueOrFalseDegree
            self.QuestionList          = QuestionList
        }
        
    }



    struct CorrectionList: Decodable {
        
        let StudentName:         String?
        let QuestionType:        Int?
        let MaxDegree:           Double?
        let StudentDeqree:       Double?
        let AnswerValue:         String?
        let StudentAnswer:       String?
        let AnwerStatus:         Bool?
        let QuestionValue:       String?
        let QuestionCorrectionStatus: String?
        let AllAnswer:           [AnswerList]?
        let Images:              [String]?
        
        init( StudentName: String, QuestionType: Int, MaxDegree: Double, StudentDeqree: Double, AnswerValue: String, StudentAnswer: String, AnwerStatus: Bool,
              QuestionValue: String, QuestionCorrectionStatus: String, AllAnswer: [AnswerList], Images: [String]) {
            
            
            self.StudentName     = StudentName
            self.QuestionType    = QuestionType
            self.MaxDegree       = MaxDegree
            self.StudentDeqree   = StudentDeqree
            self.AnswerValue     = AnswerValue
            self.StudentAnswer   = StudentAnswer
            self.AnwerStatus     = AnwerStatus
            self.QuestionValue   = QuestionValue
            self.QuestionCorrectionStatus = QuestionCorrectionStatus
            self.AllAnswer       = AllAnswer
            self.Images          = Images
            
        }
        
        init(question: CorrectionList) {
            
            self.StudentName     = question.StudentName
            self.QuestionType    = question.QuestionType
            self.MaxDegree       = question.MaxDegree
            self.StudentDeqree   = question.StudentDeqree
            self.AnswerValue     = question.AnswerValue
            self.StudentAnswer   = question.StudentAnswer
            self.AnwerStatus     = question.AnwerStatus
            self.QuestionValue   = question.QuestionValue
            self.QuestionCorrectionStatus = question.QuestionCorrectionStatus
            self.AllAnswer       = question.AllAnswer
            self.Images          = question.Images
        }
    }


    struct AnswerList: Decodable {
        
        let Value:           String?
        let IsCorectAnswer:  Bool?
        
        init( Value: String, IsCorectAnswer: Bool) {
            
            self.Value       = Value
            self.IsCorectAnswer = IsCorectAnswer
        }
    }


//{
//    "Message": "Success ",
//    "Result": {


//        "QuestionList": [


//            {
//                "AnswerValue": "True",
//                "StudentAnswer": "True",
//                "AnwerStatus": true,
//                "QuestionValue": "question 2 true/false",
//                "QuestionCorrectionStatus": "Completed",
//                "QuestionType": 1,
//                "Images": [],
//                "MaxDegree": 10,
//                "StudentDeqree": 10.00,
//                "AllAnswer": [
//                    {
//                        "IsCorectAnswer": true,
//                        "Value": "True"
//                    },
//                    {
//                        "IsCorectAnswer": false,
//                        "Value": "False"
//                    }
//                ],
//                "StudentName": "Eaglez son"
//            },


//            {
//                "AnswerValue": "acsdcsdcsd",
//                "StudentAnswer": "cadcsdc",
//                "AnwerStatus": false,
//                "QuestionValue": "multi choice question 1",
//                "QuestionCorrectionStatus": "Completed",
//                "QuestionType": 2,
//                "Images": [],
//                "MaxDegree": 10,
//                "StudentDeqree": 0.00,
//                "AllAnswer": [
//                    {
//                        "IsCorectAnswer": true,
//                        "Value": "acsdcsdcsd"
//                    },
//                    {
//                        "IsCorectAnswer": false,
//                        "Value": "cadcsdc"
//                    },
//                    {
//                        "IsCorectAnswer": false,
//                        "Value": "sdcsdc"
//                    },
//                    {
//                        "IsCorectAnswer": false,
//                        "Value": "dsczcew"
//                    }
//                ],
//                "StudentName": "Eaglez son"
//            },
//            {
//                "AnswerValue": "aczcec",
//                "StudentAnswer": "zddecv",
//                "AnwerStatus": false,
//                "QuestionValue": "choice 2 ",
//                "QuestionCorrectionStatus": "Completed",
//                "QuestionType": 2,
//                "Images": [],
//                "MaxDegree": 10,
//                "StudentDeqree": 0.00,
//                "AllAnswer": [
//                    {
//                        "IsCorectAnswer": true,
//                        "Value": "aczcec"
//                    },
//                    {
//                        "IsCorectAnswer": false,
//                        "Value": "ascwfrwe"
//                    },
//                    {
//                        "IsCorectAnswer": false,
//                        "Value": "zddecv"
//                    }
//                ],
//                "StudentName": "Eaglez son"
//            },
//            {
//                "AnswerValue": "yep too much",
//                "StudentAnswer": null,
//                "AnwerStatus": true,
//                "QuestionValue": "do you love egypt",
//                "QuestionCorrectionStatus": "Completed",
//                "QuestionType": 3,
//                "Images": [],
//                "MaxDegree": 10,
//                "StudentDeqree": 10.00,
//                "AllAnswer": [
//                    {
//                        "IsCorectAnswer": true,
//                        "Value": "yep too much"
//                    }
//                ],
//                "StudentName": "Eaglez son"
//            }
//        ],
//        "ArticleDegree": 10,
//        "TrueOrFalseDegree": 10,
//        "MultipleChoiceDegree": 10,
//        "Duration": 20,
//        "Title": "test ios 55",
//        "ArticleNumber": 1,
//        "MultipleChoiceNumber": 2,
//        "TrueOrFalseNumber": 2,
//        "TotalDegree": 30.00
//    }
//}
