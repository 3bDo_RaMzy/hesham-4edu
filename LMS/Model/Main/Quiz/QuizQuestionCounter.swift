//
//  QuizQuestionCounter.swift
//  LMS
//
//  Created by Abdo on 6/4/20.
//  Copyright © 2020 eaglez-group. All rights reserved.
//

import UIKit

struct QuizQuestionCounter: Decodable {
    
    var id:      Int?
    var title:   String?
    var status:  Bool?
    
    init( id: Int, title: String, status: Bool ) {
        
        self.id     = id
        self.title  = title
        self.status = status
    }
}
