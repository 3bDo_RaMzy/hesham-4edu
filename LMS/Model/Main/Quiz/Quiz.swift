//
//  Quiz.swift
//  LMS
//
//  Created by Abdo on 12/5/19.
//  Copyright © 2019 eaglez-group. All rights reserved.
//

import UIKit
import ObjectMapper


//---------------------------------
//---> Model
struct Quiz: Decodable {
    let id:             Int?
    let title:          String?
    let duration:       Int?
    let QuestionCount:   Int?
    let questions:      [Question]?
    init( id: Int, title: String, duration: Int, QuestionCount: Int, questions: [Question]) {
        self.id          = id
        self.title       = title
        self.duration    = duration
        self.QuestionCount = QuestionCount
        self.questions   = questions
    }
}

struct Question: Decodable {
    let id:           Int?
    let title:        String?
    let type:         String?
    let imgs:         [String]?
    let answers:      [Answer]?
    init( id: Int, title: String, type: String, imgs: [String], answers: [Answer]) {
        self.id       = id
        self.title    = title
        self.type     = type
        self.imgs     = imgs
        self.answers  = answers
    }
}

struct Answer: Decodable {
    let id:           Int?
    let title:        String?
    init( id: Int, title: String) {
        self.id       = id
        self.title    = title
    }
}

class QuizAllAnswer: NSObject {
    var id           : Int?
    var questions    : [QuizQuestionAnswer]?
    init( id: Int, questions: [QuizQuestionAnswer] ) {
        self.id        = id
        self.questions = questions
    }
}





struct QuizResult: Decodable {
    
    let studentName:    String?
    let quizTitle:      String?
    let date:           String?
    let degree:         Double?
    let maxdegree:      Double?
    let QuizeId:        Int?
    let Status:         String?
    
    init( studentName: String, quizTitle: String, date: String,
          degree: Double, maxdegree: Double, QuizeId: Int, Status: String) {
        self.studentName  = studentName
        self.quizTitle    = quizTitle
        self.date         = date
        self.degree       = degree
        self.maxdegree    = maxdegree
        self.QuizeId      = QuizeId
        self.Status       = Status
    }
}






//class QuizQuestionAnswer:  NSObject  {
//
//
//   required init?(from: Any){
//
//   }
//
//   override init() {
//
//   }
//
//    var quesionId    : Int    = 0
//    var answerId     : Int    = 0
//    var ArticleAnswer: String = ""
//
//    init( quesionId: Int, answerId: Int, ArticleAnswer: String) {
//        self.quesionId    = quesionId
//        self.answerId     = answerId
//        self.ArticleAnswer = ArticleAnswer
//    }
//}


class QuizQuestionAnswer: NSObject, Decodable, Mappable {


    required init?(from: Any){

    }

    required init?(map: Map){

    }

    override init() {

    }


    var quesionId    : Int    = 0
    var answerId     : Int    = 0
    var ArticleAnswer: String = ""

     init( quesionId: Int, answerId: Int, ArticleAnswer: String) {
        self.quesionId    = quesionId
        self.answerId     = answerId
        self.ArticleAnswer = ArticleAnswer
    }

    func mapping(map: Map) {

        quesionId  <- map["quesionId"]
        answerId   <- map["answerId"]
        ArticleAnswer   <- map["ArticleAnswer"]

    }

}


//struct QuizQuestionAnswer: Decodable {
//
//
//
//    var quesionId    : Int
//    var answerId     : Int
//    var ArticleAnswer: String
//
//
//    init( quesionId: Int, answerId: Int, ArticleAnswer: String) {
//        self.quesionId    = quesionId
//        self.answerId     = answerId
//        self.ArticleAnswer = ArticleAnswer
//    }
//
//
//}

















//-----------------------------------------------------------------------------------
//---> View Model
struct QuizViewModel {

    var id:           Int        = 0
    var title:        String     = ""
    var duration:     Int        = 0
    var QuestionCount: Int = 0
    var questions:    [Question] = []

    init(quiz: Quiz) {
        self.id          = quiz.id         ?? 0
        self.title       = quiz.title      ?? ""
        self.duration    = quiz.duration   ?? 0
        self.QuestionCount = quiz.QuestionCount   ?? 0
        self.questions   = quiz.questions  ?? []
    }
}

struct QuestionViewModel {

    var id:           Int        = 0
    var title:        String     = ""
    var type:         String     = ""
    var imgs:         [String]   = []
    var answers:      [Answer]   = []

    init(question: Question) {
        self.id        = question.id       ?? 0
        self.title     = question.title    ?? ""
        self.type      = question.type     ?? ""
        self.imgs      = question.imgs     ?? []
        self.answers   = question.answers  ?? []
    }
}

struct AnswerViewModel {

    var id:           Int        = 0
    var title:        String     = ""

    init(answer: Answer) {
        self.id        = answer.id       ?? 0
        self.title     = answer.title    ?? ""
    }
}


struct QuizAnswerViewModel {

    var quesionId:   Int        = 0
    var answerId:    Int        = 0
    var ArticleAnswer: String = ""
    
    init(q_answer: QuizQuestionAnswer) {
        self.quesionId    = q_answer.quesionId
        self.answerId     = q_answer.answerId
        self.ArticleAnswer = q_answer.ArticleAnswer
    }
}
//struct QuizAnswerViewModel {
//
//    var quesionId:   Int        = 0
//    var answerId:    Int        = 0
//
//    init(q_answer: QuizAnswer) {
//        self.quesionId    = q_answer.quesionId   ?? 0
//        self.answerId     = q_answer.answerId    ?? 0
//    }
//}


struct QuizResultViewModel {

    var studentName:   String        = ""
    var quizTitle:   String        = ""
    var date:   String        = ""
    var degree:   Double        = 0.0
    var maxdegree:   Double        = 0.0
    var QuizeId: Int = 0
    
    init(q_result: QuizResult) {
        self.studentName   = q_result.studentName  ?? ""
        self.quizTitle     = q_result.quizTitle    ?? ""
        self.date          = q_result.date         ?? ""
        self.degree        = q_result.degree       ?? 0.0
        self.maxdegree     = q_result.maxdegree    ?? 0.0
        self.QuizeId       = q_result.QuizeId      ?? 0
    }
}
