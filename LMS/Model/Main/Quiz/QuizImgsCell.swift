//
//  QuizImgsCell.swift
//  LMS
//
//  Created by Abdo on 12/10/19.
//  Copyright © 2019 eaglez-group. All rights reserved.
//

import UIKit

class QuizImgsCell: UICollectionViewCell {
    
    
    var activityIndicator = UIActivityIndicatorView()
    
    
    @IBOutlet weak var QuestionImage:     UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        //PriceView.backgroundColor = UIColor(patternImage: UIImage(named: "offerBag")!)
        
        self.activityIndicator = UIActivityIndicatorView(style: .gray)
        self.activityIndicator.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        self.activityIndicator.hidesWhenStopped = true
        //self.activityIndicator.center = BlogImage.center
 
        self.layer.cornerRadius = 10
        self.layer.borderWidth = 1.0
        self.layer.borderColor = UIColor.lightGray.cgColor

        self.layer.backgroundColor = UIColor.white.cgColor
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowOffset = CGSize(width: 2.0, height: 4.0)
        self.layer.shadowRadius = 2.0
        self.layer.shadowOpacity = 1.0
        self.layer.masksToBounds = false
    
    }
}
