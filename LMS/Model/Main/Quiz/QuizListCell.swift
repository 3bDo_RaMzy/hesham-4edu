//
//  QuizListCell.swift
//  LMS
//
//  Created by Abdo on 12/5/19.
//  Copyright © 2019 eaglez-group. All rights reserved.
//

import UIKit

class QuizListCell: UITableViewCell {

    
    var quizViewModel: QuizViewModel! {
        didSet {
            quizName?.text = quizViewModel.title
            quizQuestionCount?.text = String(quizViewModel.QuestionCount) + "  Q."
            quizTime?.text = String(quizViewModel.duration) + " min"
        }
    }
    
    
    @IBOutlet weak var quizBackView:      UIView!
    @IBOutlet weak var quizName:          UILabel!
    @IBOutlet weak var quizQuestionCount: UILabel!
    @IBOutlet weak var quizTime:          UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        quizBackView.roundedTopRight()
        quizBackView.roundedBottomRight()
        quizBackView.roundedBottomLeft()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
