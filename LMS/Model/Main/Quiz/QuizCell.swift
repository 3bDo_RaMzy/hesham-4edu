//
//  QuizCell.swift
//  LMS
//
//  Created by Abdo on 12/9/19.
//  Copyright © 2019 eaglez-group. All rights reserved.
//

import UIKit

class QuizCell: UITableViewCell {

    
    var answerViewModel: AnswerViewModel! {
        didSet {
            
            answer_name.text = answerViewModel.title
        }
    }
    
    
    @IBOutlet weak var answer_name: UILabel!
    @IBOutlet weak var answer_view: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        answer_name.sizeToFit() 
        answer_name.numberOfLines = 0
        
    }

    //For single choice selection
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        selectionStyle = .none
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    

}
