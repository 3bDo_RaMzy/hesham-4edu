//
//  QuizCounterCell.swift
//  LMS
//
//  Created by Abdo on 6/3/20.
//  Copyright © 2020 eaglez-group. All rights reserved.
//

import UIKit

class QuizCounterCell: UICollectionViewCell {

    
    @IBOutlet weak var CounterBackView: CardView!
    @IBOutlet weak var CounterName: UILabel!
    
 
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    
    override var isSelected: Bool {
        didSet {
                    
                if isSelected {
                    
                    CounterBackView.backgroundColor = UIColor(hexString: "007AFF")
                        
                } else {
                        
                    CounterBackView.backgroundColor = UIColor(hexString: "F2F2F7")
                    
                }
        }
    }
  
}
