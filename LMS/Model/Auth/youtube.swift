//
//  youtube.swift
//  LMS
//
//  Created by Abdo on 11/25/19.
//  Copyright © 2019 eaglez-group. All rights reserved.
//

import Foundation
class youtube {
    var _ChanalId:Int!
    var _Path:String!
    var _Term:String!
    init(country: Dictionary<String, AnyObject>) {
        if let ChanalId = country["ChanalId"] as? Int {
            self._ChanalId = ChanalId
        }
        if let Path = country["Path"] as? String {
            self._Path = Path
        }
        if let Term = country["Term"] as? String {
            self._Term = Term
        }
    }
}
