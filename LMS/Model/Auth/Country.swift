//
//  Country.swift
//  LMS
//
//  Created by Abdo on 11/25/19.
//  Copyright © 2019 eaglez-group. All rights reserved.
//


import Foundation
class Country {
    var _Id:Int!
    var _Name:String!
    init(country: Dictionary<String, AnyObject>) {
        if let Id = country["Id"] as? Int {
            self._Id = Id
        }
        if let Name = country["Name"] as? String {
            self._Name = Name
        }
    }
}
class Unitts {
    var _Id:Int!
    var _Name:String!
    var _TermSystem:String!
    init(country: Dictionary<String, AnyObject>) {
        if let Id = country["Id"] as? Int {
            self._Id = Id
        }
        if let Name = country["Name"] as? String {
            self._Name = Name
        }
        if let TermSystem = country["TermSystem"] as? String {
            self._TermSystem = TermSystem
        }
    }
}
class Grade {
    var _Id:Int!
    var _Name:String!
    var _UsedTermSystem:Int!
    init(country: Dictionary<String, AnyObject>) {
        if let Id = country["Id"] as? Int {
            self._Id = Id
        }
        if let Name = country["Name"] as? String {
            self._Name = Name
        }
        if let UsedTermSystem = country["UsedTermSystem"] as? Int {
            self._UsedTermSystem = UsedTermSystem
        }
    }
}
class Group {
    var _GroupId:Int!
    var _GroupName:String!
    var _Latitude:String!
    var _Longitude:String!
    var _Note:String!
    init(country: Dictionary<String, AnyObject>) {
        if let GroupId = country["GroupId"] as? Int {
            self._GroupId = GroupId
        }
        if let GroupName = country["GroupName"] as? String {
            self._GroupName = GroupName
        }
        if let Latitude = country["Latitude"] as? String {
            self._Latitude = Latitude
        }
        if let Longitude = country["Longitude"] as? String {
            self._Longitude = Longitude
        }
        if let Note = country["Note"] as? String {
            self._Note = Note
        }
    }
}

class Bank {
    var _BankId:Int!
    var _BankName:String!
    
    init(country: Dictionary<String, AnyObject>) {
        if let BankId = country["Id"] as? Int {
            self._BankId = BankId
        }
        if let BankName = country["Name"] as? String {
            self._BankName = BankName
        }
    }
}

 
class QuizeOption {
    var QuizeType:    Int!
    var QuizeCount:   Int!
    var QuizeDegree:  Int!
    
    init(option: Dictionary<String, AnyObject>) {
        if let QuizeType = option["QuizeType"] as? Int {
            self.QuizeType = QuizeType
        }
        if let QuizeCount = option["QuizeCount"] as? Int {
            self.QuizeCount = QuizeCount
        }
        if let QuizeDegree = option["QuizeDegree"] as? Int {
            self.QuizeDegree = QuizeDegree
        }
    }
    
    init(QuizeType: Int, QuizeCount: Int, QuizeDegree: Int) {
        self.QuizeType   = QuizeType
        self.QuizeCount  = QuizeCount
        self.QuizeDegree = QuizeDegree
    }
    
}


class GroupAcadimic {
    
    var _GroupId:Int!
    var _GroupName:String!
    var _Latitude:String!
    var _Longitude:String!
    var _Note:String!
    init(country: Dictionary<String, AnyObject>) {
        if let GroupId = country["GroupId"] as? Int {
            self._GroupId = GroupId
        }
        if let GroupName = country["GroupName"] as? String {
            self._GroupName = GroupName
        }
        if let Latitude = country["Latitude"] as? String {
            self._Latitude = Latitude
        }
        if let Longitude = country["Longitude"] as? String {
            self._Longitude = Longitude
        }
        if let Note = country["Note"] as? String {
            self._Note = Note
        }
    }
}
