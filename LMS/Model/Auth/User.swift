//
//  User.swift
//  LMS
//
//  Created by Abdo on 11/25/19.
//  Copyright © 2019 eaglez-group. All rights reserved.
//


import Foundation
class User {
    var _unique_name:String!
    var _nameid:String!
    var _EmailAddress:String!
    var _FullName:String!
    var _UserType :String!
    var _nbf :Int!
    var _exp :Int!
    var _iat :Int!
    var _iss:String!
    var _aud:String!
    init(user: Dictionary<String, AnyObject>) {
        if let unique_name = user["unique_name"] as? String {
            self._unique_name = unique_name
        }
        if let nameid = user["nameid"] as? String {
            self._nameid = nameid
        }
        if let EmailAddress = user["EmailAddress"] as? String {
            self._EmailAddress = EmailAddress
        }
        if let FullName = user["FullName"] as? String {
            self._FullName = FullName
        }
        if let UserType = user["UserType"] as? String {
            self._UserType = UserType
        }
        if let nbf = user["nbf"] as? Int {
            self._nbf = nbf
        }
        if let exp = user["exp"] as? Int {
            self._exp = exp
        }
        if let iat = user["iat"] as? Int {
            self._iat = iat
        }
        if let iss = user["iss"] as? String {
            self._iss = iss
        }
        if let aud = user["aud"] as? String {
            self._aud = aud
        }
    }
}
