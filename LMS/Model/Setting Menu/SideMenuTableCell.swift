//
//  SideMenuTableCell.swift
//  LMS
//
//  Created by MacBook on 6/28/20.
//  Copyright © 2020 eaglez-group. All rights reserved.
//

import UIKit

class SideMenuTableCell: UITableViewCell {

    
    var profileMenuModel: ProfileMenuModel!{
        
        didSet{
            
            self.MenuName.text       = profileMenuModel.name
            self.MenuImage.image     = UIImage(named: "\(profileMenuModel.image)")
            self.MenuNotifiLbl.text  = String(profileMenuModel.NotifiCount)
        }
    }
    
    
    func hideNotifiCountView(){
        
        MenuNotifiView.isHidden = true
        MenuNotifiView.frame.size.width = 0
    }
    func showNotifiCountView(){
        
        MenuNotifiView.isHidden = false
//        MenuNotifiView.frame.size.width = 0
    }
    
    
    @IBOutlet weak var MenuImage:      UIImageView!
    @IBOutlet weak var MenuName:       UILabel!
    @IBOutlet weak var MenuNotifiView: UIView!
    @IBOutlet weak var MenuNotifiLbl:  UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        //MenuNotifiView.cornerRadius = MenuNotifiView.frame.size.height
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
}
