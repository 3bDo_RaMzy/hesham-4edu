//
//  ProfileMenuModel.swift
//  LMS
//
//  Created by MacBook on 6/28/20.
//  Copyright © 2020 eaglez-group. All rights reserved.
//

import UIKit
import ObjectMapper

class ProfileMenuModel: NSObject, Mappable {
 
 
    required init?(from: Any){
        
    }

    required init?(map: Map){
        
    }
    
    override init() {
        
    }
        
    var id:           Int     = 0
    var name:         String  = ""
    var image:        String  = ""
    var vcName:       String  = ""
    var NotifiCount:  Int     = 0
   // var vcController: UIViewController?
    
    init( id: Int, name: String, image: String, vcName: String, NotifiCount: Int/*, vcController: UIViewController*/ ) {

        self.id           = id
        self.name         = name
        self.image        = image
        self.vcName       = vcName
        self.NotifiCount  = NotifiCount
       // self.vcController = vcController
    }

    func mapping(map: Map) {
        
        id      <- map["id"]
        name    <- map["name"]
        image   <- map["image"]
        vcName  <- map["vcName"]
        NotifiCount <- map["NotifiCount"]
       // vcController  <- map["vcController"]
        
    }
    
}
