//
//  ProfileDataModel.swift
//  LMS
//
//  Created by MacBook on 6/28/20.
//  Copyright © 2020 eaglez-group. All rights reserved.
//
import UIKit
import ObjectMapper

class ProfileDataModel: NSObject, Mappable {
 
 
    required init?(from: Any){
        
    }

    required init?(map: Map){
        
    }
    
    override init() {
        
    }
     
//    "FullName":     "Eaglez son",
//    "Mobile":       "01017127199",
//    "EmailAddress": "ramzy@Gmail.com",
//    "Image":
     
    var FullName:      String = ""
    var Mobile:        String = ""
    var EmailAddress:  String = ""
    var Image:         String = ""
    var UserCode:      String = ""
   
    func mapping(map: Map) {
         
        FullName         <- map["FullName"]
        Mobile           <- map["Mobile"]
        EmailAddress     <- map["EmailAddress"]
        Image            <- map["Image"]
        UserCode         <- map["UserCode"]
     }
    
    
    init( FullName: String, Mobile: String, EmailAddress: String, Image: String, UserCode: String ){
         
        self.FullName      = FullName
        self.Mobile        = Mobile
        self.EmailAddress  = EmailAddress
        self.Image         = Image
        self.UserCode      = UserCode
    }
    
    
    init(profileData: ProfileDataModel) {
         
        self.FullName      = profileData.FullName
        self.Mobile        = profileData.Mobile
        self.EmailAddress  = profileData.EmailAddress
        self.Image         = profileData.Image
        self.UserCode      = profileData.UserCode
    }
    
    
}
