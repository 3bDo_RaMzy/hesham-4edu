//
//  API+ProdDetails.swift
//  LMS
//
//  Created by Abdo on 11/20/19.
//  Copyright © 2019 eaglez-group. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON


extension API{

    class func getBlogs(vc: BaseVC, completion: @escaping ( Error?, [Blogs]? )->Void){
            
            var Spinner :UIView!
            let url = URLs.getBlogs

            //vc.showLoading()
            Spinner = UIViewController.displaySpinner(onView: vc.view)
            AF.request(url, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil)
                       .validate(statusCode: 200..<600)
                       .responseJSON{ response in

                        UIViewController.removeSpinner(spinner: Spinner)
                        //print("Server Get getBlogs Response___________>\(response.value)")
                        
                        switch response.result
                           {
                           case .failure(let error):
                               //vc.hideLoading()
                               completion(error, nil)
                               print("getBlogs Server Not response___________>\(error)")
                               
                           case .success(let value):
                            
                            let json = JSON(value)
                            
                                              guard let jsonArr = json[].array else{completion(nil,nil)
                                                  return
                                              }
                                              var BlogsArr = [Blogs]()// to appended and return

                                                  for data in jsonArr {
                                                    
                                                    BlogsArr.append(Blogs(ID:   data["ID"].int        ?? 0,
                                                                          Date: data["Date"].string   ?? "",
                                                                          Text: data["Text"].string   ?? ""))
                                                  }
                                                                                                                                  
                                              completion(nil, BlogsArr)
                            
                           } //End of switch case
                   }//End of alamofire response
        }//---> End Of Method getBlogs
 
}
