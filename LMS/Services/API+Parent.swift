//
//  API+Parent.swift
//  LMS
//
//  Created by Abdo on 12/12/19.
//  Copyright © 2019 eaglez-group. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON


extension API{


    class func getParentStudentsList(vc: BaseVC, Token: String,
                                     completion: @escaping ( Error?,_ TimeOut: Bool?, [ParentStudents]? )->Void){

                let url = URLs.getParentStudents

        let HeaderParam : HTTPHeaders = [ "Authorization":  Token,
                                          "Lang": "en" ]

                //vc.showLoading()
                AF.request(url, method: .get, parameters: nil, encoding: URLEncoding.default, headers: HeaderParam)
                           .validate(statusCode: 200..<600)
                           .responseJSON{ response in

                            //print("Server getParentStudentsList ___________>\(response.value)")
                               switch response.result
                               {
                               case .failure(let error):
                                   //vc.hideLoading()
                                if (response.response?.statusCode == 401) {
                                    print("force user to login again --> 401")
                                    completion(error,true, nil)
                                } else {
                                    print("Server Not response___________>\(error)")
                                    completion(error,false, nil)
                                }

                               case .success(let value):

                                    let json = JSON(value)

                                    print("\n\n JSON ___________>\(json)")
                                    guard let jsonArr = json[].array else{completion(nil,false,nil)
                                              return
                                         }
                                    var StudentsArr        = [ParentStudents]()    // to appended and return
                                    var StudentsGrpTimeArr = [StudentGroupTimes]()// to appended and return

                                   for data in jsonArr {

                                                guard let questionArr = data["AppointmentsTimeingGroups"].array else{completion(nil,false,nil)
                                                     return
                                                }

                                               for question in questionArr {
                                                
                                                StudentsGrpTimeArr.append(StudentGroupTimes(Day:  question["Day"].string ?? "",
                                                                                            From: question["From"].string ?? "",
                                                                                            To:   question["To"].string ?? ""))
                                                
                                                
                                                }
                                                

                                    StudentsArr.append(ParentStudents(StudentId:   data["StudentId"].int ?? 0,
                                                                      GroupName:   data["GroupName"].string ?? "",
                                                                      Gender:      data["Gender"].string ?? "",
                                                                      ImageUrl:    data["ImageUrl"].string ?? "",
                                                                      StundetName: data["StundetName"].string ?? "",
                                                                      Grade:       data["Grade"].string ?? "",
                                                                      AppointmentsTimeingGroups: StudentsGrpTimeArr))
                                    StudentsGrpTimeArr.removeAll()
                                    
                                    }

                                   completion(nil,false, StudentsArr)

                               } //End of switch case
                       }//End of alamofire response
            }//---> End Of Method getParentStudentsList

    
    //-------------------------------------------------------------------------------------------------------------
    class func getParentStudentQuizResult(vc: BaseVC, Token: String, from: String, to: String, studentId: Int,
                                          completion: @escaping ( Error?,_ TimeOut: Bool?, [QuizResult]? )->Void){
        
        var Spinner :UIView!
        let url = URLs.getParentStudentResult

        //vc.showLoading()
        let HeaderParam : HTTPHeaders = [ "Authorization":  Token,
                                          "Lang": "en"]
        let params       = [ "To":   to,
                             "From": from,
                             "studentId": studentId] as [String : Any]
        
        Spinner = UIViewController.displaySpinner(onView: vc.view)
        AF.request(url, method: .post, parameters: params, encoding: URLEncoding.default, headers: HeaderParam)
                   .validate(statusCode: 200..<600)
                   .responseJSON{ response in

                    UIViewController.removeSpinner(spinner: Spinner)
                    print("\n\n\n sendParentStudentQuizResult  response:>>>>>>>>>>>>>>>>> \(response)\n")
                    
                       switch response.result
                       {
                       case .failure(let error):
                           //vc.hideLoading()
                            if (response.response?.statusCode == 401) {
                                print("force user to login again --> 401")
                                completion(error,true, nil)
                            } else {
                                print("Server Not response___________>\(error)")
                                completion(error,false, nil)
                            }
                           
                           
                       case .success(let value):
                        
                        let json = JSON(value)
                        
                                          guard let jsonArr = json[].array else{completion(nil,false,nil)
                                              return
                                          }
                                          var QuizResultArr = [QuizResult]()// to appended and return

                                              for data in jsonArr {
                                                if data["Status"].string != "Pending"{
                                                QuizResultArr.append(QuizResult(studentName: data["studentName"].string   ?? "",
                                                                                quizTitle: data["quizTitle"].string   ?? "",
                                                                                date: data["date"].string   ?? "",
                                                                                degree: data["degree"].double   ?? 0.0,
                                                                                maxdegree: data["maxdegree"].double   ?? 0.0,
                                                                                QuizeId: data["QuizeId"].int ?? 0,
                                                                                Status: data["Status"].string   ?? "" ))
                                                }
                                                
                                            }
                                                                                                                              
                                          completion(nil,false, QuizResultArr)
                        
                       } //End of switch case
               }//End of alamofire response
    }//---> End Of Method sendParentStudentQuizResult

    
    //-------------------------------------------------------------------------------------------------------------
    class func getParentStudentAbsence(vc: BaseVC, Token: String, from: String, to: String, studentId: Int,
                                       completion: @escaping ( Error?,_ TimeOut: Bool?, [QuizResult]? )->Void){
        
        var Spinner :UIView!
        let url = URLs.getParentStudentAbsence

//        //vc.showLoading()

        let HeaderParam : HTTPHeaders = [ "Authorization":  Token,
                                          "Lang": "en" ]
        let params       = [ "To":   to,
                             "From":    from,
                             "StudnetId":  studentId] as [String : Any]
        
        
        Spinner = UIViewController.displaySpinner(onView: vc.view)
        AF.request(url, method: .post, parameters: params, encoding: URLEncoding.default, headers: HeaderParam)
                   .validate(contentType: ["application/json"])
                   .validate(statusCode: 200..<600)
                   .responseJSON{ response in

                    UIViewController.removeSpinner(spinner: Spinner)
                    print("\n\n\n sendParentStudentQuizResult  response:>>>>>>>>>>>>>>>>> \(response)\n")
                    
                       switch response.result
                       {
                       case .failure(let error):
                           //vc.hideLoading()
                        
                        if (response.response?.statusCode == 401) {
                                  print("force user to login again --> 401")
                                  completion(error,true, nil)
                        } else {
                                  print("Server Not response___________>\(error)")
                                  completion(error,false, nil)
                        }
                        
                           
                       case .success(let value):
                        
                        let json = JSON(value)
                        
                                          guard let jsonArr = json[].array else{completion(nil,false,nil)
                                              return
                                          }
                                          var QuizResultArr = [QuizResult]()// to appended and return

                                              for data in jsonArr {
                                                
                                                QuizResultArr.append(QuizResult(studentName: data["studentName"].string   ?? "",
                                                                                quizTitle: data["quizTitle"].string   ?? "",
                                                                                date: data["Date"].string   ?? "",
                                                                                degree: data["Degree"].double   ?? -1,
                                                                                maxdegree: data["TotalDegree"].double   ?? 0.0,
                                                                                 QuizeId: data["QuizeId"].int ?? 0,
                                                                                 Status: data["Status"].string   ?? "" ))
                                              }
                                                                                                                              
                                          completion(nil, false, QuizResultArr)
                        
                       } //End of switch case
               }//End of alamofire response
    }//---> End Of Method sendParentStudentQuizResult
    
    
}//---> End Of Class ------------------------
