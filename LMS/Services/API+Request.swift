//
//  API+Request.swift
//  LMS
//
//  Created by Abdo on 1/2/20.
//  Copyright © 2020 eaglez-group. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON


extension API{
    
    
    class func getRequestsList(vc: BaseVC, Token: String, completion: @escaping ( Error?,_ TimeOut: Bool?, [Request]? )->Void){

                    let url = URLs.getRequests

                    let HeaderParam : HTTPHeaders = [ "Authorization":  Token]

                    //vc.showLoading()
                    AF.request(url, method: .get, parameters: nil, encoding: URLEncoding.default, headers: HeaderParam)
                               .validate(statusCode: 200..<600)
                               .responseJSON{ response in

                                print("Server Get getQuizList ___________>\(response.value)")
                                   switch response.result
                                   {
                                   case .failure(let error):
                                    
                                    if (response.response?.statusCode == 401) {
                                        print("force user to login again --> 401")
                                        completion(error,true, nil)
                                    } else {
                                        print("Server Not response___________>\(error)")
                                        completion(error,false, nil)
                                    }


                                   case .success(let value):

                                        let json = JSON(value)

                                        print("\n\n JSON ___________>\(json)")
                                        guard let jsonArr = json["Result"].array else{completion(nil,false,nil)
                                                  return
                                             }
                                        var RequestArr    = [Request]()    // to appended and return
                                        var STCommentsArr   = [Comment]()// to appended and return

                                       for data in jsonArr {

                                                    guard let commentArr = data["Comments"].array else{completion(nil,false,nil)
                                                         return
                                                    }

                                                   for comment in commentArr {
                                                    STCommentsArr.append(Comment(Time:      comment["Time"].string ?? "",
                                                                                 Message:   comment["Message"].string ?? "",
                                                                                 StudentId: comment["StudentId"].int ?? 0))
                                                    
                                                    }
                                                    

                                        RequestArr.append(Request(Id:       data["Id"].int ?? 0,
                                                                  Time:     data["Time"].string ?? "",
                                                                  Status:   data["Status"].string ?? "",
                                                                  Title:    data["Title"].string ?? "",
                                                                  Detalis:  data["Detalis"].string ?? "",
                                                                  Comments: STCommentsArr))
                                        STCommentsArr.removeAll()
                                        
                                        }

                                       completion(nil,false, RequestArr)

                } //End of switch case
            }//End of alamofire response
        }//---> End Of Method getRequestsList
    
    //--------------------------------------------------------------------------------------------------------------
       class func sendRequest( vc: BaseVC, Token: String, Title: String, Detalis: String,
                                  completion: @escaping (_ error: Error?,_ TimeOut: Bool?, _ Status: Bool)->Void) {
           
                   
                   var Spinner :UIView!
                   let url = URLs.sendRequest
                  
                   let HeaderParam : HTTPHeaders = [ "Authorization":  Token]
                   let params       = [ "Title": Title,
                                       "Detalis": Detalis]

                   Spinner = UIViewController.displaySpinner(onView: vc.view)
                       
                   AF.request(url, method: .post, parameters: params, encoding: URLEncoding.default, headers: HeaderParam)
                                       .validate(contentType: ["application/json"])
                                       .validate(statusCode: 200..<600)
                                       .responseJSON{ response in
           
                           UIViewController.removeSpinner(spinner: Spinner)
                           print("\n\n\n sendStudentQuiz  response:>>>>>>>>>>>>>>>>> \(response)\n")
                   
                   switch response.result {
                       
                   case .failure(let error):
                    
                        if (response.response?.statusCode == 401) {
                            print("force user to login again --> 401")
                            completion(error,true, false)
                        } else {
                            print("Server Not response___________>\(error)")
                            completion(error,false, false)
                        }

                    
                   case .success(let value):
                       //vc.dismiss(animated: true, completion: nil)
                       let json = JSON(value)
                       print("\n\n\nLogin_success:>>>>>>>>>>>>>>>>> \(json)\n")
                       
                       if  json.string == "Added successfully" {
                               completion(nil,false, true)
                       } else {
                           
                           completion(nil,false, false)
                       }
                       
                   }
           }
       } // End of sendRequest
    
    //--------------------------------------------------------------------------------------------------------------
        class func sendRequestReplay(vc: BaseVC, Token: String, Id: Int, Message: String,
                                   completion: @escaping (_ error: Error?,_ TimeOut: Bool?, _ Status: Bool)->Void) {
            
                    
                    var Spinner :UIView!
                    let url = URLs.sendRequestReplay
                   
                    let HeaderParam : HTTPHeaders = [ "Authorization":  Token]
                    let params       = [ "Id": Id,
                                         "Message": Message] as [String : Any]


                    Spinner = UIViewController.displaySpinner(onView: vc.view)
                        
                    AF.request(url, method: .post, parameters: params, encoding: URLEncoding.default, headers: HeaderParam)
                                        .validate(contentType: ["application/json"])
                                        .validate(statusCode: 200..<600)
                                        .responseJSON{ response in
            
                            UIViewController.removeSpinner(spinner: Spinner)
                            print("\n\n\n sendStudentQuiz  response:>>>>>>>>>>>>>>>>> \(response)\n")
                    
                    switch response.result {
                        
                    case .failure(let error):
                        
                        if (response.response?.statusCode == 401) {
                            print("force user to login again --> 401")
                            completion(error,true, false)
                        } else {
                            print("Server Not response___________>\(error)")
                            completion(error,false, false)
                        }

                    case .success(let value):
                        //vc.dismiss(animated: true, completion: nil)
                        let json = JSON(value)
                        //print("\n\n\nLogin_success:>>>>>>>>>>>>>>>>> \(json)\n")
                        
                        if  json.string == "Added successfully" {
                                completion(nil,false, true)
                        } else {
                            
                            completion(nil,false, false)
                        }
                        
                    }
            }
        } // End of sendRequestReplay
}
