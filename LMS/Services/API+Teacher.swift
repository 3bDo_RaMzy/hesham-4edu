//
//  API+Teacher.swift
//  LMS
//
//  Created by MacBook on 6/22/20.
//  Copyright © 2020 eaglez-group. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON


extension API{
    
    
    class func getNewQuizQuestionsCount(vc: BaseVC, Bank_ID: Int, completion: @escaping (_ error: Error?, _ Multi_Count: Int, _ TF_Count: Int, _ Article_Count: Int )->Void) {
        
        var Spinner :UIView!
        let url = URLs.getNewQuizQuestionsCount + "\(Bank_ID)"
          
        Spinner = UIViewController.displaySpinner(onView: vc.view)
        AF.request(url, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil)
            .validate(statusCode: 200..<600)
            .responseJSON { response in
                
                //print("\n\n\n CheckOption  response:>>>>>>>>>>>>>>>>> \(response)\n")
                UIViewController.removeSpinner(spinner: Spinner)
                
                switch response.result
                {
                    
                case .failure(let error):
                      completion(error, 0, 0, 0)
                    // print(error)
                    
                case .success(let value):
 
                     let json = JSON(value)

                     //print("\n\n JSON ___________>\(json)")
                     guard let jsonArr = json[].array else{completion(nil, 0, 0, 0)
                               return
                     }
                      
                     let multiCount   = jsonArr[0]["Count_MultipleChoiceList"].int ?? 0
                     let TFCount      = jsonArr[0]["Count_TrueOrFalse"].int ?? 0
                     let articleCount = jsonArr[0]["Count_ArticleList"].int ?? 0
                    
                    
                     completion(nil, multiCount, TFCount, articleCount)
                }
        }
      //}//---> End Of Loading
    } // End of getNewQuizQuestionsCount
//-------------------------------------------------------------------------------------------------
    
    class func getPendingStudents(vc: BaseVC, Token: String, completion: @escaping ( Error?,_ TimeOut: Bool?, [PendingStudents]? )->Void){
            
            var Spinner :UIView!
            let url = URLs.getStudentsPending

            let HeaderParam : HTTPHeaders = [ "Authorization":  Token]
        
            //vc.showLoading()
            Spinner = UIViewController.displaySpinner(onView: vc.view)
            AF.request(url, method: .get, parameters: nil, encoding: URLEncoding.default, headers: HeaderParam)
                       .validate(statusCode: 200..<600)
                       .responseJSON{ response in

                        UIViewController.removeSpinner(spinner: Spinner)
                         print("Server Get getBlogs Response___________>\(response.value)")
                        
                        switch response.result
                           {
                           case .failure(let error):
                            
                            if (response.response?.statusCode == 401) {
                                print("force user to login again --> 401")
                                completion(error,true, nil)
                            } else {
                                print("Server Not response___________>\(error)")
                                completion(error,false, nil)
                            }

                               
                           case .success(let value):
                            
                            let json = JSON(value)
                            
                                              guard let jsonArr = json[].array else{completion(nil,false,nil)
                                                  return
                                              }
                                              var PendingArr = [PendingStudents]()// to appended and return

                                                  for data in jsonArr {
                                                    
                                                    PendingArr.append(PendingStudents(Id:           data["Id"].int               ?? 0,
                                                                                      FullName:     data["FullName"].string      ?? "",
                                                                                      EmailAddress: data["EmailAddress"].string  ?? "",
                                                                                      Gender:       data["Gender"].int           ?? 0,
                                                                                      CreatedTime:  data["CreatedTime"].string   ?? "",
                                                                                      Mobile:       data["Mobile"].string        ?? "",
                                                                                      GroupName:    data["GroupName"].string     ?? ""))
                                                  }
                                                                                                                                  
                                              completion(nil,false, PendingArr)
                            
                           } //End of switch case
                   }//End of alamofire response
        }//---> End Of Method getPendingStudents

    
    //--------------------------------------------------------------------------------------------------------------
        class func sendPendingRequest(vc: BaseVC, Token: String, StudentId: Int, status: String,
                                   completion: @escaping (_ error: Error?,_ TimeOut: Bool?, _ Status: Bool)->Void) {
            
                    
                    var Spinner :UIView!
                    let url = URLs.BaseUrl + "api/Student/\(StudentId)/Status/\(status)"
                   
                    let HeaderParam : HTTPHeaders = [ "Authorization":  Token]
                    Spinner = UIViewController.displaySpinner(onView: vc.view)
                        
                    AF.request(url, method: .get, parameters: nil, encoding: URLEncoding.default, headers: HeaderParam)
                                        .validate(contentType: ["application/json"])
                                        .validate(statusCode: 200..<600)
                                        .responseString { response in
                                         
                            UIViewController.removeSpinner(spinner: Spinner)
                            let statusCode = response.response?.statusCode ?? 500
 
                    switch statusCode {
                        
                    case 401:
                     
                         completion(nil, true, false)
                     
                    case 500,404:

                        completion(nil, false, false)

                    case 200:
                         
                        completion(nil, false, true)
                        
                    default:
                        
                        completion(nil, false, false)
                    }
            }
        } // End of sendPendingRequest
    
    
//-------------------------------------------------------------------------------------------------
    //-------- Question Bank ------------
    
    class func getAllQuestionsBank(vc: BaseVC, Token: String, GradId: Int,
                                   completion: @escaping ( Error?, _ TimeOut: Bool?, [BankModel]? )->Void){
            
            var Spinner :UIView!
            let url = URLs.getAllQuestionsBank + "\(GradId)/en"

            let HeaderParam : HTTPHeaders = [ "Authorization":  Token]
        
            //vc.showLoading()
            Spinner = UIViewController.displaySpinner(onView: vc.view)
            AF.request(url, method: .get, parameters: nil, encoding: URLEncoding.default, headers: HeaderParam)
                       .validate(statusCode: 200..<600)
                       .responseJSON{ response in

                        UIViewController.removeSpinner(spinner: Spinner)
                         print("Server Get getBlogs Response___________>\(response.value)")
                        
                        switch response.result
                           {
                           case .failure(let error):
                            
                                if (response.response?.statusCode == 401) {
                                    print("force user to login again --> 401")
                                    completion(error,true, nil)
                                } else {
                                    print("Server Not response___________>\(error)")
                                    completion(error,false, nil)
                                }
                               
                           case .success(let value):
                            
                            let json = JSON(value)
                            
                            guard let jsonArr = json[].array else{completion(nil,false,nil)
                                return
                            }
                            var BankArr = [BankModel]()// to appended and retur

                            for data in jsonArr {
                                                    
                                BankArr.append(BankModel(Id:   data["Id"].int       ?? 0,
                                                         Name: data["Name"].string  ?? ""))
                            }
                                                                                                                                  
                            completion(nil, false, BankArr)
                            
                    } //End of switch case
            }//End of alamofire response
    }//---> End Of Method getAllQuestionsBank
    
//--------------------------------------------------------------------------------------------------------------
    class func deleteQuestionBank(vc: BaseVC, Token: String, BankId: Int,
                                  completion: @escaping (_ error: Error?,_ TimeOut: Bool?, _ Status: Bool)->Void) {
               
                       
                var Spinner :UIView!
                let url = URLs.deleteQuestionsBank + "\(BankId)/Delete"
                      
                let HeaderParam : HTTPHeaders = [ "Authorization":  Token]
                Spinner = UIViewController.displaySpinner(onView: vc.view)
                           
                AF.request(url, method: .post, parameters: nil, encoding: URLEncoding.default, headers: HeaderParam)
                                .validate(contentType: ["application/json"])
                                .validate(statusCode: 200..<600)
                                .responseJSON { response in
                                            
                        UIViewController.removeSpinner(spinner: Spinner)
                        print("\n\n\n sendStudentQuiz  response:>>>>>>>>>>>>>>>>> \(response)\n")
                                                
                        switch response.result {
                                                    
                              case .failure(let error):
                            
                                   if (response.response?.statusCode == 401) {
                                            print("force user to login again --> 401")
                                            completion(error,true, false)
                                   } else {
                                            print("Server Not response___________>\(error)")
                                            completion(error,false, false)
                                   }
                            
                            
                               case .success(let value):
                                
                                   //vc.dismiss(animated: true, completion: nil)
                                   let json = JSON(value)
                                   //print("\n\n\nLogin_success:>>>>>>>>>>>>>>>>> \(json["Message"].string)\n")
                                   //showToast(message: json["Message"].string ?? "Bank Deleted Successfully", vc: vc)
                                   
                               if  json["Success"] == true {
                                      completion(nil,false, true)
                               } else {
                                                            
                                      completion(nil,false, false)
                               }
                                                    
                        }
        }
    } // End of deleteQuestionBank
    
//--------------------------------------------------------------------------------------------------------------
       class func createQuestionBank(vc: BaseVC, Token: String, NameAr: String, NameEn: String, GradeId: Int,
                                           completion: @escaping (_ error: Error?,_ TimeOut: Bool?, _ msg: String, _ bankId: Int)->Void) {
    
                          var Spinner :UIView!
                          let url = URLs.createQuestionsBank
                         
                          let HeaderParam : HTTPHeaders = [ "Authorization":  Token]
               
                           let params       = [ "NameAr":  NameAr,
                                                "NameEn":  NameEn,
                                                "GradeId": GradeId ] as [String : Any]
               
                          Spinner = UIViewController.displaySpinner(onView: vc.view)
                              
                          AF.request(url, method: .post, parameters: params, encoding: URLEncoding.default, headers: HeaderParam)
                                   .validate(contentType: ["application/json"])
                                   .validate(statusCode: 200..<600)
                                   .responseJSON
                           { response in
                                                             
                                           //print("\n\n\n CheckOption  response:>>>>>>>>>>>>>>>>> \(response)\n")
                                           UIViewController.removeSpinner(spinner: Spinner)
                                                             
                                           switch response.result
                                           {
                                                                 
                                               case .failure(let error):
                                                
                                                   if (response.response?.statusCode == 401) {
                                                            print("force user to login again --> 401")
                                                            completion(error, true, "", 0)
                                                   } else {
                                                            print("Server Not response___________>\(error)")
                                                            completion(error, false, "", 0)
                                                   }
                
                                               case .success(let value):
                                                  
                                                   let json = JSON(value)
                                                    
                                                   let bankId  = json["Id"].int ?? 0
                                                   let Message   = json["Message"].string ?? ""
                                                   
                                                   if Message == ""{
                                                       
                                                       completion(nil,false, "", bankId)
                                                   } else {
                                                       
                                                       completion(nil,false, Message, 0)
                                                   }
                                                   
                                           }
                    }
        } // End of createQuestionBank
    
//--------------------------------------------------------------------------------------------------------------
    class func getQuestionsBankCount(vc: BaseVC, Token: String, BankId: Int, completion:
                                     @escaping ( Error?,_ TimeOut: Bool?, _ MCQ_Count: Int, _ TF_Count: Int, _ Article: Int )->Void){
            
            var Spinner :UIView!
            let url = URLs.getQuestionsBankCount + "\(BankId)"

            let HeaderParam : HTTPHeaders = [ "Authorization":  Token]
        
            //vc.showLoading()
            Spinner = UIViewController.displaySpinner(onView: vc.view)
            AF.request(url, method: .get, parameters: nil, encoding: URLEncoding.default, headers: HeaderParam)
                       .validate(statusCode: 200..<600)
                       .responseJSON{ response in

                        UIViewController.removeSpinner(spinner: Spinner)
                         print("Server Get getBlogs Response___________>\(response.value)")
                        
                        switch response.result
                           {
                           case .failure(let error):
                            
                            if (response.response?.statusCode == 401) {
                                     print("force user to login again --> 401")
                                     completion(error,true, 0, 0, 0)
                            } else {
                                     print("Server Not response___________>\(error)")
                                     completion(error,false, 0, 0, 0)
                            }
                            
                               
                           case .success(let value):
                            
                            let json = JSON(value)
                             
                            let Count_MultipleChoiceList = json[0]["Count_MultipleChoiceList"].int ?? 0
                            let Count_TrueOrFalse        = json[0]["Count_TrueOrFalse"].int ?? 0
                            let Count_ArticleList        = json[0]["Count_ArticleList"].int ?? 0
                            
                            completion(nil,false, Count_MultipleChoiceList, Count_TrueOrFalse, Count_ArticleList)
                            
                    } //End of switch case
            }//End of alamofire response
    }//---> End Of Method getQuestionsBankCount
    
    
//---------------------------------------------------------------------------------------------------------
    class func getQuestionsBankList(vc: BaseVC, Token: String, BankId: Int, QuestionType: String,
                                    completion: @escaping ( Error?,_ TimeOut: Bool?, [BankQuestionsModel]? )->Void){
            
            var Spinner :UIView!
            let url = URLs.getQuestionsBankList + "\(BankId)"

            let HeaderParam : HTTPHeaders = [ "Authorization":  Token]
        
            //vc.showLoading()
            Spinner = UIViewController.displaySpinner(onView: vc.view)
            AF.request(url, method: .get, parameters: nil, encoding: URLEncoding.default, headers: HeaderParam)
                       .validate(statusCode: 200..<600)
                       .responseJSON{ response in

                         UIViewController.removeSpinner(spinner: Spinner)
                         print("Server Get getQuestionsBankList Response___________>\(response.value)")
                        
                        switch response.result
                           {
                           case .failure(let error):
                            
                            
                                if (response.response?.statusCode == 401) {
                                         print("force user to login again --> 401")
                                         completion(error,true, nil)
                                } else {
                                         print("Server Not response___________>\(error)")
                                         completion(error,false, nil)
                                }

                               
                           case .success(let value):
                            
                            let json = JSON(value)
                            
                            guard let jsonArr = json[][0]["\(QuestionType)"].array else{completion(nil,false,nil)
                                return
                            }
                            var BankArr = [BankQuestionsModel]()// to appended and retur

                            for data in jsonArr {
                                      
                                BankArr.append(BankQuestionsModel(Id:             data["Id"].int             ?? 0,
                                                                  Value:          data["Value"].string       ?? "",
                                                                  QuestionBankId: data["QuestionBankId"].int ?? 0,
                                                                  IsDeleted:      data["IsDeleted"].bool     ?? false))
                            }
                                                                                                                                  
                            completion(nil,false, BankArr)
                            
                    } //End of switch case
            }//End of alamofire response
    }//---> End Of Method getQuestionsBankList
    
    
    
//--------------------------------------------------------------------------------------------------------------
    class func addBankQuestionsImages( vc: BaseVC, Token: String, Q_Imgs: [String],
                                       completion: @escaping (_ error: Error?,_ TimeOut: Bool?, _ imgsArr: [String])->Void) {
    
        var Spinner :UIView!
        let HeaderParam : HTTPHeaders = [ "Authorization":  Token]
        var UploadedImages: [String] = []
        
        
        //-----> First Stip to add Question Images
        
        let urlImgs = URLs.addQuestionImages
            Spinner = UIViewController.displaySpinner(onView: vc.view)
        var request = URLRequest(url: URL(string: urlImgs)!)
            request.httpMethod = "POST"
            request.headers = HeaderParam
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.httpBody = try! JSONSerialization.data(withJSONObject: Q_Imgs)
            
        AF.request(request)
            .responseJSON { response in
                // do whatever you want here
                UIViewController.removeSpinner(spinner: Spinner)
                
                switch response.result
                {
                    
                    case .failure( let error):
                        
                       if (response.response?.statusCode == 401) {
                                    print("force user to login again --> 401")
                                    completion(error,true, [])
                       } else {
                                    print("Server Not response___________>\(error)")
                                    completion(error,false, [])
                       }
                    
     
                    case .success(let responseObject):
                        //print(responseObject)
                        let json = JSON(responseObject)
                        guard let jsonArr = json[].array else{completion(nil,false, [])
                            return
                        }
                        for data in jsonArr {
                             UploadedImages.append(  data.string ?? ""   )
                        }
                        completion(nil,false, UploadedImages)
                }
        }//End of upload imags request
        
 
} // End of addBankQuestionsImages
    
    
    
    
}





