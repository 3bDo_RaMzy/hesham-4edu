//
//  Config.swift
//  LMS
//
//  Created by Abdo on 11/20/19.
//  Copyright © 2019 eaglez-group. All rights reserved.
//

import Foundation


struct URLs {
    
    static let BaseUrl        = "http://heshamallam4edu.com/mob/" // http://heshamallam4edu.com/mob/ | https://eaglez-academy.com/
    static let ImageDomain    = "http://heshamallam4edu.com/"    // http://eaglez-001-site22.ctempurl.com/mob/
    
    //Login -------------------------------------------------
    static let Login             = BaseUrl + "login"
    
    //Signup ------------------------------------------------
    static let Signup            = BaseUrl + "signup"
    
    //getBlog -----------------------------------------------
    static let getBlog           = BaseUrl + "blogs"
    
    //GetProfile
    static let GetProfile = BaseUrl + "api/UserInfo"
    
    //postContactUs -----------------------------------------------
    static let sentContactUs     = BaseUrl + "api/ContactUs/Push"
   
    //getAboutUs -----------------------------------------------
    static let getAboutUs        = BaseUrl + "api/GetAboutUsInformation"
    
    //getSocialURL -----------------------------------------------
    static let getSocialURL      = BaseUrl + "api/SocialReferences/GetAll"
    
    //getSocialURL -----------------------------------------------
    static let getQuizListURL    = BaseUrl + "api/quiz/getStudentQuiz"

    //postStudentQuiz -----------------------------------------------
    static let sendStudentQuiz    = BaseUrl + "api/quiz/studentQuizResult"
    //static let sendStudentQuiz    = BaseUrl + "api/quiz/studentQuizResultIOS/"
    //static let sendStudentQuiz    = BaseUrl + "api/quiz/studentQuizResult/"
    
    //getStudentResult ---------------------------------------------
    static let getStudentResult   = BaseUrl + "api/quiz/studentReport"
    
    //getStudentResult ---------------------------------------------
    static let getQuizCorrection   = BaseUrl + "api/quiz/"
    
    //getParentStudents ---------------------------------------------
    static let getParentStudents  = BaseUrl + "api/v1/Parent/GetChildren"
    
    //getParentStudentResult ---------------------------------------------
    static let getParentStudentResult   = BaseUrl + "api/quiz/ParentReport"
    
    //getParentStudentAbsence ---------------------------------------------
    static let getParentStudentAbsence  = BaseUrl + "api/v1/Parent/ChildAbsences"
    
    //getBlogs ----------------------------------------------------
    static let getBlogs  = BaseUrl + "api/Blogs/GetAllBlogs"
    
    
    //getRequests -------------------------------------------------
    static let getRequests  = BaseUrl + "api/Studnet/GetSuggestionsOrComplaints"
    
    //sendRequest ---------------------------------------------
    static let sendRequest  = BaseUrl + "api/Studnet/AddSuggestionsOrComplaints"
    
    //sendRequestReplay ---------------------------------------------
    static let sendRequestReplay  = BaseUrl + "api/SuggestionsOrComplaints/AddComment"
    
    //getPosts ---------------------------------------------------
    static let getPosts  = BaseUrl + "api/Post/AllPost/"
    
    //getComments By Post Id ---------------------------------------------
    static let getComments  = BaseUrl + "api/Post/"
    
    //sendPostComment By Post Id ---------------------------------------------
    static let sendPostComment  = BaseUrl + "api/Post/AddComment"
    
    //sendPostCommentReplay By Post Id -----------------------------------------
    static let sendPostCommentReplay  = BaseUrl + "api/Comment/AddRplay"
    
    //sendNewPost  ----------------------------------------
    static let sendNewPost  = BaseUrl + "api/Post/AddPost"
    
    
    //-------------------------
    //---> Live Session
    //getLiveList  ----------------------------------------
    static let getLiveList  = BaseUrl + "api/studnet/getlivevideo"
    
    //sendSTAttendance  ----------------------------------------
    static let sendSTAttendance  = BaseUrl + "api/LiveVideo/AttendLive"
    
    //getLiveList For Teacher ----------------------------------------
    static let getTeacherLiveList  = BaseUrl + "api/LiveVideo/GetAll"
    
    //getTeacherBankQuestionList For Teacher ----------------------------------------
    static let getTeacherBankQuestionList  = BaseUrl + "api/LiveVideo/GetAll"
    
    //-------------------------
    //---> Authentication
    //CheckOption
    static let getCheckOption = BaseUrl + "api/CheckOption"
    
    //forgetPassword
    static let postForgetPassword = BaseUrl + "api/User/forgetPassword"
    
    //ResetPassword
    static let postResetPassword = BaseUrl + "api/User/ResetPassword"
    
    //UpdateProfile
    static let UpdateProfile = BaseUrl + "api/User/UpdateProfile"
    
    
    //-------------------------
    //---> Teacher
    //getNewQuizQuestionsCount
    static let getNewQuizQuestionsCount = BaseUrl + "api/QuestionsBank/GetQuestionsBankQuestionsCount/"
    
    //getNewQuizQuestionsCount
    static let getStudentsPending       = BaseUrl + "api/Student/Pending"
    
    //getAllQuestionsBank
    static let getAllQuestionsBank      = BaseUrl + "api/QuestionsBankApi/GetAll/"
    
    //deleteQuestionsBank
    static let deleteQuestionsBank      = BaseUrl + "api/QuestionsBankApi/"
    
    //createQuestionsBank
    static let createQuestionsBank      = BaseUrl + "api/QuestionsBankApi/SaveQuestionsBank"
     
    //getQuestionsBankCount
    static let getQuestionsBankCount    = BaseUrl + "api/QuestionsBankApi/GetQuestionsBankQuestionsCount/"
    
    //getQuestionsBankList
    static let getQuestionsBankList     = BaseUrl + "api/QuestionsBankApi/GetQuestionsBankQuestions/"
     
    //addQuestionImages
    static let addQuestionImages        = BaseUrl + "api/Question/AddImage"
     
    //saveQuestion
    static let saveQuestion             = BaseUrl + "api/Questions/SaveQuestion"
    
    
    
}
