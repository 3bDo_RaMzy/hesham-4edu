//
//  API+Visit.swift
//  LMS
//
//  Created by Abdo on 11/28/19.
//  Copyright © 2019 eaglez-group. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON


extension API{


    class func sendVisitContactUs(vc: BaseVC, FullName: String, PhoneNumber: String, Message: String,
                                  Subject: String, Email: String, ContactUs: String,
                                  completion: @escaping (_ error: Error?, _ status: String)->Void) {
        
        let url     = URLs.sentContactUs
        
        let params  = [ "FullName":    FullName,
                        "PhoneNumber": PhoneNumber,
                        "Message":     Message,
                        "Subject":     Subject,
                        "Email":       Email,
                        "ContactUs":   ContactUs  ] as [String : Any]
        //vc.showLoading()
        AF.request(url, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil)
            .validate(statusCode: 200..<600)
            .responseJSON{ response in
                
                //print("\n\n sendVisitContactUs response:>>>>>>>>>>>>>>>>> \(response.result)\n")
                
                switch response.result
                {
                case .failure(let error):
                    //vc.hideLoading()
                    completion(error, "No Server Response")
                    //                    print(error)
                    
                case .success(let value):
                    //vc.hideLoading()
                    //let json = JSON(value)
                    
                    //print(response.result.value as Any)   // result of response serialization
                    //let x = response.result.value as! Int
                    if value as! Int == 201
                    {
                     
                        completion(nil, "Done")
                        
                    }else {
                        
                        completion(nil, "Error")
                    }
                }
        }
    } // End of sendVisitContactUs

    //-------------------------------------------------------------------------------------------------------------
    class func getGradeUnites(vc: BaseVC,Grade_id: Int, TermSystem: String, completion: @escaping ( Error?, [Units]? )->Void){
        
        let url = URLs.BaseUrl + "api/Grade/\(Grade_id)/GetUnits"

        //vc.showLoading()
        AF.request(url, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil)
                   .validate(statusCode: 200..<600)
                   .responseJSON{ response in

                    //print("Server Get getGradeUnites Response___________>\(response.value)")
                       switch response.result
                       {
                       case .failure(let error):
                           //vc.hideLoading()
                           completion(error, nil)
                           print("Server Not response___________>\(error)")
                           
                       case .success(let value):
                        
                        let json = JSON(value)
                        
                                          guard let jsonArr = json[].array else{completion(nil,nil)
                                              return
                                          }
                                              
                                          var UnitsArr = [Units]()// to appended and return

                        if TermSystem == "Unique" {
                            
                                for data in jsonArr {
                                  UnitsArr.append(Units(Id:               data["Id"].int   ?? 0,
                                                        Name:             data["Name"].string   ?? "",
                                                        TermSystem:       data["TermSystem"].string   ?? "",
                                                        FirstTermSystem:  data["FirstTermSystem"].string   ?? "",
                                                        SecondTermSystem: data["SecondTermSystem"].string   ?? "",
                                                        file:             data["file"].string   ?? "",
                                                        Other:            data["Other"].string   ?? "",
                                                        gradetype:        data["Other"].string   ?? ""))
                                }
                                                                                                                
                                completion(nil, UnitsArr)
                        
                                              
                        } else if TermSystem == "First" {
                            
                              for data in jsonArr {
                                
                                if( data["TermSystem"].string == "FirstTerm" ){
                                
                                    UnitsArr.append(Units(Id:               data["Id"].int   ?? 0,
                                                          Name:             data["Name"].string   ?? "",
                                                          TermSystem:       data["TermSystem"].string   ?? "",
                                                          FirstTermSystem:  data["FirstTermSystem"].string   ?? "",
                                                          SecondTermSystem: data["SecondTermSystem"].string   ?? "",
                                                          file:             data["file"].string   ?? "",
                                                          Other:            data["Other"].string   ?? "",
                                                          gradetype:        data["Other"].string   ?? ""))
                                 }
                              }
                                                                                                              
                              completion(nil, UnitsArr)
                            
                        } else if TermSystem == "Second" {
                            
                              for data in jsonArr {
                                
                                if( data["TermSystem"].string == "SecondTerm" ){
                                                               
                                                                   UnitsArr.append(Units(Id:               data["Id"].int   ?? 0,
                                                                                         Name:             data["Name"].string   ?? "",
                                                                                         TermSystem:       data["TermSystem"].string   ?? "",
                                                                                         FirstTermSystem:  data["FirstTermSystem"].string   ?? "",
                                                                                         SecondTermSystem: data["SecondTermSystem"].string   ?? "",
                                                                                         file:             data["file"].string   ?? "",
                                                                                         Other:            data["Other"].string   ?? "",
                                                                                         gradetype:        data["Other"].string   ?? ""))
                                 }
                              }
                                                                                                              
                              completion(nil, UnitsArr)
                            
                        }
                        
                       } //End of switch case
               }//End of alamofire response
    }//---> End Of Method getGradeUnites

    //-------------------------------------------------------------------------------------------------------------
    class func getUnitePDF(vc: BaseVC, Unite_id: Int, completion: @escaping ( Error?, [Files]? )->Void){
        
        let url = URLs.BaseUrl + "api/Unit/\(Unite_id)/GetFiles"

        //vc.showLoading()
        AF.request(url, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil)
                   .validate(statusCode: 200..<600)
                   .responseJSON{ response in

                    //print("Server Get getUnitePDF Response___________>\(response.value)")
                       switch response.result
                       {
                       case .failure(let error):
                           //vc.hideLoading()
                           completion(error, nil)
                           print("Server Not response___________>\(error)")
                           
                       case .success(let value):
                        
                        let json = JSON(value)
                        
                                          guard let jsonArr = json[].array else{completion(nil,nil)
                                              return
                                          }
                                          var FilesArr = [Files]()// to appended and return

                                              for data in jsonArr {
                                                
                                                FilesArr.append(Files(Id: data["Id"].int   ?? 0,
                                                                      Path: data["Path"].string   ?? ""))
                                              }
                                                                                                                              
                                          completion(nil, FilesArr)
                        
                       } //End of switch case
               }//End of alamofire response
    }//---> End Of Method getUnitePDF
    
//---------------------------------------------------------------------------------------------------------
    class func getMaterialVideos(vc: BaseVC, Token: String, StudentId: Int, TermSystem: Int,
                                 completion: @escaping ( Error?,_ TimeOut: Bool?,[PrivateVideos]? )->Void){
        
        if TermSystem != 0 {
            
            let url = URLs.BaseUrl + "api/Matrial/Videos/GetAll?studentId=\(StudentId)&termsystem=\(TermSystem)"
            let HeaderParam : HTTPHeaders = [ "Authorization":  Token]
            
            AF.request(url, method: .get, parameters: nil, encoding: URLEncoding.default, headers: HeaderParam)
                       .validate(statusCode: 200..<600)
                       .responseJSON{ response in
 
                           switch response.result
                           {
                           case .failure(let error):
                               //vc.hideLoading()
                            
                            if (response.response?.statusCode == 401) {
                                print("force user to login again --> 401")
                                completion(error,true, nil)
                            } else {
                                print("Server Not response___________>\(error)")
                                completion(error,false, nil)
                            }
                                 
                           case .success(let value):
                            
                            let json = JSON(value)
                            
                                guard let jsonArr = json["Data"].array else{completion(nil,false,nil)
                                        return
                                }
                                var VideosArr = [PrivateVideos]()// to appended and return

                                for data in jsonArr {
                                                   
                                    
                                    var VideosMaterialsArr = [PrivateVideosMaterials]()
                                    guard let VideosMetArr = data["Marials"].array else{completion(nil,false,nil)
                                            return
                                    }
                                    for dataIn in VideosMetArr {
                                        
                                        VideosMaterialsArr.append(PrivateVideosMaterials(Path: dataIn["Path"].string   ?? "", Name: dataIn["Name"].string   ?? ""))
                                        
                                    }
                                    
                                    VideosArr.append(PrivateVideos(FileName: data["FileName"].string   ?? "",
                                                                   Marials: VideosMaterialsArr))
                                }
                                                                                                                                  
                            completion(nil,false, VideosArr)
                            
                           } //End of switch case
                   }//End of alamofire response
            
            
            
        } else {
            
            
            let url = URLs.BaseUrl + "api/Matrial/Videos/GetAll?studentId=\(StudentId)"
            let HeaderParam : HTTPHeaders = [ "Authorization":  Token]
            
            AF.request(url, method: .get, parameters: nil, encoding: URLEncoding.default, headers: HeaderParam)
                       .validate(statusCode: 200..<600)
                       .responseJSON{ response in

                        print("Server Get getMaterialVideos Response___________>\(response.value)")
                           switch response.result
                           {
                           case .failure(let error):
                               //vc.hideLoading()
                               if (response.response?.statusCode == 401) {
                                   print("force user to login again --> 401")
                                   completion(error,true, nil)
                               } else {
                                   print("Server Not response___________>\(error)")
                                   completion(error,false, nil)
                               }
                               
                           case .success(let value):
                            
                            let json = JSON(value)
                            
                                guard let jsonArr = json["Data"].array else{completion(nil,false,nil)
                                        return
                                }
                                var VideosArr = [PrivateVideos]()// to appended and return

                                for data in jsonArr {
                                                   
                                    
                                    
                                    var VideosMaterialsArr = [PrivateVideosMaterials]()
                                    guard let VideosMetArr = data["Marials"].array else{completion(nil,false,nil)
                                            return
                                    }
                                    for dataIn in VideosMetArr {
                                        
                                        VideosMaterialsArr.append(PrivateVideosMaterials(Path: dataIn["Path"].string   ?? "", Name: dataIn["Name"].string   ?? ""))
                                        
                                    }
                                    
                                    VideosArr.append(PrivateVideos(FileName: data["FileName"].string   ?? "",
                                                                   Marials: VideosMaterialsArr))
                                }
                                                                                                                                  
                            completion(nil,false, VideosArr)
                            
                           } //End of switch case
                   }//End of alamofire response
            
        }
        
        
        

}//---> End Of Method getMaterialVideos
    
    
    
    
    
    

    //-------------------------------------------------------------------------------------------------------------
    class func getGradeChannels(vc: BaseVC,Grade_id: Int, TermSystem: String, completion: @escaping ( Error?, [Chanals]? )->Void){
        
        let url = URLs.BaseUrl + "api/Grade/\(Grade_id)/GetYoutubeChannel"

        //vc.showLoading()
        AF.request(url, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil)
                   .validate(statusCode: 200..<600)
                   .responseJSON{ response in

                    //print("Server Get getGradeChannels Response___________>\(response.value)")
                       switch response.result
                       {
                       case .failure(let error):
                           //vc.hideLoading()
                           completion(error, nil)
                           print("Server Not response___________>\(error)")
                           
                       case .success(let value):
                        
                        let json = JSON(value)
                         guard let jsonArr = json[].array else{completion(nil,nil)
                             return
                         }
                         var ChanalsArr = [Chanals]()// to appended and return

                        if TermSystem == "Unique" {
                            
                                for data in jsonArr {
                                    ChanalsArr.append(Chanals(ChanalId:      data["ChanalId"].int   ?? 0,
                                                              Path:          data["Path"].string   ?? "",
                                                              Term:          data["Term"].string   ?? "",
                                                              YouTubeListId: data["YouTubeListId"].string   ?? ""))
                                }
                                                                                                                
                                completion(nil, ChanalsArr)
                        
                                              
                        } else if TermSystem == "First" {
                            
                              for data in jsonArr {
                                
                                if( data["Term"].string == "FirstTerm" ){
                                
                                    ChanalsArr.append(Chanals(ChanalId:      data["ChanalId"].int   ?? 0,
                                                              Path:          data["Path"].string   ?? "",
                                                              Term:          data["Term"].string   ?? "",
                                                              YouTubeListId: data["YouTubeListId"].string   ?? ""))

                                 }
                              }
                                                                                                              
                              completion(nil, ChanalsArr)
                            
                        } else if TermSystem == "Second" {
                            
                              for data in jsonArr {
                                
                                if( data["Term"].string == "SecondTerm" ){
                                                               
                                 ChanalsArr.append(Chanals(ChanalId:      data["ChanalId"].int   ?? 0,
                                                           Path:          data["Path"].string   ?? "",
                                                           Term:          data["Term"].string   ?? "",
                                                           YouTubeListId: data["YouTubeListId"].string   ?? ""))

                                 }
                              }
                                                                                                              
                              completion(nil, ChanalsArr)
                            
                        }
                        
                       } //End of switch case
               }//End of alamofire response
    }//---> End Of Method getGradeChannels

    //-------------------------------------------------------------------------------------------------------------
    class func getPlaylistVideos(vc: BaseVC, playlistId: String, completion: @escaping ( Error?, [Videos]? )->Void){

        let url =  "https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&maxResults=50&playlistId=\(playlistId)&key=AIzaSyCGJ3ZuPeikHNheYs3RloPP8VWQaQXbph8"

        //vc.showLoading()
        AF.request(url, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil)
                   .validate(statusCode: 200..<600)
                   .responseJSON{ response in

                    //print("Server Get getPlaylistVideos Response___________>\(response.value)")
                       switch response.result
                       {
                       case .failure(let error):
                           //vc.hideLoading()
                           completion(error, nil)
                           print("Server Not response___________>\(error)")

                       case .success(let value):

                        let json = JSON(value)
                         guard let jsonArr = json["items"].array else{completion(nil,nil)
                             return
                         }

                         var VideosArr = [Videos]()// to appended and return

                                for data in jsonArr {

                                      guard let snippet = data["snippet"]["title"].string else{completion(nil,nil)
                                          return
                                      }

                                      guard let thumbnailURL = data["snippet"]["thumbnails"]["default"]["url"].string else{completion(nil,nil)
                                          return
                                      }

                                      guard let videoId = data["snippet"]["resourceId"]["videoId"].string else{completion(nil,nil)
                                          return
                                      }

                                    VideosArr.append(Videos(title:         snippet,
                                                            thumbnailsURL: thumbnailURL,
                                                            videoId:       videoId))


                                }

                                completion(nil, VideosArr)

                       } //End of switch case
               }//End of alamofire response
    }//---> End Of Method getPlaylistVideos
    
    //-------------------------------------------------------------------------------------------------------------
    class func getAboutUs(vc: BaseVC, completion: @escaping ( Error?, [AboutUS]? )->Void){
        
        let url = URLs.getAboutUs

        //vc.showLoading()
        AF.request(url, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil)
                   .validate(statusCode: 200..<600)
                   .responseJSON{ response in

                    //print("Server Get getAboutUs Response___________>\(response.value)")
                       switch response.result
                       {
                       case .failure(let error):
                           //vc.hideLoading()
                           completion(error, nil)
                           print("Server Not response___________>\(error)")
                           
                       case .success(let value):
                        
                        let json = JSON(value)
                        
                                guard let jsonArr = json[].array else{completion(nil,nil)
                                        return
                                }
                                 var AboutUSArr = [AboutUS]()// to appended and return

                                    for data in jsonArr {
                                                
                                        AboutUSArr.append(AboutUS(Id:    data["Id"].int        ?? 0,
                                                                  Key:   data["Key"].string    ?? "",
                                                                  Value: data["Value"].string  ?? ""))
                                    }
                                                                                                                              
                            completion(nil, AboutUSArr)
                        
                       } //End of switch case
               }//End of alamofire response
    }//---> End Of Method getAboutUs
    
}
