//
//  API+LiveSessions.swift
//  LMS
//
//  Created by Abdo on 4/5/20.
//  Copyright © 2020 eaglez-group. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON


extension API{
    
    
    
    class func getLiveList(vc: BaseVC, Token: String,
                           completion: @escaping ( Error?,_ TimeOut: Bool?, [Live]? )->Void){

                    let url = URLs.getLiveList

                    let HeaderParam : HTTPHeaders = [ "Authorization":  Token]

                    //vc.showLoading()
                    AF.request(url, method: .get, parameters: nil, encoding: URLEncoding.default, headers: HeaderParam)
                               .validate(statusCode: 200..<600)
                               .responseJSON{ response in

                                print("Server Get getPostsList ___________>\(response.value ?? "default")")
                                   switch response.result
                                   {
                                   case .failure(let error):
                                       //vc.hideLoading()
                                    
                                          if (response.response?.statusCode == 401) {
                                              print("force user to login again --> 401")
                                              completion(error,true, nil)
                                          } else {
                                              print("Server Not response___________>\(error)")
                                              completion(error,false, nil)
                                          }
                                       
                                   case .success(let value):

                                        let json = JSON(value)

                                        print("\n\n JSON ___________>\(json)")
                                        guard let jsonArr = json["Result"].array else{completion(nil,false,nil)
                                                  return
                                             }
                                        var LiveArr    = [Live]()    // to appended and return
                                        
                                       for data in jsonArr {

                                        LiveArr.append(Live(Id:         data["Id"].int ?? 0,
                                                            Name:       data["Name"].string ?? "",
                                                            PublicName: data["PublicName"].string ?? ""))
                                        
                                        
                                        }

                                       completion(nil,false, LiveArr)

                } //End of switch case
            }//End of alamofire response
        }
    
  //-----------------------------------------------------------------------------------------------------
    
    class func sendStudentAttendance( vc: BaseVC, StudentId: Int, LiveVideoId: Int, LiveTime: String,
                                      completion: @escaping (_ error: Error?,_ TimeOut: Bool?, _ Status: Bool)->Void) {
           
                   
                   var Spinner :UIView!
                   let url = URLs.sendSTAttendance
                          
                   //let HeaderParam : HTTPHeaders = [ "Authorization":  Token]
                   let params       = [ "StudentId":    StudentId,
                                        "LiveVideoId":  LiveVideoId,
                                        "LiveTime":     LiveTime] as [String : Any]

                   Spinner = UIViewController.displaySpinner(onView: vc.view)
                       
                   AF.request(url, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil)
                                       .validate(contentType: ["application/json"])
                                       .validate(statusCode: 200..<600)
                                       .responseJSON{ response in
           
                           UIViewController.removeSpinner(spinner: Spinner)
                           print("\n\n\n sendStudentQuiz  response:>>>>>>>>>>>>>>>>> \(response)\n")
                   
                   switch response.result {
                       
                   case .failure(let error):
                       //vc.dismiss(animated: true, completion: nil)
                    
                        if (response.response?.statusCode == 401) {
                            print("force user to login again --> 401")
                            completion(error,true, false)
                        } else {
                            print("Server Not response___________>\(error)")
                            completion(error,false, false)
                        }

                   case .success(let value):
                       //vc.dismiss(animated: true, completion: nil)
                       let json = JSON(value)
                       print("\n\n\nLogin_success:>>>>>>>>>>>>>>>>> \(json["Message"].string)\n")
                       showToast(message: json["Message"].string ?? "Thanx for your attendance", vc: vc)
                       if  json["success"] == true {
                               completion(nil,false, true)
                       } else {
                           
                           completion(nil,false, false)
                       }
                       
                   }
           }
       } // End of sendRequest
    

    
    
    //--------------------------------------------------------------------------------------------------
    //-----> Teacher
     
    class func getTeacherAllLiveList(vc: BaseVC, Token: String,
                                     completion: @escaping ( Error?,_ TimeOut: Bool?, [Live]? )->Void){

                    let url = URLs.getTeacherLiveList
                    let HeaderParam : HTTPHeaders = [ "Authorization":  Token]

                    //vc.showLoading()
                    AF.request(url, method: .get, parameters: nil, encoding: URLEncoding.default, headers: HeaderParam)
                               .validate(statusCode: 200..<600)
                               .responseJSON{ response in

                                print("Server Get getPostsList ___________>\(response.value ?? "default")")
                                   switch response.result
                                   {
                                   case .failure(let error):
                                       //vc.hideLoading()
                                    
                                        if (response.response?.statusCode == 401) {
                                            print("force user to login again --> 401")
                                            completion(error,true, nil)
                                        } else {
                                            print("Server Not response___________>\(error)")
                                            completion(error,false, nil)
                                        }


                                   case .success(let value):

                                        let json = JSON(value)

                                        print("\n\n JSON ___________>\(json)")
                                        guard let jsonArr = json["Result"].array else{completion(nil,false,nil)
                                                  return
                                             }
                                        var LiveArr    = [Live]()    // to appended and return
                                        
                                       for data in jsonArr {

                                        LiveArr.append(Live(Id:         data["Id"].int ?? 0,
                                                            Name:       data["Name"].string ?? "",
                                                            PublicName: data["PublicName"].string ?? ""))
                                        
                                        
                                        }

                                       completion(nil,false, LiveArr)

                } //End of switch case
            }//End of alamofire response
        }
    
       //--------------------------------------------------------------------------------------------------------------
           class func endTeacherLiveSession(vc: BaseVC, Token: String, SessionId: Int,
                                            completion: @escaping (_ error: Error?,_ TimeOut: Bool?, _ Status: Bool)->Void) {
               
                       
                       var Spinner :UIView!
                       let url = URLs.BaseUrl + "api/LiveVideo/End/\(SessionId)"
                      
                       let HeaderParam : HTTPHeaders = [ "Authorization":  Token]
                       Spinner = UIViewController.displaySpinner(onView: vc.view)
                           
                       AF.request(url, method: .get, parameters: nil, encoding: URLEncoding.default, headers: HeaderParam)
                                           .validate(contentType: ["application/json"])
                                           .validate(statusCode: 200..<600)
                                           .responseString { response in
                                            
                               UIViewController.removeSpinner(spinner: Spinner)
                               let statusCode = response.response?.statusCode ?? 500
    
                       switch statusCode {
                           
                       case 401:
                        
                            completion(nil, true, false)
                        
                       case 500,404:

                           completion(nil, false, false)

                       case 200:
                            
                           completion(nil, false, true)
                           
                       default:
                           
                           completion(nil, false, false)
                       }
               }
           } // End of sendPendingRequest
    
    
       //--------------------------------------------------------------------------------------------------------------
    class func createTeacherLiveSession(vc: BaseVC, Token: String, PublicName: String, GradeId: Int, Groups: [Int],
                                        completion: @escaping (_ error: Error?,_ TimeOut: Bool?, _ data: String)->Void) {
 
                       var Spinner :UIView!
                       let url = URLs.BaseUrl + "api/LiveVideo/Create"
                      
                       let HeaderParam : HTTPHeaders = [ "Authorization":  Token]
            
                        let params       = [ "PublicName":  PublicName,
                                             "Password":    "",
                                             "GradeId":     GradeId,
                                             "Groups":      Groups] as [String : Any]
            
                       Spinner = UIViewController.displaySpinner(onView: vc.view)
                           
                       AF.request(url, method: .post, parameters: params, encoding: URLEncoding.default, headers: HeaderParam)
                                .validate(contentType: ["application/json"])
                                .validate(statusCode: 200..<600)
                                .responseJSON
                        { response in
                                                          
                                        //print("\n\n\n CheckOption  response:>>>>>>>>>>>>>>>>> \(response)\n")
                                        UIViewController.removeSpinner(spinner: Spinner)
                                                          
                                        switch response.result
                                        {
                                                              
                                            case .failure(let error):
                                                
                                              if (response.response?.statusCode == 401) {
                                                        print("force user to login again --> 401")
                                                        completion(error,true, "")
                                              } else {
                                                        print("Server Not response___________>\(error)")
                                                        completion(error,false, "")
                                              }
                                            
                                            case .success(let value):
                                               
                                                let json = JSON(value)
                                                
                                                var returnData   = ""
                                                let sessionName  = json["Name"].string ?? ""
                                                let Message      = json["Message"].string ?? ""
                                                
                                                if sessionName != ""{
                                                    
                                                    returnData = sessionName
                                                } else {
                                                    
                                                    returnData = Message
                                                }
                                                
                                                
                                                completion(nil,false, returnData)
                                        }
                    }
           } // End of createTeacherLiveSession
    //--------------------------------------------------------------------------------
    
    class func getTeacherBankQuestionList(vc: BaseVC, Token: String,
                                          completion: @escaping ( Error?,_ TimeOut: Bool?, [Live]? )->Void){

                    let url = URLs.getTeacherLiveList

                    let HeaderParam : HTTPHeaders = [ "Authorization":  Token]

                    //vc.showLoading()
                    AF.request(url, method: .get, parameters: nil, encoding: URLEncoding.default, headers: HeaderParam)
                               .validate(statusCode: 200..<600)
                               .responseJSON{ response in

                                print("Server Get getPostsList ___________>\(response.value ?? "default")")
                                   switch response.result
                                   {
                                   case .failure(let error):
                                    
                                     if (response.response?.statusCode == 401) {
                                                print("force user to login again --> 401")
                                                completion(error,true, nil)
                                     } else {
                                                print("Server Not response___________>\(error)")
                                                completion(error,false, nil)
                                     }
                                    

                                   case .success(let value):

                                        let json = JSON(value)

                                        print("\n\n JSON ___________>\(json)")
                                        guard let jsonArr = json["Result"].array else{completion(nil,nil,nil)
                                                  return
                                             }
                                        var LiveArr    = [Live]()    // to appended and return
                                        
                                       for data in jsonArr {

                                        LiveArr.append(Live(Id:         data["Id"].int ?? 0,
                                                            Name:       data["Name"].string ?? "",
                                                            PublicName: data["PublicName"].string ?? ""))
                                        
                                        
                                        }

                                       completion(nil, false, LiveArr)

                } //End of switch case
            }//End of alamofire response
        }
    
    
    
   //----------------------------------------
   class func showToast(message : String, vc: BaseVC) {

        let toastLabel = UILabel(frame: CGRect(x: vc.view.frame.size.width/2 - 75, y: vc.view.frame.size.height-100, width: 150, height: 35))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center;
        toastLabel.font = UIFont(name: "Montserrat-Light", size: 12.0)
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        vc.view.addSubview(toastLabel)
        UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
}
