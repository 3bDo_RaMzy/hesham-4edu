
//
//  Shared.swift
//  LMS
//
//  Created by Abdo on 11/20/19.
//  Copyright @ 2019 eaglez-group. All rights reserved.
//

import Foundation
import UIKit


final class Shared {
       static let shared = Shared()
       
   //  var companyName : String!
       var UserID      : Int!
       var Name        : String!
       var Photo       : String!
       var UserType    : String!
   //---------------------------
       var ChildrenId  : String!
       
   }

class helper: NSObject{

    //--------------------------------------------------------------
    //--------------------------------------------------------------
    class func saveUserToken(Token: String){
        
        let def = UserDefaults.standard
        def.setValue(Token,            forKey: "Token")
        def.synchronize()
    }
    class func getUserToken()-> String?{
        let    def = UserDefaults.standard
        return def.object(forKey: "Token") as? String
    }
    class func resetUserToken(){
        
        let def = UserDefaults.standard
        def.setValue(nil,            forKey: "Token")
        def.synchronize()
    }
    
    class func saveStudentGradeId(STGradID: Int){
        
        let def = UserDefaults.standard
        def.setValue(STGradID,            forKey: "STGradeId")
        def.synchronize()
    }
    class func getStudentGradeId()-> Int?{
        let    def = UserDefaults.standard
        return def.object(forKey: "STGradeId") as? Int
    }
    
    class func saveStudentTermSystem(ST_TermSys: Bool){
        
        let def = UserDefaults.standard
        def.setValue(ST_TermSys,  forKey: "ST_TermSystem")
        def.synchronize()
    }
    class func getStudentTermSystem()-> Bool?{
        let    def = UserDefaults.standard
        return def.object(forKey: "ST_TermSystem") as? Bool
    }
    
    class func saveStudentGroupId(STGroupID: Int){
        
        let def = UserDefaults.standard
        def.setValue(STGroupID,            forKey: "STGroupId")
        def.synchronize()
    }
    class func getStudentGroupId()-> Int?{
        let    def = UserDefaults.standard
        return def.object(forKey: "STGroupId") as? Int
    }
    
    class func saveStudentId(St_Id: Int){
        
        let def = UserDefaults.standard
        def.setValue(St_Id,  forKey: "ST_Id")
        def.synchronize()
    }
    class func getStudentId()-> Int?{
        let    def = UserDefaults.standard
        return def.object(forKey: "ST_Id") as? Int
    }
    
    class func saveUserFCMToken(FCMToken: String){
        
        let def = UserDefaults.standard
        def.setValue(FCMToken,            forKey: "FCMToken")
        def.synchronize()
    }
    class func getUserFCMToken()-> String?{
        let    def = UserDefaults.standard
        return def.object(forKey: "FCMToken") as? String
    }
    
    
    //-------------------------------------------------------------
    //-------------------------------------------------------------
    class func saveAppLang(Lang: String){
        
        let def = UserDefaults.standard
        def.setValue(Lang,            forKey: "Lang")
        def.synchronize()
    }
    class func getAppLang()-> String?{
        let    def = UserDefaults.standard
        return def.object(forKey: "Lang") as? String
    }
    
    //-------------------------------------------------------------
    //-------------------------------------------------------------
    class func saveAppCurrency(Currency: String){
        
        let def = UserDefaults.standard
        def.setValue(Currency,            forKey: "Currency")
        def.synchronize()
    }
    class func getAppCurrency()-> String?{
        let    def = UserDefaults.standard
        return def.object(forKey: "Currency") as? String
    }
    
    //-------------------------------------------------------------
    //-------------------------------------------------------------
    class func saveAppOption(OnlineAcdmic: Bool){
        
        let def = UserDefaults.standard
        def.setValue(OnlineAcdmic,   forKey: "OnlineAcdmic")
        def.synchronize()
    }
    class func getAppOption()-> Bool?{
        let    def = UserDefaults.standard
        return def.object(forKey: "OnlineAcdmic") as? Bool
    }
    
    
    class func saveSMSOption(SMSOption: Bool){
        
        let def = UserDefaults.standard
        def.setValue(SMSOption,   forKey: "SMSOption")
        def.synchronize()
    }
    class func getSMSOption()-> Bool?{
        let    def = UserDefaults.standard
        return def.object(forKey: "SMSOption") as? Bool
    }
    
    class func saveEmailOption(EmailOption: Bool){
           
           let def = UserDefaults.standard
           def.setValue(EmailOption,   forKey: "EmailOption")
           def.synchronize()
       }
       class func getEmailOption()-> Bool?{
           let    def = UserDefaults.standard
           return def.object(forKey: "EmailOption") as? Bool
       }
    
    //-------------------------------------------------------------
    class func restartApp(){
        
        guard let window=UIApplication.shared.keyWindow else{return}
        let sub = UIStoryboard(name: "Main", bundle: nil)
        
        var vc:UIViewController
        if helper.getUserToken() == nil{
            //--> Go to Login Page
            vc = sub.instantiateInitialViewController()!
        } else {
            //--> Go to Home Page
            vc = sub.instantiateViewController(withIdentifier: "Home")
        }
        
        window.rootViewController = vc
        UIView.transition(with: window, duration: 0.5, options: .transitionFlipFromTop, animations: nil, completion: nil)
    }
}
