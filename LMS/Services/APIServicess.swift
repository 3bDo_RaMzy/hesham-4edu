//
//  APIServicess.swift
//  LMS
//
//  Created by Abdo on 11/20/19.
//  Copyright © 2019 eaglez-group. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON


class API: NSObject {
    
//---------------------------------------------------------------------------------------------------------
    class func login(vc: UIViewController, email: String, password: String,
                     completion: @escaping (_ error: Error?, _ success: Bool, _ saveToken: String)->Void) {
        
        let url = URLs.Login
        let params  = [ "email":    email,
                        "password": password ] as [String : Any]
        
        let storyboard  = UIStoryboard(name: "Main", bundle: nil)
        let myLoading   = storyboard.instantiateViewController(withIdentifier: "LoadingVC")
        myLoading.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        myLoading.modalTransitionStyle   = .crossDissolve
        vc.present(myLoading, animated: true){
        
        AF.request(url, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil)
            .validate(statusCode: 200..<600)
            .responseJSON { response in
                
                print("\n\n\nLogin_success  response:>>>>>>>>>>>>>>>>> \(response)\n")
                
                switch response.result
                {
                    
                case .failure(let error):
                    vc.dismiss(animated: true, completion: nil)
                    completion(error, false, "")
                //                    print(error)
                case .success(let value):
                    vc.dismiss(animated: true, completion: nil)
                    let json = JSON(value)
                    //print("\n\n\nLogin_success:>>>>>>>>>>>>>>>>> \(json)\n")
                    let message = json["message"].string
                    //print("\n\n Login replay :\(message) ")
                    
                    if( message == "Done login"){
                        
                        let token = json["token"].string
                        helper.saveUserToken(Token: token!)
                        // print("\n\n\n\n\n\n\n\n\n Login tokennnn :\(token) ")
                        completion(nil, true, "\(token)")
                        
                    }
                    else if( message == "error login"){
                        
                        completion(nil, false, "error login")
                    }
                }
        }
      } //---> End Of Loading
    } // End of login
    
    //------------------------------------------------------------
    class func Register(vc: BaseVC, first_name: String, last_name: String, email: String, password: String,
                        governorat: Int, number_id: String, phone_number: String,
                        completion: @escaping (_ error: Error?, _ status: String)->Void) {
        
        let url     = URLs.Signup
        let params  = [ "first_name":   first_name,
                        "last_name":    last_name,
                        "email":        email,
                        "password":     password,
                        "governorat":   governorat,
                        "number_id":    number_id,
                        "phone_number": phone_number  ] as [String : Any]
        
        let storyboard  = UIStoryboard(name: "Main", bundle: nil)
        let myLoading   = storyboard.instantiateViewController(withIdentifier: "LoadingVC")
        myLoading.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        myLoading.modalTransitionStyle   = .crossDissolve
        vc.present(myLoading, animated: true){
            
        AF.request(url, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil)
            .validate(statusCode: 200..<600)
            .responseJSON{ response in
                
                //print("\n\n\nRegister_success  response:>>>>>>>>>>>>>>>>> \(response)\n")
                
                switch response.result
                {
                case .failure(let error):
                    vc.dismiss(animated: true, completion: nil)
                    completion(error, "No Server Response")
                    //                    print(error)
                    
                case .success(let value):
                    vc.dismiss(animated: true, completion: nil)
                    let json = JSON(value)
                    //                            print("\n\n\nRegister_success:>>>>>>>>>>>>>>>>> \(json)\n")
                    let message = json["message"].string
                    //                            print("\n\n Register replay :\(message) ")
                    
                    if( message == "Done Register User" ){
                        
                        completion(nil, "Done")
                        
                    }
                    else if( message == "Email Alredy Exist" ){
                        
                        completion(nil, "Found Email")
                        
                    }
                    else {
                        
                        completion(nil, "Error")
                    }
                }
        }
      }//---> End Of Loading
    } // End of Register

//-----------------------------------------------------------------------------------------------------------
    
    class func CheckOption(loadingView: UIActivityIndicatorView, completion: @escaping (_ error: Error?, _ Msg: String )->Void) {
        
        let url = URLs.getCheckOption
         
        loadingView.startAnimating()
        
        AF.request(url, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil)
            .validate(statusCode: 200..<600)
            .responseJSON { response in
                
                print("\n\n\n CheckOption  response:>>>>>>>>>>>>>>>>> \(response)\n")
                
                switch response.result
                {
                    
                case .failure(let error):
                     loadingView.stopAnimating()
                     completion(error, "")
                    // print(error)
                    
                case .success(let value):
                     loadingView.stopAnimating()
                     let json = JSON(value)
                     
                      let message  = json["Message"].string ?? ""
                     
                        if( message == "") {
                            
                            let OnlineAcdemic    = json["IsUseOnlineAcdmicOption"].bool ?? false
                            let UseSmsMisr       = json["UseSmsMisr"].bool              ?? false
                            let UseMailSetting   = json["UseMailSetting"].bool          ?? false
                            
                            helper.saveAppOption(OnlineAcdmic:  OnlineAcdemic)
                            helper.saveSMSOption(SMSOption:     UseSmsMisr)
                            helper.saveEmailOption(EmailOption: UseMailSetting)
                            
                            completion( nil, "" )
                            
                        } else {
                            
                            completion( nil, message )
                        }
                }
        }
      //}//---> End Of Loading
    } // End of CheckOption
    
    
//-----------------------------------------------------------------------------------------------------
    class func forgetPassword(vc: UIViewController, MobileOrEmail: String,
                     completion: @escaping (_ error: Error?, _ success: Bool, _ smsCode: String,
                                            _ provider: String)->Void) {
        
        let url = URLs.postForgetPassword
        let params  = [ "MobileOrEmail":    MobileOrEmail ]
        
        var Spinner :UIView!
        Spinner = UIViewController.displaySpinner(onView: vc.view)
        
        AF.request(url, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil)
            .validate(statusCode: 200..<600)
            .responseJSON { response in
                
                UIViewController.removeSpinner(spinner: Spinner)
                print("\n\n\n forgetPassword response:>>>>> \(response)\n")
 
                switch response.result
                {
                    
                case .failure(let error):
                    //vc.dismiss(animated: true, completion: nil)
                    completion(error, false, "","")
                    //print(error)
                    
                case .success(let value):
                    //vc.dismiss(animated: true, completion: nil)
                    
                    let json     = JSON(value)
                    let message  = json["Message"].string  ?? ""
                    let provider = json["Provider"].string ?? ""
 
                    if( message == "Send-Success"){
                        
                        let Code = json["Code"].string ?? ""
                        completion(nil, true, "\(Code)", provider)
                        
                    } else if( message != "Send-Success"){
                        
                        completion(nil, false, "\(message)", "")
                    }
                }
        }
     } // End of forgetPassword
    
    //-----------------------------------------------------------------------------------------------------
    class func ResetPassword(vc: UIViewController, MobileOrEmail: String, NewPassword: String,
                        completion: @escaping (_ error: Error?, _ success: Bool, _ smsCode: String)->Void) {
           
           let url = URLs.postResetPassword
           let params  = [ "MobileOrEmail":  MobileOrEmail,
                           "NewPassword":    NewPassword  ]
           
           var Spinner :UIView!
           Spinner = UIViewController.displaySpinner(onView: vc.view)
           
           AF.request(url, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil)
               .validate(statusCode: 200..<600)
               .responseJSON { response in
                   
                   UIViewController.removeSpinner(spinner: Spinner)
                   print("\n\n\n ResetPassword response:>>>>> \(response)\n")
                   
                   switch response.result
                   {
                       
                   case .failure(let error):
                       //vc.dismiss(animated: true, completion: nil)
                       completion(error, false, "")
                       //print(error)
                       
                   case .success(let value):
                       //vc.dismiss(animated: true, completion: nil)
                       
                       let json    = JSON(value)
                       let message = json[].string ?? ""
    
                       if( message == "Password Changed"){
                           
                            completion(nil, true, "Password Changed")
                           
                       } else if( message != "Password Changed"){
                           
                           completion(nil, false, "\(message)")
                       }
                   }
           }
        } // End of ResetPassword

    
    //---------------------------------------------------------------------------
         
        class func getProfileData( vc: BaseVC, completion: @escaping ( Error?,_ TimeOut: Bool?, ProfileDataModel? )->Void){

                    let url = URLs.GetProfile
                    let header : HTTPHeaders = ["Authorization": "\(helper.getUserToken() ?? "")",
                                                "Lang":  "en" ]
                    
                    var Spinner :UIView!
                    Spinner = UIViewController.displaySpinner(onView: vc.view)
                        
                    AF.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: header)
                        .validate(statusCode: 200..<600)
                        .validate(contentType: ["application/json"])
                        .responseJSON{ response in
         
                            UIViewController.removeSpinner(spinner: Spinner)
                            print("\n\n\n ResetPassword response:>>>>> \(response)\n")
                            
                    switch response.result
                                        {
                            case .failure(let error):
                                
                                 if (response.response?.statusCode == 401) {
                                     print("force user to login again --> 401")
                                     completion(error,true, nil)
                                 } else {
                                     print("Server Not response___________>\(error)")
                                     completion(error,false, nil)
                                 }

                            case .success(let value):

                                 let json = JSON(value)
                                print("\n\n JSON ___________>\(json)")
                                
//                                "FullName": "Eaglez son",
//                                "Mobile": "01017127199",
//                                "EmailAddress": "ramzy@Gmail.com",
//                                "Image":
                                 
                              let userFirstName   = json["FullName"].string ?? ""
                              let userLastName    = json["Mobile"].string ?? ""
                              let userEmail       = json["EmailAddress"].string ?? ""
                              let userpicture     = json["Image"].string ?? ""
                              let userUserCode    = json["UserCode"].string ?? ""
                                    
                              let ProfileData    = ProfileDataModel()   // to appended and return
                                    ProfileData.FullName     = userFirstName
                                    ProfileData.Mobile       = userLastName
                                    ProfileData.EmailAddress = userEmail
                                    ProfileData.Image        = userpicture
                                    ProfileData.UserCode     = userUserCode

                            completion(nil,false, ProfileData)

                } //End of switch case
            }//End of alamofire response
        }//end of getProfileData
        
        class func getProfileRefreshData( vc: BaseVC, completion: @escaping ( Error?,_ TimeOut: Bool?, ProfileDataModel? )->Void){

                    let url = URLs.GetProfile
                    let header : HTTPHeaders = ["Authorization": "\(helper.getUserToken() ?? "")",
                                                "Lang": "en" ]
                     
                    AF.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: header)
                        .validate(statusCode: 200..<600)
                        .validate(contentType: ["application/json"])
                        .responseJSON{ response in
         
                     print("Server Get getPostsList ___________>\(response.value ?? "No Return Value")")
                    switch response.result
                                        {
                            case .failure(let error):
                                
                                 if (response.response?.statusCode == 401) {
                                      print("force user to login again --> 401")
                                      completion(error,true, nil)
                                 } else {
                                      print("Server Not response___________>\(error)")
                                      completion(error,false, nil)
                                 }

                            case .success(let value):

                                 let json = JSON(value)
                                print("\n\n JSON ___________>\(json)")
                                
                              let userFirstName   = json["FullName"].string ?? ""
                              let userLastName    = json["Mobile"].string ?? ""
                              let userEmail       = json["EmailAddress"].string ?? ""
                              let userpicture     = json["Image"].string ?? ""
                                    
                              let ProfileData    = ProfileDataModel()   // to appended and return
                                    ProfileData.FullName     = userFirstName
                                    ProfileData.Mobile       = userLastName
                                    ProfileData.EmailAddress = userEmail
                                    ProfileData.Image        = userpicture

                            completion(nil,false, ProfileData)

                } //End of switch case
            }//End of alamofire response
        }//end of getProfileData
    //--------------------------------------------------------------------------
    
    class func UpdateProfile(vc: BaseVC, fullName: String, email: String, Mobile: String, Image: String,
                        completion: @escaping (_ error: Error?,_ TimeOut: Bool?, _ success: Bool)->Void) {
           
           let url = URLs.UpdateProfile
           let header : HTTPHeaders = [ "Authorization": "\(helper.getUserToken() ?? "")",
                                        "Lang": "en" ]
           let params  = [  "Mobile":       Mobile,
                            "EmailAddress": email,
                            "FullName":     fullName,
                            "Password":     "",
                            "Image":        Image]
           
           var Spinner :UIView!
           Spinner = UIViewController.displaySpinner(onView: vc.view)
           
           AF.request(url, method: .post, parameters: params, encoding: URLEncoding.default, headers: header)
               .validate(statusCode: 200..<600)
               .responseJSON { response in
                   
                   UIViewController.removeSpinner(spinner: Spinner)
                   print("\n\n\n ResetPassword response:>>>>> \(response)\n")
                   
                   switch response.result
                   {
                       
                   case .failure(let error):
                       //vc.dismiss(animated: true, completion: nil)
                      if (response.response?.statusCode == 401) {
                            print("force user to login again --> 401")
                            completion(error,true, false)
                       } else {
                            print("Server Not response___________>\(error)")
                            completion(error,false, false)
                       }

                       
                   case .success(let value):
                       //vc.dismiss(animated: true, completion: nil)
                       
                       let json    = JSON(value)
                       let Token = json["Token"].string ?? ""
    
                       if( Token != ""){
                           
                            helper.saveUserToken(Token: Token)
                            completion(nil,false, true)
                           
                       } else {
                           
                           completion(nil,false, false)
                       }
                   }
           }
        } // End of UpdateProfile
    //-----------------------------------------------------------------------------------------------------
    
    
}//------ End Of Class
