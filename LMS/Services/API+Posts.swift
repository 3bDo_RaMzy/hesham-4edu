//
//  API+Posts.swift
//  LMS
//
//  Created by Abdo on 1/25/20.
//  Copyright © 2020 eaglez-group. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON


extension API{
    
    
    class func getPostsList(vc: BaseVC, Token: String, PostType: String, PostCustom: Bool, completion: @escaping ( Error?, [Post]? )->Void){

                    let url = URLs.getPosts + "\(PostType)" + "/\(PostCustom)"

                    let HeaderParam : HTTPHeaders = [ "Authorization":  Token]

                    //vc.showLoading()
                    AF.request(url, method: .get, parameters: nil, encoding: URLEncoding.default, headers: HeaderParam)
                               .validate(statusCode: 200..<600)
                               .responseJSON{ response in

                                print("Server Get getPostsList ___________>\(response.value)")
                                   switch response.result
                                   {
                                   case .failure(let error):
                                       //vc.hideLoading()
                                       completion(error, nil)
                                       print("Server Not response___________>\(error)")

                                   case .success(let value):

                                        let json = JSON(value)

                                        print("\n\n JSON ___________>\(json)")
                                        guard let jsonArr = json[].array else{completion(nil,nil)
                                                  return
                                             }
                                        var PostArr    = [Post]()    // to appended and return
                                        
                                       for data in jsonArr {

                                        PostArr.append(Post(Id:           data["Id"].int ?? 0,
                                                            Post:         data["Post"].string ?? "",
                                                            PostDate:     data["PostDate"].string ?? "",
                                                            UserName:     data["UserName"].string ?? "",
                                                            UserImage:    data["UserImage"].string ?? "",
                                                            UserId:       data["UserId"].int ?? 0,
                                                            CommentCount: data["CommentCount"].int ?? 0))
                                        
                                        }

                                       completion(nil, PostArr)

                } //End of switch case
            }//End of alamofire response
        }
    
    //-------------------------------------------------------------------------------------------------------------------------
    class func getCommentsList(vc: BaseVC, Token: String, PostId: Int, completion: @escaping ( Error?, [CommentPost]? )->Void){

                    let url = URLs.getComments + "\(PostId)" + "/Comments"

                    let HeaderParam : HTTPHeaders = [ "Authorization":  Token]

                    //vc.showLoading()
                    AF.request(url, method: .get, parameters: nil, encoding: URLEncoding.default, headers: HeaderParam)
                               .validate(statusCode: 200..<600)
                               .responseJSON{ response in

                                print("Server Get getCommentsList ___________>\(response.value)")
                                   switch response.result
                                   {
                                   case .failure(let error):
                                       //vc.hideLoading()
                                       completion(error, nil)
                                       print("Server Not response___________>\(error)")

                                   case .success(let value):

                                        let json = JSON(value)

                                        print("\n\n JSON ___________>\(json)")
                                        guard let jsonArr = json[].array else{completion(nil,nil)
                                                  return
                                             }
                                        var CommentArr    = [CommentPost]()    // to appended and return
                                        
                                       for data in jsonArr {

                                        CommentArr.append(CommentPost(CommentId:   data["CommentId"].int ?? 0,
                                                                      Comment:     data["Comment"].string ?? "",
                                                                      CommentDate: data["CommentDate"].string ?? "",
                                                                      UserName:    data["UserName"].string ?? "",
                                                                      UserImage:   data["UserImage"].string ?? "",
                                                                      UserId:      data["UserId"].int ?? 0,
                                                                      ReplayCount: data["ReplayCount"].int ?? 0))
                                        }

                                       completion(nil, CommentArr)

                } //End of switch case
            }//End of alamofire response
        }
    
    //-------------------------------------------------------------------------------------------------------------------------
    class func getReplaysList(vc: BaseVC, Token: String, PostId: Int, CommentId: Int, completion: @escaping ( Error?, [ReplayCommentPost]? )->Void){

                    let url = URLs.getComments + "\(PostId)" + "/Comments/" + "\(CommentId)"

                    let HeaderParam : HTTPHeaders = [ "Authorization":  Token]

                    //vc.showLoading()
                    AF.request(url, method: .get, parameters: nil, encoding: URLEncoding.default, headers: HeaderParam)
                               .validate(statusCode: 200..<600)
                               .responseJSON{ response in

                                print("Server Get getReplaysList ___________>\(response.value)")
                                
                                switch response.result
                                   {
                                   case .failure(let error):
                                       //vc.hideLoading()
                                       completion(error, nil)
                                       print("Server Not response___________>\(error)")

                                   case .success(let value):

                                        let json = JSON(value)

                                        print("\n\n JSON ___________>\(json)")
                                        guard let jsonArr = json[].array else{completion(nil,nil)
                                                  return
                                             }
                                        var ReplayArr    = [ReplayCommentPost]()    // to appended and return
                                        
                                       for data in jsonArr {

                                        ReplayArr.append(ReplayCommentPost(ReplayId:   data["ReplayId"].int ?? 0,
                                                                           Replay:     data["Replay"].string ?? "",
                                                                           ReplayDate: data["ReplayDate"].string ?? "",
                                                                           UserName:   data["UserName"].string ?? "",
                                                                           UserImage:  data["UserImage"].string ?? "",
                                                                           UserId:     data["UserId"].int ?? 0))
                                        
                                        }

                                       completion(nil, ReplayArr)

                } //End of switch case
            }//End of alamofire response
        }
    
    //--------------------------------------------------------------------------------------------------------------
       class func sendPostComment( vc: BaseVC, Token: String, PostId: Int, Commentt: String,
                                   completion: @escaping (_ error: Error?, _ Status: Bool)->Void) {
           
                   
                   var Spinner :UIView!
                   let url = URLs.sendPostComment
                          
                   let HeaderParam : HTTPHeaders = [ "Authorization":  Token]
                   let params       = [ "PostId":   PostId,
                                        "Comment":  Commentt] as [String : Any]

                   Spinner = UIViewController.displaySpinner(onView: vc.view)
                       
                   AF.request(url, method: .post, parameters: params, encoding: URLEncoding.default, headers: HeaderParam)
                                       .validate(contentType: ["application/json"])
                                       .validate(statusCode: 200..<600)
                                       .responseJSON{ response in
           
                           UIViewController.removeSpinner(spinner: Spinner)
                           print("\n\n\n sendStudentQuiz  response:>>>>>>>>>>>>>>>>> \(response)\n")
                   
                   switch response.result {
                       
                   case .failure(let error):
                       //vc.dismiss(animated: true, completion: nil)
                       completion(error, false)
                   //                    print(error)
                   case .success(let value):
                       //vc.dismiss(animated: true, completion: nil)
                       let json = JSON(value)
                       print("\n\n\nLogin_success:>>>>>>>>>>>>>>>>> \(json)\n")
                       
                       if  json.string == "Added successfully" {
                               completion(nil, true)
                       } else {
                           
                           completion(nil, false)
                       }
                       
                   }
           }
       } // End of sendRequest
    
    //--------------------------------------------------------------------------------------------------------------
       class func sendCommentReplay( vc: BaseVC, Token: String, CommentId: Int, Replay: String,
                                   completion: @escaping (_ error: Error?, _ Status: Bool)->Void) {
           
                   
                   var Spinner :UIView!
                   let url = URLs.sendPostCommentReplay

                   let HeaderParam : HTTPHeaders = [ "Authorization":  Token]
                   let params       = [ "CommentId": CommentId,
                                        "Replay":    Replay] as [String : Any]

                   Spinner = UIViewController.displaySpinner(onView: vc.view)
                       
                   AF.request(url, method: .post, parameters: params, encoding: URLEncoding.default, headers: HeaderParam)
                                       .validate(contentType: ["application/json"])
                                       .validate(statusCode: 200..<600)
                                       .responseJSON{ response in
           
                           UIViewController.removeSpinner(spinner: Spinner)
                           print("\n\n\n sendStudentQuiz  response:>>>>>>>>>>>>>>>>> \(response)\n")
                   
                   switch response.result {
                       
                   case .failure(let error):
                       //vc.dismiss(animated: true, completion: nil)
                       completion(error, false)
                   //                    print(error)
                   case .success(let value):
                       //vc.dismiss(animated: true, completion: nil)
                       let json = JSON(value)
                       print("\n\n\nLogin_success:>>>>>>>>>>>>>>>>> \(json)\n")
                       
                       if  json.string == "Added successfully" {
                               completion(nil, true)
                       } else {
                           
                           completion(nil, false)
                       }
                       
                   }
           }
       } // End of sendRequest
    
    
//--------------------------------------------------------------------------------------------------------------
       class func sendNewPost( vc: BaseVC, Token: String, Post: String, OnGroup: Bool, OnGrade: Bool,
                               completion: @escaping (_ error: Error?, _ Status: Bool)->Void ) {
           
                   var Spinner :UIView!
                   let url = URLs.sendNewPost

                   let HeaderParam : HTTPHeaders = [ "Authorization":  Token]
                   let params       = [ "Post":     Post,
                                        "OnGroup":  OnGroup,
                                        "OnGrade":  OnGrade] as [String : Any]

                   Spinner = UIViewController.displaySpinner(onView: vc.view)
                   AF.request(url, method: .post, parameters: params, encoding: URLEncoding.default, headers: HeaderParam)
                                       .validate(contentType: ["application/json"])
                                       .validate(statusCode: 200..<600)
                                       .responseJSON{ response in
           
                           UIViewController.removeSpinner(spinner: Spinner)
                           print("\n\n\n sendNewPost  response:>>>>>>>>>>>>>>>>> \(response)\n")
                   
                   switch response.result {
                       
                   case .failure(let error):
                       //vc.dismiss(animated: true, completion: nil)
                       completion(error, false)
                   //                    print(error)
                   case .success(let value):
                       //vc.dismiss(animated: true, completion: nil)
                       let json = JSON(value)
                       print("\n\n\nLogin_success:>>>>>>>>>>>>>>>>> \(json)\n")
                       
                       if  json.string == "Added successfully" {
                               completion(nil, true)
                       } else {
                               completion(nil, false)
                       }
                       
                   }
           }
       } // End of sendRequest
    
    
}
