//
//  API+Main.swift
//  LMS
//
//  Created by Abdo on 12/5/19.
//  Copyright © 2019 eaglez-group. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON


extension API{
    
    
    //-------------------------------------------------------------------------------------------------------------
    class func getQuizList(vc: BaseVC, Token: String,
                           completion: @escaping ( Error?,_ TimeOut: Bool?, [Quiz]? )->Void){

        
        let url = URLs.getQuizListURL

        let HeaderParam : HTTPHeaders = [ "Authorization":  Token]

        //vc.showLoading()
        AF.request(url, method: .get, parameters: nil, encoding: URLEncoding.default, headers: HeaderParam)
                   .validate(statusCode: 200..<600)
                   .responseJSON{ response in

                    print("Server Get getQuizList ___________>\(response.value)")
                       switch response.result
                       {
                       case .failure(let error):
                           //vc.hideLoading()
                           if (response.response?.statusCode == 401) {
                               print("force user to login again --> 401")
                               completion(error,true, nil)
                           } else {
                               print("Server Not response___________>\(error)")
                               completion(error,false, nil)
                           }

                       case .success(let value):

                            let json = JSON(value)

                            print("\n\n JSON ___________>\(json)")
                            guard let jsonArr = json[].array else{completion(nil,false,nil)
                                      return
                                 }
                            var QuizArr     = [Quiz]()    // to appended and return
                            var QuestionArr = [Question]()// to appended and return
                            var AnswerArr   = [Answer]()  // to appended and return

                           for data in jsonArr {

                                        guard let questionArr = data["questions"].array else{completion(nil,false,nil)
                                             return
                                        }

                                       for question in questionArr {

                                           guard let answerArr = question["answers"].array else{completion(nil,false,nil)
                                                 return
                                           }

                                                   for answer in answerArr{

                                                    AnswerArr.append(Answer(id:    answer["id"].int ?? 0,
                                                                            title: answer["title"].string ?? ""))
                                                   }
                                                    QuestionArr.append(Question(id:      question["id"].int       ?? 0,
                                                                                title:   question["title"].string ?? "",
                                                                                type:    question["type"].string  ?? "",
                                                                                imgs:    question["imgs"].arrayObject as! [String] ,
                                                                                answers: AnswerArr))
                                                    AnswerArr.removeAll()
                                        }


                            QuizArr.append(Quiz(id:        data["id"].int      ?? 0,
                                                title:     data["title"].string ?? "",
                                                duration:  data["duration"].int    ?? 0,
                                                QuestionCount:  data["QuestionCount"].int    ?? 0,
                                                questions: QuestionArr))
                            QuestionArr.removeAll()
                            }

                           completion(nil,false, QuizArr)

                       } //End of switch case
               }//End of alamofire response
        
        
        
}//---> End Of Method getQuizList
//-------------------------------------------------------------------------------------------------
 
    
    //-------------------------------------------------------------------------------------------------------------
    class func sendStudentQuizResult(vc: BaseVC, Token: String, from: String, to: String,
                                     completion: @escaping ( Error?,_ TimeOut: Bool?, [QuizResult]? )->Void){
        
        var Spinner :UIView!
        let url = URLs.getStudentResult

        //vc.showLoading()
        let HeaderParam : HTTPHeaders = [ "Authorization":  Token]
        let params       = [ "To":   to,
                             "From": from ] as [String : Any]
        
        Spinner = UIViewController.displaySpinner(onView: vc.view)
        AF.request(url, method: .post, parameters: params, encoding: URLEncoding.default, headers: HeaderParam)
                   .validate(statusCode: 200..<600)
                   .responseJSON{ response in

                    UIViewController.removeSpinner(spinner: Spinner)
                    print("\n\n\n sendStudentQuizResult  response:>>>>>>>>>>>>>>>>> \(response)\n")
                    
                       switch response.result
                       {
                       case .failure(let error):
                           //vc.hideLoading()
                           if (response.response?.statusCode == 401) {
                               print("force user to login again --> 401")
                               completion(error,true, nil)
                           } else {
                               print("Server Not response___________>\(error)")
                               completion(error,false, nil)
                           }
                           
                       case .success(let value):
                        
                        let json = JSON(value)
                        
                                          guard let jsonArr = json[].array else{completion(nil, false, nil)
                                              return
                                          }
                                          var QuizResultArr = [QuizResult]()// to appended and return

                                              for data in jsonArr {
                                                
                                                if data["Status"].string != "Pending"{
                                                
                                                QuizResultArr.append(QuizResult(studentName: data["studentName"].string   ?? "",
                                                                                quizTitle: data["quizTitle"].string   ?? "",
                                                                                date: data["date"].string   ?? "",
                                                                                degree: data["degree"].double   ?? 0.0,
                                                                                maxdegree: data["maxdegree"].double   ?? 0.0,
                                                                                QuizeId: data["QuizeId"].int ?? 0,
                                                                                Status: data["Status"].string   ?? ""))
                                                }
                                              }
                                                                                                                              
                                          completion(nil,false, QuizResultArr)
                        
                       } //End of switch case
               }//End of alamofire response
    }//---> End Of Method sendStudentQuizResult
  
   
    
    //-------------------------------------------------------------------------------------------------------------
    class func getQuizCorrection(vc: BaseVC, Token: String, Quiz_Id: Int,
                                 completion: @escaping ( Error?,_ TimeOut: Bool?, QuizCorrection? )->Void){
        
          
        var Spinner :UIView!
        let url = URLs.getQuizCorrection + "\(Quiz_Id)/ShowCorrectionAnswer"

        //vc.showLoading()
        let HeaderParam : HTTPHeaders = [ "Authorization":  Token]

        Spinner = UIViewController.displaySpinner(onView: vc.view)
        AF.request(url, method: .get, parameters: nil, encoding: URLEncoding.default, headers: HeaderParam)
                   .validate(statusCode: 200..<600)
                   .responseJSON{ response in

                    UIViewController.removeSpinner(spinner: Spinner)
                    print("\n\n\n sendStudentQuizResult  response:>>>>>>>>>>>>>>>>> \(response)\n")

                       switch response.result
                       {
                       case .failure(let error):
                           //vc.hideLoading()
                           
                        if (response.response?.statusCode == 401) {
                            print("force user to login again --> 401")
                            completion(error,true, nil)
                        } else {
                            print("Server Not response___________>\(error)")
                            completion(error,false, nil)
                        }


                       case .success(let value):


                        let json          = JSON(value)
                        var responseModel : QuizCorrection =  QuizCorrection()
                        var AnswerArr     = [AnswerList]()    // to appended and return
                        var QuizImageArr  = [String]()        // to appended and return
                        var QuizResultArr = [CorrectionList]()// to appended and return

                        let jsonQuizTitle            = json["Result"]["Title"].string                 ?? ""
                        let jsonQuizDuration         = json["Result"]["Duration"].double              ?? 0.0
                        let jsonTotalDegree          = json["Result"]["TotalDegree"].double           ?? 0.0
                        let jsonMultipleChoiceNumber = json["Result"]["MultipleChoiceNumber"].int     ?? 0
                        let jsonTrueOrFalseNumber    = json["Result"]["TrueOrFalseNumber"].int        ?? 0
                        let jsonArticleNumber        = json["Result"]["ArticleNumber"].int            ?? 0
                        let jsonMultipleChoiceDegree = json["Result"]["MultipleChoiceDegree"].double  ?? 0.0
                        let jsonTrueOrFalseDegree    = json["Result"]["TrueOrFalseDegree"].double     ?? 0.0
                        let jsonArticleDegree        = json["Result"]["ArticleDegree"].double         ?? 0.0


                        guard let jsonArr = json["Result"]["QuestionList"].array else{completion(nil,false,nil)
                                return
                        }

                        for data in jsonArr {

                                        guard let answerArr = data["AllAnswer"].array else{completion(nil,false,nil)
                                              return
                                        }
                                        for answer in answerArr{

                                             AnswerArr.append(AnswerList(Value:          answer["Value"].string ?? "",
                                                                         IsCorectAnswer: answer["IsCorectAnswer"].bool ?? false))
                                         }
                                        //----------------------
                                        guard let answerImageArr = data["Images"].array else{completion(nil,false,nil)
                                              return
                                        }
                                        for img in answerImageArr{

                                            QuizImageArr.append( img.string ?? "" )  // change variable names from postman
                                         }


                            QuizResultArr.append(CorrectionList(StudentName:     data["StudentName"].string   ?? "",
                                                                QuestionType:    data["QuestionType"].int ?? 0,
                                                                MaxDegree:       data["MaxDegree"].double   ?? 0.0,
                                                                StudentDeqree:   data["StudentDeqree"].double   ?? 0.0,
                                                                AnswerValue:     data["AnswerValue"].string   ?? "",
                                                                StudentAnswer:   data["StudentAnswer"].string   ?? "",
                                                                AnwerStatus:     data["AnwerStatus"].bool   ?? false,
                                                                QuestionValue:   data["QuestionValue"].string   ?? "",
                                                                QuestionCorrectionStatus: data["QuestionCorrectionStatus"].string   ?? "",
                                                                AllAnswer:       AnswerArr,
                                                                Images:          QuizImageArr))

                            AnswerArr.removeAll()
                            QuizImageArr.removeAll()

                        }


                        responseModel.Title                = jsonQuizTitle
                        responseModel.Duration             = jsonQuizDuration
                        responseModel.TotalDegree          = jsonTotalDegree
                        responseModel.MultipleChoiceNumber = jsonMultipleChoiceNumber
                        responseModel.TrueOrFalseNumber    = jsonTrueOrFalseNumber
                        responseModel.ArticleNumber        = jsonArticleNumber
                        responseModel.MultipleChoiceDegree = jsonMultipleChoiceDegree
                        responseModel.TrueOrFalseDegree    = jsonTrueOrFalseDegree
                        responseModel.ArticleDegree        = jsonArticleDegree
                        responseModel.QuestionList         = QuizResultArr


                        completion(nil, false, responseModel)

                       } //End of switch case
               }//End of alamofire response
    }//---> End Of Method getQuizCorrection
    
    

}
