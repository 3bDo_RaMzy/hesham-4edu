//
//  SocialNetwork.swift
//  LMS
//
//  Created by Abdo on 11/24/19.
//  Copyright © 2019 eaglez-group. All rights reserved.
//

import Foundation
class SocialNetwork {
    var _InstagramUrl:String!
    var _TwitterUrl:String!
    var _FaceBookUrl:String!
    var _GoogleUrl:String!
    init(social: Dictionary<String, AnyObject>) {
        if let InstagramUrl = social["InstagramUrl"] as? String {
            self._InstagramUrl = InstagramUrl
        }
        if let TwitterUrl = social["TwitterUrl"] as? String {
            self._TwitterUrl = TwitterUrl
        }
        if let FaceBookUrl = social["FaceBookUrl"] as? String {
            self._FaceBookUrl = FaceBookUrl
        }
        if let GoogleUrl = social["GoogleUrl"] as? String {
            self._GoogleUrl = GoogleUrl
        }
    }
}
