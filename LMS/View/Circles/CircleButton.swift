//
//  CircleButton.swift
//  LMS
//
//  Created by Abdo on 11/27/19.
//  Copyright © 2019 eaglez-group. All rights reserved.
//

import UIKit

class CircleButton: UIButton {
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        setTitle("MyTitle", for: .normal)
        setTitleColor(UIColor.blue, for: .normal)
        //        self.layer.cornerRadius = frame.size.height/2
    }
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
//    @IBInspectable
//    override public var cornerRadius: CGFloat = 2.0 {
//        didSet {
//            self.layer.cornerRadius = self.frame.size.height/2// self.cornerRadius
//        }
//    }
    
}
//--------------------------------------------------------------------------------
class Roundedview: UIView {
    
    override func layoutSubviews() {
        layer.cornerRadius = 10
        layer.borderWidth  = 2
        //layer.borderColor  = UIColor.black.withAlphaComponent(0.5).cgColor
        layer.borderColor  = UIColor(hexString: "D1D3D4").cgColor
        clipsToBounds = true
    }
    
}

class RoundedviewColored: UIView {
    
    override func layoutSubviews() {
        layer.cornerRadius = 10
        layer.borderWidth  = 2
        //layer.borderColor  = UIColor.black.withAlphaComponent(0.5).cgColor
        layer.borderColor  = UIColor(hexString: "65C3CF").cgColor
        clipsToBounds = true
    }
    
}

//------------------------



