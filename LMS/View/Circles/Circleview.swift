//
//  Circleview.swift
//  LMS
//
//  Created by Abdo on 11/27/19.
//  Copyright © 2019 eaglez-group. All rights reserved.
//

import UIKit

class Circleview: UIImageView {
    
    override func layoutSubviews() {
        layer.cornerRadius = self.frame.size.height/2
        clipsToBounds = true
    }
    
}
//----------------------------------------------------
//----------------------------------------------------
class txtAreaStyle : UITextView {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.layer.borderWidth  = 2
        self.layer.borderColor  = UIColor.black.withAlphaComponent(0.5).cgColor
    }
    
    //-------------------------------------
    //--> For Any change in the view of any screen layout !
    //--> it will calculate size for it.
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.layer.cornerRadius = 10
        
    }
}

