//
//  roundButton.swift
//  LMS
//
//  Created by Abdo on 11/27/19.
//  Copyright © 2019 eaglez-group. All rights reserved.
//

import UIKit

class roundButton: UIButton {
    
    required init() {
        super.init(frame: .zero)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        layer.cornerRadius = self.frame.height / 2
        clipsToBounds = true
    }
}
