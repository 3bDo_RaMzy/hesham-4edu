//
//  CircleImage.swift
//  LMS
//
//  Created by MacBook on 6/28/20.
//  Copyright © 2020 eaglez-group. All rights reserved.
//

import Foundation
import UIKit

class CircleImage: UIImageView {
  
    override init(frame: CGRect) {
        super.init(frame: frame)

        clipsToBounds = true
         
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        clipsToBounds = true
    }

    override func layoutSubviews() {
        super.layoutSubviews()

         layer.borderWidth   = 1
         layer.masksToBounds = false
         layer.borderColor   = UIColor.gray.cgColor
         layer.cornerRadius  = self.frame.height/2 //This will change with corners of image and height/2 will make this circle shape
         clipsToBounds = true
    }
}
