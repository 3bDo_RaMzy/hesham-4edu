//
//  BaseVC.swift
//  LMS
//
//  Created by Abdo on 11/20/19.
//  Copyright © 2019 eaglez-group. All rights reserved.
//
//--------------------------------------------------------------------
/*       let success =      MessageView.viewFromNib(layout: .cardView)
                     success.configureTheme(.success)
                     success.configureDropShadow()
                     success.configureContent(title: "Success", body: "Creating Your Gif !")
                     success.button?.isHidden = false
                     success.button?.titleLabel?.text = "Gallery"
                     success.buttonTapHandler = { (sender) -> Void in

       var successConfig = SwiftMessages.defaultConfig
                     successConfig.presentationStyle = .center
                     successConfig.presentationContext = .window(windowLevel: UIWindow.Level.normal)
       SwiftMessages.show(config: successConfig, view: success)
*/

import UIKit
import SVProgressHUD

class BaseVC: UIViewController {
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    
    func showLoading () {
        SVProgressHUD.setDefaultMaskType(.clear)
        SVProgressHUD.show()
    }
    
    func hideLoading () {
        SVProgressHUD.dismiss()
    }
    
    func ShowAlert( Msg: String ){
           
           let alert = UIAlertController(title: "Attention !", message: "\(Msg)", preferredStyle: .alert)
               alert.addAction(UIAlertAction(title: "Ok", style: .destructive, handler: nil))
           self.present(alert, animated: true)
        }
    
    func alertmessage (Message : String) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "check", message: Message, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "ok", style: .default, handler: { action in
                switch action.style{
                case .default:
                    break
                //                                print("default")
                case .cancel: break
                //                                print("cancel")
                case .destructive: break
                    //                                print("destructive")
                }}))
            self.present(alert, animated: true, completion: nil)
            
        }
    }
    
    func showToast(message : String, vc: BaseVC) {

        let toastLabel = UILabel(frame: CGRect(x: vc.view.frame.size.width/2 - 150, y: vc.view.frame.size.height-150, width: 300, height: 35))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center;
        toastLabel.font = UIFont(name: "Montserrat-Light", size: 12.0)
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        vc.view.addSubview(toastLabel)
        UIView.animate(withDuration: 5.0, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
    
    //----------------------------------------------------------------
    //---- For Documents, attachments, photos
    
    func validAttachmentSize(data : Data) -> Bool {
        let imageSize: Int = data.count
        let kb = Double(imageSize) / 1000.0
        
        if(kb > 2048) {
            return false
        }
        else {
            return true
        }
    }
    
    func attachmentSupportedFile (url : String) -> Bool {
          if(url.lowercased().contains(".png") || url.lowercased().contains(".jpg") ||
              url.lowercased().contains(".jpeg") || url.lowercased().contains(".pdf") || url.lowercased().contains(".doc") ||
              url.lowercased().contains(".txt") ||
              url.lowercased().contains(".docx") ||
              url.lowercased().contains(".xls") ||
              url.lowercased().contains(".xlsx")) {
              return true
          }
          return false
      }
    
    func isImage (url : String) -> Bool {
        if(url.lowercased().contains(".png") ||
            url.lowercased().contains(".jpg") ||
            url.lowercased().contains(".jpeg")) {
            return true
        }
        return false
    }
    
    func getExtention(url : String) -> String {
        let array = url.split(separator: ".")
        if(array.count > 0) {
            return String(array[array.count - 1])
        }
        else {
            return "jpeg"
        }
    }
}

