//
//  DesignableTextField.swift
//  LMS
//
//  Created by Abdo on 11/24/19.
//  Copyright © 2019 eaglez-group. All rights reserved.
//


import UIKit
@IBDesignable
class DesignableTextField: UITextField {

    @IBInspectable var leftImage: UIImage? {
        didSet {
            updateView()
        }
    }
    @IBInspectable var cornerRaduis: CGFloat = 0{
        didSet {
            updateView()
        }
    }
    @IBInspectable var leftPadding: CGFloat = 0{
        didSet {
            updateView()
        }
    }
    @IBInspectable var y: UIColor? {
        didSet {
            layer.borderColor = y?.cgColor
        }
    }
    @IBInspectable var x: CGFloat = 0 {
        didSet {
            layer.borderWidth = x
        }
    }
    func updateView(){
        if let image = leftImage{
            leftViewMode  = .always
            let imageView = UIImageView(frame: CGRect(x: leftPadding, y: 0, width: 16, height: 16))
            imageView.image = image
            imageView.tintColor = tintColor
            var width = leftPadding + 20
            if borderStyle == UITextField.BorderStyle.none || borderStyle == UITextField.BorderStyle.line{
                width = width + 5
            }
            let view = UIView(frame: CGRect(x: 0, y: 0, width: width, height: 16))
            view.addSubview(imageView)
            self.leftView = view
            
        }else{
            leftViewMode = .never
        }
    }

}
