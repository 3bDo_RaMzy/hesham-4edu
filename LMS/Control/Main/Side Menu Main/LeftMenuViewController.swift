//
//  LeftMenuViewController.swift
//  LMS
//
//  Created by Abdo on 11/19/19.
//  Copyright © 2019 eaglez-group. All rights reserved.
//

import UIKit

public class LeftMenuViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var tableView: UITableView?
    var window: UIWindow?

    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override public func viewDidLoad() {
        super.viewDidLoad()

        let tableView = UITableView(frame: CGRect(x: 0, y: (self.view.frame.size.height - 54 * 6) / 2.0, width: self.view.frame.size.width, height: 54 * 6), style: .plain)
            tableView.autoresizingMask = [.flexibleTopMargin, .flexibleBottomMargin, .flexibleWidth]
            tableView.delegate = self
            tableView.dataSource = self
            tableView.isOpaque = false
            tableView.backgroundColor = .clear
            tableView.backgroundView = nil
            tableView.separatorStyle = .none
            tableView.bounces = false

        self.tableView = tableView
        self.view.addSubview(self.tableView!)
    }

    // MARK: - <UITableViewDelegate>

    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        switch indexPath.row {
        case 0:
           
            self.sideMenuViewController!.hideMenuViewController()
            let storyboard            = UIStoryboard(name: "Visitor", bundle: nil)
//            self.sideMenuViewController!.hideMenuViewController()
//            self.sideMenuViewController!.setContentViewController(UINavigationController(rootViewController: storyboard.instantiateViewController(withIdentifier: "visitContactUsVC")), animated: true)

            let vc                    = storyboard.instantiateViewController(withIdentifier: "visitContactUsVC") as! visitContactUsVC
//                vc.modalPresentationStyle = .overFullScreen
//                vc.modalTransitionStyle   = .crossDissolve
            //self.present(vc, animated: true, completion: nil)
            self.show(vc, sender: nil)

        case 1:
            
                         self.sideMenuViewController!.hideMenuViewController()
                         let storyboard            = UIStoryboard(name: "Visitor", bundle: nil)
             //            self.sideMenuViewController!.hideMenuViewController()
             //            self.sideMenuViewController!.setContentViewController(UINavigationController(rootViewController: storyboard.instantiateViewController(withIdentifier: "visitContactUsVC")), animated: true)

                         let vc                    = storyboard.instantiateViewController(withIdentifier: "visitAboutUsVC") as! visitAboutUsVC
             //                vc.modalPresentationStyle = .overFullScreen
             //                vc.modalTransitionStyle   = .crossDissolve
                         //self.present(vc, animated: true, completion: nil)
                         self.show(vc, sender: nil)
            
        case 2:
            
            print("\n\n Sign Out Now")
            helper.saveUserToken(Token: "")
            //            let windowScene = UIApplication.shared
            //                                           .connectedScenes
            //                                           .filter { $0.activationState == .foregroundActive }
            //                                           .first
            //guard let windowScene = (scene as? UIWindowScene) else { return }
            //self.window = UIWindow(windowScene: windowScene as! UIWindowScene)
            
            
            self.window =  UIWindow(frame: UIScreen.main.bounds)
            let storyboard = UIStoryboard(name: "Auth", bundle: nil)
            self.sideMenuViewController!.setContentViewController(UINavigationController(rootViewController: storyboard.instantiateViewController(withIdentifier: "loginVC")), animated: true)
            self.sideMenuViewController!.hideMenuViewController()
            //self.window?.rootViewController = rootVC
            self.window?.makeKeyAndVisible()
//            self.sideMenuViewController!.setContentViewController(UINavigationController(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "secondViewController")), animated: true)
//            self.sideMenuViewController!.hideMenuViewController()

        default:
            break
        }
    }

    // MARK: - <UITableViewDataSource>

    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 54
    }

    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    public func tableView(_ tableView: UITableView, numberOfRowsInSection sectionIndex: Int) -> Int {
        return 3
    }

    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier: String = "Cell"

        var cell: UITableViewCell? = tableView.dequeueReusableCell(withIdentifier: cellIdentifier)

        if cell == nil {
            cell = UITableViewCell(style: .default, reuseIdentifier: cellIdentifier)
            cell!.backgroundColor = .clear
            cell!.textLabel?.font = UIFont(name: "HelveticaNeue", size: 21)
            cell!.textLabel?.textColor = .white
            cell!.textLabel?.highlightedTextColor = .lightGray
            cell!.selectedBackgroundView = UIView()
        }

        let titles = ["Contact Us", "About Us", "Sign Out"]
        let images = ["support", "info", "IconSettings"]
        cell!.textLabel?.text = titles[indexPath.row]
        cell!.imageView?.image = UIImage(named: images[indexPath.row])

        return cell!
    }
}

