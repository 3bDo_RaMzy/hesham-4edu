//
//  BlogVC.swift
//  LMS
//
//  Created by Abdo on 11/20/19.
//  Copyright © 2019 eaglez-group. All rights reserved.
//

import UIKit

class BlogVC: BaseVC, UITableViewDelegate, UITableViewDataSource{
    

                @IBOutlet weak var BlogTable: UITableView!
                var blogViewModel = [BlogViewModel]()
                private let refreshControl = UIRefreshControl()
                
                override func viewDidLoad() {
                    super.viewDidLoad()
                    // Do any additional setup after loading the view.
                    self.title = "News"
                                
                    self.BlogTable.dataSource     = self
                    self.BlogTable.delegate       = self
                    self.BlogTable.layoutMargins  = UIEdgeInsets.zero
                    self.BlogTable.separatorInset = UIEdgeInsets.zero
                    self.BlogTable.rowHeight      = UITableView.automaticDimension
                    self.BlogTable.estimatedRowHeight = 200
                     
                    self.fetchData()
                    
                    
                    
                    // set up the refresh control
                    if #available(iOS 10.0, *) {
                         BlogTable.refreshControl = refreshControl
                     } else {
                         BlogTable.addSubview(refreshControl)
                     }
                    refreshControl.tintColor = UIColor(red:0.25, green:0.72, blue:0.85, alpha:1.0)
                    refreshControl.attributedTitle = NSAttributedString(string: "Refresh Data...", attributes: nil)
                    refreshControl.addTarget(self, action: #selector(refreshData(_:)), for: .valueChanged)
                    
                    //self.tabBarController?.tabBar.isHidden = false
                }//------------ End Of ViewDidLoad ---------------------
                    
                @objc private func refreshData(_ sender: Any) {
                    // Fetch Weather Data
                    self.refreshData()
                }
                
//                func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
//
//                    return UITableView.automaticDimension
//                }
    
                 func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

                        return blogViewModel.count
                    }
                
                 func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
                    
                        let cell = tableView.dequeueReusableCell(withIdentifier: "BlogCell", for: indexPath) as! BlogCell
                    cell.blogViewModel = blogViewModel[indexPath.row]
                     return cell
                    }
                
                
                func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
                          
//                    // Do something now
//                    let storyboard            = UIStoryboard(name: "Visitor", bundle: nil)
//                    let vc                    = storyboard.instantiateViewController(withIdentifier: "visitVideoPalyerVC") as! visitVideoPalyerVC
//                        vc.modalPresentationStyle = .overFullScreen
//                        vc.modalTransitionStyle   = .crossDissolve
//                        vc.VideoID  = self.videoViewModel[indexPath.row].videoId
//                    self.present(vc, animated: true, completion: nil)
//                    //self.show(vc, sender: nil)
            }
    
                    //------------------------------
                    fileprivate func refreshData() {
                           
                           self.refreshControl.beginRefreshing()
                           API.getBlogs(vc: self ) { ( err, blogs ) in
                               
                            self.refreshControl.endRefreshing()
                               if let err = err {
                                       print("getBlogs Failed to fetch blogs:", err)
                                       return
                               }
                               
                               self.blogViewModel = blogs?.map({return BlogViewModel(blog: $0)}) ?? []
                                if self.blogViewModel.count <= 0 {
                                    self.showToast(message: "Special News For You Soon !!", vc: self)
                                }
                               self.BlogTable.reloadData()
                           }
                           
                       }//---> End Of fetchData method -------
    
                    //------------------------------
                    fileprivate func fetchData() {
                        
                        API.getBlogs(vc: self ) { ( err, blogs ) in
                            
                            if let err = err {
                                    print("getBlogs Failed to fetch blogs:", err)
                                    return
                            }
                            
                            self.blogViewModel = blogs?.map({return BlogViewModel(blog: $0)}) ?? []
                            if self.blogViewModel.count <= 0 {
                                self.showToast(message: "Special News For You Soon !!", vc: self)
                            }
                            self.BlogTable.reloadData()
                        }
                        
                    }//---> End Of fetchData method -------

        
        
    }//---> End Of Class -----------------
