//
//  SettingsLanguageVC.swift
//  LMS
//
//  Created by MacBook on 6/28/20.
//  Copyright © 2020 eaglez-group. All rights reserved.
//

import UIKit
//import MOLH
//import SwiftEventBus



class SettingsLanguageVC: BaseVC {

    @IBOutlet weak var PannerHieght: NSLayoutConstraint!
    
    //MARK:- Properties
    @IBOutlet weak var viewLanguageEnglish: CardView!
    @IBOutlet weak var viewLanguageArabic:  CardView!
    @IBOutlet weak var lblEnglishText:      UILabel!
    @IBOutlet weak var lblArabicText:       UILabel!
    @IBOutlet weak var imgEnglishImage:     UIImageView!
    @IBOutlet weak var imgArabicImage:      UIImageView!
    @IBOutlet weak var btnLanguageEnglish:  UIButton!
    @IBOutlet weak var btnLanguageArabic:   UIButton!
    
    var SelectedLanguageValue = "en"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.title = "App Language"
        
        if UIDevice().userInterfaceIdiom == .phone {
                switch UIScreen.main.nativeBounds.height {
                    case 1136, 1334, 1920, 2208:
                        print("iPhone 5 or 5S or 5C | 6/6S/7/8 | 6+/6S+/7+/8+ ")
                        self.PannerHieght.constant = 60
                    case 2436, 2688, 1792:
                        print("iPhone X/XS/11 Pro  |  XS Max/11 Pro Max  |  XR/ 11")
                        self.PannerHieght.constant = 80
                    default:
                        self.PannerHieght.constant = 80
                }
        }
        
        if helper.getAppLang() == "en" {
            selectEn()
        } else if helper.getAppLang() == "ar" {
            selectAr()
        }
        
    }
    


    @IBAction func btnMainMoneyOptionAction(_ sender: UIButton) {
        
        if sender == btnLanguageEnglish {
            selectEn()
        } else if sender == btnLanguageArabic {
            selectAr()
        }
        
    }
    
    func selectAr(){
        self.SelectedLanguageValue = "ar"
        viewLanguageArabic.backgroundColor = UIColor(hexString: "DAD448")
        viewLanguageEnglish.backgroundColor = .white
        lblArabicText.textColor = .white
        lblEnglishText.textColor = .black
        imgArabicImage.image = UIImage(named: "write")
        imgEnglishImage.image = UIImage(named: "wrong")
    }
    
    func selectEn(){
        self.SelectedLanguageValue = "en"
        viewLanguageEnglish.backgroundColor = UIColor(hexString: "DAD448")
        viewLanguageArabic.backgroundColor = .white
        lblEnglishText.textColor = .white
        lblArabicText.textColor = .black
        imgEnglishImage.image = UIImage(named: "write")
        imgArabicImage.image = UIImage(named: "wrong")
    }
    
    @IBAction func ChangeLanguageAction(_ sender: UIButton) {
      
        
//        if Reachable.isConnectedToNetwork() {
//
//            API.UpdateLanguage(vc: self, language: self.SelectedLanguageValue)
//                { (error, result) in
//
//                        if let err = error {
//                              print("Failed to UpdatePhoneNumber:", err)
//                              return
//                        }
//
//
//                        if result == "done"{
//
//                            // print("\n\n\n Successful .. Updating Language ")
//                            // create the alert
//                            let alert = UIAlertController(title: "", message: self.getString(title: "Successful .. Updating Language"), preferredStyle: UIAlertController.Style.alert)
//                                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { action in
//
//                                // do something like...
//                                 self.RestartApp(Lang: self.SelectedLanguageValue)
//
//                            }))
//                            self.present(alert, animated: true, completion: nil)
//
//
//                        } else if result == "failed" {
//
//                            self.alertmessage(Message: self.getString(title: " Failed .. Updating Language"))
//                //            print("\n\n\n Failed .. Updating Phone Number ")
//                //            self.navigationController?.popViewController(animated: true)
//
//                        }
//
//                 }
//
//        } else {
//
//            let storyboard  = UIStoryboard(name: "Main", bundle: nil)
//            let noInternet   = storyboard.instantiateViewController(withIdentifier: "NoInternetVC") as! NoInternetVC
//                noInternet.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
//                noInternet.modalTransitionStyle   = .crossDissolve
//            self.present(noInternet, animated: true)
//        }
    }
    
    func RestartApp(Lang:String) {
        // save lang into shared
        helper.saveAppLang(Lang: Lang)
        //MOLH.setLanguageTo(Lang)
        
//        UIApplication.shared.keyWindow?.rootViewController?.dismiss(animated: true, completion: nil)
//        self.view.window!.rootViewController?.dismiss(animated: false, completion: nil)


//        let storyboard  = UIStoryboard(name: "Main", bundle: nil)
//        let noInternet  = storyboard.instantiateViewController(withIdentifier: "Splash") as! SplashVC
//            noInternet.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
//            noInternet.modalTransitionStyle   = .crossDissolve
//
//        self.navigationController?.setViewControllers([noInternet], animated: true)
        
//        MOLH.reset(transition: .transitionCrossDissolve, duration: 0.25)
 
        // restart the app
//        let appDelegate = AppDelegate()
//        appDelegate.reset()
//        SwiftEventBus.post("SplashViewController", sender:"-")
//
//        SwiftEventBus.post("SplashVC", sender: 3)
         
//        OperationQueue.main.addOperation {
//             [weak self] in
//            self?.performSegue(withIdentifier: "backToSplash", sender: self)
//        }
        
        
 
        DispatchQueue.main.async(){
             [weak self] in
            
            UIApplication.shared.keyWindow?.rootViewController?.dismiss(animated: true, completion: nil)
            self?.view.window!.rootViewController?.dismiss(animated: false, completion: nil)
            
            self?.navigationController?.dismiss(animated: true, completion: nil)
            self?.performSegue(withIdentifier: "backToSplash", sender: self)
        }
        
            
         
    }
    
    
    @IBAction func goBackAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }

}
