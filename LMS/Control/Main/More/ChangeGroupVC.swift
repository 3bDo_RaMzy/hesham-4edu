//
//  ChangeGroupVC.swift
//  LMS
//
//  Created by MacBook on 7/28/20.
//  Copyright © 2020 eaglez-group. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ChangeGroupVC: BaseVC {

    
    @IBOutlet weak var PannerHieght:    NSLayoutConstraint!
    @IBOutlet weak var LblGroupName:    UILabel!
    @IBOutlet weak var PickerGroupName: UIPickerView!
     
    var Spinner  : UIView!
    var groupes = [Group]()
    
    var CurrentGroupName = ""
    var NextGroupName    = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.title = "Change Study Group"
        
        if UIDevice().userInterfaceIdiom == .phone {
                switch UIScreen.main.nativeBounds.height {
                    case 1136, 1334, 1920, 2208:
                        print("iPhone 5 or 5S or 5C | 6/6S/7/8 | 6+/6S+/7+/8+ ")
                        self.PannerHieght.constant = 60
                    case 2436, 2688, 1792:
                        print("iPhone X/XS/11 Pro  |  XS Max/11 Pro Max  |  XR/ 11")
                        self.PannerHieght.constant = 80
                    default:
                        self.PannerHieght.constant = 80
                }
        }
        hideKeyboardWhenTappedAround()
        
        PickerGroupName.dataSource = self
        PickerGroupName.delegate   = self
         
        self.getGroupes()
        
    }//------> End Of viewDidLoad
    
    
    
    
    @IBAction func SendRequestAction(_ sender: UIButton) {
        //print("Current GroupName: \(CurrentGroupName)  || Next GroupName: \(NextGroupName)")
        
        let mTitle   = "Change study group request"
        let mDetails = "I want to change my study group from \(CurrentGroupName) to be in \(NextGroupName) "
        
        API.sendRequest(vc: self, Token: helper.getUserToken()!, Title: mTitle, Detalis: mDetails )
        { (error,TimeOut, status) in
                        
           if let err = error {
                if TimeOut! {
                    let storyboard = UIStoryboard(name: "Auth", bundle: nil)
                    let vc         = storyboard.instantiateViewController(withIdentifier: "SessionExpireVC")
                        vc.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency
                    self.present(vc, animated: true, completion: nil)
                   // self.navigationController?.pushViewController(vc, animated: true)
                    return
                    
                } else {
                    self.alertmessage(Message: "Failed to send change group request:\n \(err) \n\n Try Again Later.." )
                    return
                }
            } else  {
                
                        //print("Message Sent Successfully")
                        let alert = UIAlertController(title: "Successfully sent", message: "You can follow your request in the Message Box", preferredStyle: UIAlertController.Style.alert)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: {(alert: UIAlertAction!) in

                            self.navigationController?.popViewController(animated: true)
                        }))
                        self.present(alert, animated: true)
                                               
             }
                         
         }
 
    }
    
    
    
    
    
    //------> API Get Groups data
    private func getGroupes(){
        
        Spinner = UIViewController.displaySpinner(onView: self.view)
        let headerParams : HTTPHeaders = ["Lang": "en"]
        
        let mGradeId = helper.getStudentGradeId() ?? 0
        
        AF.request("\(URLs.BaseUrl)api/Groups/GetGroupsByGradeId/\(mGradeId)", method: .get, parameters: nil, encoding: URLEncoding.default, headers: headerParams).responseJSON{ response in
            
            UIViewController.removeSpinner(spinner: self.Spinner)
            let result = response.result
            print(result)
            switch response.result {
            case .success(let value):
                //print(String(data: value as! Data, encoding: .utf8)!)
                if let dict = value as? [Dictionary<String, AnyObject>] {
                    self.groupes.removeAll()
                    
                    for obj in dict {
                        let result = Group(country: obj)
                         
                        if result._GroupId != helper.getStudentGroupId(){
                                    self.groupes.append(result)
                        } else {
                                    //print("Group Id: \(result._GroupName)")
                                    self.LblGroupName.text = result._GroupName
                                    self.CurrentGroupName = result._GroupName
                        }
                    }
                    self.PickerGroupName.reloadAllComponents()
                    if self.groupes.count <= 0 {
                        let alert = UIAlertController(title: "ooops!", message: "Error loading data", preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: .destructive, handler: { (UIAlertAction) in
                                //self.ShowHomeView(ScrollMain: true, ViewGroup: false, ViewBank: false)
                                self.navigationController?.popViewController(animated: true)
                            }))
                        self.present(alert, animated: true)
                    } else {
                        
                        self.NextGroupName = self.groupes[0]._GroupName
                    }
                        
                }
                
            case .failure(let error):
                print(error)
                
            }
            
         }
    }
    
    
    
    
    
    @IBAction func goBackAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
        
        
}//------------------------------> End Of Class



extension ChangeGroupVC: UIPickerViewDataSource, UIPickerViewDelegate {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return groupes.count
    }
     
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return groupes[row]._GroupName
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        self.NextGroupName = self.groupes[row]._GroupName
    }
}
