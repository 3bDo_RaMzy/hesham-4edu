//
//  ProfileVC.swift
//  LMS
//
//  Created by MacBook on 6/28/20.
//  Copyright © 2020 eaglez-group. All rights reserved.
//

import UIKit

class ProfileVC: BaseVC {

    var ActionTarget = "Update"
    
    @IBOutlet weak var PannerHieght: NSLayoutConstraint!
    
    @IBOutlet weak var ProfilePicture:   Circleview!
    @IBOutlet weak var ProfileFirstName: DesignableTextField!
    @IBOutlet weak var ProfileLastName:  DesignableTextField!
    @IBOutlet weak var ProfileEmail:     DesignableTextField!
    @IBOutlet weak var ProfileUserCode: UILabel!
    
    @IBOutlet weak var ProfileBtnSave:   UIButton!
    @IBOutlet weak var ProfileBackBtn:   UIButton!
    @IBOutlet weak var imge: UIButton!
    
    //------------------
    //---> For Image
    var image:UIImage!
    var imgBase64 = ""
    var ProfileID = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.title = "Profile Info"
        
        if UIDevice().userInterfaceIdiom == .phone {
                switch UIScreen.main.nativeBounds.height {
                    case 1136, 1334, 1920, 2208:
                        print("iPhone 5 or 5S or 5C | 6/6S/7/8 | 6+/6S+/7+/8+ ")
                        self.PannerHieght.constant = 60
                    case 2436, 2688, 1792:
                        print("iPhone X/XS/11 Pro  |  XS Max/11 Pro Max  |  XR/ 11")
                        self.PannerHieght.constant = 80
                    default:
                        self.PannerHieght.constant = 80
                }
        }
        hideKeyboardWhenTappedAround()
        
        if ActionTarget ==  "new" {
            ProfileBtnSave.setTitle("Save", for: .normal)
            ProfileBtnSave.tag = 1
            ProfileBackBtn.isHidden = true
        } else {
            ProfileBtnSave.setTitle("Update", for: .normal)
            ProfileBtnSave.tag = 2
            //---> Get ProfileData..
            getProfileData()
        }
    }//-------> End Of ViewDidLoad
    
    
    
    private func getProfileData(){
     
        if Reachable.isConnectedToNetwork() {
        
                    API.getProfileData(vc: self) { (err,TimeOut, profileDataModel) in
                        
                        if let err = err {
                            if TimeOut! {
                                let storyboard = UIStoryboard(name: "Auth", bundle: nil)
                                let vc         = storyboard.instantiateViewController(withIdentifier: "SessionExpireVC")
                                    vc.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency
                                self.present(vc, animated: true, completion: nil)
                               // self.navigationController?.pushViewController(vc, animated: true)
                                return
                                
                            } else {
                                self.alertmessage(Message: "Failed to fetch Profile Data:\n \(err) \n\n Try Again Later.." )
                                return
                            }
                        }
                        
                        self.ProfileUserCode.text  = "Student Code: " + (profileDataModel?.UserCode ?? "") 
                        self.ProfileFirstName.text = profileDataModel?.FullName ?? ""
                        self.ProfileLastName.text  = profileDataModel?.EmailAddress ?? ""
                        self.ProfileEmail.text     = profileDataModel?.Mobile ?? ""
                        
                        if profileDataModel  == nil {
                            
                            self.ProfilePicture.image = UIImage(named: "student.png")
                        } else if profileDataModel?.Image == "" {
                            
                            self.ProfilePicture.image = UIImage(named: "student.png")
                        } else {
                            
                            self.ProfilePicture.sd_setImage(with: URL(string:  URLs.ImageDomain + profileDataModel!.Image.dropFirst(1)), placeholderImage: UIImage(named: "student.png"))
                        }
                        
                        self.imgBase64 = self.ProfilePicture.image!.toBase64() ?? (UIImage(named: "student")?.toBase64())!
                        //self.ProfileID = profileDataModel?.id ?? 0
                    }
            
        } else {
            
            let storyboard  = UIStoryboard(name: "Main", bundle: nil)
            let noInternet   = storyboard.instantiateViewController(withIdentifier: "NoInternetVC") as! NoInternetVC
                noInternet.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                noInternet.modalTransitionStyle   = .crossDissolve
            self.present(noInternet, animated: true)
        }
    }//--- End Of getProfileData
    
 

    
    @IBAction func SaveProfileAction(_ sender: UIButton) {
        
        if ProfileBtnSave.tag == 1 {
            
            
//            if ProfileFirstName.text == ""{
//
//                    self.alertmessage(Message:  "Attention!! First name is empty")
//
//            } else if ProfileLastName.text == "" {
//
//                    self.alertmessage(Message: "Attention!! Last name is empty")
//
//            } else if ProfileEmail.text == "" {
//
//                    self.alertmessage(Message: "Attention!! Email is empty")
//
//            } else if imgBase64 == "" {
//
//                    self.alertmessage(Message: "Please choose photo first")
//
//            } else {
//
//
//                if Reachable.isConnectedToNetwork() {
//
//                        API.CreateProfile(vc: self, email: self.ProfileEmail.text!,
//                                          firstName: self.ProfileFirstName.text!,
//                                          lastName: self.ProfileLastName.text!,
//                                          picture: self.imgBase64) { (error, status) in
//
//                            if let err = error {
//                                   print("Failed to fetch getrequestsList:", err)
//                                   return
//                            }
//
//
//                            if status == "done"{
//
//    //                            self.alertmessage(Message: "Successful .. creating your profile")
//                                helper.saveUserProfile(isHasProfile: true)
//                                //self.dismiss(animated: true) {
//
//                                    print("\n\n Save Me..")
//                                    let storyboard            = UIStoryboard(name: "Main", bundle: nil)
//                                    let vc                    = storyboard.instantiateViewController(withIdentifier: "HomeTabsController")
//                                    vc.modalPresentationStyle = .overFullScreen
//                                    vc.modalTransitionStyle   = .crossDissolve
//                                    //self.present(vc, animated: true, completion: nil)
//                                    self.show(vc, sender: nil)
//
//                                //}
//
//
//                            } else if status == "failed" {
//
//                                self.alertmessage(Message: self.getString(title: "Failed .. your profile has a problem"))
//
//                            }
//
//                        }
//
//                } else {
//
//                    let storyboard  = UIStoryboard(name: "Main", bundle: nil)
//                    let noInternet   = storyboard.instantiateViewController(withIdentifier: "NoInternetVC") as! NoInternetVC
//                        noInternet.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
//                        noInternet.modalTransitionStyle   = .crossDissolve
//                    self.present(noInternet, animated: true)
//                }
//
//
//            }
             
        } else if ProfileBtnSave.tag == 2 {
            
                if ProfileFirstName.text == ""{

                        self.alertmessage(Message: "Attention!! Full name is empty")
                
                } else if ProfileLastName.text == "" {

                        self.alertmessage(Message: "Attention!! E-mail is empty")

                } else if ProfileEmail.text == "" {

                        self.alertmessage(Message: "Attention!! Phone is empty")

                } else if imgBase64 == "" {

                        self.alertmessage(Message: "Please choose photo first")

                } else {


                    if Reachable.isConnectedToNetwork() {

                        API.UpdateProfile(vc: self, fullName: self.ProfileFirstName.text!,
                                          email: self.ProfileLastName.text!, Mobile: self.ProfileEmail.text!,
                                          Image: imgBase64 )
                            
                                { (error,TimeOut, status) in
  
                                    
                                    if let err = error {
                                        if TimeOut! {
                                            let storyboard = UIStoryboard(name: "Auth", bundle: nil)
                                            let vc         = storyboard.instantiateViewController(withIdentifier: "SessionExpireVC")
                                                vc.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency
                                            self.present(vc, animated: true, completion: nil)
                                           // self.navigationController?.pushViewController(vc, animated: true)
                                            return
                                            
                                        } else {
                                            self.alertmessage(Message: "Failed to update Profile Data:\n \(err) \n\n Try Again Later.." )
                                            return
                                        }
                                    }
                                    if status == true{


                                        // create the alert
                                        let alert = UIAlertController(title: "", message: "Successful .. updating your profile" , preferredStyle: UIAlertController.Style.alert)
                                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { action in

                                            // do something like...
                                            // self.navigationController?.popViewController(animated: true)

                                        }))
                                        self.present(alert, animated: true, completion: nil)



                                    } else if status == false{

                                        //self.dismiss(animated: true) {
                                            self.alertmessage(Message:  "Failed .. your profile has a problem")
                                        //}
                                    }



                                }

                        } else {

                            let storyboard  = UIStoryboard(name: "Main", bundle: nil)
                            let noInternet   = storyboard.instantiateViewController(withIdentifier: "NoInternetVC") as! NoInternetVC
                                noInternet.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                                noInternet.modalTransitionStyle   = .crossDissolve
                            self.present(noInternet, animated: true)
                        }



                        }



            print("\n\n Update Me..")
            
            
            
             
        }
 
        
    }//-------->End Of SaveProfileAction
    
    
   @IBAction func goBackAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
}//------------------------------> End Of Class






extension ProfileVC: UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    ///start upload image
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        ProfilePicture.image = image
        imgBase64 = image.toBase64() ?? (UIImage(named: "user1")?.toBase64())!
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func changePhoto(_ sender: Any) {
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        let actionSheet = UIAlertController(title: "photo source", message: "choose a source", preferredStyle: .actionSheet)
        
            actionSheet.popoverPresentationController?.sourceView = imge
            actionSheet.popoverPresentationController?.sourceRect = imge.bounds
        
            actionSheet.addAction(UIAlertAction(title: "camera", style: .default, handler: {(action:UIAlertAction) in
                if UIImagePickerController.isSourceTypeAvailable(.camera){
                    imagePickerController.sourceType = .camera
                    self.present(imagePickerController, animated: true, completion: nil)
                }else{
                    print("camera is not avaliable")
                }
                
            }))
            
            actionSheet.addAction(UIAlertAction(title: "Photo library", style: .default, handler: {(action:UIAlertAction) in
                imagePickerController.sourceType = .photoLibrary
                self.present(imagePickerController, animated: true, completion: nil)
            }))
            
            actionSheet.addAction(UIAlertAction(title: "cancel", style: .cancel, handler: nil))
        self.present(actionSheet, animated: true, completion: nil)
    }
    ///end upload image
    
    
    
}
