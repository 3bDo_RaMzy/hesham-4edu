//
//  PaymentVC.swift
//  LMS
//
//  Created by MacBook on 8/31/20.
//  Copyright © 2020 eaglez-group. All rights reserved.
//

import UIKit

class PaymentVC: BaseVC {
 
    
    @IBOutlet weak var PannerHieght: NSLayoutConstraint!
     
    @IBOutlet weak var CollectionPaymentMethods: UICollectionView!
    @IBOutlet weak var TextInstructions: UILabel!
    @IBOutlet weak var HieghtInstructions: NSLayoutConstraint!
    
    //------------------
    //---> For Image
    var image:UIImage!
    var imgBase64 = ""
    
    let columnLayout = ColumnFlowLayout(cellsPerRow: 3,
                                        minimumInteritemSpacing: 10,
                                        minimumLineSpacing: 10,
                                        sectionInset: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0))
    
    var PaymentList = [Payment]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.title = "Payment"
        
         
        self.PaymentList.append(Payment(id: 0, title: "Vodafon Cashe", instructions: "Vodafone Cash is an E-Wallet provided by Vodafone to make your life easier and run all your errands with a click", logo: "1"))
        self.PaymentList.append(Payment(id: 1, title: "Etisalat Cashe", instructions: "Transfer money from your mobile to any other mobile number. cash in/out from the nearest Etisalat store", logo: "2"))
        self.PaymentList.append(Payment(id: 2, title: "Orange Cashe", instructions: "Subscribe now for free and start using cash-in, cash out, transfer money to others and pay your electricity bill, water bill, and mobile bills. In addition, you can pay your Orange Home Internet bill, recharge your credit and make donations.", logo: "3"))
        self.PaymentList.append(Payment(id: 3, title: "Meza", instructions: "Subscribe Now to WE Pay from home easily without visiting store, to enable its customers to control their various financial transactions such as transferring money and paying WE services bills, utilities, donations and others with ease and safety", logo: "4"))
        self.PaymentList.append(Payment(id: 4, title: "Fawry", instructions: "FawryPay is an easy e-commerce solution that connects sellers with buyers offering different payment options", logo: "5"))
        
        if UIDevice().userInterfaceIdiom == .phone {
                switch UIScreen.main.nativeBounds.height {
                    case 1136, 1334, 1920, 2208:
                        print("iPhone 5 or 5S or 5C | 6/6S/7/8 | 6+/6S+/7+/8+ ")
                        self.PannerHieght.constant = 60
                    case 2436, 2688, 1792:
                        print("iPhone X/XS/11 Pro  |  XS Max/11 Pro Max  |  XR/ 11")
                        self.PannerHieght.constant = 80
                    default:
                        self.PannerHieght.constant = 80
                }
        }
        hideKeyboardWhenTappedAround()
          
        
//        if let layout = CollectionPaymentMethods.collectionViewLayout as? UICollectionViewFlowLayout {
//            layout.scrollDirection = .horizontal
//        }
        
        CollectionPaymentMethods.register(UINib(nibName: "PaymentCell", bundle: .main),
                                          forCellWithReuseIdentifier: "PaymentCell")
        self.CollectionPaymentMethods.dataSource = self
        self.CollectionPaymentMethods.delegate   = self
        columnLayout.scrollDirection = .horizontal
        self.CollectionPaymentMethods?.collectionViewLayout = columnLayout
        self.CollectionPaymentMethods?.contentInsetAdjustmentBehavior = .always
        
        //---> Get ProfileData..
        getProfileData()
        
        
    }//-------> End Of ViewDidLoad
    
    
    
    private func getProfileData(){
     
//        if Reachable.isConnectedToNetwork() {
//
//                    API.getProfileData(vc: self) { (error, profileDataModel) in
//
//                        if let err = error {
//                               print("Failed to fetch getrequestsList:", err)
//                               return
//                        }
//
//                        //self.ProfileFirstName.text = profileDataModel?.FullName ?? ""
//
//                    }
//
//        } else {
//
//            let storyboard  = UIStoryboard(name: "Main", bundle: nil)
//            let noInternet   = storyboard.instantiateViewController(withIdentifier: "NoInternetVC") as! NoInternetVC
//                noInternet.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
//                noInternet.modalTransitionStyle   = .crossDissolve
//            self.present(noInternet, animated: true)
//        }
    }//--- End Of getProfileData
    
 

    
    @IBAction func SaveProfileAction(_ sender: UIButton) {
          
//                if ProfileFirstName.text == ""{
//
//                        self.alertmessage(Message: "Attention!! Full name is empty")
//
//                } else if ProfileLastName.text == "" {
//
//                        self.alertmessage(Message: "Attention!! E-mail is empty")
//
//                } else if ProfileEmail.text == "" {
//
//                        self.alertmessage(Message: "Attention!! Phone is empty")
//
//                } else if imgBase64 == "" {
//
//                        self.alertmessage(Message: "Please choose photo first")
//
//                } else {
//
//
//                    if Reachable.isConnectedToNetwork() {
//
//                        API.UpdateProfile(vc: self, fullName: self.ProfileFirstName.text!,
//                                          email: self.ProfileLastName.text!, Mobile: self.ProfileEmail.text!,
//                                          Image: imgBase64 )
//
//                                { (error, status) in
//
//                                            if let err = error {
//                                                self.alertmessage(Message:  "Failed .. your profile has a problem")
//                                                   print("Failed to fetch getrequestsList:", err)
//                                                   return
//                                            }
//
//                                            if status == true{
//
//
//                                                // create the alert
//                                                let alert = UIAlertController(title: "", message: "Successful .. updating your profile" , preferredStyle: UIAlertController.Style.alert)
//                                                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { action in
//
//                                                    // do something like...
//                                                    // self.navigationController?.popViewController(animated: true)
//
//                                                }))
//                                                self.present(alert, animated: true, completion: nil)
//
//
//
//                                            } else if status == false{
//
//                                                //self.dismiss(animated: true) {
//                                                    self.alertmessage(Message:  "Failed .. your profile has a problem")
//                                                //}
//                                            }
//
//                                        }
//
//                        } else {
//
//                            let storyboard  = UIStoryboard(name: "Main", bundle: nil)
//                            let noInternet   = storyboard.instantiateViewController(withIdentifier: "NoInternetVC") as! NoInternetVC
//                                noInternet.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
//                                noInternet.modalTransitionStyle   = .crossDissolve
//                            self.present(noInternet, animated: true)
//                        }
//
//
//
//                        }
//
//            print("\n\n ..")
           
        
    }//-------->End Of SaveProfileAction
    
    
   @IBAction func goBackAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
}//------------------------------> End Of Class






extension PaymentVC: UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    ///start upload image
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
//        ProfilePicture.image = image
        imgBase64 = image.toBase64() ?? (UIImage(named: "user1")?.toBase64())!
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func changePhoto(_ sender: Any) {
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        let actionSheet = UIAlertController(title: "photo source", message: "choose a source", preferredStyle: .actionSheet)
        
//            actionSheet.popoverPresentationController?.sourceView = imge
//            actionSheet.popoverPresentationController?.sourceRect = imge.bounds
        
            actionSheet.addAction(UIAlertAction(title: "camera", style: .default, handler: {(action:UIAlertAction) in
                if UIImagePickerController.isSourceTypeAvailable(.camera){
                    imagePickerController.sourceType = .camera
                    self.present(imagePickerController, animated: true, completion: nil)
                }else{
                    print("camera is not avaliable")
                }
                
            }))
            
            actionSheet.addAction(UIAlertAction(title: "Photo library", style: .default, handler: {(action:UIAlertAction) in
                imagePickerController.sourceType = .photoLibrary
                self.present(imagePickerController, animated: true, completion: nil)
            }))
            
            actionSheet.addAction(UIAlertAction(title: "cancel", style: .cancel, handler: nil))
        self.present(actionSheet, animated: true, completion: nil)
    }
    ///end upload image
    
    
    
}



extension PaymentVC: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    
//            func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
//             {
//                     return CGSize(width: 80.0, height: 70.0)
//              }
    
            func numberOfSections(in collectionView: UICollectionView) -> Int {
                return 1
            }
    
            func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
                 
                    return PaymentList.count
            }
            
            
            func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
                 
                let cellPayment  = collectionView.dequeueReusableCell(withReuseIdentifier: "PaymentCell",
                                                                          for: indexPath as IndexPath) as! PaymentCell
                    cellPayment.paymentViewModel = self.PaymentList[indexPath.row]
                return cellPayment
                    
            }
            
            
            func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
                
                self.TextInstructions.text  = PaymentList[indexPath.row].instructions
                self.HieghtInstructions.constant = self.TextInstructions.retrieveTextHeight() + 30
                
//                if collectionView == QuizCollectionCounter {
//
//                   self.QuestionCounter = indexPath.row
//                   fillNextQuestion(Counter: self.QuestionCounter )
//
//                } else {
//
//                         let cellImages = collectionView.cellForItem(at: indexPath) as! QuizImgsCell
//                         self.imageTapped(image: cellImages.QuestionImage.image!)
//
//                }
   
            }
    
     
    
}//---------> End Of CollecyionViewDelegate ----------------------










//------------------------------------------------------------------------------
class ColumnFlowLayout: UICollectionViewFlowLayout {
    
    let cellsPerRow: Int
    
    init(cellsPerRow: Int, minimumInteritemSpacing: CGFloat = 0,
         minimumLineSpacing: CGFloat = 0, sectionInset: UIEdgeInsets = .zero) {
        self.cellsPerRow = cellsPerRow
        super.init()
        
        self.minimumInteritemSpacing = minimumInteritemSpacing
        self.minimumLineSpacing = minimumLineSpacing
        self.sectionInset = sectionInset
        self.scrollDirection = .horizontal
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepare() {
        super.prepare()
        
        guard let collectionView = collectionView else { return }
        let marginsAndInsets = sectionInset.left + sectionInset.right + collectionView.safeAreaInsets.left + collectionView.safeAreaInsets.right + minimumInteritemSpacing * CGFloat(cellsPerRow - 1)
        let itemWidth = ((collectionView.bounds.size.width - marginsAndInsets) / CGFloat(cellsPerRow)).rounded(.down)
        itemSize = CGSize(width: itemWidth, height: itemWidth)
    }
    
    override func invalidationContext(forBoundsChange newBounds: CGRect) -> UICollectionViewLayoutInvalidationContext {
        let context = super.invalidationContext(forBoundsChange: newBounds) as! UICollectionViewFlowLayoutInvalidationContext
        context.invalidateFlowLayoutDelegateMetrics = newBounds.size != collectionView?.bounds.size
        return context
    }
    
}
