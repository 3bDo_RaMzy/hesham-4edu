//
//  SettingsVC.swift
//  LMS
//
//  Created by MacBook on 6/28/20.
//  Copyright © 2020 eaglez-group. All rights reserved.
//

import UIKit

class SettingsVC: BaseVC {

    @IBOutlet weak var PannerHieght: NSLayoutConstraint!
    @IBOutlet weak var UserImage: UIImageView!
    @IBOutlet weak var UserName: UILabel!
    
    @IBOutlet weak var TableSettings: UITableView!
    @IBOutlet weak var TableSettingsHeight: NSLayoutConstraint!
    
    
    var profileMenuItems  = [ProfileMenuModel]()
    var NotificationCount = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        //if UIDevice().userInterfaceIdiom == .phone {
        //        switch UIScreen.main.nativeBounds.height {
        //            case 1136, 1334, 1920, 2208:
        //                print("iPhone 5 or 5S or 5C | 6/6S/7/8 | 6+/6S+/7+/8+ ")
        //                self.PannerHieght.constant = 60
        //            case 2436, 2688, 1792:
        //                print("iPhone X/XS/11 Pro  |  XS Max/11 Pro Max  |  XR/ 11")
        //                self.PannerHieght.constant = 80
        //           default:
        //                self.PannerHieght.constant = 80
        //        }
        //}
        
        //self.profileMenuItems.append(ProfileMenuModel(id: 0, name: "News",           image: "notification", vcName: "NotificationsVC",    NotifiCount: 0))
        //self.profileMenuItems.append(ProfileMenuModel(id: 1, name: "Language" ,      image: "language",     vcName: "SettingsLanguageVC", NotifiCount: 0))
        self.profileMenuItems.append(ProfileMenuModel(id: 0, name: "Profile Info", image: "info",   vcName: "ProfileVC",          NotifiCount: 0))
        self.profileMenuItems.append(ProfileMenuModel(id: 1, name: "Message Box",  image: "info",   vcName: "RequestsVC",         NotifiCount: 0))
        self.profileMenuItems.append(ProfileMenuModel(id: 2, name: "About Us",     image: "info",   vcName: "visitAboutUsVC",     NotifiCount: 0))
        self.profileMenuItems.append(ProfileMenuModel(id: 3, name: "Change Password", image: "info", vcName: "ForgotPassword",    NotifiCount: 0))
        self.profileMenuItems.append(ProfileMenuModel(id: 4, name: "Change Study Group", image: "info", vcName: "ChangeGroupVC",  NotifiCount: 0))
        self.profileMenuItems.append(ProfileMenuModel(id: 5, name: "Language",     image: "info",   vcName: "SettingsLanguageVC", NotifiCount: 0))
        self.profileMenuItems.append(ProfileMenuModel(id: 6, name: "Payment",     image: "info",   vcName: "PaymentVC", NotifiCount: 0))
        self.profileMenuItems.append(ProfileMenuModel(id: 7, name: "Logout",       image: "logout", vcName: "AboutVC",            NotifiCount: 0))
           
            self.TableSettings.dataSource = self//
            self.TableSettings.delegate   = self
            self.TableSettings.rowHeight  = 70
                
            TableSettings.register(UINib(nibName: "SideMenuTableCell", bundle: nil), forCellReuseIdentifier: "SideMenuTableCell")
    
        self.getProfileData()
    }//----- End Of ViewDidLoad -----
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.getProfileRefreshData()
    }
    
    override func viewWillLayoutSubviews() {
            super.updateViewConstraints()
            self.TableSettingsHeight.constant = self.TableSettings.contentSize.height
            self.view.setNeedsLayout()
    }
     

    private func getProfileData(){
       
        if Reachable.isConnectedToNetwork() {
        
             API.getProfileData(vc: self) { (err,TimeOut, profileDataModel) in
                 
                if let err = err {
                    if TimeOut! {
                        let storyboard = UIStoryboard(name: "Auth", bundle: nil)
                        let vc         = storyboard.instantiateViewController(withIdentifier: "SessionExpireVC")
                            vc.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency
                        self.present(vc, animated: true, completion: nil)
                       // self.navigationController?.pushViewController(vc, animated: true)
                        return
                        
                    } else {
                        self.alertmessage(Message: "Failed to fetch Profile Data:\n \(err) \n\n Try Again Later.." )
                        return
                    }
                }
                
                self.UserName.text     =   profileDataModel?.FullName
                self.NotificationCount =   0 //( profileDataModel?.notificationsCount ?? 0 )
                
                if profileDataModel  == nil {
                    
                    self.UserImage.image = UIImage(named: "student.png")
                } else if profileDataModel?.Image == "" {
                    
                    self.UserImage.image = UIImage(named: "student.png")
                } else {
                    
                    self.UserImage.sd_setImage(with: URL(string:  URLs.ImageDomain + profileDataModel!.Image.dropFirst(1)), placeholderImage: UIImage(named: "student.png"))
                }
                 
                self.TableSettings.reloadData()
                  
             }
            
        } else {
            
            let storyboard  = UIStoryboard(name: "Main", bundle: nil)
            let noInternet   = storyboard.instantiateViewController(withIdentifier: "NoInternetVC") as! NoInternetVC
                noInternet.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                noInternet.modalTransitionStyle   = .crossDissolve
            self.present(noInternet, animated: true)
        }
         
     }//--- End Of getProfileData
    
    
    private func getProfileRefreshData(){
        
        if Reachable.isConnectedToNetwork() {
        
             API.getProfileRefreshData(vc: self) { (error,TimeOut, profileDataModel) in
                 
                 if let err = error {
                     if TimeOut! {
                         let storyboard = UIStoryboard(name: "Auth", bundle: nil)
                         let vc         = storyboard.instantiateViewController(withIdentifier: "SessionExpireVC")
                             vc.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency
                         self.present(vc, animated: true, completion: nil)
                        // self.navigationController?.pushViewController(vc, animated: true)
                         return
                         
                     } else {
                         self.alertmessage(Message: "Failed to fetch Profile Data:\n \(err) \n\n Try Again Later.." )
                         return
                     }
                 }
                 
                self.UserName.text     =   profileDataModel?.FullName
                self.NotificationCount =   0 //( profileDataModel?.notificationsCount ?? 0 )
                
                if profileDataModel  == nil {
                    
                    self.UserImage.image = UIImage(named: "student.png")
                } else if profileDataModel?.Image == "" {
                    
                    self.UserImage.image = UIImage(named: "student.png")
                } else {
                    
                    self.UserImage.sd_setImage(with: URL(string:  URLs.ImageDomain + profileDataModel!.Image.dropFirst(1)), placeholderImage: UIImage(named: "student.png"))
                }
                 
                self.TableSettings.reloadData()
                  
             }
            
        } else {
            
            let storyboard  = UIStoryboard(name: "Main", bundle: nil)
            let noInternet   = storyboard.instantiateViewController(withIdentifier: "NoInternetVC") as! NoInternetVC
                noInternet.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                noInternet.modalTransitionStyle   = .crossDissolve
            self.present(noInternet, animated: true)
        }
    }//--- End Of getProfileRefreshData
    
    
    
}//--------- End Of Class --------


extension SettingsVC: UITableViewDataSource, UITableViewDelegate {
    
     
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        self.viewWillLayoutSubviews()
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.profileMenuItems.count
    }
           
           
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
               
        let cell = tableView.dequeueReusableCell(withIdentifier: "SideMenuTableCell", for: indexPath) as! SideMenuTableCell
               
            cell.profileMenuModel = self.profileMenuItems[indexPath.row]
            cell.hideNotifiCountView()
        
        
//            if indexPath.row != 0 {
//
//                cell.hideNotifiCountView()
//            } else if indexPath.row == 0 && self.NotificationCount == 0 {
//
//                cell.hideNotifiCountView()
//            } else {
//
//                cell.showNotifiCountView()
//                cell.MenuNotifiLbl.text = String(self.NotificationCount)
//
//            }
        
        return cell
    }
     

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         
                switch indexPath.row {
                    case 0,1,4,5,6:
                            let storyboard            = UIStoryboard(name: "Main", bundle: nil)
                            let vc                    = storyboard.instantiateViewController(withIdentifier: "\(profileMenuItems[indexPath.row].vcName)")
                            self.navigationController?.pushViewController(vc, animated: true)
                    case 2:
                            let storyboard            = UIStoryboard(name: "Visitor", bundle: nil)
                            let vc                    = storyboard.instantiateViewController(withIdentifier: "\(profileMenuItems[indexPath.row].vcName)")
                            self.navigationController?.pushViewController(vc, animated: true)
                    case 3:
                            let storyboard            = UIStoryboard(name: "Auth", bundle: nil)
                            let vc                    = storyboard.instantiateViewController(withIdentifier: "\(profileMenuItems[indexPath.row].vcName)") as! ForgotPassword
                                if helper.getSMSOption() == true {
                                        vc.optionUsed = 2 // 1: email   |  2: SMS code
                                }else {
                                        vc.optionUsed = 1 // 1: email   |  2: SMS code
                                }
                            self.navigationController?.pushViewController(vc, animated: true)
                    case 7:
                         
                            helper.resetUserToken()
                            
                            guard let window = UIApplication.shared.keyWindow else{return}
                            let storyboard   = UIStoryboard(name: "Auth", bundle: nil)
                            let vc = storyboard.instantiateViewController(withIdentifier: "Splash-Login")
                            window.rootViewController = vc
                            UIView.transition(with: window, duration: 0.5, options: .transitionFlipFromRight, animations: nil, completion: nil)
                    
                default:
                    return
                }
                  
        }
         

        
     
    
}

