//
//  PostReplayVC.swift
//  LMS
//
//  Created by Abdo on 1/17/20.
//  Copyright © 2020 eaglez-group. All rights reserved.
//

import UIKit

class PostReplayVC: BaseVC, UITableViewDataSource, UITableViewDelegate {

    
    @IBOutlet weak var msgTxtField: UITextField!
    @IBOutlet weak var ReplaysTableView: UITableView!
    
    var replayCommentsViewModel = [ReplayCommentPost]()
    var Spinner :UIView!
    private let refreshControl = UIRefreshControl()
    var PostId    = 0
    var CommentId = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.title = "Replays"
        
        // set up the refresh control
       if #available(iOS 10.0, *) {
            ReplaysTableView.refreshControl = refreshControl
        } else {
            ReplaysTableView.addSubview(refreshControl)
        }
         refreshControl.tintColor = UIColor(red:0.25, green:0.72, blue:0.85, alpha:1.0)
         refreshControl.attributedTitle = NSAttributedString(string: "Refresh Data...", attributes: nil)
         refreshControl.addTarget(self, action: #selector(refreshData(_:)), for: .valueChanged)
                                    
         self.ReplaysTableView.dataSource     = self
         self.ReplaysTableView.delegate       = self
         self.ReplaysTableView.layoutMargins  = UIEdgeInsets.zero
         self.ReplaysTableView.separatorInset = UIEdgeInsets.zero
         self.ReplaysTableView.rowHeight      = 180
         self.fetchData()
                 
        NotificationCenter.default.addObserver(self, selector: #selector(loadList(_:)), name: Notification.Name("load"), object: nil)
    }//------------ End Of ViewDidLoad ---------------------
    
    
    
        @objc func loadList(_ notification:Notification){
            //load data here
            self.fetchData()
        }
        
        @objc private func refreshData(_ sender: Any) {
            // Fetch Weather Data
            self.refreshData()
        }

                    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

                            return replayCommentsViewModel.count
                        }
                    
                     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
                        
                            let cell = tableView.dequeueReusableCell(withIdentifier: "PostReplayCell", for: indexPath) as! PostReplayCell
                                  cell.replayCommentPostViewModel = replayCommentsViewModel[indexPath.row]

                         return cell
                        }
                    
                    
                    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
                             
                        }
    
    
    //--------------------------------------------------------------------
    fileprivate func fetchData() {
                        
            Spinner = UIViewController.displaySpinner(onView: self.view)
            API.getReplaysList(vc: self, Token: helper.getUserToken()!, PostId: self.PostId, CommentId: self.CommentId) { ( err, replays ) in
                            
                    UIViewController.removeSpinner(spinner: self.Spinner)
                    if let err = err {
                                print("Failed to fetch getrequestsList:", err)
                                return
                    }
                            
            self.replayCommentsViewModel = replays?.map({return ReplayCommentPost(replay:  $0)}) ?? []
                    self.ReplaysTableView.reloadData()
            }
                        
     }//---> End Of fetchData method -------
    fileprivate func refreshData() {
                        
        self.refreshControl.beginRefreshing()
           API.getReplaysList(vc: self, Token: helper.getUserToken()!, PostId: self.PostId, CommentId: self.CommentId) { ( err, replays ) in
                           
                   self.refreshControl.endRefreshing()
                   if let err = err {
                               print("Failed to fetch getrequestsList:", err)
                               return
                   }
                           
           self.replayCommentsViewModel = replays?.map({return ReplayCommentPost(replay:  $0)}) ?? []
                   self.ReplaysTableView.reloadData()
           }
                       
    }//---> End Of fetchData method -------


    //---------------------------------------------------------------------------
    
    @IBAction func sendSMS(_ sender: Any) {
          
          if msgTxtField.text == "" {
              
                let alert = UIAlertController(title: "Attention !!", message: "Replay Details Is Empty", preferredStyle: .alert)
                             alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.cancel, handler: nil))
                self.present(alert, animated: true)
          }else {
                 
            API.sendCommentReplay( vc: self, Token: helper.getUserToken()!,
                                   CommentId: self.CommentId, Replay: msgTxtField.text! )
                { (error, status) in
                                    
                        if error != nil {
                                                           
                                        self.alertmessage(Message: "Error: \(error)")
                         } else  {
                            
                                    //print("Message Sent Successfully")
                                    let alert = UIAlertController(title: "", message: " Successfully sent ",
                                                                  preferredStyle: UIAlertController.Style.alert)
                                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: {(alert: UIAlertAction!) in

                                         self.msgTxtField.text = ""
                                         self.dismiss(animated: true){
                                             NotificationCenter.default.post(name: NSNotification.Name(rawValue: "load"), object: nil)
                                         }

                                        }))
                                    self.present(alert, animated: true)
                                                           
                         }
                                     
                     }
               }
         }
    
    
}// End Of Class
