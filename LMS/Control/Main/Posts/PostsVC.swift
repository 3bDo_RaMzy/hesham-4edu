//
//  PostsVC.swift
//  LMS
//
//  Created by Abdo on 1/17/20.
//  Copyright © 2020 eaglez-group. All rights reserved.
//

import UIKit
//import firebaseDatabase


class PostsVC: BaseVC, UITableViewDataSource, UITableViewDelegate {

    
    @IBOutlet weak var PostsTableView: UITableView!
    var postsViewModel = [Post]()
    var Spinner :UIView!
    private let refreshControl = UIRefreshControl()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.title = "Timeline"
        
        // set up the refresh control
       if #available(iOS 10.0, *) {
            PostsTableView.refreshControl = refreshControl
        } else {
            PostsTableView.addSubview(refreshControl)
        }
         refreshControl.tintColor = UIColor(red:0.25, green:0.72, blue:0.85, alpha:1.0)
         refreshControl.attributedTitle = NSAttributedString(string: "Refresh Data...", attributes: nil)
         refreshControl.addTarget(self, action: #selector(refreshData(_:)), for: .valueChanged)
                                    
         self.PostsTableView.dataSource     = self
         self.PostsTableView.delegate       = self
         self.PostsTableView.layoutMargins  = UIEdgeInsets.zero
         self.PostsTableView.separatorInset = UIEdgeInsets.zero
         self.PostsTableView.rowHeight      = 180
         self.fetchData()
                 
            NotificationCenter.default.addObserver(self, selector: #selector(loadList(_:)), name: Notification.Name("load"), object: nil)
            NotificationCenter.default.addObserver(self, selector: #selector(FilterSearch(_:)), name: Notification.Name("Filter"), object: nil)
        }//------------ End Of ViewDidLoad ---------------------
          
          
          @objc func loadList(_ notification:Notification){
              //load data here
              self.fetchData()
          }
    
        @objc func FilterSearch(_ notification:Notification){
            
            if let dict = notification.userInfo as NSDictionary? {
                 let custom = dict["Custom"] as? Bool
                 let type   = dict["Type"]   as? String
                
                //load data here
                self.fetchFilterData(Type: type!, Custom: custom!)
            }
            
            
        }
        
        @objc private func refreshData(_ sender: Any) {
            // Fetch Weather Data
            self.refreshData()
        }

                    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

                            return postsViewModel.count
                        }
                    
                     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
                        
                            let cell = tableView.dequeueReusableCell(withIdentifier: "PostCell", for: indexPath) as! PostCell
                                cell.postViewModel = postsViewModel[indexPath.row]
                                cell.PostCommentsCount.tag = indexPath.row
                                cell.PostCommentsCount.addTarget(self, action: #selector(FuncOpenComments(_:)), for: .touchUpInside)
                         return cell
                        }
                    
                    
                    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
                             
    //                    ParentStudentsVC.StudentId = self.requestsViewModel[indexPath.row].StudentId
    //
    //                    // Do something now
//                        let storyboard            = UIStoryboard(name: "Main", bundle: nil)
//                        let vc                    = storyboard.instantiateViewController(withIdentifier: "PostCommentVC") as! PostCommentVC
//                            vc.modalPresentationStyle = .overFullScreen
//                            vc.modalTransitionStyle   = .crossDissolve
//                            vc.PostId = postsViewModel[indexPath.row].Id
//                        //self.present(vc, animated: true, completion: nil)
//                        self.show(vc, sender: nil)
                        }
            
                    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
                        return true
                    }
    
                    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
                        
                        let editAction = UITableViewRowAction(style: .default, title: "Edit") { (action, indexPath) in
                            
                            print("\n Edit action")
                            
                        }
                        
                        let deleteAction = UITableViewRowAction(style: .default, title: "Delete") { (action, indexPath) in
                            
                            print("\n Delete action")
                            
                        }
                        
                        editAction.backgroundColor = .lightGray
                        deleteAction.backgroundColor = .red
                        
                        return [deleteAction, editAction]
                        
                    }
    
    
//                    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
//                        let action = UIContextualAction(
//                            style: .destructive,
//                            title: "Delete",
//                            handler: { (action, view, completion) in
//                                //do what you want here
//                                completion(true)
//                        })
//                        //action.image = UIImage(named: "My Image")
//                        action.backgroundColor = .red
//
//                        let action2 = UIContextualAction(
//                            style: .normal,
//                            title: "Edit",
//                            handler: { (action, view, completion) in
//                                //do what you want here
//                                completion(true)
//                        })
//                        //action.image = UIImage(named: "My Image")
//                        action.backgroundColor = .gray
//
//                        let configuration = UISwipeActionsConfiguration(actions: [action, action2])
//                        configuration.performsFirstActionWithFullSwipe = false
//                        return configuration
//                    }

    
        
                    
        
        
        //--------------------------------------------------------------------
        @objc func FuncOpenComments(_ sender: UIButton){
                      
    //                  let mm = requestsViewModel[sender.tag].commentes.count
    //                  print(" \n\n FuncOpenComments Now with tag: \(sender.tag) Comments Count; \(mm)")
                      
                      let storyboard            = UIStoryboard(name: "Main", bundle: nil)
                      let vc                    = storyboard.instantiateViewController(withIdentifier: "PostCommentVC") as! PostCommentVC
                          vc.modalPresentationStyle = .overFullScreen
                          vc.modalTransitionStyle   = .crossDissolve
                          vc.PostId = postsViewModel[sender.tag].Id
                      //self.present(vc, animated: true, completion: nil)
                      self.show(vc, sender: nil)
        }
    
    
    //--------------------------------------------------------------------
    fileprivate func fetchData() {
                        
            Spinner = UIViewController.displaySpinner(onView: self.view)
        API.getPostsList(vc: self, Token: helper.getUserToken()!, PostType: "Public", PostCustom: true) { ( err, requests ) in
                            
                    UIViewController.removeSpinner(spinner: self.Spinner)
                    if let err = err {
                                print("Failed to fetch getrequestsList:", err)
                                return
                    }
                            
                    self.postsViewModel = requests?.map({return Post(post:  $0)}) ?? []
                    if self.postsViewModel.count <= 0 {
                        self.showToast(message: "No Posts Available Now !!", vc: self)
                    }
                    self.PostsTableView.reloadData()
            }
                        
     }//---> End Of fetchData method -------
    fileprivate func refreshData() {
                       
        self.refreshControl.beginRefreshing()
        API.getPostsList(vc: self, Token: helper.getUserToken()!, PostType: "Public", PostCustom: true) { ( err, requests ) in
                           
                   self.refreshControl.endRefreshing()
                   if let err = err {
                               print("Failed to fetch getrequestsList:", err)
                               return
                   }
                           
                   self.postsViewModel = requests?.map({return Post(post:  $0)}) ?? []
                    if self.postsViewModel.count <= 0 {
                        self.showToast(message: "No Posts Available Now !!", vc: self)
                    }
                   self.PostsTableView.reloadData()
           }
                       
    }//---> End Of fetchData method -------
    fileprivate func fetchFilterData(Type: String, Custom: Bool) {
                       
           Spinner = UIViewController.displaySpinner(onView: self.view)
        API.getPostsList(vc: self, Token: helper.getUserToken()!, PostType: Type, PostCustom: Custom) { ( err, requests ) in
                           
                   UIViewController.removeSpinner(spinner: self.Spinner)
                   if let err = err {
                               print("Failed to fetch getrequestsList:", err)
                               return
                   }
                           
                   self.postsViewModel = requests?.map({return Post(post:  $0)}) ?? []
                   self.PostsTableView.reloadData()
           }
                       
    }//---> End Of fetchData method -------
    
}// End Of Class
