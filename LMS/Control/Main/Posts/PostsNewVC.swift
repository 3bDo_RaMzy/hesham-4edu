//
//  PostsNewVC.swift
//  LMS
//
//  Created by Abdo on 1/17/20.
//  Copyright © 2020 eaglez-group. All rights reserved.
//

import UIKit

class PostsNewVC: BaseVC, UIPickerViewDelegate, UIPickerViewDataSource {
   
    let postTypes = ["Public", "Grade", "Group"]
    
    @IBOutlet weak var PostTypePicker: UIPickerView!
    @IBOutlet weak var PostText: UITextView!
    
    var Spinner :UIView!
    
    var onGrade: Bool = false
    var onGroup: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        PostText!.delegate = self
        PostText!.autocorrectionType = UITextAutocorrectionType.no
        PostText!.text = "What do you want to share here .. ?"
        PostText!.textColor = UIColor.lightGray
        
        self.PostTypePicker.dataSource = self
        self.PostTypePicker.delegate   = self
    }
    

     func numberOfComponents(in pickerView: UIPickerView) -> Int {
           1
       }
       
       func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        self.postTypes.count
       }
    
        func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
            return postTypes[row]
        }

        func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
            
            if row == 0 {
                self.onGrade = false
                self.onGroup = false
            } else if row == 1 {
                self.onGrade = true
                self.onGroup = false
            } else {
                self.onGrade = false
                self.onGroup = true
            }
        }

    @IBAction func ShareNewPostAction(_ sender: Any) {
         
        if ( PostText.text == "" ) {
            
            alertmessage(Message: "Post is empty")
            
        } else {
            
            Spinner = UIViewController.displaySpinner(onView: self.view)
            API.sendNewPost(vc: self, Token: helper.getUserToken()!,
                                Post: PostText.text!, OnGroup: self.onGroup, OnGrade: self.onGrade){
                            
                    (error, status) in
                                       
                           if error != nil {
                                           UIViewController.removeSpinner(spinner: self.Spinner)
                                           self.alertmessage(Message: "Error: \(error)")
                            } else  {
                                       UIViewController.removeSpinner(spinner: self.Spinner)
                                       //print("Message Sent Successfully")
                                       let alert = UIAlertController(title: "", message: " Successfully sent ", preferredStyle: UIAlertController.Style.alert)
                                       alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: {(alert: UIAlertAction!) in

                                        self.PostText.text = ""
                                        self.dismiss(animated: true){
                                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "load"), object: nil)
                                        }

                                       }))
                                       self.present(alert, animated: true)
                                                              
                            }
                                        
                        }
                }
        
         }
    

}//------- End Of Class ---------------



extension PostsNewVC: UITextViewDelegate {
    
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if (PostText?.text == "What do you want to share here .. ?")

        {
            PostText!.text = nil
            PostText!.textColor = UIColor.black
        }
    }

    func textViewDidEndEditing(_ textView: UITextView) {
        if PostText!.text.isEmpty
        {
            PostText!.text = "What do you want to share here .. ?"
            PostText!.textColor = UIColor.lightGray
        }
        textView.resignFirstResponder()
    }
    
}
