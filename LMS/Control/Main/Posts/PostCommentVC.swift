//
//  PostCommentVC.swift
//  LMS
//
//  Created by Abdo on 1/17/20.
//  Copyright © 2020 eaglez-group. All rights reserved.
//

import UIKit

class PostCommentVC: BaseVC, UITableViewDataSource, UITableViewDelegate {

    
    @IBOutlet weak var msgTxtField: UITextField!
    
    @IBOutlet weak var CommentsTableView: UITableView!
    var commentsViewModel = [CommentPost]()
    var Spinner :UIView!
    private let refreshControl = UIRefreshControl()
    var PostId = 0
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        // Do any additional setup after loading the view.
        self.title = "Comments"
        
        // set up the refresh control
       if #available(iOS 10.0, *) {
            CommentsTableView.refreshControl = refreshControl
        } else {
            CommentsTableView.addSubview(refreshControl)
        }
         refreshControl.tintColor = UIColor(red:0.25, green:0.72, blue:0.85, alpha:1.0)
         refreshControl.attributedTitle = NSAttributedString(string: "Refresh Data...", attributes: nil)
         refreshControl.addTarget(self, action: #selector(refreshData(_:)), for: .valueChanged)
                                    
         self.CommentsTableView.dataSource     = self
         self.CommentsTableView.delegate       = self
         self.CommentsTableView.layoutMargins  = UIEdgeInsets.zero
         self.CommentsTableView.separatorInset = UIEdgeInsets.zero
         self.CommentsTableView.rowHeight      = 180
         self.fetchData()
                 
        NotificationCenter.default.addObserver(self, selector: #selector(loadList(_:)), name: Notification.Name("load"), object: nil)
    }//------------ End Of ViewDidLoad ---------------------
    
    
    
        @objc func loadList(_ notification:Notification){
            //load data here
            self.fetchData()
        }
        
        @objc private func refreshData(_ sender: Any) {
            // Fetch Weather Data
            self.refreshData()
        }

                    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

                            return commentsViewModel.count
                        }
                    
                     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
                        
                            let cell = tableView.dequeueReusableCell(withIdentifier: "PostCommentCell", for: indexPath) as! PostCommentCell
                                  cell.postCommentViewModel = commentsViewModel[indexPath.row]
                                cell.CommentReplayCount.tag = indexPath.row
                                cell.CommentReplayCount.addTarget(self, action: #selector(FuncOpenReplies(_:)), for: .touchUpInside)
                         return cell
                        }
                    
                    
                    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
                             
    //                    ParentStudentsVC.StudentId = self.requestsViewModel[indexPath.row].StudentId
    //
    //                    // Do something now
//                        let storyboard            = UIStoryboard(name: "Main", bundle: nil)
//                        let vc                    = storyboard.instantiateViewController(withIdentifier: "PostReplayVC") as! PostReplayVC
//                            vc.modalPresentationStyle = .overFullScreen
//                            vc.modalTransitionStyle   = .crossDissolve
//                            vc.PostId    = self.PostId
//                            vc.CommentId = commentsViewModel[indexPath.row].CommentId
//                        //self.present(vc, animated: true, completion: nil)
//                        self.show(vc, sender: nil)
                        }
            
        
        
        
        //--------------------------------------------------------------------
        @objc func FuncOpenReplies(_ sender: UIButton){
                      
    //                  let mm = requestsViewModel[sender.tag].commentes.count
    //                  print(" \n\n FuncOpenComments Now with tag: \(sender.tag) Comments Count; \(mm)")
                      
                      let storyboard            = UIStoryboard(name: "Main", bundle: nil)
                      let vc                    = storyboard.instantiateViewController(withIdentifier: "PostReplayVC") as! PostReplayVC
                          vc.modalPresentationStyle = .overFullScreen
                          vc.modalTransitionStyle   = .crossDissolve
                          vc.PostId    = self.PostId
                          vc.CommentId = commentsViewModel[sender.tag].CommentId
                      //self.present(vc, animated: true, completion: nil)
                      self.show(vc, sender: nil)
        }
    
    
    //--------------------------------------------------------------------
    fileprivate func fetchData() {
                        
            Spinner = UIViewController.displaySpinner(onView: self.view)
        
        API.getCommentsList(vc: self, Token: helper.getUserToken()!, PostId:  self.PostId) { ( err, comments ) in
                            
                    UIViewController.removeSpinner(spinner: self.Spinner)
                    if let err = err {
                                print("Failed to fetch getrequestsList:", err)
                                return
                    }
                            
            self.commentsViewModel = comments?.map({return CommentPost(comment:  $0)}) ?? []
                    self.CommentsTableView.reloadData()
            }
                        
     }//---> End Of fetchData method -------
    fileprivate func refreshData() {
                       
        self.refreshControl.beginRefreshing()
           API.getCommentsList(vc: self, Token: helper.getUserToken()!, PostId:  self.PostId) { ( err, comments ) in
                           
            self.refreshControl.endRefreshing()
                   if let err = err {
                               print("Failed to fetch getrequestsList:", err)
                               return
                   }
                           
           self.commentsViewModel = comments?.map({return CommentPost(comment:  $0)}) ?? []
                   self.CommentsTableView.reloadData()
           }
                       
    }//---> End Of fetchData method -------

    
    
    //---------------------------------------------------------------------------
    
    @IBAction func sendSMS(_ sender: Any) {
          
          if msgTxtField.text == "" {
              
            let alert = UIAlertController(title: "Attention !!", message: "Comment Details Is Empty", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.cancel, handler: nil))
            self.present(alert, animated: true)
            
        
          }else {
                    
            API.sendPostComment(vc: self, Token: helper.getUserToken()!,
                                PostId: self.PostId, Commentt: msgTxtField.text!)
            { (error, status) in
                                    
                        if error != nil {
                                                           
                                        self.alertmessage(Message: "Error: \(error)")
                         } else  {
                            
                                    //print("Message Sent Successfully")
                                    let alert = UIAlertController(title: "", message: " Successfully sent ", preferredStyle: UIAlertController.Style.alert)
                                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: {(alert: UIAlertAction!) in

                                     self.msgTxtField.text = ""
                                     self.dismiss(animated: true){
                                         NotificationCenter.default.post(name: NSNotification.Name(rawValue: "load"), object: nil)
                                     }

                                    }))
                                    self.present(alert, animated: true)
                                                           
                         }
                                     
                     }
               }
         }
    

}// End Of Class
