//
//  PostsFilterVC.swift
//  LMS
//
//  Created by Abdo on 2/7/20.
//  Copyright © 2020 eaglez-group. All rights reserved.
//

import UIKit

class PostsFilterVC: BaseVC, UIPickerViewDelegate, UIPickerViewDataSource {
 
        let postsCustom = ["All Posts", "My Posts"]
        let postTypes = ["Public", "Grade", "Group"]
    
    @IBOutlet weak var FilterCustomPicker: UIPickerView!
    @IBOutlet weak var FilterTypePicker: UIPickerView!
    
        var FilterCustom = false
        var FilterType   = "Public"
    var filterDataDict:[String: Any] = [:]
        
        
        override func viewDidLoad() {
            super.viewDidLoad()

            // Do any additional setup after loading the view.
            
            
            
            self.FilterCustomPicker.dataSource = self
            self.FilterCustomPicker.delegate   = self
            self.FilterTypePicker.dataSource   = self
            self.FilterTypePicker.delegate     = self
            
        }
    
    
    

        func numberOfComponents(in pickerView: UIPickerView) -> Int {
            1
        }
        
        func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
            
                if pickerView == self.FilterCustomPicker {
                    
                        return postsCustom.count
                } else {
                    
                        return postTypes.count
                }
        }
     
         func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
             
                if pickerView == self.FilterCustomPicker {
                               
                        return postsCustom[row]
                } else {
                               
                        return postTypes[row]
                }
            
         }

         func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
             
                if pickerView == self.FilterCustomPicker {
                               
                    if row == 0 {
                            self.FilterCustom = false
                    } else if row == 1 {
                            self.FilterCustom = true
                    }
                    
                } else {
                               
                    if row == 0 {
                            self.FilterType = "Public"
                    } else if row == 1 {
                            self.FilterType = "Grade"
                    } else if row == 2 {
                            self.FilterType = "Group"
                    }
                }
             
         }

    
    @IBAction func SearchFilterAction(_ sender: Any) {
    
        self.filterDataDict = ["Custom": self.FilterCustom, "Type": self.FilterType]
    
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Filter"), object: nil, userInfo: self.filterDataDict )
        self.dismiss(animated: true, completion: nil)
    
    }
    
    
    
}


extension PostsFilterVC: UITextViewDelegate {
    
    
    
    
}
