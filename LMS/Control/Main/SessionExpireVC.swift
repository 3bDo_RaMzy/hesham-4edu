//
//  SessionExpireVC.swift
//  LMS
//
//  Created by MacBook on 11/24/20.
//  Copyright © 2020 eaglez-group. All rights reserved.
//

import UIKit

class SessionExpireVC: UIViewController {
  
        override func viewDidLoad() {
            super.viewDidLoad()
             // Do any additional setup after loading the view.
            
        }//--- End Of ViewDidLoading
        
        
        @IBAction func goLoginAction(_ sender: UIButton) {
            
            helper.resetUserToken()
            
            guard let window = UIApplication.shared.keyWindow else{return}
            let storyboard   = UIStoryboard(name: "Auth", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "Splash-Login")
            window.rootViewController = vc
            UIView.transition(with: window, duration: 0.5, options: .transitionFlipFromRight, animations: nil, completion: nil)
        }
        
        
    }
