//
//  FinishQuizVC.swift
//  LMS
//
//  Created by Abdo on 12/11/19.
//  Copyright © 2019 eaglez-group. All rights reserved.
//

import UIKit

class FinishQuizVC: UIViewController {

    var resultMaxDegree = 0.0
    var resultDegree    = 0.0
     
    @IBOutlet weak var textDegree: UILabel!
    @IBOutlet weak var textMaxDegree: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        if resultMaxDegree > 0 {
            textDegree.text = String(resultDegree)
            textMaxDegree.text = String(resultMaxDegree)
        }
        
    }
    
    @IBAction func finishBtnAction(_ sender: Any) {
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "closeQuiz"), object: nil)
        self.dismiss(animated: true, completion: nil)
        
    }
    
    

}
