//
//  QuizCorrectionVC.swift
//  LMS
//
//  Created by Abdo on 6/13/20.
//  Copyright © 2020 eaglez-group. All rights reserved.
//

import UIKit

class QuizCorrectionVC: BaseVC {

    
    //--> variables
    var Spinner :UIView!
    
    var Quiz_Id:             Int          = 0
    var Quiz_Title:          String       = ""
    var Quiz_Degree:         Double       = 0.0
    var Quiz_DegreeMax:      Double       = 0.0
    
    
    var responseQuestionModel: QuizCorrection =  QuizCorrection()
    var QuestionArr:         [CorrectionList] = [] //---> for all quiz questions
//    var QuizAnswerArr =      [QuizQuestionAnswer]()  //---> for all quiz answer
    var QuestionCounterArr = [QuizQuestionCounter]() //---> for showing question number
    
    var QuestionCounter  = 0
    var LastPosition     = 0

    
    @IBOutlet weak var QuizHeaderHieght:        NSLayoutConstraint!
    @IBOutlet weak var QuizTitle:               UILabel!
    @IBOutlet weak var QuizDegreeFrom:          UILabel!
    @IBOutlet weak var QuizDegreeTo:            UILabel!
    @IBOutlet weak var QuestionDegreeFrom:      UILabel!
    @IBOutlet weak var QuestionDegreeTo:        UILabel!
    @IBOutlet weak var QuizCollectionCounter:   UICollectionView!
      
    @IBOutlet weak var quiz_CollBackView:       UIView!
    @IBOutlet weak var quiz_CollBackViewHieght: NSLayoutConstraint!
    @IBOutlet weak var QuizQuestionTitle:       UILabel!
    @IBOutlet weak var QuizQuestionTitleHieght: NSLayoutConstraint!
    @IBOutlet weak var QuizCollectionImages:    UICollectionView!
    @IBOutlet weak var QuizTableQuestions:      UITableView!
    @IBOutlet weak var QuizBtnNext:             UIButton!
    @IBOutlet weak var QuizBtnPrevious:         UIButton!
    @IBOutlet weak var QuizShortNoteView:       UIView!
    @IBOutlet weak var QuizShortNoteText:       UITextView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
                hideKeyboardWhenTappedAround()
                
                if UIDevice().userInterfaceIdiom == .phone {
                    switch UIScreen.main.nativeBounds.height {
                        case 1136, 1334, 1920, 2208:
                            print("iPhone 5 or 5S or 5C | 6/6S/7/8 | 6+/6S+/7+/8+ ")
                            self.QuizHeaderHieght.constant = 60
                        case 2436, 2688, 1792:
                            print("iPhone X/XS/11 Pro  |  XS Max/11 Pro Max  |  XR/ 11")
                            self.QuizHeaderHieght.constant = 80
                        default:
                            self.QuizHeaderHieght.constant = 80
                    }
                }
               
               self.QuizShortNoteView.isHidden  = true
               self.QuizTableQuestions.isHidden = false
 
               QuizShortNoteText.delegate   = self
               QuizShortNoteText.text       =  "Add your answer here..."
               QuizShortNoteText.textColor  = .lightGray
               
               self.QuizTableQuestions.dataSource     = self
               self.QuizTableQuestions.delegate       = self
               self.QuizTableQuestions.layoutMargins  = UIEdgeInsets.zero
               self.QuizTableQuestions.separatorInset = UIEdgeInsets.zero
               //self.QuizTableQuestions.rowHeight      = 60
               
               self.QuizTableQuestions.estimatedRowHeight = 60.0
               self.QuizTableQuestions.rowHeight = UITableView.automaticDimension
               
               if let layout = QuizCollectionCounter.collectionViewLayout as? UICollectionViewFlowLayout {
                   layout.scrollDirection = .horizontal
               }
               
               self.QuizCollectionCounter.dataSource = self
               self.QuizCollectionCounter.delegate   = self
        
               self.QuizCollectionImages.dataSource = self
               self.QuizCollectionImages.delegate   = self
               
//               self.navigationController?.setNavigationBarHidden(true, animated: true)
//               self.navigationItem.setHidesBackButton(true, animated:true);
               self.QuizTitle.text      = Quiz_Title
               self.QuizDegreeFrom.text = String(Quiz_Degree)
               self.QuizDegreeTo.text   = String(Quiz_DegreeMax)
                
               self.getQuizCorrectionData()
        
    }//---- End Of ViewDidLoad -----//
    

    
        func getQuizCorrectionData(){
        
            Spinner = UIViewController.displaySpinner(onView: self.view)
            
            API.getQuizCorrection(vc: self, Token: helper.getUserToken() ?? "", Quiz_Id: self.Quiz_Id)
            { (error,TimeOut, quizCorrectionData) in
                
                UIViewController.removeSpinner(spinner: self.Spinner)
                
                if let err = error {
                    if TimeOut! {
                        let storyboard = UIStoryboard(name: "Auth", bundle: nil)
                        let vc         = storyboard.instantiateViewController(withIdentifier: "SessionExpireVC")
                            vc.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency
                        self.present(vc, animated: true, completion: nil)
                       // self.navigationController?.pushViewController(vc, animated: true)
                        return
                        
                    } else {
                        self.alertmessage(Message: "Failed to fetch quiz correction:\n \(err) \n\n Try Again Later.." )
                        return
                    }
                }
                
                self.responseQuestionModel = quizCorrectionData!
                
                self.QuestionArr = quizCorrectionData?.QuestionList?.map({return CorrectionList(question: $0)}) ?? []
                if self.QuestionArr.count <= 0 {
                    self.showToast(message: "No Quiz Data Available Now !!", vc: self)
                }
                
                
                self.fillNextQuestion(Counter: self.QuestionCounter)
                self.fillQuizCounterCollection()
//                self.QuizTableQuestions.reloadData()
                
            }
            
        }
    
    
//----------------------------------------------------
    
        func fillNextQuestion( Counter: Int ){
 
                if QuestionArr[Counter].QuestionType != 3 { // 1,2: Question is true-false / multi choice
                     
                            self.QuizShortNoteView.isHidden  = true
                            self.QuizTableQuestions.isHidden = false
                            self.QuizShortNoteText.resignFirstResponder()
                            // remove any selection
                            // if let indexPathForSelectedRow = self.QuizTableQuestions.indexPathForSelectedRow  {
                            //    self.QuizTableQuestions.cellForRow(at: indexPathForSelectedRow as IndexPath)?.accessoryType = .none
                            // }
                            
//                            for cell in QuizTableQuestions.visibleCells {
//                                cell.setSelected(false, animated: true)
//                                cell.accessoryType = .none
//                            }
                    
                            
                    
                            for _ in QuizTableQuestions.visibleCells {// check if tableview already have cell ?
                                
                                // remove colored answer in table
                                for (index, _) in  (QuestionArr[self.LastPosition].AllAnswer?.enumerated())! {
                                    
                                    let indexPath = IndexPath(row: index, section: 0)
                                    let cell = QuizTableQuestions.cellForRow(at: indexPath) as! QuizCell
                                    //cell.setSelected(true, animated: true)
                                    cell.answer_name.textColor       = .black
                                    cell.answer_name.backgroundColor = UIColor(hexString: "F2F2F7")
                                    cell.answer_view.backgroundColor = UIColor(hexString: "F2F2F7")
                                }//----------------------------------------
                            }
                    
                     
                            self.QuestionDegreeFrom.text = String(QuestionArr[Counter].StudentDeqree ?? 0.0)
                            self.QuestionDegreeTo.text   = String(QuestionArr[Counter].MaxDegree ?? 0.0)
                            self.QuizQuestionTitle.text  = QuestionArr[Counter].QuestionValue
                            self.QuizQuestionTitleHieght.constant = self.QuizQuestionTitle.retrieveTextHeight() + 30
                     
                            DispatchQueue.main.async {
                                self.QuizTableQuestions.reloadData()
                                self.QuizCollectionImages.reloadData()
                            }
                    
                            
                    
                            //------------------------------------------
                            self.LastPosition = Counter //Last position is the previous question location
                    
                    
                    } else { // 3: Question is Short Note
                                
                                self.QuizShortNoteView.isHidden  = false
                                self.QuizTableQuestions.isHidden = true
                                self.QuizShortNoteText.resignFirstResponder()
                            
                                       self.QuestionDegreeFrom.text = String(QuestionArr[Counter].StudentDeqree ?? 0.0)
                                       self.QuestionDegreeTo.text   = String(QuestionArr[Counter].MaxDegree ?? 0.0)
                                       self.QuizQuestionTitle.text  = QuestionArr[Counter].QuestionValue
                                       self.QuizShortNoteText.text  = QuestionArr[Counter].AllAnswer?[0].Value
                                       self.QuizShortNoteText.isEditable     = false
                                       self.QuizShortNoteText.textColor      = .darkGray
                                       self.QuizQuestionTitleHieght.constant = self.QuizQuestionTitle.retrieveTextHeight() + 30
                    
                                        DispatchQueue.main.async {
                                            self.QuizCollectionImages.reloadData()
                                        }
                                       
                                
                    }
                    
                 
    }//---> End Of fillNextQuestion
            
            
 
    
     
    func fillQuizCounterCollection(){
       
       for (index, _) in QuestionArr.enumerated() {
           
           self.QuestionCounterArr.append(QuizQuestionCounter(id: index, title: String("Q " + "\(index+1)"), status: false))
        DispatchQueue.main.async {
            self.QuizCollectionCounter.reloadData()
        }
           
       }
    }
    
    
    @IBAction func goNextAction(_ sender: Any) {
           
        // check if question is already answered or not ?
           
        print("\n\n QuestionCounter: \(QuestionCounter)")
        print("  QuestionCounter +1: \(QuestionCounter + 1)")
        print("self.QuestionArr.count: \(self.QuestionArr.count) \n\n ")
           
           
        if (QuestionCounter + 1) < self.QuestionArr.count {
               
            self.QuestionCounter += 1
            fillNextQuestion(Counter: self.QuestionCounter )
               
               
        } else {
               
               print("\n\n\n No More Next Questions ")
        }
           
    }
       
    @IBAction func goBackAction(_ sender: Any) {
    
        print("\n\n QuestionCounter: \(QuestionCounter)")
        print("  QuestionCounter -1: \(QuestionCounter - 1)")
        print("self.QuestionArr.count: \(self.QuestionArr.count) \n\n ")
           
        if (QuestionCounter - 1) < 0 {
               
            print("\n\n\n No More Back Questions ")
    
        } else {
            self.QuestionCounter -= 1
            fillNextQuestion(Counter: self.QuestionCounter )
               
        }
           
    }
    
    
    //-------------------------------
   func imageTapped(image:UIImage){
        let newImageView = UIImageView(image: image)
        newImageView.frame = UIScreen.main.bounds
        newImageView.backgroundColor = .black
        newImageView.contentMode = .scaleAspectFit
        newImageView.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.dismissFullscreenImage(_:)))
        newImageView.addGestureRecognizer(tap)
        self.view.addSubview(newImageView)
        self.navigationController?.isNavigationBarHidden = true
        self.tabBarController?.tabBar.isHidden = true
    }

    @objc func dismissFullscreenImage(_ sender: UITapGestureRecognizer) {
        self.navigationController?.isNavigationBarHidden = false
        self.tabBarController?.tabBar.isHidden = false
        sender.view?.removeFromSuperview()
    }
    
    
    
}//----------> End Of Class ---------------

extension QuizCorrectionVC: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
        
        
                func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
                 {
                     if collectionView == QuizCollectionCounter
                     {
                         return CGSize(width: 80.0, height: 70.0)
                     } else {
                         return CGSize(width: 80.0, height: 70.0)
                     }
                  }
        
                func numberOfSections(in collectionView: UICollectionView) -> Int {
                    return 1
                }
        
                func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
                    
                    
                    if collectionView == QuizCollectionCounter {
                        
                        return QuestionCounterArr.count
                        
                        
                    } else {

                        if QuestionArr.count > 0 {
                            
                            if QuestionArr[QuestionCounter].Images!.count > 0 {

                                self.quiz_CollBackView.isHidden       = false
                                self.QuizCollectionImages.isHidden   = false
                                self.quiz_CollBackViewHieght.constant = 80
                                return QuestionArr[QuestionCounter].Images!.count

                            } else {

                                self.quiz_CollBackView.isHidden       = true
                                self.QuizCollectionImages.isHidden   = true
                                self.quiz_CollBackViewHieght.constant = 0
                            }
                            
                        }
                        

                    }
                     
                    return 0
                }
                
                
                func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
                    
                    let cell = UICollectionViewCell()
                    
                    
                    if collectionView == QuizCollectionCounter {
                        
                        let cellCounter  = collectionView.dequeueReusableCell(withReuseIdentifier: "QuizCounterCell",
                                                                              for: indexPath as IndexPath) as! QuizCounterCell
                        
                            cellCounter.CounterName.text = self.QuestionCounterArr[indexPath.row].title
                        
                        if ( self.QuestionCounterArr[indexPath.row].status == true ) {  // true == answered
                                     cellCounter.CounterName.textColor = .white
                                     cellCounter.CounterName.backgroundColor = UIColor(hexString: "008F00")//green
                         } else {
                                     cellCounter.CounterName.textColor = .darkGray
                                     cellCounter.CounterName.backgroundColor = UIColor(hexString: "F2F2F7")
                        }
                            
                        return cellCounter
                        
                        
                    } else {
                        
                        let cellImages  = collectionView.dequeueReusableCell(withReuseIdentifier: "QuizImgsCell", for: indexPath as IndexPath) as! QuizImgsCell
                        
                            cellImages.QuestionImage.addSubview(cellImages.activityIndicator)
                            cellImages.activityIndicator.startAnimating()
                            cellImages.QuestionImage.sd_setImage(with: URL(string:  URLs.ImageDomain + "attachments/QuestionsImages/" + String((QuestionArr[QuestionCounter].Images?[indexPath.row])!)), placeholderImage: UIImage(named: "open-book"))
                            cellImages.activityIndicator.stopAnimating()
                        return cellImages
                        
                    }
                    
                
                    return cell
                    
                }
                
                
                func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
                    
                    if collectionView == QuizCollectionCounter {
                                       
                       self.QuestionCounter = indexPath.row
                       fillNextQuestion(Counter: self.QuestionCounter )
                                       
                    } else {
                    
                             let cellImages = collectionView.cellForItem(at: indexPath) as! QuizImgsCell
                             self.imageTapped(image: cellImages.QuestionImage.image!)
                        
                    }
       
                }
        
        
        
        
}//---------> End Of CollecyionViewDelegate ----------------------



extension QuizCorrectionVC: UITableViewDataSource, UITableViewDelegate {
        
//             func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//                 return UITableView.automaticDimension + 50
//            }
        
             func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
 
                if QuestionArr.count > 0 {
                     return QuestionArr[QuestionCounter].AllAnswer?.count ?? 0
                 }else {
                     return 0
                }
                 
             }
            
        
             func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
                
                    let  cell = tableView.dequeueReusableCell(withIdentifier: "QuizCell", for: indexPath) as! QuizCell
                         cell.answer_name.text = QuestionArr[QuestionCounter].AllAnswer?[indexPath.row].Value
                
                
                    if QuestionArr[QuestionCounter].StudentAnswer == QuestionArr[QuestionCounter].AnswerValue && (QuestionArr[QuestionCounter].AllAnswer?[indexPath.row].IsCorectAnswer == true) {
                        
                        cell.answer_name.textColor       = .white
                        cell.answer_name.backgroundColor = UIColor(hexString: "008F00")
                        cell.answer_view.backgroundColor = UIColor(hexString: "008F00")
                        //cell.backgroundColor = UIColor(hexString: "008F00")
                     
                    } else {
                        
                        if ( QuestionArr[QuestionCounter].AllAnswer?[indexPath.row].IsCorectAnswer == true )
                        {
                            
                            
                            cell.answer_name.textColor       = .white
                            cell.answer_name.backgroundColor = UIColor(hexString: "008F00")
                            cell.answer_view.backgroundColor = UIColor(hexString: "008F00")
                            //cell.backgroundColor = UIColor(hexString: "008F00")
                            
                        } else if QuestionArr[QuestionCounter].StudentAnswer == QuestionArr[QuestionCounter].AllAnswer?[indexPath.row].Value {
                            
                            cell.answer_name.textColor       = .white
                            cell.answer_name.backgroundColor = UIColor(hexString: "ff4040")
                            cell.answer_view.backgroundColor = UIColor(hexString: "ff4040")
                            //cell.backgroundColor = UIColor(hexString: "ff4040")
                            
                        }
                        
                     }
                 
                  return cell
                }
            
            
            //------> For single choice selection
//               func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//
//
//                    for cell in QuizTableQuestions.visibleCells {
//                        cell.setSelected(false, animated: true)
//                        cell.accessoryType = .none
//                    }
//
//                   tableView.cellForRow(at: indexPath as IndexPath)?.accessoryType = .checkmark
//
//    //               self.SelectedAnswerId = (QuestionArr[QuestionCounter].answers?[indexPath.row].id)!
//    //               self.SelectedAnswerQ  = (QuestionArr[QuestionCounter].id)!
//
////                    self.saveAnswerToModel(quesionId:     QuestionArr[QuestionCounter].id!,
////                                           answerId:      QuestionArr[QuestionCounter].answers![indexPath.row].id!,
////                                           AnswerArticle: "")
//
//               }
//
//               func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
//                   tableView.cellForRow(at: indexPath as IndexPath)?.accessoryType = .none
//               }
        
        
}




extension QuizCorrectionVC: UITextViewDelegate {
        
        public func textViewDidBeginEditing(_ textView: UITextView) {
            if textView.text ==  "Add your answer here..." {
                QuizShortNoteText.text = ""
                QuizShortNoteText.textColor = .darkGray

            }
            textView.becomeFirstResponder()
        }
     
        func textViewDidChange(_ textView: UITextView) {
            
            if textView.text ==  "" {
                
                print("\n empty")
//                saveAnswerToModel(quesionId: QuestionArr[QuestionCounter].id!,
//                                  answerId: QuestionArr[QuestionCounter].answers![0].id!,
//                                  AnswerArticle: "")
                
            } else {
                
                print("\n not empty")
//                saveAnswerToModel(quesionId: QuestionArr[QuestionCounter].id!,
//                                  answerId:  QuestionArr[QuestionCounter].answers![0].id!,
//                                  AnswerArticle: textView.text)
                
            }
            
        }

        
}
