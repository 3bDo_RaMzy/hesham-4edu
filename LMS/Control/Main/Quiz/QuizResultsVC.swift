//
//  QuizResultsVC.swift
//  LMS
//
//  Created by Abdo on 12/11/19.
//  Copyright © 2019 eaglez-group. All rights reserved.
//

import UIKit

class QuizResultsVC: BaseVC {

    //------------------------
    
    //--> Date Choise
    @IBOutlet weak var dateFromButton: UIButton!
    @IBOutlet weak var dateToButton:   UIButton!
    @IBOutlet var      dateFromView:   UIView!
    @IBOutlet var      dateToView:     UIView!
    @IBOutlet weak var dateFromPicker: UIDatePicker!
    @IBOutlet weak var dateToPicker:   UIDatePicker!
    @IBOutlet weak var dateFromTxt:    UILabel!
    @IBOutlet weak var dateToTxt:      UILabel!
    
    @IBOutlet weak var bgView: UIView!
    
    @IBOutlet weak var QuizResultsTable: UITableView!
    var quizResultViewModel = [QuizResultViewModel]()
    
    var dateFrom             = ""
    var dateTo               = ""
    let dateFormatterView    = DateFormatter()
    
    override func viewDidLoad() {
        super.viewDidLoad()

             self.QuizResultsTable.dataSource     = self
             self.QuizResultsTable.delegate       = self
             self.QuizResultsTable.layoutMargins  = UIEdgeInsets.zero
             self.QuizResultsTable.separatorInset = UIEdgeInsets.zero
             self.QuizResultsTable.rowHeight      = 100

            //-------------------------------
            //---> For DatePicker / Date
            dateFromPicker.date      = NSDate.init(timeIntervalSinceNow: 0) as Date
            dateFormatterView.dateFormat = "dd/MM/yyyy"
            dateFromTxt.text = String(dateFormatterView.string(from: dateFromPicker.date))
            dateFrom = String(dateFormatterView.string(from: dateFromPicker.date))
            
            dateToPicker.date      = NSDate.init(timeIntervalSinceNow: 0) as Date
            dateFormatterView.dateFormat = "dd/MM/yyyy"
            dateToTxt.text = String(dateFormatterView.string(from: dateToPicker.date))
            dateTo = String(dateFormatterView.string(from: dateToPicker.date))
        
         }//------------ End Of ViewDidLoad ---------------------
    
    @IBAction func showDateFromView(_ sender: Any) {
        
        inAnimateShowDateFrom()
    }
    @IBAction func showDateToView(_ sender: Any) {
        
        inAnimateShowDateTo()
    }
    @IBAction func removeDateFromView(_ sender: Any) {
        
        dateFormatterView.dateFormat = "dd/MM/yyyy"
        dateFromTxt.text = String(dateFormatterView.string(from: dateFromPicker.date))
        dateFrom = String(dateFormatterView.string(from: dateFromPicker.date))
        
        outAnimateShowDateFrom()
    }
    @IBAction func removeDateToView(_ sender: Any) {
        
        dateFormatterView.dateFormat = "dd/MM/yyyy"
        dateToTxt.text = String(dateFormatterView.string(from: dateToPicker.date))
        dateTo = String(dateFormatterView.string(from: dateToPicker.date))
        
        outAnimateShowDateTo()
    }
    
    //--------------------------------------------------------------------------------
    func inAnimateShowDateFrom() {
        self.bgView.isUserInteractionEnabled = false
        self.dateToView.removeFromSuperview()
        self.view.addSubview(dateFromView)
        dateFromView.center    = self.view.center
        dateFromView.transform = CGAffineTransform.init(scaleX: 1.3, y: 1.3)
        dateFromView.alpha     = 0
        UIView.animate(withDuration: 0.4) {
            self.dateFromView.alpha     = 1
            self.dateFromView.transform = CGAffineTransform.identity
        }
    }
    func outAnimateShowDateFrom() {
        self.bgView.isUserInteractionEnabled = true
        UIView.animate(withDuration: 0.3, animations: {
            self.dateFromView.transform = CGAffineTransform.init(scaleX: 1.3, y: 1.3)
            self.dateFromView.alpha     = 0
        }) { (success:Bool) in
            self.dateFromView.removeFromSuperview()
        }
    }//--------------------------------------------
    func inAnimateShowDateTo() {
        self.bgView.isUserInteractionEnabled = false
        self.dateFromView.removeFromSuperview()
        self.view.addSubview(dateToView)
        dateToView.center    = self.view.center
        dateToView.transform = CGAffineTransform.init(scaleX: 1.3, y: 1.3)
        dateToView.alpha     = 0
        UIView.animate(withDuration: 0.4) {
            self.dateToView.alpha     = 1
            self.dateToView.transform = CGAffineTransform.identity
        }
    }
    func outAnimateShowDateTo() {
        self.bgView.isUserInteractionEnabled = true
        UIView.animate(withDuration: 0.3, animations: {
            self.dateToView.transform = CGAffineTransform.init(scaleX: 1.3, y: 1.3)
            self.dateToView.alpha     = 0
        }) { (success:Bool) in
            self.dateToView.removeFromSuperview()
        }
    }//--------------------------------------------
    

    @IBAction func showResultAction(_ sender: Any) {
        
        if dateFrom == "" || dateTo == "" {
            
            self.alertmessage(Message: "Please, choose your search dates")
            
        }else {
        
            if ParentStudentsVC.StudentId == 0 {
                
                API.sendStudentQuizResult(vc: self, Token:  helper.getUserToken() ?? "",
                                          from: dateFrom, to: dateTo  ) { ( err, TimeOut, results ) in
                                        
                        
                        if let err = err {
                            if TimeOut! {
                                let storyboard = UIStoryboard(name: "Auth", bundle: nil)
                                let vc         = storyboard.instantiateViewController(withIdentifier: "SessionExpireVC")
                                    vc.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency
                                self.present(vc, animated: true, completion: nil)
                               // self.navigationController?.pushViewController(vc, animated: true)
                                return
                                
                            } else {
                                self.alertmessage(Message: "Failed to fetch quiz result:\n \(err) \n\n Try Again Later.." )
                                return
                            }
                        }
                                        
                        self.quizResultViewModel = results?.map({return QuizResultViewModel(q_result: $0)}) ?? []
//                        self.QuizResultsTable.reloadData()
                                            DispatchQueue.main.async {
                                                self.QuizResultsTable.reloadData()
                                            }
                                            
                    }
                
            } else {
                
                API.getParentStudentQuizResult(vc: self, Token:  helper.getUserToken() ?? "",
                                                from: dateFrom, to: dateTo, studentId: ParentStudentsVC.StudentId   )
                { ( err,TimeOut, results ) in
                                                       
                                       
                                             if let err = err {
                                                 if TimeOut! {
                                                     let storyboard = UIStoryboard(name: "Auth", bundle: nil)
                                                     let vc         = storyboard.instantiateViewController(withIdentifier: "SessionExpireVC")
                                                         vc.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency
                                                     self.present(vc, animated: true, completion: nil)
                                                    // self.navigationController?.pushViewController(vc, animated: true)
                                                     return
                                                     
                                                 } else {
                                                     self.alertmessage(Message: "Failed to fetch quiz result:\n \(err) \n\n Try Again Later.." )
                                                     return
                                                 }
                                             }
                                        
                                                       
                                       self.quizResultViewModel = results?.map({return QuizResultViewModel(q_result: $0)}) ?? []
                                                    
                                                    DispatchQueue.main.async {
                                                        self.QuizResultsTable.reloadData()
                                                        // your other code here
                                                    }
                                       
                                   }
            }
            
            
        }
        
        
    }
    
 
}//---- End Of Class ------------------------------------

extension QuizResultsVC: UITableViewDataSource, UITableViewDelegate {
    
    
    
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

            return quizResultViewModel.count
        }
    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
            let cell = tableView.dequeueReusableCell(withIdentifier: "QuizResultCell", for: indexPath) as! QuizResultCell
        cell.quizResultViewModel = quizResultViewModel[indexPath.row]
         return cell
        }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        if ParentStudentsVC.StudentId == 0 {
            
            if self.quizResultViewModel[indexPath.row].QuizeId != 0 {

                // Do something now
                let storyboard            = UIStoryboard(name: "Main", bundle: nil)
                let vc                    = storyboard.instantiateViewController(withIdentifier: "QuizCorrectionVC") as! QuizCorrectionVC
                //    vc.modalPresentationStyle = .overFullScreen
                vc.modalTransitionStyle   = .partialCurl

                vc.Quiz_Id        = self.quizResultViewModel[indexPath.row].QuizeId
                vc.Quiz_Title     = self.quizResultViewModel[indexPath.row].quizTitle
                vc.Quiz_Degree    = self.quizResultViewModel[indexPath.row].degree
                vc.Quiz_DegreeMax = self.quizResultViewModel[indexPath.row].maxdegree

                //self.present(vc, animated: true, completion: nil)
                //self.show(vc, sender: nil)
                self.navigationController?.pushViewController(vc, animated: true)

            }
            
        }
        
     }
 }
