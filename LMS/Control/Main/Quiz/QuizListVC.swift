//
//  QuizListVC.swift
//  LMS
//
//  Created by Abdo on 12/5/19.
//  Copyright © 2019 eaglez-group. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class QuizListVC: BaseVC {

            //--> variables
            var Spinner :UIView!
    
            @IBOutlet weak var QuizListTable: UITableView!
            var quizViewModel = [QuizViewModel]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.title = "Quiz List"
                                       
        self.QuizListTable.dataSource     = self
        self.QuizListTable.delegate       = self
        self.QuizListTable.layoutMargins  = UIEdgeInsets.zero
        self.QuizListTable.separatorInset = UIEdgeInsets.zero
        self.QuizListTable.rowHeight      = 100
        
        
        
    }//---> End Of ViewDidLoad ------
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.fetchData()
    }
    
    //------------------------------
    fileprivate func fetchData() {
        
        Spinner = UIViewController.displaySpinner(onView: self.view)

        API.getQuizList(vc: self, Token: helper.getUserToken() ?? "" ) { ( err,TimeOut, videos ) in

            UIViewController.removeSpinner(spinner: self.Spinner)
            
            if let err = err {
                if TimeOut! {
                    let storyboard = UIStoryboard(name: "Auth", bundle: nil)
                    let vc         = storyboard.instantiateViewController(withIdentifier: "SessionExpireVC")
                        vc.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency
                    self.present(vc, animated: true, completion: nil)
                   // self.navigationController?.pushViewController(vc, animated: true)
                    return
                    
                } else {
                    self.alertmessage(Message: "Failed to fetch quiz list:\n \(err) \n\n Try Again Later.." )
                    return
                }
            }
            

            self.quizViewModel = videos?.map({return QuizViewModel(quiz: $0)}) ?? []
            if self.quizViewModel.count <= 0 {
                self.showToast(message: "No Quiz Available Now !!", vc: self)
            }
            
            DispatchQueue.main.async {
                self.QuizListTable.reloadData()
                // your other code here
            }
            
        }
         
    
        
        
         
    }//---> End Of fetchData method -------
    

}//-----> End Of Class -----------

extension QuizListVC: UITableViewDataSource, UITableViewDelegate {
    
    
         func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

                return quizViewModel.count
            }
        
         func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            
                let cell = tableView.dequeueReusableCell(withIdentifier: "QuizListCell", for: indexPath) as! QuizListCell
            cell.quizViewModel = quizViewModel[indexPath.row]
             return cell
            }
        
        
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
                  
//            // Do something now
//            let storyboard            = UIStoryboard(name: "Main", bundle: nil)
//            let vc                    = storyboard.instantiateViewController(withIdentifier: "QuizVC") as! QuizVC
//            //    vc.modalPresentationStyle = .overFullScreen
//                vc.modalTransitionStyle   = .crossDissolve
//                vc.Quiz_Id                = self.quizViewModel[indexPath.row].id
//                vc.QuestionArr            = self.quizViewModel[indexPath.row].questions
//                vc.Quiz_Title             = self.quizViewModel[indexPath.row].title
//                vc.Quiz_Time              = self.quizViewModel[indexPath.row].duration * 60 //---> send time in seconds
//            //self.present(vc, animated: true, completion: nil)
//            self.show(vc, sender: nil)
            
            
            
            // Do something now
            let storyboard            = UIStoryboard(name: "Main", bundle: nil)
            let vc                    = storyboard.instantiateViewController(withIdentifier: "Quiz2VC") as! Quiz2VC
            //    vc.modalPresentationStyle = .overFullScreen
                vc.modalTransitionStyle   = .crossDissolve
                vc.Quiz_Id                = self.quizViewModel[indexPath.row].id
                vc.QuestionArr            = self.quizViewModel[indexPath.row].questions
                vc.Quiz_Title             = self.quizViewModel[indexPath.row].title
                vc.Quiz_Time              = self.quizViewModel[indexPath.row].duration * 60 //---> send time in seconds
            //self.present(vc, animated: true, completion: nil)
            self.show(vc, sender: nil)
            
            
            
            
    }
    
    
}
