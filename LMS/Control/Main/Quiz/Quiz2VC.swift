//
//  Quiz2VC.swift
//  LMS
//
//  Created by MacBook on 6/3/20.
//  Copyright © 2020 eaglez-group. All rights reserved.
//

import UIKit
import SDWebImage
import Alamofire
import SwiftyJSON


class Quiz2VC: BaseVC {

    @IBOutlet weak var QuizHeaderHieght:        NSLayoutConstraint!
    @IBOutlet weak var QuizTitle:               UILabel!
    @IBOutlet weak var QuizTime:                UILabel!
    @IBOutlet weak var QuizCountFrom:           UILabel!
    @IBOutlet weak var QuizCountTo:             UILabel!
    @IBOutlet weak var QuizCollectionCounter:   UICollectionView!
      
    @IBOutlet weak var quiz_CollBackView:       UIView!
    @IBOutlet weak var quiz_CollBackViewHieght: NSLayoutConstraint!
    @IBOutlet weak var QuizQuestionTitle:       UILabel!
    @IBOutlet weak var QuizQuestionTitleHieght: NSLayoutConstraint!
    @IBOutlet weak var QuizCollectionImages:    UICollectionView!
    @IBOutlet weak var QuizTableQuestions:      UITableView!
    @IBOutlet weak var QuizBtnNext:             UIButton!
    @IBOutlet weak var QuizBtnPrevious:         UIButton!
    @IBOutlet weak var QuizBtnSubmit:           UIButton!
    @IBOutlet weak var QuizShortNoteView:       UIView!
    @IBOutlet weak var QuizShortNoteText:       UITextView!
    
    
    var Quiz_Id:             Int          = 0
    var Quiz_Title:          String       = ""
    var Quiz_Time:           Int          = 0
    var QuestionArr:         [Question]   = []      //---> for all quiz questions
    var QuizAnswerArr =      [QuizQuestionAnswer]() //---> for all quiz answer
    var QuestionCounterArr = [QuizQuestionCounter]()             //---> for showing
    
    
    //---> Timer
    var countdownTimer: Timer!
    var QuestionCounter  = 0
    var SelectedAnswerId = 0
    var SelectedAnswerQ  = 0
     
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        hideKeyboardWhenTappedAround()
        
        if UIDevice().userInterfaceIdiom == .phone {
        switch UIScreen.main.nativeBounds.height {
            case 1136, 1334, 1920, 2208:
                print("iPhone 5 or 5S or 5C | 6/6S/7/8 | 6+/6S+/7+/8+ ")
                self.QuizHeaderHieght.constant = 60
            case 2436, 2688, 1792:
                print("iPhone X/XS/11 Pro  |  XS Max/11 Pro Max  |  XR/ 11")
                self.QuizHeaderHieght.constant = 80
            default:
                self.QuizHeaderHieght.constant = 80
            }
        }
        
        self.QuizShortNoteView.isHidden  = true
        self.QuizTableQuestions.isHidden = false
        self.QuizBtnSubmit.isHidden      = true
        
        QuizShortNoteText.delegate   = self
        QuizShortNoteText.text       =  "Add your answer here..."
        QuizShortNoteText.textColor  = .lightGray
        
        self.QuizTableQuestions.dataSource     = self
        self.QuizTableQuestions.delegate       = self
        self.QuizTableQuestions.layoutMargins  = UIEdgeInsets.zero
        self.QuizTableQuestions.separatorInset = UIEdgeInsets.zero
        //self.QuizTableQuestions.rowHeight      = 60
        
        //self.QuizTableQuestions.estimatedRowHeight = 60.0
        //self.QuizTableQuestions.rowHeight = UITableView.automaticDimension
        
        self.QuizTableQuestions.estimatedRowHeight = 60.0
        self.QuizTableQuestions.rowHeight = UITableView.automaticDimension
        
        
        if let layout = QuizCollectionCounter.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.scrollDirection = .horizontal
        }
        
        self.QuizCollectionCounter.dataSource = self
        self.QuizCollectionCounter.delegate   = self
 
        self.QuizCollectionImages.dataSource = self
        self.QuizCollectionImages.delegate   = self
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.navigationItem.setHidesBackButton(true, animated:true);
        self.QuizTitle.text      = Quiz_Title
          
        self.fillNextQuestion(Counter: self.QuestionCounter)
        //--> Timer
        self.startTimer()
        self.fillQuizCounterCollection()
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(closeQuizAction(_:)),
        name: Notification.Name("closeQuiz"),
        object: nil)
        
        
    }//-------> End Of ViewDidLoad -----------
    

    @objc func closeQuizAction(_ notification:Notification) {
        // Do something now
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    //-----------------------------------------------
    //--- Start Timer ---//
        func startTimer() {
            countdownTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
        }
        
        @objc func updateTime() {
            QuizTime.text = "\(timeFormatted(Quiz_Time))"
            
            if Quiz_Time != 0 {
                Quiz_Time -= 1
                
            } else {
                
                let alert = UIAlertController(title: "ooops!", message: "Time is up", preferredStyle: .alert)

                alert.addAction(UIAlertAction(title: "Ok", style: .destructive, handler: { (UIAlertAction) in
                    
                    self.endTimer()
                    
                }))

                self.present(alert, animated: true)
                
                
             }
        }
        
        func endTimer() {
            countdownTimer.invalidate()
            print("\n\n Time is up \n\n")
            self.uploadQuizAnswer()
 
            
        }//--> End of timer out ------------------------------------------------------------------------
        
        func timeFormatted(_ totalSeconds: Int) -> String {
            let seconds: Int = totalSeconds % 60
            let minutes: Int = (totalSeconds / 60) % 60
            //     let hours: Int = totalSeconds / 3600
            return String(format: "%02d:%02d", minutes, seconds)
        }
    //--- End Of Timer ---//
    //-----------------------------------------------
    
    
    
    func fillNextQuestion( Counter: Int ){
        
         
        if let _ = QuizAnswerArr.filter({ $0.quesionId == QuestionArr[Counter].id }).first
        {
            // add the existed question answer using question id ..
            print("\n\n\n found")

            if QuestionArr[Counter].type != "3" { // 1,2: Question is true-false / multi choice
                 
                                    self.QuizShortNoteView.isHidden  = true
                                    self.QuizTableQuestions.isHidden = false
                                    self.QuizShortNoteText.resignFirstResponder()
                                    // remove any selection
                                    // if let indexPathForSelectedRow = self.QuizTableQuestions.indexPathForSelectedRow  {
                                    //    self.QuizTableQuestions.cellForRow(at: indexPathForSelectedRow as IndexPath)?.accessoryType = .none
                                    // }
                                    
                                    for cell in QuizTableQuestions.visibleCells {
                                        cell.setSelected(false, animated: true)
                                        cell.accessoryType = .none
                                    }
                             
                                    self.QuizCountFrom.text      = String(Counter + 1)
                                    self.QuizCountTo.text        = String(QuestionArr.count)
                                    //self.quiz_time.text        = String(Quiz_Time)
                                    self.QuizQuestionTitle.text  = QuestionArr[Counter].title
                                    self.QuizQuestionTitleHieght.constant = self.QuizQuestionTitle.retrieveTextHeight() + 30
                             
                                    self.QuizTableQuestions.reloadData()
                                    self.QuizCollectionImages.reloadData()
                            
                                    //-------------------------------------------
                                    // get selected answer id
                                    let selectedAnswerId = QuizAnswerArr.filter({$0.quesionId ==  QuestionArr[Counter].id }).first?.answerId
                                    
                                    // mark seleted answer in table
                                    for (index, _) in (QuestionArr[QuestionCounter].answers?.enumerated())! {
                                        
                                         if  selectedAnswerId == QuestionArr[QuestionCounter].answers?[index].id {
                                            
                                            let indexPath = IndexPath(row: index, section: 0)
                                            let cell = QuizTableQuestions.cellForRow(at: indexPath) as! QuizCell
                                                cell.setSelected(true, animated: true)
                                                cell.accessoryType = .checkmark
                                            
                                        } else {
                                            
                                            let indexPath = IndexPath(row: index, section: 0)
                                            let cell = QuizTableQuestions.cellForRow(at: indexPath) as! QuizCell
                                                cell.setSelected(false, animated: true)
                                                cell.accessoryType = .none
                                        }
                                    }//----------------------------------------
                
                } else { // 3: Question is Short Note
                    
                    self.QuizShortNoteView.isHidden  = false
                    self.QuizTableQuestions.isHidden = true
                    self.QuizShortNoteText.resignFirstResponder()
                
                           self.QuizCountFrom.text      = String(Counter + 1)
                           self.QuizCountTo.text        = String(QuestionArr.count)
                           //self.quiz_time.text        = String(Quiz_Time)
                           self.QuizQuestionTitle.text  = QuestionArr[Counter].title
                           self.QuizQuestionTitleHieght.constant = self.QuizQuestionTitle.retrieveTextHeight() + 30
                           self.QuizCollectionImages.reloadData()
                
                           //self.QuizShortNoteText.becomeFirstResponder()
                           self.QuizShortNoteText.text = QuizAnswerArr.filter({$0.quesionId ==  QuestionArr[Counter].id }).first?.ArticleAnswer
                    
                }
             
        } else { //---> next question is not answered before !!
                   
                   print("\n\n\n not found")
            
                    if QuestionArr[Counter].type != "3" { // 1,2: Question is true-false / multi choice
                    
                                self.QuizShortNoteView.isHidden  = true
                                self.QuizTableQuestions.isHidden = false
//                                self.QuizShortNoteText.resignFirstResponder()
//                                self.QuizShortNoteText.text       =  "Add your answer here..."
//                                self.QuizShortNoteText.textColor  = .lightGray
                        
                               //if let indexPathForSelectedRow = self.QuizTableQuestions.indexPathForSelectedRow  {

                               //    self.QuizTableQuestions.deselectRow(at: indexPathForSelectedRow, animated: true)
                               //    self.QuizTableQuestions.cellForRow(at: indexPathForSelectedRow as IndexPath)?.accessoryType = .none
                               //}
                                for cell in QuizTableQuestions.visibleCells {
                                    cell.setSelected(false, animated: true)
                                    cell.accessoryType = .none
                                }
                        
                        
                               self.QuizCountFrom.text      = String(Counter + 1)
                               self.QuizCountTo.text        = String(QuestionArr.count)
                               //self.quiz_time.text        = String(Quiz_Time)
                               self.QuizQuestionTitle.text  = QuestionArr[Counter].title
                               self.QuizQuestionTitleHieght.constant = self.QuizQuestionTitle.retrieveTextHeight() + 30
                               self.QuizTableQuestions.reloadData()
                               self.QuizCollectionImages.reloadData()
                    
                    } else { // 3: Question is Short Note
                    
                        
                                   self.QuizShortNoteView.isHidden  = false
                                   self.QuizTableQuestions.isHidden = true
                                   self.QuizShortNoteText.resignFirstResponder()
                                   self.QuizShortNoteText.text       =  "Add your answer here..."
                                   self.QuizShortNoteText.textColor  = .lightGray
                        
                                   self.QuizCountFrom.text      = String(Counter + 1)
                                   self.QuizCountTo.text        = String(QuestionArr.count)
                                   //self.quiz_time.text        = String(Quiz_Time)
                                   self.QuizQuestionTitle.text  = QuestionArr[Counter].title
                                   self.QuizQuestionTitleHieght.constant = self.QuizQuestionTitle.retrieveTextHeight() + 30
                                   self.QuizCollectionImages.reloadData()
                        
                                   //self.QuizShortNoteText.becomeFirstResponder()
                    }
            
                  
                   
            
        }
        
        
    }
    
    func fillQuizCounterCollection(){
        
        for (index, _) in QuestionArr.enumerated() {
            
            self.QuestionCounterArr.append(QuizQuestionCounter(id: index, title: String("Q " + "\(index+1)"), status: false))
            self.QuizCollectionCounter.reloadData()
        }
     }
    
    
    
    @IBAction func goNextAction(_ sender: Any) {
        
        // check if question is already answered or not ?
        
        
        print("\n\n QuestionCounter: \(QuestionCounter)")
        print("  QuestionCounter +1: \(QuestionCounter + 1)")
        print("self.QuestionArr.count: \(self.QuestionArr.count) \n\n ")
        
        
        if (QuestionCounter + 1) < self.QuestionArr.count {
            
            self.QuestionCounter += 1
            fillNextQuestion(Counter: self.QuestionCounter )
            
            
        } else {
            
            print("\n\n\n No More Next Questions ")
        }
        
     }
    
    @IBAction func goBackAction(_ sender: Any) {
 
        print("\n\n QuestionCounter: \(QuestionCounter)")
        print("  QuestionCounter -1: \(QuestionCounter - 1)")
        print("self.QuestionArr.count: \(self.QuestionArr.count) \n\n ")
        
        if (QuestionCounter - 1) < 0 {
            
            print("\n\n\n No More Back Questions ")
 
        } else {
            self.QuestionCounter -= 1
            fillNextQuestion(Counter: self.QuestionCounter )
            
        }
        
    }
    
    
    
    func saveAnswerToModel( quesionId: Int, answerId: Int, AnswerArticle: String = "" ){
        
        if QuestionArr[QuestionCounter].type != "3" { // 1,2: Question is true-false / multi choice
        
                if let _ = QuizAnswerArr.filter({ $0.quesionId == quesionId }).first
                {
                    // update the existed question answer using question id ..
                    print("\n\n\nfound")
                    QuizAnswerArr.filter({$0.quesionId ==  quesionId}).first?.answerId      = answerId
                    QuizAnswerArr.filter({$0.quesionId ==  quesionId}).first?.ArticleAnswer = AnswerArticle
                    
                } else { // add the answer if it is not addes before
                    
                    print("\n\n\n not found")
                    QuizAnswerArr.append(QuizQuestionAnswer(quesionId:     quesionId,
                                                            answerId:      answerId,
                                                            ArticleAnswer: AnswerArticle))
                  }
                
                self.QuestionCounterArr[QuestionCounter].status = true  // update counter collection status with selected for item
                self.QuizCollectionCounter.reloadData()
        
            
        } else {
        
            
                if let _ = QuizAnswerArr.filter({ $0.quesionId == quesionId }).first
                {
                    // update the existed question answer using question id ..
                    print("\n\n\nfound")
                    QuizAnswerArr.filter({$0.quesionId ==  quesionId}).first?.answerId      = answerId
                    QuizAnswerArr.filter({$0.quesionId ==  quesionId}).first?.ArticleAnswer = AnswerArticle
                    
                } else { // add the answer if it is not addes before
                    
                    print("\n\n\n not found")
                    QuizAnswerArr.append(QuizQuestionAnswer(quesionId:     quesionId,
                                                            answerId:      answerId,
                                                            ArticleAnswer: AnswerArticle))
                  }
                
                if AnswerArticle == ""{
                    
                    self.QuestionCounterArr[QuestionCounter].status = false
                    self.QuizCollectionCounter.reloadData()
                    
                } else {
                    
                    self.QuestionCounterArr[QuestionCounter].status = true
                    self.QuizCollectionCounter.reloadData()
                    
                }
            }
        
        //--------------------------------
        //Check if all questions are answered ?!!
            var notAnswered = 0
            for item in QuestionCounterArr {
                if item.status == false {
                    notAnswered += 1
                }
            }
             if notAnswered > 0 {
                self.QuizBtnSubmit.isHidden = true
            } else {
                self.QuizBtnSubmit.isHidden = false
            }
        //-------------------------------
         
    }
    
    
    
    //--------------------------------------------------------------------------------
         func uploadQuizAnswer(){
             
//             API.sendStudentQuiz(vc: self,             Token: helper.getUserToken() ?? "",
//                                 quizId: self.Quiz_Id, quizAnswers: self.QuizAnswerArr){
                 
             let serviceArray =  [
                                     "answerId": 0,
                                     "quesionId": 0,
                                     "ArticleAnswer": "" ] as [String : Any] //ArticleAnswer
             
             var servicesArray =  [  serviceArray  ] as [[String : Any]]
                 servicesArray.removeAll()
             
             
             for (_, model) in self.QuizAnswerArr.enumerated() {
                                 
                 let serviceArray =  [
                                         "answerId": model.answerId,
                                         "quesionId": model.quesionId,
                                         "ArticleAnswer": model.ArticleAnswer ] as [String : Any]

                 servicesArray.append(serviceArray)

             }
             
             let paramsData =  [
                                 "id": self.Quiz_Id,
                                 "questions": servicesArray  ] as [String : Any]
            
            let url = URLs.sendStudentQuiz
            
            self.createRequest(token: helper.getUserToken() ?? "", parameters: paramsData, serviceUrl: url)
             
         }
    //--------------------------------------------------------------------------------
    
            func createRequest(token:       String,
                                       parameters: Dictionary<String, Any>,
                                       serviceUrl: String ) {
                        
                        var Spinner:           UIView!
                        let apiURL          =  URL(string:serviceUrl)!
                        var mutableRequest  =  URLRequest(url: apiURL)
                        let authorizedToken =  token
                       
                       // print(authorizedToken)
                        mutableRequest.httpMethod = "POST"
                        mutableRequest.timeoutInterval = TimeInterval(MAXFLOAT)//
                        mutableRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
                        //mutableRequest.addValue(Prefs.getLng(), forHTTPHeaderField: "Lang")
                        mutableRequest.addValue(authorizedToken, forHTTPHeaderField: "Authorization")
                        
                        do {
                            
                            mutableRequest.httpBody = try JSONSerialization.data(withJSONObject: parameters)//, options: JSONSerialization.WritingOptions())
                            
                        } catch {
                            // No-op
                            print("catechecexp")
                        }
                        
                        
                        
                        Spinner = UIViewController.displaySpinner(onView: self.view)
                        _ = AF.request(mutableRequest as URLRequestConvertible).responseJSON {
                            
                        (response) in
                            
                            
                            UIViewController.removeSpinner(spinner: Spinner)
                            print("\n\n Response: \(response.value)")
                            
                            switch response.result {
                                
                            case .failure(let error):
                                        
                                
                                   if (response.response?.statusCode == 401) {
                                    
                                       let storyboard = UIStoryboard(name: "Auth", bundle: nil)
                                       let vc         = storyboard.instantiateViewController(withIdentifier: "SessionExpireVC")
                                           vc.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency
                                       self.present(vc, animated: true, completion: nil)
                                       // self.navigationController?.pushViewController(vc, animated: true)
                                       return
                                    
                                   } else {
                                    
                                       self.alertmessage(Message: "Failed to send quiz solutions to server:\n \(error) \n\n Try Again Later.." )
                                       return
                                    
                                   }
                                
                                
                            case .success(let value):
                                
                                        //vc.dismiss(animated: true, completion: nil)
                                        let json = JSON(value)
                                        print("\n\n\nLogin_success:>>>>>>>>>>>>>>>>> \(value)\n")
                                        
                                        let message   = json["Status"].string     // Completed  /
                                        let degree    = json["degree"].double    ?? 0.0
                                        let maxdegree = json["maxdegree"].double ?? 0.0
                                        print("\n\n Replay Degree:\(degree) | Maxdegree: \(maxdegree)")
                                        
                                        if message == "Completed"{
                                            
                                            let storyboard            = UIStoryboard(name: "Main", bundle: nil)
                                            let vc                    = storyboard.instantiateViewController(withIdentifier: "FinishQuizVC") as! FinishQuizVC
                                                vc.modalPresentationStyle = .overFullScreen
                                                vc.modalTransitionStyle   = .crossDissolve
                                                vc.resultDegree = degree
                                                vc.resultMaxDegree = maxdegree
                                            self.present(vc, animated: true, completion: nil)
                                            //self.show(vc, sender: nil)
                                            
                                        } else {
                                            
                                            let storyboard            = UIStoryboard(name: "Main", bundle: nil)
                                            let vc                    = storyboard.instantiateViewController(withIdentifier: "FinishQuizVC2") as! FinishQuizVC
                                                vc.modalPresentationStyle = .overFullScreen
                                                vc.modalTransitionStyle   = .crossDissolve 
                                            self.present(vc, animated: true, completion: nil)
                                            //self.show(vc, sender: nil)
                                        }
                                        
                                        

                             }
                         
                        }
                  
            }
    //--------------------------------------------------------------------------------
    
    func imageTapped(image:UIImage){
        let newImageView = UIImageView(image: image)
        newImageView.frame = UIScreen.main.bounds
        newImageView.backgroundColor = .black
        newImageView.contentMode = .scaleAspectFit
        newImageView.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.dismissFullscreenImage(_:)))
        newImageView.addGestureRecognizer(tap)
        self.view.addSubview(newImageView)
        self.navigationController?.isNavigationBarHidden = true
        self.tabBarController?.tabBar.isHidden = true
    }

    @objc func dismissFullscreenImage(_ sender: UITapGestureRecognizer) {
        self.navigationController?.isNavigationBarHidden = false
        self.tabBarController?.tabBar.isHidden = false
        sender.view?.removeFromSuperview()
    }
     
    
    
    @IBAction func SubmitAction(_ sender: UIButton) {
        
        self.endTimer()
    }
    
    
   

}//----------> End Of Class ---------------
 
extension UILabel {

    func retrieveTextHeight () -> CGFloat {
        let attributedText = NSAttributedString(string: self.text!, attributes: [NSAttributedString.Key.font:self.font as Any])

        let rect = attributedText.boundingRect(with: CGSize(width: self.frame.size.width, height: CGFloat.greatestFiniteMagnitude), options: .usesLineFragmentOrigin, context: nil)

        return ceil(rect.size.height)
    }

}

extension Quiz2VC: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    
            func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
             {
                 if collectionView == QuizCollectionCounter
                 {
                     return CGSize(width: 80.0, height: 70.0)
                 } else {
                     return CGSize(width: 80.0, height: 70.0)
                 }
              }
    
            func numberOfSections(in collectionView: UICollectionView) -> Int {
                return 1
            }
    
            func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
                
                
                if collectionView == QuizCollectionCounter {
                    
                    return QuestionCounterArr.count
                    
                    
                } else {
                
                    if QuestionArr[QuestionCounter].imgs!.count > 0 {
                        
                        self.quiz_CollBackView.isHidden       = false
                        self.QuizCollectionImages.isHidden   = false
                        self.quiz_CollBackViewHieght.constant = 80
                        return QuestionArr[QuestionCounter].imgs!.count
                        
                    } else {
                        
                        self.quiz_CollBackView.isHidden       = true
                        self.QuizCollectionImages.isHidden   = true
                        self.quiz_CollBackViewHieght.constant = 0
                    }
                
                }
                 
                return 0
            }
            
            
            func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
                                
                
                if collectionView == QuizCollectionCounter {
                    
                    let cellCounter  = collectionView.dequeueReusableCell(withReuseIdentifier: "QuizCounterCell",
                                                                          for: indexPath as IndexPath) as! QuizCounterCell
                    
                        cellCounter.CounterName.text = self.QuestionCounterArr[indexPath.row].title
                    
                    if ( self.QuestionCounterArr[indexPath.row].status == true ) {  // true == answered
                                 cellCounter.CounterName.textColor = .white
                                 cellCounter.CounterName.backgroundColor = UIColor(hexString: "008F00")
                     } else {
                                 cellCounter.CounterName.textColor = .darkGray
                                 cellCounter.CounterName.backgroundColor = UIColor(hexString: "F2F2F7")
                    }
                        
                    return cellCounter
                    
                    
                } else {
                    
                    let cellImages  = collectionView.dequeueReusableCell(withReuseIdentifier: "QuizImgsCell", for: indexPath as IndexPath) as! QuizImgsCell
                    
                    cellImages.QuestionImage.addSubview(cellImages.activityIndicator)
                    cellImages.activityIndicator.startAnimating()
                    cellImages.QuestionImage.sd_setImage(with: URL(string:  URLs.ImageDomain + String((QuestionArr[QuestionCounter].imgs?[indexPath.row])!)), placeholderImage: UIImage(named: "open-book"))
                    cellImages.activityIndicator.stopAnimating()
                    return cellImages
                    
                }
                
            
                
            }
            
            
            func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
                
                if collectionView == QuizCollectionCounter {
                                   
                   self.QuestionCounter = indexPath.row
                   fillNextQuestion(Counter: self.QuestionCounter )
                                   
                } else {

                         let cellImages = collectionView.cellForItem(at: indexPath) as! QuizImgsCell
                         self.imageTapped(image: cellImages.QuestionImage.image!)

                }
   
            }
    
     
    
}//---------> End Of CollecyionViewDelegate ----------------------


extension Quiz2VC: UITableViewDataSource, UITableViewDelegate {
    
//         func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//             return UITableView.automaticDimension + 50
//        }
    
         func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

            return QuestionArr[QuestionCounter].answers!.count
            }
        
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if cell.isSelected {
            tableView.cellForRow(at: indexPath as IndexPath)?.accessoryType = .checkmark
        } else {
            tableView.cellForRow(at: indexPath as IndexPath)?.accessoryType = .none
        }
    }
    
         func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            
                let  cell = tableView.dequeueReusableCell(withIdentifier: "QuizCell", for: indexPath) as! QuizCell
                     cell.answer_name.text = QuestionArr[QuestionCounter].answers?[indexPath.row].title
            
              return cell
            }
        
        
        //For single choice selection
           func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
               
            
                for cell in QuizTableQuestions.visibleCells {
                    cell.setSelected(false, animated: true)
                    cell.accessoryType = .none
                }
            
               tableView.cellForRow(at: indexPath as IndexPath)?.accessoryType = .checkmark
            
//               self.SelectedAnswerId = (QuestionArr[QuestionCounter].answers?[indexPath.row].id)!
//               self.SelectedAnswerQ  = (QuestionArr[QuestionCounter].id)!
            
                self.saveAnswerToModel(quesionId:     QuestionArr[QuestionCounter].id!,
                                       answerId:      QuestionArr[QuestionCounter].answers![indexPath.row].id!,
                                       AnswerArticle: "")
            
           }
           
           func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
               tableView.cellForRow(at: indexPath as IndexPath)?.accessoryType = .none
           }
    
    
}//---------> End Of TableViewDelegate ----------------------




extension Quiz2VC: UITextViewDelegate {
    
    public func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text ==  "Add your answer here..." {
            QuizShortNoteText.text = ""
            QuizShortNoteText.textColor = .darkGray

        }
        textView.becomeFirstResponder()
    }
 
    func textViewDidChange(_ textView: UITextView) {
        
        if textView.text ==  "" {
            
            print("\n empty")
            saveAnswerToModel(quesionId: QuestionArr[QuestionCounter].id!,
                              answerId: QuestionArr[QuestionCounter].answers![0].id!,
                              AnswerArticle: "")
            
        } else {
            
            print("\n not empty")
            saveAnswerToModel(quesionId: QuestionArr[QuestionCounter].id!,
                              answerId:  QuestionArr[QuestionCounter].answers![0].id!,
                              AnswerArticle: textView.text)
            
        }
        
    }

    
}
