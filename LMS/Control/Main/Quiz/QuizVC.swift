//
//  QuizVC.swift
//  LMS
//
//  Created by Abdo on 12/9/19.
//  Copyright © 2019 eaglez-group. All rights reserved.
//

import UIKit

class QuizVC: BaseVC {

    var Quiz_Id:         Int          = 0
    var Quiz_Title:      String       = ""
    var Quiz_Time:       Int          = 0
    var QuestionArr:     [Question]   = []
    var QuizAnswerArr =   [QuizQuestionAnswer]()
    
    @IBOutlet weak var quiz_name:             UILabel!
    @IBOutlet weak var quiz_FromQ:            UILabel!
    @IBOutlet weak var quiz_ToQ:              UILabel!
    @IBOutlet weak var quiz_time:             UILabel!
    @IBOutlet weak var quiz_question:         UITextView!
    @IBOutlet weak var quiz_CollBackView:     UIView!
    @IBOutlet weak var quiz_CollBackViewHieght: NSLayoutConstraint!
    @IBOutlet weak var quiz_CollectionPhotos: UICollectionView!
    @IBOutlet weak var quiz_TableOptions:     UITableView!
    @IBOutlet weak var quiz_Next:             UIButton!
    
    
    //---> Timer
    var countdownTimer: Timer!
    var QuestionCounter  = 0
    var SelectedAnswerId = 0
    var SelectedAnswerQ  = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.navigationItem.setHidesBackButton(true, animated:true);
        self.quiz_name.text      = Quiz_Title
        
        self.quiz_TableOptions.dataSource     = self
        self.quiz_TableOptions.delegate       = self
        self.quiz_TableOptions.layoutMargins  = UIEdgeInsets.zero
        self.quiz_TableOptions.separatorInset = UIEdgeInsets.zero
        //self.quiz_TableOptions.rowHeight      = 60
        
        self.quiz_TableOptions.estimatedRowHeight = 60.0
        self.quiz_TableOptions.rowHeight = UITableView.automaticDimension
        
        self.quiz_CollectionPhotos.dataSource = self
        self.quiz_CollectionPhotos.delegate   = self
        
        self.fillNextQuestion(Counter: self.QuestionCounter)
        //--> Timer
        self.startTimer()
        
        NotificationCenter.default.addObserver(self, selector: #selector(closeQuizAction(_:)),
        name: Notification.Name("closeQuiz"),
        object: nil)
        
    }//----- End of viewDidLoad ------------------------------------------
    
    @objc func closeQuizAction(_ notification:Notification) {

               // Do something now
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func NextAction(_ sender: Any) {
        
        if self.quiz_Next.titleLabel?.text == "Submit" {
            
                //Upload Question answer here..
                print("\n Upload Result Now\n")
                countdownTimer.invalidate()
                self.uploadQuizAnswer()
            
        } else if (self.QuestionCounter + 1 < self.QuestionArr.count) {
               
            
                if self.SelectedAnswerId == 0 {
                    
                    self.alertmessage(Message: "Please choose your answer first")
                } else {
                    
                    self.QuizAnswerArr.append(QuizQuestionAnswer(quesionId:  self.SelectedAnswerQ, answerId: self.SelectedAnswerId, ArticleAnswer: ""))
                    self.QuestionCounter += 1
                    self.fillNextQuestion(Counter: self.QuestionCounter)
                    self.SelectedAnswerQ   = 0
                    self.SelectedAnswerId  = 0
                }
            
        } else {
            
                if self.SelectedAnswerId == 0 {
                    
                    self.alertmessage(Message: "Please choose your answer first")
                } else {
                    //---- append last choice
                    self.QuizAnswerArr.append(QuizQuestionAnswer(quesionId:  self.SelectedAnswerQ, answerId: self.SelectedAnswerId, ArticleAnswer: ""))
                    self.quiz_Next.setTitle("Submit",for: .normal)
                    self.quiz_Next.backgroundColor = UIColor(hexString: "007AFF")
                }
        }
        
    }
    
    
    //--------------------------------------------------------------------------------
    func uploadQuizAnswer(){
        
//        API.sendStudentQuiz(vc: self,             Token: helper.getUserToken() ?? "",
//                            quizId: self.Quiz_Id, quizAnswers: self.QuizAnswerArr){
//                                
//                                (error, degree, maxdegree) in
//                                
//                                if error != nil {
//                                    
//                                    self.alertmessage(Message: "Error: \(error)")
//                                } else  {
                                    //FinishQuizVC
                                    //self.alertmessage(Message: "Your Degree: \(degree) From \(maxdegree)")
//                                    let storyboard            = UIStoryboard(name: "Main", bundle: nil)
//                                    let vc                    = storyboard.instantiateViewController(withIdentifier: "FinishQuizVC") as! FinishQuizVC
//                                    vc.modalPresentationStyle = .overFullScreen
//                                    vc.modalTransitionStyle   = .crossDissolve
//                                    self.present(vc, animated: true, completion: nil)
//                                    //self.show(vc, sender: nil)
//                                    
//                                }
//        }
        
        
    }
    
    //--------------------------------------------------------------------------------
    //--- Timer ---//
    func startTimer() {
        countdownTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
    }
    
    @objc func updateTime() {
        quiz_time.text = "\(timeFormatted(Quiz_Time))"
        
        if Quiz_Time != 0 {
            Quiz_Time -= 1
            
        } else {
            endTimer()
        }
    }
    
    func endTimer() {
        countdownTimer.invalidate()
        print("\n\n Time is up \n\n")
        self.uploadQuizAnswer()
        
//        if answered {
//
//            uploadAnswerAPI()
//
//        } else {
//
//            if( allAnswers.count > 0 ) {
//
//                uploadAnswerAPI()
//            } else {
//
//                for var i in (0..<allQuestions.count)
//                {
//                    allAnswers.append(
//                        Answer(q_id: allQuestions[i].Q_id,
//                               q_ans: "X", q_degr: 0))
//                }
//                uploadAnswerAPI()
//            }
//
//
//        }// enf of else answered check
        
    }//--> End of timer out ------------------------------------------------------------------------
    
    func timeFormatted(_ totalSeconds: Int) -> String {
        let seconds: Int = totalSeconds % 60
        let minutes: Int = (totalSeconds / 60) % 60
        //     let hours: Int = totalSeconds / 3600
        return String(format: "%02d:%02d", minutes, seconds)
    }

    func fillNextQuestion( Counter: Int ){
        
        if let indexPathForSelectedRow = self.quiz_TableOptions.indexPathForSelectedRow  {
            self.quiz_TableOptions.cellForRow(at: indexPathForSelectedRow as IndexPath)?.accessoryType = .none
        }

        self.quiz_FromQ.text     = String(Counter + 1)
        self.quiz_ToQ.text       = String(QuestionArr.count)
        //self.quiz_time.text      = String(Quiz_Time)
        self.quiz_question.text  = QuestionArr[Counter].title
        self.quiz_TableOptions.reloadData()
        self.quiz_CollectionPhotos.reloadData()
    }
    
    @objc func dismissFullscreenImage(_ sender: UITapGestureRecognizer) {
        self.navigationController?.isNavigationBarHidden = false
        self.tabBarController?.tabBar.isHidden = false
        sender.view?.removeFromSuperview()
    }
    
}//-------- End of class










//--------------------------------------------------------------

extension QuizVC: UITableViewDataSource, UITableViewDelegate {
    
         func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
             return UITableView.automaticDimension + 20
        }
    
         func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

            return QuestionArr[QuestionCounter].answers!.count
            }
        
    
         func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            
                let cell = tableView.dequeueReusableCell(withIdentifier: "QuizCell", for: indexPath) as! QuizCell
            cell.answer_name.text = QuestionArr[QuestionCounter].answers?[indexPath.row].title
            
             return cell
            }
        
        
        //For single choice selection
           func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
               
               tableView.cellForRow(at: indexPath as IndexPath)?.accessoryType = .checkmark
               self.SelectedAnswerId = (QuestionArr[QuestionCounter].answers?[indexPath.row].id)!
               self.SelectedAnswerQ  = (QuestionArr[QuestionCounter].id)!
           }
           
           func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
               tableView.cellForRow(at: indexPath as IndexPath)?.accessoryType = .none
           }
    
    
}

extension QuizVC: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    
            func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
             {
                return CGSize(width: 80.0, height: 70.0)
             }
    
            func numberOfSections(in collectionView: UICollectionView) -> Int {
                return 1
            }
    
            func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
                
                if QuestionArr[QuestionCounter].imgs!.count > 0 {
                    
                    self.quiz_CollBackView.isHidden       = false
                    self.quiz_CollectionPhotos.isHidden   = false
                    self.quiz_CollBackViewHieght.constant = 80
                    return QuestionArr[QuestionCounter].imgs!.count
                    
                } else {
                    
                    self.quiz_CollBackView.isHidden       = true
                    self.quiz_CollectionPhotos.isHidden   = true
                    self.quiz_CollBackViewHieght.constant = 0
                }
                return 0
            }
            
            
            func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
                
                let cellImages  = collectionView.dequeueReusableCell(withReuseIdentifier: "QuizImgsCell", for: indexPath as IndexPath) as! QuizImgsCell
                
                    cellImages.QuestionImage.addSubview(cellImages.activityIndicator)
                    cellImages.activityIndicator.startAnimating()
                
//                    if let decodedData = Data(base64Encoded: QuestionArr[QuestionCounter].imgs?[indexPath.row] ?? "", options: .ignoreUnknownCharacters) {
//                        cellImages.QuestionImage.image = UIImage(data: decodedData)
//                        cellImages.activityIndicator.stopAnimating()
//                    }
                
                
                let url = NSURL(string: URLs.BaseUrl + String((QuestionArr[QuestionCounter].imgs?[indexPath.row])!))!
                let task = URLSession.shared.dataTask(with: url as URL) { (responseData, responseUrl, error) -> Void in
                        if (responseData != nil && responseUrl != nil && error == nil)
                            {
                                if let data = responseData{
                                DispatchQueue.main.async {
                                              cellImages.QuestionImage.image = UIImage(data: data)
                                              cellImages.activityIndicator.stopAnimating()
                                    }
                                }
                            } else {
                                DispatchQueue.main.async { // Make sure you're on the main thread here
                                    cellImages.QuestionImage.image = UIImage(named: "open-book")
                                    cellImages.activityIndicator.stopAnimating()
                                }
                            }
                                  
                        }
                task.resume()
                
                return cellImages
            }
            
            
            func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
                let cellImages  = collectionView.dequeueReusableCell(withReuseIdentifier: "QuizImgsCell", for: indexPath as IndexPath) as! QuizImgsCell
                
                var newImageView = UIImageView()
                
                newImageView.addSubview(cellImages.activityIndicator)
                cellImages.activityIndicator.startAnimating()
                
                  let url = NSURL(string: URLs.BaseUrl + String((QuestionArr[QuestionCounter].imgs?[indexPath.row].dropFirst(2))!))!
                              let task = URLSession.shared.dataTask(with: url as URL) { (responseData, responseUrl, error) -> Void in
                                      if (responseData != nil && responseUrl != nil && error == nil)
                                          {
                                              if let data = responseData{
                                              DispatchQueue.main.async {
                                                            newImageView.image = UIImage(data: data)
                                                            cellImages.activityIndicator.stopAnimating()
                                                  }
                                              }
                                          } else {
                                              DispatchQueue.main.async { // Make sure you're on the main thread here
                                                  newImageView.image = UIImage(named: "open-book")
                                                  cellImages.activityIndicator.stopAnimating()
                                              }
                                          }
                                                
                                      }
                  task.resume()
                
                   newImageView.frame = UIScreen.main.bounds
                   newImageView.backgroundColor = .black
                   newImageView.contentMode = .scaleAspectFit
                   newImageView.isUserInteractionEnabled = true
               let tap = UITapGestureRecognizer(target: self, action: #selector(dismissFullscreenImage))
               newImageView.addGestureRecognizer(tap)
               self.view.addSubview(newImageView)
               //self.navigationController?.isNavigationBarHidden = true
               self.tabBarController?.tabBar.isHidden = true
            }
    
    
    
    
}


 
extension UITableView {

    func deselectSelectedRow(animated: Bool)
    {
        if let indexPathForSelectedRow = self.indexPathForSelectedRow
        {
            self.deselectRow(at: indexPathForSelectedRow, animated: animated)
            
        }
    }

}
