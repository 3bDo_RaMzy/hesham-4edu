//
//  TicketsNewVC.swift
//  LMS
//
//  Created by Abdo on 1/2/20.
//  Copyright © 2020 eaglez-group. All rights reserved.
//

import UIKit

class RequestsNewVC: BaseVC {

    @IBOutlet weak var msgSubTxtField: DesignableTextField!
    @IBOutlet weak var msgTxtField: UITextView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        hideKeyboardWhenTappedAround()
    }
    
 
    @IBAction func sendSMS(_ sender: Any) {
          
          if msgSubTxtField.text == "" {
              
              let alert = UIAlertView(title: "", message: "Message Subject Is Empty", delegate: nil, cancelButtonTitle: "OK")
                  alert.show()
              return
          }else if msgTxtField.text == "" {
              
              let alert = UIAlertView(title: "", message: "Message Details Is Empty", delegate: nil, cancelButtonTitle: "OK")
                  alert.show()
              return
          } else {
                    
            
            API.sendRequest(vc: self, Token: helper.getUserToken()!, Title: self.msgSubTxtField.text!, Detalis: self.msgTxtField.text! )
                    { (error, TimeOut, status) in
                            
                        
                        if let err = error {
                            if TimeOut! {
                                let storyboard = UIStoryboard(name: "Auth", bundle: nil)
                                let vc         = storyboard.instantiateViewController(withIdentifier: "SessionExpireVC")
                                    vc.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency
                                self.present(vc, animated: true, completion: nil)
                               // self.navigationController?.pushViewController(vc, animated: true)
                                return
                                
                            } else {
                                self.alertmessage(Message: "Failed to send new SMS:\n \(err) \n\n Try Again Later.." )
                                return
                            }
                        } else  {
                            
                                    //print("Message Sent Successfully")
                                    let alert = UIAlertController(title: "", message: " Successfully sent ", preferredStyle: UIAlertController.Style.alert)
                                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: {(alert: UIAlertAction!) in

                                     self.msgSubTxtField.text = ""
                                     self.msgTxtField.text = ""
                                     self.dismiss(animated: true){
                                         NotificationCenter.default.post(name: NSNotification.Name(rawValue: "load"), object: nil)
                                     }

                                    }))
                                    self.present(alert, animated: true)
                                                           
                         }
                                     
                     }
               }
         }

}
