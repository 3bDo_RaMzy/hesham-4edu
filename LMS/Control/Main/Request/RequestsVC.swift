//
//  TicketsVC.swift
//  LMS
//
//  Created by Abdo on 1/2/20.
//  Copyright © 2020 eaglez-group. All rights reserved.
//

import UIKit

class RequestsVC: BaseVC, UITableViewDataSource, UITableViewDelegate {

    
        @IBOutlet weak var StudentsTable: UITableView!
        
        var requestsViewModel = [Request]()
        var Spinner :UIView!
        private let refreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = "My Requests"
        
        // set up the refresh control
       if #available(iOS 10.0, *) {
            StudentsTable.refreshControl = refreshControl
        } else {
            StudentsTable.addSubview(refreshControl)
        }
         refreshControl.tintColor = UIColor(red:0.25, green:0.72, blue:0.85, alpha:1.0)
         refreshControl.attributedTitle = NSAttributedString(string: "Refresh Data...", attributes: nil)
         refreshControl.addTarget(self, action: #selector(refreshData(_:)), for: .valueChanged)
                                    
         self.StudentsTable.dataSource     = self
         self.StudentsTable.delegate       = self
         self.StudentsTable.layoutMargins  = UIEdgeInsets.zero
         self.StudentsTable.separatorInset = UIEdgeInsets.zero
         self.StudentsTable.rowHeight      = 180
         self.fetchData()
                 
        NotificationCenter.default.addObserver(self, selector: #selector(loadList(_:)), name: Notification.Name("load"), object: nil)
    }//------------ End Of ViewDidLoad ---------------------
    
    
    @objc func loadList(_ notification:Notification){
        //load data here
        self.fetchData()
    }
    
    @objc private func refreshData(_ sender: Any) {
        // Fetch Weather Data
        self.refreshData()
    }

                func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

                        return requestsViewModel.count
                    }
                
                 func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
                    
                        let cell = tableView.dequeueReusableCell(withIdentifier: "RequestCell", for: indexPath) as! RequestCell
                            cell.requestViewModel = requestsViewModel[indexPath.row]
                            cell.RequestReplayCount.tag = indexPath.row
                            cell.RequestReplayCount.addTarget(self, action: #selector(FuncOpenComments(_:)), for: .touchUpInside)
                     return cell
                    }
                
                
                func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
                         
//                    ParentStudentsVC.StudentId = self.requestsViewModel[indexPath.row].StudentId
//                    
//                    // Do something now
//                    let storyboard            = UIStoryboard(name: "Parent", bundle: nil)
//                    let vc                    = storyboard.instantiateViewController(withIdentifier: "ParentChoiseVC") as! ParentChoiseVC
//                        vc.modalPresentationStyle = .overFullScreen
//                        vc.modalTransitionStyle   = .crossDissolve
//
//                    //self.present(vc, animated: true, completion: nil)
//                    self.show(vc, sender: nil)
                    }
        
    
    
    
    //--------------------------------------------------------------------
    @objc func FuncOpenComments(_ sender: UIButton){
                  
//                  let mm = requestsViewModel[sender.tag].commentes.count
//                  print(" \n\n FuncOpenComments Now with tag: \(sender.tag) Comments Count; \(mm)")
                  
                  let storyboard            = UIStoryboard(name: "Main", bundle: nil)
                  let vc                    = storyboard.instantiateViewController(withIdentifier: "RequestsReplayVC") as! RequestsReplayVC
                      vc.messageCommentArr  = requestsViewModel[sender.tag].Comments
                      vc.requestId   = requestsViewModel[sender.tag].Id
                      
                      vc.modalPresentationStyle    = UIModalPresentationStyle.overCurrentContext
                      vc.modalTransitionStyle      = .crossDissolve
                  
                  self.present(vc, animated: true)
    }
    
    //--------------------------------------------------------------------
    fileprivate func fetchData() {
                        
            Spinner = UIViewController.displaySpinner(onView: self.view)
            API.getRequestsList(vc: self, Token: helper.getUserToken()!) { ( err, TimeOut, requests ) in
                            
                    UIViewController.removeSpinner(spinner: self.Spinner)
                
                    if let err = err {
                           if TimeOut! {
                               let storyboard = UIStoryboard(name: "Auth", bundle: nil)
                               let vc         = storyboard.instantiateViewController(withIdentifier: "SessionExpireVC")
                                   vc.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency
                               self.present(vc, animated: true, completion: nil)
                              // self.navigationController?.pushViewController(vc, animated: true)
                               return
                               
                           } else {
                               self.alertmessage(Message: "Failed to send new request:\n \(err) \n\n Try Again Later.." )
                               return
                           }
                    }
                     
                    self.requestsViewModel = requests?.map({return Request(request:  $0)}) ?? []
                    if self.requestsViewModel.count <= 0 {
                        self.showToast(message: "No Requests Available Now !!", vc: self)
                    }
                    self.StudentsTable.reloadData()
            }
                        
     }//---> End Of fetchData method -------
    fileprivate func refreshData() {
                       
        self.refreshControl.beginRefreshing()
           API.getRequestsList(vc: self, Token: helper.getUserToken()!) { ( err, TimeOut, requests ) in
                           
                   self.refreshControl.endRefreshing()
                    if let err = err {
                           if TimeOut! {
                               let storyboard = UIStoryboard(name: "Auth", bundle: nil)
                               let vc         = storyboard.instantiateViewController(withIdentifier: "SessionExpireVC")
                                   vc.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency
                               self.present(vc, animated: true, completion: nil)
                              // self.navigationController?.pushViewController(vc, animated: true)
                               return
                               
                           } else {
                               self.alertmessage(Message: "Failed to send new request:\n \(err) \n\n Try Again Later.." )
                               return
                           }
                    }

            
                   self.requestsViewModel = requests?.map({return Request(request:  $0)}) ?? []
                   if self.requestsViewModel.count <= 0 {
                      self.showToast(message: "No Requests Available Now !!", vc: self)
                   }
                   self.StudentsTable.reloadData()
           }
                       
    }//---> End Of fetchData method -------
    


}//End of class
