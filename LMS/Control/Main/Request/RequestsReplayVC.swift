//
//  TicketsReplayVC.swift
//  LMS
//
//  Created by Abdo on 1/2/20.
//  Copyright © 2020 eaglez-group. All rights reserved.
//

import UIKit

class RequestsReplayVC: BaseVC, UITableViewDataSource, UIImagePickerControllerDelegate {

    
    var messageCommentArr: [Comment] = []
    var requestId: Int = 0
    
    
    @IBOutlet weak var commentBackView: UIView!
    @IBOutlet weak var commentTableView: UITableView!
    @IBOutlet weak var commentTableBackView: UIView!
    @IBOutlet weak var msgTxtField: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view. 
        hideKeyboardWhenTappedAround()
        
         //loadingBackView.backgroundColor = UIColor(white: 1, alpha: 0.8)
         commentBackView.backgroundColor = UIColor(hexString: "000000", alpha: 0.5)
         //loadingBackView.tintColor = UIColor.black
         commentBackView.isUserInteractionEnabled = true
         
         self.commentTableBackView.backgroundColor      = UIColor.clear
         self.commentTableBackView.layer.shadowColor    = UIColor.darkGray.cgColor
         self.commentTableBackView.layer.shadowOffset   = CGSize(width: 2.0, height: 2.0)
         self.commentTableBackView.layer.shadowOpacity  = 1.0
         self.commentTableBackView.layer.shadowRadius   = 2
         self.commentTableView.layer.cornerRadius       = 10
         self.commentTableView.layer.masksToBounds      = true
                 
         self.commentTableView.dataSource     = self
         self.commentTableView.layoutMargins  = UIEdgeInsets.zero
         self.commentTableView.separatorInset = UIEdgeInsets.zero
         self.commentTableView.rowHeight      = 120
        //self.fetchData()
        
    }//------------ End Of ViewDidLoad ---------------------
    

     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

            return messageCommentArr.count
        }
    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
            //---------> Student  access these features:
            if messageCommentArr[indexPath.row].StudentId == 0 {
                        
                let cell = tableView.dequeueReusableCell(withIdentifier: "MsgDialogMe", for: indexPath) as! RequestCell
                    cell.commentViewModel = messageCommentArr[indexPath.row]
                    return cell
                        
            } else {
                    
                let cell = tableView.dequeueReusableCell(withIdentifier: "MsgDialogAdmin", for: indexPath) as! RequestCell
                    cell.commentViewModel = messageCommentArr[indexPath.row]
                    return cell
            }
          
    }
    //---------------------------------------------------------------------------
    
    @IBAction func sendSMS(_ sender: Any) {
          
          if msgTxtField.text == "" {
              
                let alert = UIAlertController(title: "Attention !!", message: "Message Details Is Empty", preferredStyle: .alert)
                             alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.cancel, handler: nil))
                self.present(alert, animated: true)
          }else {
                    
            API.sendRequestReplay(vc: self, Token: helper.getUserToken()!,
                                  Id: self.requestId, Message: msgTxtField.text!)
                    { (error, TimeOut, status) in
                                    
                      if let err = error {
                            if TimeOut! {
                                let storyboard = UIStoryboard(name: "Auth", bundle: nil)
                                let vc         = storyboard.instantiateViewController(withIdentifier: "SessionExpireVC")
                                    vc.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency
                                self.present(vc, animated: true, completion: nil)
                               // self.navigationController?.pushViewController(vc, animated: true)
                                return
                                
                            } else {
                                self.alertmessage(Message: "Failed to send new SMS:\n \(err) \n\n Try Again Later.." )
                                return
                            }
                        
                        } else  {
                            
                                    //print("Message Sent Successfully")
                                    let alert = UIAlertController(title: "", message: " Successfully sent ", preferredStyle: UIAlertController.Style.alert)
                                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: {(alert: UIAlertAction!) in

                                     self.msgTxtField.text = ""
                                     self.dismiss(animated: true){
                                         NotificationCenter.default.post(name: NSNotification.Name(rawValue: "load"), object: nil)
                                     }

                                    }))
                                    self.present(alert, animated: true)
                                                           
                         }
                                     
                     }
               }
         }
    
    
    
    

    
    
    
    //---------------------------------------------------------------------------
    @IBAction func closeComments(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
}
