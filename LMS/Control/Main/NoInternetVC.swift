//
//  NoInternetVC.swift
//  LMS
//
//  Created by MacBook on 6/28/20.
//  Copyright © 2020 eaglez-group. All rights reserved.
//

import UIKit

class NoInternetVC: BaseVC {

    
    @IBOutlet weak var noInternetBackView: UIView!
    @IBOutlet weak var NoInternetImage: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
         // Do any additional setup after loading the view.
        
        //noInternetBackView.backgroundColor = UIColor(white: 1, alpha: 0.8)
        
        
        //noInternetBackView.backgroundColor = UIColor(hexString: "000000", alpha: 0.5)
        
        
        
        //noInternetBackView.tintColor = UIColor.black
         
//        NoInternetImage.backgroundColor  = #colorLiteral(red: 0.9490196078, green: 0.9490196078, blue: 0.968627451, alpha: 1)
//        NoInternetImage.image = NoInternetImage.image?.withRenderingMode(.alwaysTemplate)
//        NoInternetImage.tintColor        = #colorLiteral(red: 0.6196078431, green: 0.7921568627, blue: 0.968627451, alpha: 1)
    
    }//--- End Of ViewDidLoading
    

    @IBAction func DissmissViewAction(_ sender: UITapGestureRecognizer) {
        
        //self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func CloseAction(_ sender: UIButton) {
        
        //self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func goLoginAction(_ sender: UIButton) {
        
        helper.resetUserToken()
        
        guard let window = UIApplication.shared.keyWindow else{return}
        let storyboard   = UIStoryboard(name: "Auth", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "Splash-Login")
        window.rootViewController = vc
        UIView.transition(with: window, duration: 0.5, options: .transitionFlipFromRight, animations: nil, completion: nil)
    }
    
    
}
