//
//  ApprovalStudentVC.swift
//  LMS
//
//  Created by MacBook on 6/18/20.
//  Copyright © 2020 eaglez-group. All rights reserved.
//

import UIKit

class ApprovalStudentVC: BaseVC {

    @IBOutlet weak var ViewNoRequests: UIView!
    @IBOutlet weak var PendingSTTable: UITableView!
    
    var PendingStudentsList    = [PendingStudents]()
    private let refreshControl = UIRefreshControl()
    
        

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.title = "Pending Students"
        
        self.ViewNoRequests.isHidden = true
        self.PendingSTTable.isHidden = false
                    
        self.PendingSTTable.dataSource         = self
        self.PendingSTTable.delegate           = self
        self.PendingSTTable.layoutMargins      = UIEdgeInsets.zero
        self.PendingSTTable.separatorInset     = UIEdgeInsets.zero
        self.PendingSTTable.rowHeight          = 150
        
        self.PendingSTTable.register(UINib(nibName: "PendingStudentsCell", bundle: nil), forCellReuseIdentifier: "PendingStudentsCell")
 
        self.fetchData()
        
        // set up the refresh control
        if #available(iOS 10.0, *) {
             PendingSTTable.refreshControl = refreshControl
         } else {
             PendingSTTable.addSubview(refreshControl)
         }
        refreshControl.tintColor = UIColor(red:0.25, green:0.72, blue:0.85, alpha:1.0)
        refreshControl.attributedTitle = NSAttributedString(string: "Refresh Data...", attributes: nil)
        refreshControl.addTarget(self, action: #selector(refreshData(_:)), for: .valueChanged)
        
        //self.tabBarController?.tabBar.isHidden = false
    }//------------ End Of ViewDidLoad ---------------------
        
    
    override func viewWillAppear(_ animated: Bool) {
        if self.PendingStudentsList.isEmpty {
            self.fetchData()
        }
    }
     
    @objc private func refreshData(_ sender: Any) {
        // Fetch Weather Data
        self.refreshData()
    }
                    
                        
    fileprivate func fetchData() {
                   
        API.getPendingStudents(vc: self, Token: helper.getUserToken() ?? "" )
        { ( err, TimeOut, students ) in
                            
            
                if let err = err {
                    if TimeOut! {
                        let storyboard = UIStoryboard(name: "Auth", bundle: nil)
                        let vc         = storyboard.instantiateViewController(withIdentifier: "SessionExpireVC")
                            vc.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency
                        self.present(vc, animated: true, completion: nil)
                       // self.navigationController?.pushViewController(vc, animated: true)
                        return
                        
                    } else {
                        self.alertmessage(Message: "Failed to fetch private material videos:\n \(err) \n\n Try Again Later.." )
                        return
                    }
                }

                            
                self.PendingStudentsList = students?.map({return PendingStudents(student: $0)}) ?? []
                    
                    if self.PendingStudentsList.count <= 0 {
                       self.ViewNoRequests.isHidden = false
                       self.PendingSTTable.isHidden = true
                    }
            
                    self.PendingSTTable.reloadData()
                }
     }//---> End Of fetchData method -------
       
    
    fileprivate func refreshData() {
        
        self.refreshControl.beginRefreshing()
        API.getPendingStudents(vc: self, Token: helper.getUserToken() ?? "" )
        { ( err, TimeOut, students ) in
            
         self.refreshControl.endRefreshing()

            if let err = err {
                if TimeOut! {
                    let storyboard = UIStoryboard(name: "Auth", bundle: nil)
                    let vc         = storyboard.instantiateViewController(withIdentifier: "SessionExpireVC")
                        vc.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency
                    self.present(vc, animated: true, completion: nil)
                   // self.navigationController?.pushViewController(vc, animated: true)
                    return
                    
                } else {
                    self.alertmessage(Message: "Failed to fetch private material videos:\n \(err) \n\n Try Again Later.." )
                    return
                }
            }

            
            self.PendingStudentsList = students?.map({return PendingStudents(student: $0)}) ?? []
             if self.PendingStudentsList.count <= 0 {
                 self.ViewNoRequests.isHidden = false
                 self.PendingSTTable.isHidden = true
             }
            self.PendingSTTable.reloadData()
        }
     }//---> End Of refreshData method -------
    
    
    func sendStudentStatus( ST_Id: Int, ST_Status: String ){
        
        API.sendPendingRequest(vc: self, Token: helper.getUserToken() ?? "",
                               StudentId: ST_Id, status: ST_Status)
        { (error,TimeOut, status) in

        
        if (TimeOut == true) {
            
              let storyboard = UIStoryboard(name: "Auth", bundle: nil)
              let vc         = storyboard.instantiateViewController(withIdentifier: "SessionExpireVC")
                  vc.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency
              self.present(vc, animated: true, completion: nil)
             // self.navigationController?.pushViewController(vc, animated: true)
              return
            
        } else if (TimeOut != true && status != true) {

                        self.alertmessage(Message: " Request faild  ")
         } else  {

                        //print("Message Sent Successfully")
                        let alert = UIAlertController(title: "Your Request", message: " Successfully updated ", preferredStyle: UIAlertController.Style.alert)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: {(alert: UIAlertAction!) in
                                self.fetchData()
                            }))
                        self.present(alert, animated: true)

             }
       }
     }//--- End Of sendStudentStatus
    
    
    
    
}//---> End Of Class -----------------


extension ApprovalStudentVC: UITableViewDelegate, UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
                        
            return UITableView.automaticDimension
    }
        
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

            return PendingStudentsList.count
    }
                    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
                        
            let cell = tableView.dequeueReusableCell(withIdentifier: "PendingStudentsCell", for: indexPath) as! PendingStudentsCell
                cell.PendingStudentsViewModel = PendingStudentsList[indexPath.row]
            return cell
    }
      
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let alert = UIAlertController(title: "", message: "Please Select an Option", preferredStyle: .actionSheet)

            alert.addAction(UIAlertAction(title: "Approve", style: .default , handler:{ (UIAlertAction)in
                self.sendStudentStatus(ST_Id: self.PendingStudentsList[indexPath.row].Id, ST_Status: "Approval")
            }))
            alert.addAction(UIAlertAction(title: "Refuse", style: .destructive , handler:{ (UIAlertAction)in
                self.sendStudentStatus(ST_Id: self.PendingStudentsList[indexPath.row].Id, ST_Status: "Refuse")
            }))

            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))

        self.present(alert, animated: true, completion: nil)
        
    }
                    
 }




 
