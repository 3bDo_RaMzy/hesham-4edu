//
//  SplashTeacherVC.swift
//  LMS
//
//  Created by MacBook on 6/18/20.
//  Copyright © 2020 eaglez-group. All rights reserved.
//

import UIKit

class SplashTeacherVC: BaseVC {

    @IBOutlet weak var loadingImage: UIImageView!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
        
        
        override func viewDidLoad() {
            super.viewDidLoad()
            // Do any additional setup after loading the view.
            
            self.loadingIndicator.stopAnimating()
             
                self.loadingImage.alpha = 0
                UIView.animate(withDuration: 3, delay: 1,
                               options: UIView.AnimationOptions.curveEaseInOut,
                               animations: { () -> Void in
                                
                                self.loadingImage.alpha = 1
                                
                         }, completion: { (Bool) -> Void in
                            
                            
//                            if helper.getAppOption() == nil {
                                
                                
                                API.CheckOption(loadingView: self.loadingIndicator) { (error, status) in
                                        
                                        if error != nil {
                                             
                                            let alert = UIAlertController(title: "ooops!", message: "Sorry! Can not add system settings", preferredStyle: .alert)
                                                alert.addAction(UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                                                         self.redirectAppToLogin()
                                                })
                                            self.present(alert, animated: true)
                                        
                                        }else if status != "" {
                                             
                                            let alert = UIAlertController(title: "ooops!", message: "Error! .. \(status)", preferredStyle: .alert)
                                                alert.addAction(UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                                                         self.redirectAppToLogin()
                                                })
                                            self.present(alert, animated: true)
                                            
                                        }else if status == ""{
                                            
                                            self.redirectAppToLogin()
                                        }
                                         
                                    }
                                
                                
//                            }else {
//
//                                    self.redirectAppToLogin()
//
//                            }
                               
                })
     
            
            
        }//--- End Of ViewDidLoad ----------

        
        
        func redirectAppToLogin(){
             
            let storyboard            = UIStoryboard(name: "Teacher", bundle: nil)
            let vc                    = storyboard.instantiateViewController(withIdentifier: "teacherContentViewController"/*"teacherRootController"*/)
            vc.modalPresentationStyle = .overFullScreen
            vc.modalTransitionStyle   = .crossDissolve
            //self.present(vc, animated: true, completion: nil)
            //self.show(vc, sender: nil)
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
       
        
        

    }//----- End Of Class -----------

