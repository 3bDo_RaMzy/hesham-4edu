//
//  NewQuizVC.swift
//  LMS
//
//  Created by MacBook on 6/17/20.
//  Copyright © 2020 eaglez-group. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class NewQuizVC: BaseVC, UIPickerViewDataSource, UIPickerViewDelegate,
                 UITableViewDataSource, UITableViewDelegate{
     
    // force Time input to accept numbers only
    
    
    @IBOutlet weak var quiz_name: DesignableTextField!
    @IBOutlet weak var quiz_dateFrom:    UILabel!
    @IBOutlet weak var quiz_dateTo:      UILabel!
    @IBOutlet weak var quiz_time:        UITextField!
    @IBOutlet weak var quiz_grade:       UILabel!
    @IBOutlet weak var quiz_studentType: UILabel!
    @IBOutlet weak var quiz_group:       UILabel!
    @IBOutlet weak var quiz_bankName:    UILabel!
    
    @IBOutlet weak var ViewPickerDate:   UIView!
    @IBOutlet weak var ViewPickerData:   UIView!
    @IBOutlet weak var PickerSelectView: UIPickerView! //for Grade | Student type
    @IBOutlet weak var PickerDateView:   UIDatePicker!
    
    
    @IBOutlet weak var ViewStudentType: UIView!
    
    @IBOutlet weak var ScrollViewMain: UIScrollView!
    @IBOutlet weak var ViewGroupList:  CardView!
    @IBOutlet weak var TableViewGroup: UITableView!
    @IBOutlet weak var ViewBankList:   CardView!
    @IBOutlet weak var TableViewBank:  UITableView!
    
    var Spinner  : UIView!
    var AppSysOption:Bool = false //--> true: app use online|academic option
 
    // Selected variables:
    var selectedDateFrom          = ""
    var selectedDateTo            = ""
    var selectedGradeID           = 0
    var selectedStudentType       = -1   // 0: academic | 1: online
    var selectedGroupID: [String] = []  // multi select group id
    var selectedBankID            = 0
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.title = "New Quiz"
        self.hideKeyboardWhenTappedAround()
        
        self.PickerSelectView.dataSource = self
        self.PickerSelectView.delegate   = self
        
        self.TableViewGroup.dataSource   = self
        self.TableViewGroup.delegate     = self
        self.TableViewGroup.allowsMultipleSelection = true
        self.TableViewGroup.layoutMargins  = UIEdgeInsets.zero
        self.TableViewGroup.separatorInset = UIEdgeInsets.zero
        //self.TableViewGroup.rowHeight      = 60
        self.TableViewGroup.estimatedRowHeight = 60.0
        self.TableViewGroup.rowHeight = UITableView.automaticDimension
        
        self.TableViewBank.dataSource    = self
        self.TableViewBank.delegate      = self
        
        //helper.saveAppOption(OnlineAcdmic: false) //To Enable Academic only system
        if helper.getAppOption() == false {   //false: academic system only
            self.AppSysOption             = false
            self.selectedStudentType      = 0
            self.ViewStudentType.isHidden = true
        }else {
            self.AppSysOption = true    //true: online and academic system
            self.ViewStudentType.isHidden = false
        }
        
        self.ShowHomeView(ScrollMain: true, ViewGroup: false, ViewBank: false)
        self.ShowPickerView(ViewPickerDate: false, ViewPickerData: false)
        
        
    }//----End Of ViewDidLoad
    
     
    override func viewWillAppear(_ animated: Bool) {
         if helper.getAppOption() == false {   //false: academic system only
            self.AppSysOption = false
            self.selectedStudentType = 0
            self.ViewStudentType.isHidden = true
        }else {
            self.AppSysOption = true    //true: online and academic system
            self.ViewStudentType.isHidden = false
        }
    }
    
    
    //-------------------------------------------------------
    //----> Strart Date(From/To) selection
    @IBAction func ShowDateOption(_ sender: UIButton) {//show picker view of data
        if sender.tag == 1 { //Date From option
            self.ShowPickerView(ViewPickerDate: true, ViewPickerData: false)
            self.BtnSaveDate.tag = 1
 
        } else if sender.tag == 2 { //Date To option
            self.ShowPickerView(ViewPickerDate: true, ViewPickerData: false)
            self.BtnSaveDate.tag = 2
         }
    }
    
    func ShowPickerView( ViewPickerDate:Bool, ViewPickerData:Bool ){
        
        //----> Add Animation here !!
        self.ViewPickerDate.isHidden  = !ViewPickerDate
        self.ViewPickerData.isHidden  = !ViewPickerData
     }
    
    @IBOutlet weak var BtnSaveDate: UIButton!
    //--- Save / Close Action
     @IBAction func SaveDateAction(_ sender: UIButton) {
        
        let dateFormatter = DateFormatter()
            dateFormatter.dateStyle  = .short
            dateFormatter.timeStyle  = .none
            dateFormatter.dateFormat = "dd-MM-yyyy" // View :"01-05-2020"
        let dateFormatter2 = DateFormatter()
            dateFormatter2.dateStyle  = .short
            dateFormatter2.timeStyle  = .none
            dateFormatter2.dateFormat = "yyyy-MM-dd" // API Formate:"2020-05-01"
        
        if sender.tag == 1 {
            quiz_dateFrom.text = dateFormatter.string(from: PickerDateView.date)
            quiz_dateFrom.textColor = .darkGray
            selectedDateFrom = dateFormatter2.string(from: PickerDateView.date)
            self.ShowPickerView(ViewPickerDate: false, ViewPickerData: false)
        } else if sender.tag == 2 {
            quiz_dateTo.text = dateFormatter.string(from: PickerDateView.date)
            quiz_dateTo.textColor = .darkGray
            selectedDateTo = dateFormatter2.string(from: PickerDateView.date)
            self.ShowPickerView(ViewPickerDate: false, ViewPickerData: false)
        }
     }
    @IBAction func CloseDateAction(_ sender: UIButton) {
        
        self.ShowPickerView(ViewPickerDate: false, ViewPickerData: false)
    }
    //----> END Date(From/To) selection
    //-------------------------------------------------------
    
    
    
    
    //-------------------------------------------------------
    //----> Strart Picker(Grade/ST.type) selection

    var pikType : Int = 1   //--> 1:Grade , 2:ST.type
    var grades = [Grade]()
    var options = ["Academic", "Online"]
    @IBAction func ShowPickerOption(_ sender: UIButton) {   //show picker view of data
        if sender.tag == 1 { //--Select Grade!
            
            pikType = 1
            if grades.isEmpty {
                self.getGrades()
            } else {
                self.ShowPickerView(ViewPickerDate: false, ViewPickerData: true)
                self.PickerSelectView.reloadAllComponents()
            }
             
        } else if sender.tag == 2 { //--Select Student Type!
            
            pikType = 2
            self.ShowPickerView(ViewPickerDate: false, ViewPickerData: true)
            self.PickerSelectView.reloadAllComponents()
            self.selectedStudentType = 0   //0: Academic
            self.quiz_studentType.text = options[0]
        }
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pikType == 1 {
                            return grades.count
        }else  if pikType == 2 {
                            return options.count
        } else {
            return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pikType == 1 {
            return grades[row]._Name
        }else  if  pikType == 2{
            return options[row]
        } else {
            return  ""
        }
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pikType ==  1{
            
            self.selectedGradeID = grades[row]._Id
            self.quiz_grade.text = grades[row]._Name
            
        }else if  pikType == 2{
            
            if row == 0 {
                    self.selectedStudentType = 0   //0: Academic
                    self.quiz_studentType.text = options[row]
            }else {
                    self.selectedStudentType = 1   //1: Online
                    self.quiz_studentType.text = options[row]
            }
        }
    }
    
    //--->API Get Picker Data!!
    
    func getGrades(){
        Spinner = UIViewController.displaySpinner(onView: self.view)
        //Downloading article  data for TableView //api/Grades/GetAll/filter/  groups or all  /ar
        let headerParams : HTTPHeaders = ["Lang": "en"]
        AF.request("\(URLs.BaseUrl)api/Grades/GetAll/filter/groups", method: .get, parameters: nil, encoding: URLEncoding.default, headers: headerParams)
            .responseJSON{ response in
            
            UIViewController.removeSpinner(spinner: self.Spinner)
            let result = response.result
            print(response)
            switch response.result {
            case .success(let value):
                //print(String(data: value as! Data, encoding: .utf8)!)
                if let dict = value as? [Dictionary<String, AnyObject>] {
                    self.grades.removeAll()
                    for obj in dict {
                        let result = Grade(country: obj)
                        self.grades.append(result)
                        print(result._Name)
                    }
                    self.PickerSelectView.reloadAllComponents()
                    if self.grades.count > 0 {
                        
                            self.ShowPickerView(ViewPickerDate: false, ViewPickerData: true)
                            self.quiz_grade.text = self.grades[0]._Name
                            self.selectedGradeID = self.grades[0]._Id
                        
                    } else {
                        let alert = UIAlertController(title: "ooops!", message: "Error loading data", preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: .destructive, handler: { (UIAlertAction) in
                                self.ShowPickerView(ViewPickerDate: false, ViewPickerData: false)
                            }))
                        self.present(alert, animated: true)
                    }
                }
                
            case .failure(let error):
                print(error)
            }
        }
    }
    
    
    @IBAction func SaveClosePickerAction(_ sender: UIButton) {
        
        self.ShowPickerView(ViewPickerDate: false, ViewPickerData: false)
    }
    //----> End Picker(Grade/ST.type) selection
    //-------------------------------------------------------
    
    
    
    
    
    //-------------------------------------------------------
    //----> Start Table Groups/Bank Data
    @IBAction func ShowGroupBankOption(_ sender: UIButton) {//show table of data
        if sender.tag == 1 {
            
            if self.selectedGradeID == 0 {
                
                        let alert = UIAlertController(title: "Attention!", message: "Please select Grade first", preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: .destructive, handler: { (UIAlertAction) in
                            }))
                        self.present(alert, animated: true)
                
            } else {
                 
                        if groupes.isEmpty {
                                self.getGroupes()
                        } else {
                                self.ShowHomeView(ScrollMain: false, ViewGroup: true, ViewBank: false)
                                //self.TableViewGroup.reloadData()
                        }
                              
             }
                
        } else if sender.tag == 2 {
            
            if self.selectedGradeID == 0 {
                
                        let alert = UIAlertController(title: "Attention!", message: "Please select Grade first", preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: .destructive, handler: { (UIAlertAction) in
                            }))
                        self.present(alert, animated: true)
                
            } else {
                 
                        if banks.isEmpty {
                                self.getBanks()
                        } else {
                                self.ShowHomeView(ScrollMain: false, ViewGroup: false, ViewBank: true)
                                //self.TableViewGroup.reloadData()
                        }
                              
             }
                
        }
    }
     func ShowHomeView( ScrollMain:Bool, ViewGroup:Bool, ViewBank:Bool ){

        //----> Add Animation here !!
        self.ScrollViewMain.isHidden = !ScrollMain
        self.ViewGroupList.isHidden  = !ViewGroup
        self.ViewBankList.isHidden   = !ViewBank
    }
    @IBAction func CloseTableView(_ sender: UIButton) {
        if sender.tag == 1 {   //select from group
            self.selectedGroupID = []
            for cell in TableViewGroup.visibleCells {
                cell.setSelected(false, animated: true)
                cell.accessoryType = .none
            }
            self.ShowHomeView(ScrollMain: true, ViewGroup: false, ViewBank: false)
        } else if sender.tag == 2 {   //select from bank
            self.selectedBankID = 0
            for cell in TableViewBank.visibleCells {
                cell.setSelected(false, animated: true)
                cell.accessoryType = .none
            }
            self.ShowHomeView(ScrollMain: true, ViewGroup: false, ViewBank: false)
        }
    }
    @IBAction func SaveTableView(_ sender: UIButton) {
        if sender.tag == 1 {   //select from group
            
            self.ShowHomeView(ScrollMain: true, ViewGroup: false, ViewBank: false)
            
        } else if sender.tag == 2 {   //select from bank
            
            self.ShowHomeView(ScrollMain: true, ViewGroup: false, ViewBank: false)
            
        }
    }
    
    
    
    
    //------> API Get Groups data
    var groupes = [Group]()
    func getGroupes(){
        
        Spinner = UIViewController.displaySpinner(onView: self.view)
        let headerParams : HTTPHeaders = ["Lang": "en"]
        AF.request("\(URLs.BaseUrl)api/Groups/GetGroupsByGradeId/\(self.selectedGradeID)", method: .get, parameters: nil, encoding: URLEncoding.default, headers: headerParams).responseJSON{ response in
            
            UIViewController.removeSpinner(spinner: self.Spinner)
            let result = response.result
            print(result)
            switch response.result {
            case .success(let value):
                //print(String(data: value as! Data, encoding: .utf8)!)
                if let dict = value as? [Dictionary<String, AnyObject>] {
                    self.groupes.removeAll()
                    for obj in dict {
                        let result = Group(country: obj)
                        self.groupes.append(result)
                        print(result._GroupName)
                    }
                    
                     self.TableViewGroup.reloadData()
                    if self.groupes.count > 0 {
                       
                        self.ShowHomeView(ScrollMain: false, ViewGroup: true, ViewBank: false)
                    } else {
                        let alert = UIAlertController(title: "ooops!", message: "Error loading data", preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: .destructive, handler: { (UIAlertAction) in
                                self.ShowHomeView(ScrollMain: true, ViewGroup: false, ViewBank: false)
                            }))
                        self.present(alert, animated: true)
                    }
                        
                }
                
            case .failure(let error):
                print(error)
                
            }
            
         }
    }
 
    
    //------> API Get Bank data
    var banks = [Bank]()
    func getBanks(){
        
        Spinner = UIViewController.displaySpinner(onView: self.view)
        let headerParams : HTTPHeaders = ["Authorization": helper.getUserToken() ?? ""]
        
        AF.request("\(URLs.BaseUrl)api/QuestionsBank/GetAll/\(self.selectedGradeID)/en", method: .get, parameters: nil, encoding: URLEncoding.default, headers: headerParams).responseJSON{ response in
            
            UIViewController.removeSpinner(spinner: self.Spinner)
            let result = response.result
            print(result)
            switch response.result {
            case .success(let value):
                //print(String(data: value as! Data, encoding: .utf8)!)
                if let dict = value as? [Dictionary<String, AnyObject>] {
                    self.banks.removeAll()
                    for obj in dict {
                        let result = Bank(country: obj)
                        self.banks.append(result)
                        print(result._BankName)
                    }
 
                    self.TableViewBank.reloadData()
                    if self.banks.count > 0 {
                       
                        self.ShowHomeView(ScrollMain: false, ViewGroup: false, ViewBank: true)
                    } else {
                        let alert = UIAlertController(title: "ooops!", message: "Error loading data", preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: .destructive, handler: { (UIAlertAction) in
                                self.ShowHomeView(ScrollMain: true, ViewGroup: false, ViewBank: false)
                            }))
                        self.present(alert, animated: true)
                    }
                        
                }
                
            case .failure(let error):
                
                if (response.response?.statusCode == 401) {
                 
                    let storyboard = UIStoryboard(name: "Auth", bundle: nil)
                    let vc         = storyboard.instantiateViewController(withIdentifier: "SessionExpireVC")
                        vc.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency
                    self.present(vc, animated: true, completion: nil)
                    // self.navigationController?.pushViewController(vc, animated: true)
                    return
                 
                } else {
                 
                    self.alertmessage(Message: "Failed to fetch question banks data:\n \(error) \n\n Try Again Later.." )
                    return
                 
                }
                
            }
            
         }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
         return UITableView.automaticDimension + 50
    }
     
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == self.TableViewGroup {
         
              return self.groupes.count
            
        } else if tableView == self.TableViewBank {
            
              return self.banks.count
            
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
       
        if tableView == self.TableViewGroup {
         
                    let cell = self.TableViewGroup.dequeueReusableCell(withIdentifier: "QuizOptionCell", for: indexPath) as! QuizOptionCell
                        cell.option_name.text = groupes[indexPath.row]._GroupName
                    return cell
              
        } else if tableView == self.TableViewBank {
            
                    let cell = self.TableViewBank.dequeueReusableCell(withIdentifier: "QuizOptionCell", for: indexPath) as! QuizOptionCell
                        cell.option_name.text = banks[indexPath.row]._BankName
                    return cell
            
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == self.TableViewGroup {
                
              self.selectedGroupID.append(String(self.groupes[indexPath.row]._GroupId))
                if self.selectedGroupID.count > 1{
                    self.quiz_group.text = "\(self.selectedGroupID.count) Groups selected"
                }else {
                    self.quiz_group.text = "\(self.selectedGroupID.count) Group selected"
                }
              self.TableViewGroup.cellForRow(at: indexPath as IndexPath)?.accessoryType = .checkmark
           
        } else if tableView == self.TableViewBank {
                
            self.selectedBankID = self.banks[indexPath.row]._BankId
            self.quiz_bankName.text = self.banks[indexPath.row]._BankName
            self.TableViewBank.cellForRow(at: indexPath as IndexPath)?.accessoryType = .checkmark
                   
        }
    }
  
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {

        if tableView == self.TableViewGroup {
         
            for item in self.selectedGroupID {
                if Int(item) == self.groupes[indexPath.row]._GroupId {
                     self.selectedGroupID.remove(at: self.selectedGroupID.firstIndex(where: {$0 == item})!)
                }
            }
            if self.selectedGroupID.count > 1{
                self.quiz_group.text = "\(self.selectedGroupID.count) Groups selected"
            }else {
                self.quiz_group.text = "\(self.selectedGroupID.count) Group selected"
            }
            
        }
        
        tableView.cellForRow(at: indexPath as IndexPath)?.accessoryType = .none
    }

    //----> End Table Groups/Bank Data
    //-------------------------------------------------------

    
    
    //-------------------------------------------------------
    //----> Next Step Action :
    @IBAction func nextStepAction(_ sender: UIButton) {
        
        if quiz_name.text == "" {
            self.ShowAlert(Msg: "Please add quiz name first")
            
        }else if selectedDateFrom == "" {
            self.ShowAlert(Msg: "Please select date from first")
            
        }else if selectedDateTo == "" {
            self.ShowAlert(Msg: "Please select date to first")
            
        }else if quiz_time.text == "" {
            self.ShowAlert(Msg: "Please add quiz time first")
            
        }else if selectedGradeID == 0 {
            self.ShowAlert(Msg: "Please select grade first")
            
        }else if self.AppSysOption == true && selectedStudentType == -1{
            self.ShowAlert(Msg: "Please select student type first")
 
        }else if selectedGroupID.isEmpty {
            self.ShowAlert(Msg: "Please select groups first")
            
        }else if selectedBankID == 0 {
            self.ShowAlert(Msg: "Please select bank question first")
            
        }else {
            
            let storyboard            = UIStoryboard(name: "Teacher", bundle: nil)
            let vc                    = storyboard.instantiateViewController(withIdentifier: "NewQuizCompVC") as! NewQuizCompVC
               //vc.modalPresentationStyle = .overFullScreen
               vc.modalTransitionStyle   = .crossDissolve

               vc.newQuiz_Name         = quiz_name.text ?? ""
               vc.newQuiz_From         = selectedDateFrom
               vc.newQuiz_To           = selectedDateTo
               vc.newQuiz_Duration     = Int(quiz_time.text ?? "0")!
               vc.newQuiz_GradeID      = selectedGradeID
               vc.newQuiz_StudentType  = selectedStudentType
               vc.newQuiz_GroupID      = selectedGroupID
               vc.newQuiz_BankID       = selectedBankID
             
            //self.present(vc, animated: true, completion: nil)
            //self.show(vc, sender: nil)
            self.navigationController?.pushViewController(vc, animated: true)
        }
 
    }//---- End of nextStepAction func ----
    
    
    
    
    
    
}//----End Of Class

 
//extension NewQuizVC {
//    
//    func ShowAlert( Msg: String ){
//        
//        let alert = UIAlertController(title: "Attention !", message: "\(Msg)", preferredStyle: .alert)
//            alert.addAction(UIAlertAction(title: "Ok", style: .destructive, handler: nil))
//        self.present(alert, animated: true)
//     }
// }
