//
//  NewBankVC.swift
//  LMS
//
//  Created by MacBook on 7/1/20.
//  Copyright © 2020 eaglez-group. All rights reserved.
//
 

//class NewBankVC: UIViewController

 
import UIKit
import Alamofire
import SwiftyJSON

class NewBankVC: BaseVC, UIPickerViewDataSource, UIPickerViewDelegate {
     
    // force Time input to accept numbers only
 
    @IBOutlet weak var bank_name_ar: DesignableTextField!
    @IBOutlet weak var bank_name_en: DesignableTextField!
    @IBOutlet weak var bank_grade:       UILabel!
    
    @IBOutlet weak var ViewPickerData:   UIView!
    @IBOutlet weak var PickerSelectView: UIPickerView! //for Grade | Student type
    
    @IBOutlet weak var ScrollViewMain: UIScrollView!
    
    var Spinner  : UIView!
 
    // Selected variables:
    var selectedGradeID           = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.hideKeyboardWhenTappedAround()
        
        self.PickerSelectView.dataSource = self
        self.PickerSelectView.delegate   = self
        
        self.ShowHomeView(ScrollMain: true, ViewGroup: false, ViewBank: false)
        self.ShowPickerView(ViewPickerDate: false, ViewPickerData: false)
        
        
    }//----End Of ViewDidLoad
    
 
    func ShowPickerView( ViewPickerDate:Bool, ViewPickerData:Bool ){
        
        //----> Add Animation here !!
         self.ViewPickerData.isHidden  = !ViewPickerData
     }
 
    
    
    
    //-------------------------------------------------------
    //----> Strart Picker(Grade/ST.type) selection

    var grades = [Grade]()
    @IBAction func ShowPickerOption(_ sender: UIButton) {//show picker view of data
          
            if grades.isEmpty {
                self.getGrades()
            } else {
                self.ShowPickerView(ViewPickerDate: false, ViewPickerData: true)
                self.PickerSelectView.reloadAllComponents()
            }
         
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return grades.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return grades[row]._Name
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
            self.selectedGradeID = grades[row]._Id
            self.bank_grade.text = grades[row]._Name
    }
    
    //--->API Get Picker Data!!
    
    func getGrades(){
        Spinner = UIViewController.displaySpinner(onView: self.view)
        //Downloading article  data for TableView //api/Grades/GetAll/filter/  groups or all  /ar
        let headerParams : HTTPHeaders = ["Lang": "en"]
        AF.request("\(URLs.BaseUrl)api/Grades/GetAll/filter/groups", method: .get, parameters: nil, encoding: URLEncoding.default, headers: headerParams)
            .responseJSON{ response in
            
            UIViewController.removeSpinner(spinner: self.Spinner)
            let result = response.result
            print(response)
            switch response.result {
            case .success(let value):
                //print(String(data: value as! Data, encoding: .utf8)!)
                if let dict = value as? [Dictionary<String, AnyObject>] {
                    self.grades.removeAll()
                    for obj in dict {
                        let result = Grade(country: obj)
                        self.grades.append(result)
                        print(result._Name)
                    }
                    self.PickerSelectView.reloadAllComponents()
                    if self.grades.count > 0 {
                        
                            self.ShowPickerView(ViewPickerDate: false, ViewPickerData: true)
                            self.bank_grade.text = self.grades[0]._Name
                            self.selectedGradeID = self.grades[0]._Id
                        
                    } else {
                        let alert = UIAlertController(title: "ooops!", message: "Error loading data", preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: .destructive, handler: { (UIAlertAction) in
                                self.ShowPickerView(ViewPickerDate: false, ViewPickerData: false)
                            }))
                        self.present(alert, animated: true)
                    }
                }
                
            case .failure(let error):
                print(error)
            }
        }
    }
    
    
    @IBAction func SaveClosePickerAction(_ sender: UIButton) {
        
        self.ShowPickerView(ViewPickerDate: false, ViewPickerData: false)
    }
    //----> End Picker(Grade/ST.type) selection
    //-------------------------------------------------------
    
     
     func ShowHomeView( ScrollMain:Bool, ViewGroup:Bool, ViewBank:Bool ){

        //----> Add Animation here !!
        self.ScrollViewMain.isHidden = !ScrollMain
    }
    
    //-------------------------------------------------------
    //----> Next Step Action :
    @IBAction func nextStepAction(_ sender: UIButton) {
        
        if bank_name_ar.text == "" {
            self.ShowAlert(Msg: "Please add arabic bank name first")
            
        } else if bank_name_en.text == "" {
            self.ShowAlert(Msg: "Please add english bank name first")
            
        } else if selectedGradeID == 0 {
            self.ShowAlert(Msg: "Please select grade first")
            
        }else {
            
            API.createQuestionBank(vc: self, Token: helper.getUserToken() ?? "",
                                   NameAr: self.bank_name_ar.text!,
                                   NameEn: self.bank_name_en.text!,
                                   GradeId: self.selectedGradeID)
            { (err,TimeOut, msg, bankId) in
                                    
                                    
                        if let err = err {
                            if TimeOut! {
                                let storyboard = UIStoryboard(name: "Auth", bundle: nil)
                                let vc         = storyboard.instantiateViewController(withIdentifier: "SessionExpireVC")
                                    vc.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency
                                self.present(vc, animated: true, completion: nil)
                               // self.navigationController?.pushViewController(vc, animated: true)
                                return
                                
                            } else {
                                self.alertmessage(Message: "Failed to fetch private material videos:\n \(err) \n\n Try Again Later.." )
                                return
                            }
                        
                        } else if msg != "" {
                            
                            self.ShowAlert(Msg: "\(msg)")
                        
                        } else {
                            
                             //print("Message Sent Successfully")
                            self.resetCreateNewBank()
                            
                             let alert = UIAlertController(title: "Done", message: " Bank Successfully Created ", preferredStyle: UIAlertController.Style.alert)
                                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                                alert.addAction(UIAlertAction(title: "Next Step", style: .destructive, handler:
                                        {(alert: UIAlertAction!) in
                                         
                                            if bankId != 0 {
                                               
                                                //------> start Bank Questions <------
                                                let storyboard   = UIStoryboard(name: "Teacher", bundle: nil)
                                                let vc           = storyboard.instantiateViewController(withIdentifier: "BankDetailsVC") as! BankDetailsVC
                                                    vc.modalPresentationStyle = .overFullScreen
                                                    vc.modalTransitionStyle   = .crossDissolve
                                                    vc.BankId = bankId
                                                    vc.BankStatus = "New"
                                                //self.present(vc, animated: true, completion: nil)
                                                self.navigationController?.pushViewController(vc, animated: true)
                                                
                                            }
                                            
                                        
                                          
                                     }))
                            self.present(alert, animated: true)
                        }
                                    
            }
             
        }
 
    }//---- End of nextStepAction func ----
    
    
    func resetCreateNewBank(){
        
        self.selectedGradeID     = 0
        self.bank_name_ar.text   = ""
        self.bank_name_en.text   = ""
        self.bank_grade.text     = "Choose Grade"
    }
    
     
}//----End Of Class

 
