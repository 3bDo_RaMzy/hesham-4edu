//
//  FinishNewQuizVC.swift
//  LMS
//
//  Created by MacBook on 6/22/20.
//  Copyright © 2020 eaglez-group. All rights reserved.
//

import UIKit

class FinishNewQuizVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.navigationItem.setHidesBackButton(true, animated:true);
    }
    

    @IBAction func FinishNewQuizAction(_ sender: UIButton) {
         
        self.navigationController?.popToRootViewController(animated: true)
     }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
