//
//  NewQuizCompVC.swift
//  LMS
//
//  Created by MacBook on 6/17/20.
//  Copyright © 2020 eaglez-group. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class NewQuizCompVC: BaseVC {

    var newQuiz_Name              = ""
    var newQuiz_From              = ""
    var newQuiz_To                = ""
    var newQuiz_Duration          = 0
    var newQuiz_GradeID           = 0
    var newQuiz_StudentType       = 0   // 0: academic | 1: online
    var newQuiz_GroupID: [String] = []  // multi select group id
    var newQuiz_BankID            = 0
    var newQuiz_SelectedOptions   = [QuizeOption]()
    
    
    @IBOutlet weak var Available_Multi:     UILabel!
    @IBOutlet weak var Available_TrueFalse: UILabel!
    @IBOutlet weak var Available_Article:   UILabel!
    
    @IBOutlet weak var txtCount_Multi:      UITextField!
    @IBOutlet weak var txtCount_TrueFalse:  UITextField!
    @IBOutlet weak var txtCount_Article:    UITextField!
    
    @IBOutlet weak var txtDegree_Multi:     UITextField!
    @IBOutlet weak var txtDegree_TrueFalse: UITextField!
    @IBOutlet weak var txtDegree_Article:   UITextField!
    
    
    var Spinner  : UIView!
    var newQuiz_Otions = [QuizeOption]()
    
    var countMulti     = 0
    var countTrueFalse = 0
    var countArticle   = 0
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.hideKeyboardWhenTappedAround()
 
        self.txtCount_Multi.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        self.txtCount_TrueFalse.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        self.txtCount_Article.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
        self.getQuizeOptions()
        
    }
    
    
    @objc func textFieldDidChange(_ textField: UITextField) {

        if textField == self.txtCount_Multi {
            
            if textField.text != ""{
                
                if Int(textField.text!)! > self.countMulti {
                    
                    let alert = UIAlertController(title: "Attention!", message: "Multi-Choice Count is bigger than Available Count", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: .destructive, handler: { (UIAlertAction) in
                            self.txtCount_Multi.text = ""
                        }))
                    self.present(alert, animated: true)
                }
            }
            
            
        } else if textField == self.txtCount_TrueFalse {
            
            if textField.text != ""{
                
                if Int(textField.text!)! > self.countTrueFalse {
                    
                    let alert = UIAlertController(title: "Attention!", message: "True/False Count is bigger than Available Count", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: .destructive, handler: { (UIAlertAction) in
                            self.txtCount_TrueFalse.text = ""
                        }))
                    self.present(alert, animated: true)
                }
            }
            
        } else if textField == self.txtCount_Article {
                   
            if textField.text != ""{
                
                if Int(textField.text!)! > self.countArticle {
                    
                    let alert = UIAlertController(title: "Attention!", message: "Article Count exceeds Available Count", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: .destructive, handler: { (UIAlertAction) in
                            self.txtCount_Article.text = ""
                        }))
                    self.present(alert, animated: true)
                }
            }
                   
        }
    }
    
    
     
    
    //----> Get API For QuizOption Data: 
    func getQuizeOptions(){
           
        API.getNewQuizQuestionsCount(vc: self, Bank_ID: self.newQuiz_BankID) {
            (error, Multi, TF, Article) in
            
            if error != nil {
                
                let alert = UIAlertController(title: "ooops!", message: "Error loading data", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Try again", style: .destructive, handler: { (UIAlertAction) in
                    self.getQuizeOptions()
                }))
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (UIAlertAction) in
                    
                }))
                self.present(alert, animated: true)
                
            } else {
                
                self.countMulti     = Multi
                self.countTrueFalse = TF
                self.countArticle   = Article
                
                self.Available_Multi.text     = String(Multi)
                self.Available_TrueFalse.text = String(TF)
                self.Available_Article.text   = String(Article)
                
                if Multi == 0 {
                    self.txtCount_Multi.isEnabled      = false
                    self.txtDegree_Multi.isEnabled     = false
                }
                if TF == 0 {
                    self.txtCount_TrueFalse.isEnabled  = false
                    self.txtDegree_TrueFalse.isEnabled = false
                }
                if Article == 0 {
                    self.txtCount_Article.isEnabled    = false
                    self.txtDegree_Article.isEnabled   = false
                }
                
                
            }
        }
    }//---End Of getQuizeOptions
    
 
    
    
    @IBAction func StartQuizAction(_ sender: UIButton) {
 
        if self.Available_Multi.text == "0" {
            
             self.newQuiz_Otions.append(QuizeOption(QuizeType: 1, QuizeCount: 0, QuizeDegree: 0))
            
         } else {
             self.newQuiz_Otions.append(QuizeOption(QuizeType:   1,
                                                    QuizeCount:  Int(self.txtCount_Multi.text  ?? "0") ?? 0,
                                                    QuizeDegree: Int(self.txtDegree_Multi.text ?? "0") ?? 0 ))
         }
        
        
        if self.Available_TrueFalse.text == "0" {
            self.newQuiz_Otions.append(QuizeOption(QuizeType: 2, QuizeCount: 0, QuizeDegree: 0))
            
        } else {
            self.newQuiz_Otions.append(QuizeOption(QuizeType:   2,
                                                   QuizeCount:  Int(self.txtCount_TrueFalse.text  ?? "0" ) ?? 0,
                                                   QuizeDegree: Int(self.txtDegree_TrueFalse.text ?? "0") ?? 0 ))
        }
        
        if self.Available_Article.text   == "0" {
            self.newQuiz_Otions.append(QuizeOption(QuizeType: 3, QuizeCount: 0, QuizeDegree: 0))
            
        } else {
            self.newQuiz_Otions.append(QuizeOption(QuizeType:   3,
                                                   QuizeCount:  Int(self.txtCount_Article.text  ?? "0" ) ?? 0 ,
                                                   QuizeDegree: Int(self.txtDegree_Article.text ?? "0") ?? 0 ))
        }
        
        
        
        
        
        
        
        
        
        
        
         let optionArray =  [
                                 "QuizeType": 0,
                                 "Count": 0,
                                 "Degree": "" ] as [String : Any] //ArticleAnswer
         
         var optionsArray =  [  optionArray  ] as [[String : Any]]
             optionsArray.removeAll()
         
         
         for (_, model) in self.newQuiz_Otions.enumerated() {

             let serviceArray =  [
                                    "QuizeType": model.QuizeType,
                                    "Count":     model.QuizeCount,
                                    "Degree":    model.QuizeDegree ] as [String : Any]

             optionsArray.append(serviceArray)

         }
          
         let paramsData =  [
                             "text":        self.newQuiz_Name,
                             "from":        self.newQuiz_From,
                             "to":          self.newQuiz_To,
                             "duration":    self.newQuiz_Duration,
                             "GradeId":     self.newQuiz_GradeID,
                             "StudnetType": self.newQuiz_StudentType,
                             "group":       self.newQuiz_GroupID,
                             "bankId":      self.newQuiz_BankID,
                             "QuizeOption": optionsArray  ] as [String : Any]
        
        let url = URLs.BaseUrl + "api/Mob/quizes/add"
        
        self.createRequest(token: helper.getUserToken() ?? "", parameters: paramsData, serviceUrl: url)
        
        
    } //---> StartQuizAction -----------
    
    
    func createRequest(token:       String,
                               parameters: Dictionary<String, Any>,
                               serviceUrl: String ) {
                
                var Spinner:           UIView!
                let apiURL          =  URL(string:serviceUrl)!
                var mutableRequest  =  URLRequest(url: apiURL)
                let authorizedToken =  token
               
               // print(authorizedToken)
                mutableRequest.httpMethod = "POST"
                mutableRequest.timeoutInterval = TimeInterval(MAXFLOAT)//
                mutableRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
                //mutableRequest.addValue(Prefs.getLng(), forHTTPHeaderField: "Lang")
                mutableRequest.addValue(authorizedToken, forHTTPHeaderField: "Authorization")
                
                do {
                    
                    mutableRequest.httpBody = try JSONSerialization.data(withJSONObject: parameters)//, options: JSONSerialization.WritingOptions())
                    
                } catch {
                    // No-op
                    print("catechecexp")
                }
                
                
                
                Spinner = UIViewController.displaySpinner(onView: self.view)
                _ = AF.request(mutableRequest as URLRequestConvertible).responseJSON {
                    
                (response) in
                    
                    
                    UIViewController.removeSpinner(spinner: Spinner)
                    print("\n\n Response: \(response.value)")
                    
                    switch response.result {
                        
                    case .failure(let error):
                        
                                if (response.response?.statusCode == 401) {
                                 
                                    let storyboard = UIStoryboard(name: "Auth", bundle: nil)
                                    let vc         = storyboard.instantiateViewController(withIdentifier: "SessionExpireVC")
                                        vc.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency
                                    self.present(vc, animated: true, completion: nil)
                                    // self.navigationController?.pushViewController(vc, animated: true)
                                    return
                                 
                                } else {
                                 
                                    self.alertmessage(Message: "Failed to create new quiz:\n \(error) \n\n Try Again Later.." )
                                    return
                                 
                                }
                        
                    case .success(let value):
                        
                                //vc.dismiss(animated: true, completion: nil)
                                let json = JSON(value)

                                let message   = json["note"].string     // Completed
                                
                                if message == "saved"{
                                    
                                    let storyboard            = UIStoryboard(name: "Teacher", bundle: nil)
                                    let vc                    = storyboard.instantiateViewController(withIdentifier: "FinishNewQuizVC") as! FinishNewQuizVC
                                        vc.modalPresentationStyle = .overFullScreen
                                        vc.modalTransitionStyle   = .crossDissolve
                                    
                                    //self.present(vc, animated: true, completion: nil)
                                    //self.show(vc, sender: nil)
                                    self.navigationController?.pushViewController(vc, animated: true)
                                    
                                } else {
                                    
                                    self.ShowAlert(Msg: "\(message)")
                                }
                                
                                

                     }
                 
                }
          
    }
    
    
    

}//-------- End Of Class -------------

 
