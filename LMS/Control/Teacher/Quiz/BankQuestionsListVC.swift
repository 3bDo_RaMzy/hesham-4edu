//
//  BankQuestionsListVC.swift
//  LMS
//
//  Created by MacBook on 7/12/20.
//  Copyright © 2020 eaglez-group. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class BankQuestionsListVC: BaseVC {
 
    var BankId: Int    = 0
    var QuestionType: Int    = 0   //  1: true-False  |  2: MCQ  |  3: Article
    var QuestionStringType: String = ""
    
    @IBOutlet weak var ViewNoQuestionBank:    UIView!
    @IBOutlet weak var QuestionBankTableView: UITableView!
    @IBOutlet weak var BankQuestionsTitle:    UILabel!
    
    var BanksQuestionsViewModel = [BankQuestionsModel]()
    var Spinner        :UIView!
    private let refreshControl = UIRefreshControl()
    
    let dateFormatter = DateFormatter()
    let date          = Date()
      
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Questions List"
        
        if QuestionType == 1 {
            self.BankQuestionsTitle.text = "Bank True-False Questions List"
        } else if QuestionType == 2 {
            self.BankQuestionsTitle.text = "Bank MCQ Questions List"
        } else if QuestionType == 3 {
            self.BankQuestionsTitle.text = "Bank Article Questions List"
        }
        
         dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
          
        self.ShowHomeView(ScrollMain: false, ViewGroup: false, ViewBank: false)
        self.ViewNoQuestionBank.isHidden = false
        
         // set up the refresh control
        if #available(iOS 10.0, *) {
             QuestionBankTableView.refreshControl = refreshControl
         } else {
             QuestionBankTableView.addSubview(refreshControl)
         }
          refreshControl.tintColor = UIColor(red:0.25, green:0.72, blue:0.85, alpha:1.0)
          refreshControl.attributedTitle = NSAttributedString(string: "Refresh Data...", attributes: nil)
          refreshControl.addTarget(self, action: #selector(refreshData(_:)), for: .valueChanged)
                                      
          self.QuestionBankTableView.dataSource     = self
          self.QuestionBankTableView.delegate       = self
          self.QuestionBankTableView.layoutMargins  = UIEdgeInsets.zero
          self.QuestionBankTableView.separatorInset = UIEdgeInsets.zero
 
          self.QuestionBankTableView.estimatedRowHeight = 80.0
          self.QuestionBankTableView.rowHeight = UITableView.automaticDimension
            
         
    } //-----> End of ViewDidLoad --------
     
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.fetchData()
    }
    
    @IBAction func AddNewQuestionAction(_ sender: UIBarButtonItem) {
        
        if QuestionType == 1 {
            
            //------> start Bank Questions <------
            let storyboard   = UIStoryboard(name: "Teacher", bundle: nil)
            let vc           = storyboard.instantiateViewController(withIdentifier: "TrueFalse_QListVC") as! TrueFalse_QListVC
                //vc.modalPresentationStyle = .overFullScreen
                vc.modalTransitionStyle   = .crossDissolve
                vc.BankId = BankId
            //self.present(vc, animated: true, completion: nil)
            self.navigationController?.pushViewController(vc, animated: true)
            
        } else if QuestionType == 2 {
            
            //------> start Bank Questions <------
            let storyboard   = UIStoryboard(name: "Teacher", bundle: nil)
            let vc           = storyboard.instantiateViewController(withIdentifier: "MCQ_QListVC") as! MCQ_QListVC
                //vc.modalPresentationStyle = .overFullScreen
                vc.modalTransitionStyle   = .crossDissolve
                vc.BankId = BankId
            //self.present(vc, animated: true, completion: nil)
            self.navigationController?.pushViewController(vc, animated: true)
            
        } else if QuestionType == 3 {
            
            //------> start Bank Questions <------
            let storyboard   = UIStoryboard(name: "Teacher", bundle: nil)
            let vc           = storyboard.instantiateViewController(withIdentifier: "Article_QListVC") as! Article_QListVC
                //vc.modalPresentationStyle = .overFullScreen
                vc.modalTransitionStyle   = .crossDissolve
                vc.BankId = BankId
            //self.present(vc, animated: true, completion: nil)
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
    }
    
    
    
    
    
    @objc private func refreshData(_ sender: Any) {
        // Fetch Weather Data
            self.refreshData()
    }
 
    
    func ShowHomeView( ScrollMain:Bool, ViewGroup:Bool, ViewBank:Bool ){

        //----> Add Animation here !!
        self.QuestionBankTableView.isHidden = !ScrollMain
    }
 
    
    //--------------------------------------------------------------------
    fileprivate func fetchData() {
                        
        Spinner = UIViewController.displaySpinner(onView: self.view)
         
        API.getQuestionsBankList(vc: self, Token: helper.getUserToken() ?? "", BankId: self.BankId, QuestionType: self.QuestionStringType) {
            (err,TimeOut, Questions) in
                                        
                    UIViewController.removeSpinner(spinner: self.Spinner)
            
                     if let err = err {
                         if TimeOut! {
                             let storyboard = UIStoryboard(name: "Auth", bundle: nil)
                             let vc         = storyboard.instantiateViewController(withIdentifier: "SessionExpireVC")
                                 vc.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency
                             self.present(vc, animated: true, completion: nil)
                            // self.navigationController?.pushViewController(vc, animated: true)
                             return
                             
                         } else {
                             self.alertmessage(Message: "Failed to fetch Question Bank List:\n \(err) \n\n Try Again Later.." )
                             return
                         }
                     }

                    self.BanksQuestionsViewModel = Questions?.map({return BankQuestionsModel(bank:  $0)}) ?? []
                    if self.BanksQuestionsViewModel.count <= 0 {
                        self.showToast(message: "No Questions Available Now", vc: self)
                        self.ShowHomeView(ScrollMain: false, ViewGroup: false, ViewBank: false)
                        self.ViewNoQuestionBank.isHidden = false
                    } else {
                        
                        self.ViewNoQuestionBank.isHidden = true
                        self.ShowHomeView(ScrollMain: true, ViewGroup: false, ViewBank: false)
                        self.QuestionBankTableView.reloadData()
                        
                    }
            
                    
            }
                        
     }//---> End Of fetchData method -------
    fileprivate func refreshData() {
                       
        self.refreshControl.beginRefreshing()
        API.getQuestionsBankList(vc: self, Token: helper.getUserToken() ?? "", BankId: self.BankId, QuestionType: self.QuestionStringType) {
            (err,TimeOut, Questions) in
                           
                   self.refreshControl.endRefreshing()
            
                    if let err = err {
                        if TimeOut! {
                            let storyboard = UIStoryboard(name: "Auth", bundle: nil)
                            let vc         = storyboard.instantiateViewController(withIdentifier: "SessionExpireVC")
                                vc.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency
                            self.present(vc, animated: true, completion: nil)
                           // self.navigationController?.pushViewController(vc, animated: true)
                            return
                            
                        } else {
                            self.alertmessage(Message: "Failed to fetch Question Bank List:\n \(err) \n\n Try Again Later.." )
                            return
                        }
                    }

                           
                   self.BanksQuestionsViewModel = Questions?.map({return BankQuestionsModel(bank:  $0)}) ?? []
                   if self.BanksQuestionsViewModel.count <= 0 {
                       self.showToast(message: "No Questions Available Now", vc: self)
                       self.ShowHomeView(ScrollMain: false, ViewGroup: false, ViewBank: false)
                       self.ViewNoQuestionBank.isHidden = false
                   } else {
                       
                       self.ViewNoQuestionBank.isHidden = true
                       self.ShowHomeView(ScrollMain: true, ViewGroup: false, ViewBank: false)
                       self.QuestionBankTableView.reloadData()
                       
                   }
                    
           }
                       
    }//---> End Of fetchData method -------
 
     
//--------------------------------------------------------------------
    func EditQuestionBank(_ bankId: Int){
        
        self.ShowAlert(Msg: "This option  is not available")
            
//        //------> start Bank Questions <------
//        let storyboard   = UIStoryboard(name: "Teacher", bundle: nil)
//        let vc           = storyboard.instantiateViewController(withIdentifier: "BankDetailsVC") as! BankDetailsVC
//            vc.modalPresentationStyle = .overFullScreen
//            vc.modalTransitionStyle   = .crossDissolve
//            vc.BankId = bankId
//            vc.BankStatus = "Edit"
//        //self.present(vc, animated: true, completion: nil)
//        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func DeleteQuestionBank(_ bankId: Int){
        
        self.ShowAlert(Msg: "This option  is not available")
        
//        API.deleteQuestionBank(vc:        self,
//                                  Token:     helper.getUserToken() ?? "",
//                                  BankId: bankId )
//        { (error, status) in
//
//            if status != true {
//
//                            self.ShowAlert(Msg: " Request server faild  ")
//             } else  {
//
//                        //print("Message Sent Successfully")
//                        let alert = UIAlertController(title: "", message: " Bank Deleted Successfully", preferredStyle: UIAlertController.Style.alert)
//                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: {(alert: UIAlertAction!) in
//                                self.fetchData()
//                            }))
//                        self.present(alert, animated: true)
//
//             }
//       }
     }//--- End Of EndLiveSession
    
        
    

}//-------- End Of Class ------------------------------------------------

extension BankQuestionsListVC: UITableViewDataSource, UITableViewDelegate{
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return UITableView.automaticDimension + 50
//    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
 
            return BanksQuestionsViewModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
            let cell = tableView.dequeueReusableCell(withIdentifier: "QuestionsListCell", for: indexPath) as! QuestionsListCell
                cell.BankQuestionsViewModel = BanksQuestionsViewModel[indexPath.row]
                //cell.QuestionName.retrieveTextHeight()
         return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let alert = UIAlertController(title: "", message: "Please Select an Option", preferredStyle: .actionSheet)

            alert.addAction(UIAlertAction(title: "Edit", style: .default , handler:{ (UIAlertAction)in
                self.EditQuestionBank( self.BanksQuestionsViewModel[indexPath.row].Id! )
            }))
            alert.addAction(UIAlertAction(title: "Delete", style: .destructive , handler:{ (UIAlertAction)in
                self.DeleteQuestionBank( self.BanksQuestionsViewModel[indexPath.row].Id! )
            }))

            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))

        self.present(alert, animated: true, completion: nil)
        
    }
                
}

 
