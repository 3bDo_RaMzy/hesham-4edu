//
//  BankDetailsVC.swift
//  LMS
//
//  Created by Abdo on 7/3/20.
//  Copyright © 2020 eaglez-group. All rights reserved.
//

import UIKit

class BankDetailsVC: BaseVC {

    var BankId = 0
    var BankStatus = "" // New | Edit
    
    @IBOutlet weak var LblMCQ:     UILabel!
    @IBOutlet weak var LblTF:      UILabel!
    @IBOutlet weak var LblArticle: UILabel!
    
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        API.getQuestionsBankCount(vc: self, Token: helper.getUserToken() ?? "",
                                  BankId: BankId) { (err, TimeOut, MCQ_Count, TF_Count, Article_Count) in
                  
                if let err = err {
                    if TimeOut! {
                        let storyboard = UIStoryboard(name: "Auth", bundle: nil)
                        let vc         = storyboard.instantiateViewController(withIdentifier: "SessionExpireVC")
                            vc.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency
                        self.present(vc, animated: true, completion: nil)
                       // self.navigationController?.pushViewController(vc, animated: true)
                        return
                        
                    } else {
                        self.alertmessage(Message: "Failed to fetch Question bank details:\n \(err) \n\n Try Again Later.." )
                        return
                    }
                } else {
                    
                    self.LblMCQ.text     = String(MCQ_Count)
                    self.LblTF.text      = String(TF_Count)
                    self.LblArticle.text = String(Article_Count)
                }
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
    }
    

//--------------------------------------------------------------
    
    @IBAction func QuestionMCQAction(_ sender: UIButton) {
        
        //------> start Bank Questions <------
        let storyboard   = UIStoryboard(name: "Teacher", bundle: nil)
        let vc           = storyboard.instantiateViewController(withIdentifier: "BankQuestionsListVC") as! BankQuestionsListVC
            //vc.modalPresentationStyle = .overFullScreen
            vc.modalTransitionStyle   = .crossDissolve
            vc.BankId = BankId
            vc.QuestionType = 2
            vc.QuestionStringType = "MultipleChoiceList"
        //self.present(vc, animated: true, completion: nil)
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func QuestionTrueFalseAction(_ sender: UIButton) {
        
        //------> start Bank Questions <------
        let storyboard   = UIStoryboard(name: "Teacher", bundle: nil)
        let vc           = storyboard.instantiateViewController(withIdentifier: "BankQuestionsListVC") as! BankQuestionsListVC
            //vc.modalPresentationStyle = .overFullScreen
            vc.modalTransitionStyle   = .crossDissolve
            vc.BankId = BankId
            vc.QuestionType = 1
            vc.QuestionStringType = "TrueOrFalseList"
        //self.present(vc, animated: true, completion: nil)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func QuestionArticleAction(_ sender: UIButton) {
        
        //------> start Bank Questions <------
        let storyboard   = UIStoryboard(name: "Teacher", bundle: nil)
        let vc           = storyboard.instantiateViewController(withIdentifier: "BankQuestionsListVC") as! BankQuestionsListVC
            //vc.modalPresentationStyle = .overFullScreen
            vc.modalTransitionStyle   = .crossDissolve
            vc.BankId = BankId
            vc.QuestionType = 3
            vc.QuestionStringType = "ArticleList"
        //self.present(vc, animated: true, completion: nil)
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
