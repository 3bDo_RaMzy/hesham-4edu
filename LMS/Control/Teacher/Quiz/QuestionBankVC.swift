//
//  QuestionBankVC.swift
//  LMS
//
//  Created by MacBook on 7/7/20.
//  Copyright © 2020 eaglez-group. All rights reserved.
//
import UIKit
import Alamofire
import SwiftyJSON

class QuestionBankVC: BaseVC {

    @IBOutlet weak var quiz_grade:       UILabel! 
    @IBOutlet weak var ViewPickerData:   UIView!
    @IBOutlet weak var PickerSelectView: UIPickerView! //for Grade | Student type
    @IBOutlet weak var ViewNoQuestionBank: UIView!
    
    @IBOutlet weak var QuestionBankTableView: UITableView!
    var BanksViewModel  = [BankModel]()
    var Spinner        :UIView!
    private let refreshControl = UIRefreshControl()
    
    let dateFormatter = DateFormatter()
    let date          = Date()
     
    // Selected variables:
    var selectedGradeID = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

         dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
         
        self.PickerSelectView.dataSource = self
        self.PickerSelectView.delegate   = self
        
        self.ShowHomeView(ScrollMain: false, ViewGroup: false, ViewBank: false)
        self.ShowPickerView(ViewPickerDate: false, ViewPickerData: false)
        self.ViewNoQuestionBank.isHidden = false
        
         // set up the refresh control
        if #available(iOS 10.0, *) {
             QuestionBankTableView.refreshControl = refreshControl
         } else {
             QuestionBankTableView.addSubview(refreshControl)
         }
          refreshControl.tintColor = UIColor(red:0.25, green:0.72, blue:0.85, alpha:1.0)
          refreshControl.attributedTitle = NSAttributedString(string: "Refresh Data...", attributes: nil)
          refreshControl.addTarget(self, action: #selector(refreshData(_:)), for: .valueChanged)
                                     
          self.QuestionBankTableView.dataSource     = self
          self.QuestionBankTableView.delegate       = self
          self.QuestionBankTableView.layoutMargins  = UIEdgeInsets.zero
          self.QuestionBankTableView.separatorInset = UIEdgeInsets.zero
          self.QuestionBankTableView.rowHeight      = 80
          
         
    } //-----> End of ViewDidLoad --------
     
    
    @objc private func refreshData(_ sender: Any) {
        // Fetch Weather Data
        if selectedGradeID == 0 {
            
            self.refreshControl.endRefreshing()
            self.ShowAlert(Msg: "Please select grade first")
                   
        } else {
            
            self.refreshData()
        }
        
    }

    func ShowPickerView( ViewPickerDate:Bool, ViewPickerData:Bool ){
       
       //----> Add Animation here !!
        self.ViewPickerData.isHidden  = !ViewPickerData
    }
    
    func ShowHomeView( ScrollMain:Bool, ViewGroup:Bool, ViewBank:Bool ){

        //----> Add Animation here !!
        self.QuestionBankTableView.isHidden = !ScrollMain
    }
    
    
    //-------------------------------------------------------
    //----> Strart Picker(Grade/ST.type) selection

    var grades = [Grade]()
    @IBAction func ShowPickerOption(_ sender: UIButton) {//show picker view of data
          
            if grades.isEmpty {
                self.getGrades()
            } else {
                self.ShowPickerView(ViewPickerDate: false, ViewPickerData: true)
                self.PickerSelectView.reloadAllComponents()
            }
         
    }
    
    //--->API Get Picker Data!!
    
    func getGrades(){
        Spinner = UIViewController.displaySpinner(onView: self.view)
        //Downloading article  data for TableView //api/Grades/GetAll/filter/  groups or all  /ar
        let headerParams : HTTPHeaders = ["Lang": "en"]
        AF.request("\(URLs.BaseUrl)api/Grades/GetAll/filter/groups", method: .get, parameters: nil, encoding: URLEncoding.default, headers: headerParams)
            .responseJSON{ response in
            
            UIViewController.removeSpinner(spinner: self.Spinner)
            let result = response.result
            print(response)
            switch response.result {
            case .success(let value):
                //print(String(data: value as! Data, encoding: .utf8)!)
                if let dict = value as? [Dictionary<String, AnyObject>] {
                    self.grades.removeAll()
                    for obj in dict {
                        let result = Grade(country: obj)
                        self.grades.append(result)
                        print(result._Name)
                    }
                    self.PickerSelectView.reloadAllComponents()
                    if self.grades.count > 0 {
                        
                            self.ShowPickerView(ViewPickerDate: false, ViewPickerData: true)
                            self.quiz_grade.text = self.grades[0]._Name
                            self.selectedGradeID = self.grades[0]._Id
                        
                    } else {
                        let alert = UIAlertController(title: "ooops!", message: "Error loading data", preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: .destructive, handler: { (UIAlertAction) in
                                self.ShowPickerView(ViewPickerDate: false, ViewPickerData: false)
                            }))
                        self.present(alert, animated: true)
                    }
                }
                
            case .failure(let error):
                print(error)
            }
        }
    }
    
    
    @IBAction func SaveClosePickerAction(_ sender: UIButton) {
        
        self.ShowPickerView(ViewPickerDate: false, ViewPickerData: false)
    }
    //----> End Picker(Grade/ST.type) selection
    //-------------------------------------------------------
    
    
    
    
    //--------------------------------------------------------------------
    fileprivate func fetchData() {
                        
        Spinner = UIViewController.displaySpinner(onView: self.view)
         
        API.getAllQuestionsBank(vc: self, Token: helper.getUserToken() ?? "", GradId: self.selectedGradeID)
        
        { (err, TimeOut, Banks) in
                                        
                    UIViewController.removeSpinner(spinner: self.Spinner)

                    if let err = err {
                        if TimeOut! {
                            let storyboard = UIStoryboard(name: "Auth", bundle: nil)
                            let vc         = storyboard.instantiateViewController(withIdentifier: "SessionExpireVC")
                                vc.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency
                            self.present(vc, animated: true, completion: nil)
                           // self.navigationController?.pushViewController(vc, animated: true)
                            return
                            
                        } else {
                            self.alertmessage(Message: "Failed to fetch private material videos:\n \(err) \n\n Try Again Later.." )
                            return
                        }
                    }

            
                    self.BanksViewModel = Banks?.map({return BankModel(bank:  $0)}) ?? []
                    if self.BanksViewModel.count <= 0 {
                        self.showToast(message: "No Bank Questions Available Now !!", vc: self)
                        self.ShowHomeView(ScrollMain: false, ViewGroup: false, ViewBank: false)
                        self.ViewNoQuestionBank.isHidden = false
                    } else {
                        
                        self.ViewNoQuestionBank.isHidden = true
                        self.ShowHomeView(ScrollMain: true, ViewGroup: false, ViewBank: false)
                        self.QuestionBankTableView.reloadData()
                        
                    }
            
                    
            }
                        
     }//---> End Of fetchData method -------
    fileprivate func refreshData() {
                       
        self.refreshControl.beginRefreshing()
        API.getAllQuestionsBank(vc: self, Token: helper.getUserToken() ?? "", GradId: self.selectedGradeID)
        { (err, TimeOut, Banks) in
                           
                   self.refreshControl.endRefreshing()
            
                    if let err = err {
                        if TimeOut! {
                            let storyboard = UIStoryboard(name: "Auth", bundle: nil)
                            let vc         = storyboard.instantiateViewController(withIdentifier: "SessionExpireVC")
                                vc.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency
                            self.present(vc, animated: true, completion: nil)
                           // self.navigationController?.pushViewController(vc, animated: true)
                            return
                            
                        } else {
                            self.alertmessage(Message: "Failed to fetch private material videos:\n \(err) \n\n Try Again Later.." )
                            return
                        }
                    }

            
                   self.BanksViewModel = Banks?.map({return BankModel(bank:  $0)}) ?? []
                   if self.BanksViewModel.count <= 0 {
                      self.showToast(message: "No Bank Questions Available Now !!", vc: self)
                      self.ShowHomeView(ScrollMain: false, ViewGroup: false, ViewBank: false)
                      self.ViewNoQuestionBank.isHidden = false
                   } else {
                    
                    self.ViewNoQuestionBank.isHidden = true
                    self.ShowHomeView(ScrollMain: true, ViewGroup: false, ViewBank: false)
                    self.QuestionBankTableView.reloadData()
                  }
                    
           }
                       
    }//---> End Of fetchData method -------
    
    

    @IBAction func SearchQuestionBankAction(_ sender: UIButton) {
        
        if selectedGradeID == 0 {
            
            self.ShowAlert(Msg: "Please select grade first")
                   
        } else {
            
            self.fetchData()
        }
         
    }
    
    
    
    
    
    
    //--------------------------------------------------------------------
    func EditQuestionBank(_ bankId: Int){
            
        //------> start Bank Questions <------
        let storyboard   = UIStoryboard(name: "Teacher", bundle: nil)
        let vc           = storyboard.instantiateViewController(withIdentifier: "BankDetailsVC") as! BankDetailsVC
            vc.modalPresentationStyle = .overFullScreen
            vc.modalTransitionStyle   = .crossDissolve
            vc.BankId = bankId
            vc.BankStatus = "Edit"
        //self.present(vc, animated: true, completion: nil)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func DeleteQuestionBank(_ bankId: Int){
        
        API.deleteQuestionBank(vc:        self,
                                  Token:     helper.getUserToken() ?? "",
                                  BankId: bankId )
        { (error,TimeOut, status) in

            if let err = error {
                if TimeOut! {
                    let storyboard = UIStoryboard(name: "Auth", bundle: nil)
                    let vc         = storyboard.instantiateViewController(withIdentifier: "SessionExpireVC")
                        vc.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency
                    self.present(vc, animated: true, completion: nil)
                   // self.navigationController?.pushViewController(vc, animated: true)
                    return
                    
                } else {
                    self.alertmessage(Message: "Failed to delete question bank:\n \(err) \n\n Try Again Later.." )
                    return
                }
                
            }  else  {

                        //print("Message Sent Successfully")
                        let alert = UIAlertController(title: "", message: " Bank Deleted Successfully", preferredStyle: UIAlertController.Style.alert)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: {(alert: UIAlertAction!) in
                                self.fetchData()
                            }))
                        self.present(alert, animated: true)

             }
       }
     }//--- End Of EndLiveSession
    
        
    

}//-------- End Of Class ------------------------------------------------

extension QuestionBankVC: UITableViewDataSource, UITableViewDelegate{
    

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
 
            return BanksViewModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
            let cell = tableView.dequeueReusableCell(withIdentifier: "LiveCell", for: indexPath) as! LiveCell
                cell.QuestionBankViewModel = BanksViewModel[indexPath.row]
         return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let alert = UIAlertController(title: "", message: "Please Select an Option", preferredStyle: .actionSheet)

            alert.addAction(UIAlertAction(title: "Edit", style: .default , handler:{ (UIAlertAction)in
                self.EditQuestionBank( self.BanksViewModel[indexPath.row].Id! )
            }))
            alert.addAction(UIAlertAction(title: "Delete", style: .destructive , handler:{ (UIAlertAction)in
                self.DeleteQuestionBank( self.BanksViewModel[indexPath.row].Id! )
            }))

            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))

        self.present(alert, animated: true, completion: nil)
        
    }
                
}




extension QuestionBankVC: UIPickerViewDataSource, UIPickerViewDelegate {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return grades.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return grades[row]._Name
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
            self.selectedGradeID = grades[row]._Id
            self.quiz_grade.text = grades[row]._Name
    }
 
}

 
