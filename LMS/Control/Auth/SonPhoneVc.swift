//
//  SonPhoneVc.swift
//  LMS
//
//  Created by Abdo on 11/25/19.
//  Copyright © 2019 eaglez-group. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON


class SonPhoneVc: UIViewController ,UITableViewDataSource,UITableViewDelegate{
    @IBOutlet weak var table: UITableView!
    var fullName = ""
    var pass = ""
    var email = ""
    var phone = ""
    var gender = ""
    var img = ""
    var job = ""
    var numbers = [String]()
    var Spinner :UIView!
    
    @IBOutlet weak var viewBg: UIView!
    @IBOutlet weak var addView: UIView!
    @IBOutlet weak var phoneNumberSon: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Sons Phone Number"
        hideKeyboardWhenTappedAround()
        
        table.delegate = self
        table.dataSource = self
        
        let tapGesture2 = UITapGestureRecognizer(target: self, action: #selector(ViewTapped(tapGestureRecognizer:)))
        viewBg.isUserInteractionEnabled = true
        viewBg.addGestureRecognizer(tapGesture2)
    }
    
    
    func CreateParent(){
        print("in signUp")
        if self.img == "" {
                   self.img = (UIImage(named: "boss")?.toBase64())!
               }
        let parameters = [
            "Mobile":        phone,
            "EmailAddress":  email,
            "FireBaseToken": helper.getUserFCMToken() ?? "can not get token from iphone",
            "FullName":      fullName,
            "Password":      pass,
            "JobName":       job,
            "SonNumbers":    numbers,
            "Image":         img,
            ] as [String : Any]
        print(parameters)
        let header : HTTPHeaders = [
            "Content-Type":"application/json"
        ]
        
        Spinner = UIViewController.displaySpinner(onView: self.view)
        AF.request("\(URLs.BaseUrl)api/Parent/Create", method: .post, parameters: parameters as Parameters
            , encoding: JSONEncoding.default , headers : header)
            .responseJSON { response in
                
                UIViewController.removeSpinner(spinner: self.Spinner)
                switch response.result {
                    
                                         case .failure(let error):
                                             //vc.hideLoading()
                                             self.alertmessage(Message: "Server Not response___________>\(error)")
                                             return
                                             
                                         case .success(let value):
                                          
                                              let json = JSON(value)
                                              
                                              print("\n JSON: \(json)")
                                              
                                              if let jsonMsg = json["Message"].string {
                                                
                                                self.alertmessage(Message: jsonMsg)
                                                return
                                                
                                              } else {
                                                
                                                
                                                    
                                                
                                                
                                                    //self.alertmessage(Message: "Successful Registration")
                                                    // go to login
                                                    //navigation to next step
                                                    let storyBoard : UIStoryboard = UIStoryboard(name: "Auth", bundle:nil)
                                                    let nextViewController = storyBoard.instantiateViewController(withIdentifier: "registerComplete") as! registerComplete
                                                    //self.present(nextViewController, animated: true, completion: nil)
                                                        nextViewController.feedbackTxt = ""
                                                    self.navigationController?.pushViewController(nextViewController, animated: true)
                                                    
//
                    
                                                }
                    
                                 }
                
                
                
                
//                print(response as Any)
//
//                print(response.result.value as Any)   // result of response serialization
//                let dictResponse = response.result.value as? Dictionary<String ,AnyObject>
//                if dictResponse == nil {
//                    return
//                }
//                //
//                let dictData = dictResponse!["Message"] as? String
//                self.alertmessage(Message: dictData ?? "")
//                if dictData == "" {
//                    // go to login
//                    //navigation to next step
//                    let storyBoard : UIStoryboard = UIStoryboard(name: "Auth", bundle:nil)
//                    let nextViewController = storyBoard.instantiateViewController(withIdentifier: "registerComplete") as! registerComplete
//                    self.navigationController?.pushViewController(nextViewController, animated:
//                        true)
//                }
        }
    }

    
    @IBAction func addNumber(_ sender: Any) {
        if phoneNumberSon.text != ""{
            numbers.append(phoneNumberSon.text!)
            table.reloadData()
            viewBg.isHidden = true
            addView.isHidden = true
            phoneNumberSon.text = ""
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return numbers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = table.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! SonPhoneCell
        cell.num.text = "\((indexPath.row) + 1)"
        cell.phone.text = numbers[indexPath.row]
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        return true
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            // handle delete (by removing the data from your array and updating the tableview)
            self.numbers.remove(at: indexPath.row)
            self.table.deleteRows(at: [indexPath], with: .fade)
            self.table.reloadData()
        }
    }
    
    
    
    
    
    @IBAction func add(_ sender: Any) {
        // Your action
        viewBg.isHidden = false
        addView.isHidden = false
    }
    @IBAction func dond(_ sender: Any) {
        if numbers.isEmpty == false{
            CreateParent()
        }else{
            alertmessage(Message: "add son number")
        }
        
    }
    func alertmessage (Message : String) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "check", message: Message, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "ok", style: .default, handler: { action in
                switch action.style{
                case .default:
                    break
                //                                print("default")
                case .cancel: break
                //                                print("cancel")
                case .destructive: break
                    //                                print("destructive")
                }}))
            self.present(alert, animated: true, completion: nil)
            
        }
    }
    @objc func ViewTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        // Your action
        viewBg.isHidden = true
        addView.isHidden = true
    }
}
