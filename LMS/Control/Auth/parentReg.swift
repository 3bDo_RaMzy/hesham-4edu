//
//  parentReg.swift
//  LMS
//
//  Created by Abdo on 11/25/19.
//  Copyright © 2019 eaglez-group. All rights reserved.
//

import UIKit

class parentReg: UIViewController   ,UITextFieldDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    @IBOutlet weak var scroll: UIScrollView!
    @IBOutlet weak var logo: UIImageView!
    @IBOutlet weak var warningLb: UILabel!
    @IBOutlet weak var firstName: UITextField!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var phone: UITextField!
    @IBOutlet weak var pass: UITextField!
    @IBOutlet weak var confirmPass: UITextField!
    var UserType = ""
    @IBOutlet weak var viewBg: UIView!
    @IBOutlet weak var job: UITextField!
    @IBOutlet weak var imge: UIButton!
    @IBOutlet weak var img: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        hideKeyboardWhenTappedAround()
        navigationController?.setNavigationBarHidden(false, animated: true)
        self.title = "Parent New Account"
        
        viewBg.shadowView()
        firstName.delegate = self
        email.delegate = self
        phone.delegate = self
        pass.delegate = self
        confirmPass.delegate = self
        //scroll step 1
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(didTapView(gesture:)))
        view.addGestureRecognizer(tapGesture)
        
        let contentInset = UIEdgeInsets(top: 0, left: 0, bottom:150, right: 0)
        scroll.contentInset = contentInset
    }
    
    
    //step2 add method to handle tap event
    @objc func didTapView(gesture:UIGestureRecognizer){
        view.endEditing(true)
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.firstName {
            self.email.becomeFirstResponder()
        }else if textField == self.email {
            self.phone.becomeFirstResponder()
        }else if textField == self.phone {
            self.job.becomeFirstResponder()
        }else if textField == self.job {
            self.pass.becomeFirstResponder()
        }else if textField == self.pass {
            self.confirmPass.becomeFirstResponder()
        }else if textField == self.confirmPass {
            view.endEditing(true)
        }
        return true
    }
    ///start upload image
    var image:UIImage!
    var imgBase64 = ""
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        img.image = image
        imgBase64 = image.toBase64() ?? (UIImage(named: "boss")?.toBase64())!
        picker.dismiss(animated: true, completion: nil)
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
     
    @IBAction func changePhoto(_ sender: Any) {
        let imagePickerController = UIImagePickerController()
            imagePickerController.delegate = self
        
        let actionSheet = UIAlertController(title: "photo source", message: "choose a source", preferredStyle: .actionSheet)
        
            actionSheet.popoverPresentationController?.sourceView = imge
            actionSheet.popoverPresentationController?.sourceRect = imge.bounds
        
            actionSheet.addAction(UIAlertAction(title: "camera", style: .default, handler: {(action:UIAlertAction) in
                if UIImagePickerController.isSourceTypeAvailable(.camera){
                    imagePickerController.sourceType = .camera
                    self.present(imagePickerController, animated: true, completion: nil)
                }else{
                    print("camera not avaliable")
                }
                
            }))
        
            actionSheet.addAction(UIAlertAction(title: "Photo library", style: .default, handler: {(action:UIAlertAction) in
                imagePickerController.sourceType = .photoLibrary
                self.present(imagePickerController, animated: true, completion: nil)
            }))
        
            actionSheet.addAction(UIAlertAction(title: "cancel", style: .cancel, handler: nil))
        self.present(actionSheet, animated: true, completion: nil)
    }

    ///end upload image
    @IBAction func next(_ sender: Any) {
     
        if  firstName.text == "" {
            warningLb.isHidden = false
            warningLb.text = "enter your first name"
            return
        }
        if  email.text == "" {
            warningLb.isHidden = false
            warningLb.text = "enter your email"
            return
        }
        if  !(String(email.text!).isValidEmail()) {
            warningLb.isHidden = false
            warningLb.text = "your email is invalid format"
            return
        }
        if  phone.text == "" {
            warningLb.isHidden = false
            warningLb.text = "enter your phone"
            return
        } 
        if  pass.text == "" {
            warningLb.isHidden = false
            warningLb.text = "enter your password"
            return
        }
        if  confirmPass.text == "" {
            warningLb.isHidden = false
            warningLb.text = "enter your confirm password "
            return
        }
        if (self.pass.text?.count)! < 6 {
            warningLb.isHidden = false
            warningLb.text = "the password must be 6 char or more "
            return
        }
        if pass.text != confirmPass.text {
            warningLb.isHidden = false
            warningLb.text = "password and confirm password not matched"
            return
        }
        
        warningLb.isHidden = true
        warningLb.text = ""
        //navigation to next step
        let storyBoard : UIStoryboard = UIStoryboard(name: "Auth", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "SonPhoneVc") as! SonPhoneVc
        nextViewController.fullName = firstName.text ?? ""
        nextViewController.pass = pass.text ?? ""
        nextViewController.phone = phone.text ?? ""
        nextViewController.email = email.text ?? ""
        nextViewController.gender = gender
        nextViewController.job = job.text ?? ""
        nextViewController.img = imgBase64
        self.show(nextViewController, sender: nil)
        //self.navigationController?.pushViewController(nextViewController, animated:            true)
    }
    var gender = "male"
    
    @IBOutlet weak var femaleBtn: UIButton!
    @IBOutlet weak var maleBtn: UIButton!
    @IBAction func female(_ sender: UIButton) {
        logo.image = UIImage(named: "kristy.png")
        gender = "female"
        femaleBtn.backgroundColor = UIColor(red: 140 / 255, green: 199 / 255, blue: 181 / 255, alpha: 1.0)
        maleBtn.backgroundColor = UIColor(red: 224 / 255, green: 225 / 255, blue: 226 / 255, alpha: 1.0)
        femaleBtn.setTitleColor(UIColor.white, for: .normal)
        maleBtn.setTitleColor(UIColor.darkGray, for: .normal)
    }
    
    @IBAction func male(_ sender: UIButton) {
        logo.image = UIImage(named: "boss.png")
        gender = "male"
        maleBtn.backgroundColor = UIColor(red: 140 / 255, green: 199 / 255, blue: 181 / 255, alpha: 1.0)
        femaleBtn.backgroundColor = UIColor(red: 224 / 255, green: 225 / 255, blue: 226 / 255, alpha: 1.0)
        maleBtn.setTitleColor(UIColor.white, for: .normal)
        femaleBtn.setTitleColor(UIColor.darkGray, for: .normal)
    }
}

