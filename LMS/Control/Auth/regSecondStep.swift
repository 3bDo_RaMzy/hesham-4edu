//
//  regSecondStep.swift
//  LMS
//
//  Created by Abdo on 11/25/19.
//  Copyright © 2019 eaglez-group. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import MapKit


class regSecondStep: BaseVC ,UITextFieldDelegate,UIPickerViewDataSource,UIPickerViewDelegate, MKMapViewDelegate, CLLocationManagerDelegate{

    @IBOutlet weak var mapK:          MKMapView!
    @IBOutlet weak var viewBg:        UIView!
    @IBOutlet weak var scroll:        UIScrollView!
    @IBOutlet weak var warningLb:     UILabel!
    @IBOutlet weak var grade:         UITextField!
    @IBOutlet weak var group:         UITextField!
    @IBOutlet weak var country:       UITextField!
    @IBOutlet weak var city:          UITextField!
    @IBOutlet weak var picker:        UIPickerView!
    @IBOutlet weak var lblGroupNotes: UILabel!
    @IBOutlet weak var OptionSelectionView: UIStackView!
    @IBOutlet weak var OptionOrView: UIView!
    
    
    var Spinner  : UIView!
    var fullName = ""
    var pass     = ""
    var email    = ""
    var phone    = ""
    var gender   = ""
    var img      = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        //getGrades()
        //getGroupes()
        picker.dataSource = self
        picker.delegate = self
        self.title = "Student Group Info"
        
        //scroll step 1
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(didTapView(gesture:)))
        view.addGestureRecognizer(tapGesture)
        
        let contentInset = UIEdgeInsets(top: 0, left: 0, bottom:50, right: 0)
        scroll.contentInset = contentInset
        let tapGesture2 = UITapGestureRecognizer(target: self, action: #selector(ViewTapped(tapGestureRecognizer:)))
        viewBg.isUserInteractionEnabled = true
        viewBg.addGestureRecognizer(tapGesture2)
        //mapkit
        
        if helper.getAppOption() == true {  //---> isOnlineAndAcademic
             self.OptionSelectionView.isHidden = false
             self.OptionOrView.isHidden        = false
        }else {                             //---> isOnline Only
             self.OptionSelectionView.isHidden = true
             self.OptionOrView.isHidden        = true
            type = "online"
            onlineBtn.backgroundColor = UIColor(red: 140 / 255, green: 199 / 255, blue: 181 / 255, alpha: 1.0)
            academyBtn.backgroundColor = UIColor(red: 224 / 255, green: 225 / 255, blue: 226 / 255, alpha: 1.0)
            onlineBtn.setTitleColor(UIColor.white, for: .normal)
            academyBtn.setTitleColor(UIColor.darkGray, for: .normal)
            //
            country.isHidden = true
            city.isHidden = true
            arrow1.isHidden = true
            arrow2.isHidden = true
            cityBtn.isHidden = true
            countryBtn.isHidden = true
            heightConst.constant = 20
            //
            pinLocation.isEnabled = false
            
            picker.isHidden = true
            grades.removeAll()
            self.picker.reloadAllComponents()
            grade.text = ""
            gradeId    = 0
            group.text = ""
            groupId    = 0
            getGrades()
        }
         
    }
    
    
    
    //step2 add method to handle tap event
    @objc func didTapView(gesture:UIGestureRecognizer){
        view.endEditing(true)
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    @IBAction func LocationGroup(_ sender: Any) {
        viewBg.isHidden = false
        mapK.isHidden = false
    }
    
    var pikType : Int = 1 //country
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pikType == 1 {
            return countries.count
        }else  if pikType == 2 {
            return  cities.count
        }else  if pikType == 3 {
            return  grades.count
        }else  if pikType == 4 {
            if type == "online"{
                return  groupes.count
            }else{
               return  groupsAcdmic.count
            }
            
        } else {
            return 0
        }
    }
    var countryLb = ""
    var cityLb = ""
    var gradeLb = ""
    var groupLb = ""
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pikType == 1 {
            countryLb = countries[row]._Name
            return countries[row]._Name
        }else  if  pikType == 2{
            cityLb = cities[row]._Name
            return cities[row]._Name
        }else  if  pikType == 3{
            gradeLb = grades[row]._Name
            return grades[row]._Name
        }else  if  pikType == 4{
            if type == "online"{
                groupLb = groupes[row]._GroupName
                return groupes[row]._GroupName
            }else{
                groupLb = groupsAcdmic[row]._GroupName
                return groupsAcdmic[row]._GroupName
            }
        }else{
            return  ""
        }
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pikType ==  1{
            
                country.text  =  countries[row]._Name
                countryId = countries[row]._Id
                cities.removeAll()
                city.text = ""
                getCity(id: countries[row]._Id)
                picker.isHidden = true
            
        }else if  pikType == 2{
            
                city.text  =  cities[row]._Name
                cityId = cities[row]._Id
                picker.isHidden = true
           
        }else if  pikType == 3{
            
            if type == "online"{
                grade.text  =  grades[row]._Name
                gradeId = grades[row]._Id
                groupes.removeAll()
                group.text = ""
                getGroupes()
                self.lblGroupNotes.text = "You can review your group notes here.."
                picker.isHidden = true
            }else{
                grade.text  =  grades[row]._Name
                gradeId = grades[row]._Id
                groupsAcdmic.removeAll()
                group.text = ""
                getGroupesAcadimic()
                self.lblGroupNotes.text = "You can review your group notes here.."
                picker.isHidden = true
            }
            
        }else if  pikType == 4{
            
            if type == "online"{
                group.text  =  groupes[row]._GroupName
                groupId = groupes[row]._GroupId
                self.lblGroupNotes.text = groupes[row]._Note
                picker.isHidden = true
            }else{
                group.text  =  groupsAcdmic[row]._GroupName
                groupId = groupsAcdmic[row]._GroupId
                self.lblGroupNotes.text = groupsAcdmic[row]._Note
                picker.isHidden = true
            }
            
        }
    }
    var countries = [Country]()
    var cities = [Country]()
    
    func getCountry() {
        
         Spinner = UIViewController.displaySpinner(onView: self.view)
         //Downloading article  data for TableView
        
        let headerParams : HTTPHeaders = ["Lang": "en"]
        
         AF.request("\(URLs.BaseUrl)api/Countries/GetALL", method: .get, parameters: nil, encoding: URLEncoding.default, headers: headerParams).responseJSON{ response in
                    
                    print("Response: \(response)")
                    UIViewController.removeSpinner(spinner: self.Spinner)
            
                    switch response.result
                    {
                        
                    case .failure(let error):
                        print("News Get Error Alamofire....... \(error)")
                        
                    case .success(let value):
                        
                        if let dict = value as? [Dictionary<String, AnyObject>] {
                            self.countries.removeAll()
                                           for obj in dict {
                                               let result = Country(country: obj)
                                               self.countries.append(result)
                                               //print(result._Name)
                                           }
                                   }
                        self.picker.reloadAllComponents()
                        if self.countries.count > 0 {
                            self.country.text  =  self.countries[0]._Name
                            self.countryId = self.countries[0]._Id
                            self.cities.removeAll()
                            self.city.text = ""
                            self.getCity(id: self.countries[0]._Id)
                            //self.picker.isHidden = true
                        }
                        
                        
                    }//end of switch case
        }
    }
    
    func getCity(id:Int){
        
        Spinner = UIViewController.displaySpinner(onView: self.view)
        //Downloading article  data for TableView
        
        let headerParams : HTTPHeaders = ["Lang": "en"]
        AF.request("\(URLs.BaseUrl)api/Country/\(id)/Cities/GetAll", method: .get, parameters: nil, encoding: URLEncoding.default, headers: headerParams).responseJSON{ response in
                           
            //print("Response: \(response.value)")
                           UIViewController.removeSpinner(spinner: self.Spinner)
                           switch response.result
                           {
                               
                           case .failure(let error):
                               print("News Get Error Alamofire....... \(error)")
                               
                           case .success(let value):
                               
                               if let dict = value as? [Dictionary<String, AnyObject>] {
                                       self.cities.removeAll()
                                       for obj in dict {
                                           let result = Country(country: obj)
                                           self.cities.append(result)
                                           print(result._Name)
                                       }
                               }
                               
                                self.picker.reloadAllComponents()
                                if self.cities.count > 0 {
                                    self.city.text  =  self.cities[0]._Name
                                    self.cityId = self.cities[0]._Id
                                    self.getGrades()
                                    //picker.isHidden = true
                                }
                            
                           }//end of switch case
               }
    }
    var grades = [Grade]()
    func getGrades(){
        Spinner = UIViewController.displaySpinner(onView: self.view)
        //Downloading article  data for TableView //api/Grades/GetAll/filter/  groups or all  /ar
        let headerParams : HTTPHeaders = ["Lang": "en"]
        AF.request("\(URLs.BaseUrl)api/Grades/GetAll/filter/groups", method: .get, parameters: nil, encoding: URLEncoding.default, headers: headerParams).responseJSON{ response in
            
            UIViewController.removeSpinner(spinner: self.Spinner)
            let result = response.result
            print(response)
            switch response.result {
            case .success(let value):
                //print(String(data: value as! Data, encoding: .utf8)!)
                if let dict = value as? [Dictionary<String, AnyObject>] {
                    self.grades.removeAll()
                    for obj in dict {
                        let result = Grade(country: obj)
                        self.grades.append(result)
                        print(result._Name)
                    }
                    self.picker.reloadAllComponents()
                    if self.grades.count > 0 {
                        
                        if self.type == "online"{
                            self.grade.text  =  self.grades[0]._Name
                            self.gradeId = self.grades[0]._Id
                            self.groupes.removeAll()
                            self.group.text = ""
                            self.getGroupes()
                            self.lblGroupNotes.text = "You can review your group notes here.."
                            //picker.isHidden = true
                        }else{
                            self.grade.text  =  self.grades[0]._Name
                            self.gradeId = self.grades[0]._Id
                            self.groupsAcdmic.removeAll()
                            self.group.text = ""
                            self.getGroupesAcadimic()
                            self.lblGroupNotes.text = "You can review your group notes here.."
                            //picker.isHidden = true
                        }
                        
                    }
                }
                
            case .failure(let error):
                print(error)
                
            }
            
            

        }
    }
    var groupes = [Group]()
    var groupsAcdmic = [GroupAcadimic]()
    func getGroupes(){
        //online
        //Downloading article  data for TableView
        //"api/Grade/{gradeId}/StudnetType/{StudnetType}/City/{cityId}/Groups/GetAll/{lang}"
        //Alamofire.request("\(BaseUrl)api/Grade/\(gradeId)/StudnetType/OnlineStudents/City/\(cityId)/Groups/GetAll/en").responseJSON { response in
        Spinner = UIViewController.displaySpinner(onView: self.view)
        let headerParams : HTTPHeaders = ["Lang": "en"]
        AF.request("\(URLs.BaseUrl)api/Grade/\(gradeId)/StudnetType/AcademicStudents/Groups/GetAll", method: .get, parameters: nil, encoding: URLEncoding.default, headers: headerParams).responseJSON{ response in
            
            UIViewController.removeSpinner(spinner: self.Spinner)
            let result = response.result
            print(result)
            switch response.result {
            case .success(let value):
                //print(String(data: value as! Data, encoding: .utf8)!)
                if let dict = value as? [Dictionary<String, AnyObject>] {
                    self.groupes.removeAll()
                    for obj in dict {
                        let result = Group(country: obj)
                        self.groupes.append(result)
                        print(result._GroupName)
                    }
                    self.picker.reloadAllComponents()
                    if self.groupes.count > 0 {
                        self.group.text  =  self.groupes[0]._GroupName
                        self.groupId     =  self.groupes[0]._GroupId
                        self.lblGroupNotes.text = self.groupes[0]._Note
                        self.picker.isHidden         = true
                    }
                        
                }
                
            case .failure(let error):
                print(error)
                
            }
            

        }
    }
    @IBOutlet weak var pinLocation: UIButton!
    func getGroupesAcadimic(){
        //Downloading article  data for TableView
        // "api/Grade/{gradeId}/StudnetType/{StudnetType}/Groups/GetAll/{lang}"
        //Alamofire.request("\(BaseUrl)api/Grade/\(gradeId)/StudnetType/AcademicStudents/Groups/GetAll/en").responseJSON { response in
        Spinner = UIViewController.displaySpinner(onView: self.view)
        let headerParams : HTTPHeaders = ["Lang": "en"]
        AF.request("\(URLs.BaseUrl)api/Grade/\(gradeId)/StudnetType/AcademicStudents/City/\(cityId)/Groups/GetAll", method: .get, parameters: nil, encoding: URLEncoding.default, headers: headerParams).responseJSON{ response in
            
            UIViewController.removeSpinner(spinner: self.Spinner)
            let result = response.result
            print(result)
            switch response.result {
            case .success(let value):
                //print(String(data: value as! Data, encoding: .utf8)!)
                if let dict = value as? [Dictionary<String, AnyObject>] {
                                self.groupsAcdmic.removeAll()
                                for obj in dict {
                                    let result = GroupAcadimic(country: obj)
                                    self.groupsAcdmic.append(result)
                                    print(result._GroupName)
                                    print(result._Longitude)
                                    let annotation = MKPointAnnotation()
                                    annotation.title = result._GroupName
                                    //You can also add a subtitle that displays under the annotation such as
                //                    annotation.coordinate = CLLocationCoordinate2D(latitude: Double(result._Latitude) ?? 0.0, longitude: Double(result._Longitude) ?? 0.0)
                //                    self.mapK.addAnnotation(annotation)
                                }
                                self.picker.reloadAllComponents()
                                if self.groupsAcdmic.count > 0 {
                                    self.group.text  =  self.groupsAcdmic[0]._GroupName
                                    self.groupId     =  self.groupsAcdmic[0]._GroupId
                                    self.lblGroupNotes.text = self.groupsAcdmic[0]._Note
                                    self.picker.isHidden         = true
                                }
                            }
                
            case .failure(let error):
                print(error)
                
            }
            
            
            
        }
    }
    var cityId = 0
    var countryId = 0
    var gradeId = 0
    var groupId = 0
    func CreateStudnet(){
        print("in signUp")
        if self.img == "" {
            self.img = (UIImage(named: "boy")?.toBase64())!
        }
        let parameters = [
                            "Mobile":phone,
                            "EmailAddress":email,
                            "GroupId":"\(groupId)",
                            "Gender":gender,
                            "FireBaseToken": helper.getUserFCMToken() ?? "can not get token from iphone",
                            "CityId":"\(cityId)",
                            "FullName":fullName,
                            "Password":pass,
                            "Image":  img, //---- "iVBORw0KGgoAAAANSUhEUgAAAXEAAAH/",
                            ] as [String : Any]
        print(parameters)
        let header : HTTPHeaders = [
            "Content-Type":"application/json"
        ]
         Spinner = UIViewController.displaySpinner(onView: self.view)
        AF.request("\(URLs.BaseUrl)api/Studnets/Create", method: .post, parameters: parameters as Parameters
            , encoding: JSONEncoding.default , headers : header)
            .responseJSON { response in
                
                
                UIViewController.removeSpinner(spinner: self.Spinner)
                switch response.result {
                    
                                         case .failure(let error):
                                             //vc.hideLoading()
                                             self.alertmessage(Message: "Server Not response___________>\(error)")
                                             return
                                             
                                         case .success(let value):
                                          
                                              let json = JSON(value)
                                              
                                              print("\n JSON: \(json)")
                                              
                                              if let jsonMsg = json["Message"].string {
                                                
                                                self.alertmessage(Message: jsonMsg)
                                                return
                                                
                                              } else {
                                                
                                                //self.alertmessage(Message: "Successful Registration")
                                                // go to login
                                                //navigation to next step
                                                let storyBoard : UIStoryboard = UIStoryboard(name: "Auth", bundle:nil)
                                                let nextViewController = storyBoard.instantiateViewController(withIdentifier: "registerComplete") as! registerComplete
                                                //self.present(nextViewController, animated: true, completion: nil)
                                                nextViewController.feedbackTxt = "Your Request Is Pending Now , It Will  Be Activated In Less Than 24-hr"
                                                self.navigationController?.pushViewController(nextViewController, animated: true)
                                              }
                    
                                 }
                
                /*
                UIViewController.removeSpinner(spinner: self.Spinner)
                print(response.result.value as Any)   // result of response serialization
                
                let dictResponse = response.result.value as? Dictionary<String ,AnyObject>
                if dictResponse == nil {
                    return
                }
//
                let dictData = dictResponse!["Message"] as? String
                self.alertmessage(Message: dictData ?? "")
                */
        }
    }
    @IBAction func create(_ sender: Any) {
        
        
        
        if type == "online" {
            
            if  grade.text == "" {
                warningLb.isHidden = false
                warningLb.text = "enter your grade"
                return
            }
            
            if  group.text == "" {
                warningLb.isHidden = false
                warningLb.text = "choose your group"
                return
            }
            
            warningLb.isHidden = true
            warningLb.text     = ""
            CreateStudnet()
            
        } else {
            
            if  country.text == "" {
                warningLb.isHidden = false
                warningLb.text = "choose your country"
                return
            }
            if  city.text == "" {
                warningLb.isHidden = false
                warningLb.text = "choose your city"
                return
            }
            
            if  grade.text == "" {
                warningLb.isHidden = false
                warningLb.text = "enter your grade"
                return
            }
            
            if  group.text == "" {
                warningLb.isHidden = false
                warningLb.text = "choose your group"
                return
            }
            
            warningLb.isHidden = true
            warningLb.text     = ""
            CreateStudnet()
            
        }
        
        
        
        
        
        
        
        
    }
    @IBAction func country(_ sender: Any) {
        // Your action
        if countries.count > 0 {
                print("country tapped")
                pikType = 1
                picker.dataSource = self
                picker.delegate = self
                
                if picker.isHidden == true {
                    picker.isHidden = false
                }else {
                    picker.isHidden = true
                }
        } else {
                getCountry()
                print("country tapped")
                pikType = 1
                picker.dataSource = self
                picker.delegate = self
                
                if picker.isHidden == true {
                    picker.isHidden = false
                }else {
                    picker.isHidden = true
                }
        }
    }
    @IBAction func city(_ sender: Any) {
        
        // Your action
        print("city tapped")
        
        if country.text == ""{
            warningLb.text = "choose your country first"
        }else{
            if cities.count == 0 {
                picker.isHidden = true
                warningLb.text = "no cities in this country"
            }else{
                 
                    pikType = 2
                    picker.dataSource = self
                    picker.delegate = self
                    
                    if picker.isHidden == true {
                        picker.isHidden = false
                    }else {
                        picker.isHidden = true
                    }
                
                
                
            }
            
        }
        
    }
    @IBAction func Grad(_ sender: Any) {
        
        print("grads tapped")
        if grades.count == 0 {
            
            picker.isHidden = true
            warningLb.text = "no group in this grade"
            
        } else {
            pikType = 3
            picker.dataSource = self
            picker.delegate = self
            
            if picker.isHidden == true {
                picker.isHidden = false
            }else {
                picker.isHidden = true
            }
        }
        
        
    }
    
    @IBAction func Groupp(_ sender: Any) {
        // Your action
        print("groups tapped")
        if grade.text == ""{
            warningLb.text = "choose your grade first"
        }
        if type == "online"{
//            if city.text == ""{
//                warningLb.text = "choose your city first"
//            }else{
                if groupes.count == 0 {
                    picker.isHidden = true
                    warningLb.text = "no group in this grade"
                }else{
                    pikType = 4
                    picker.dataSource = self
                    picker.delegate = self
                    
                    if picker.isHidden == true {
                        picker.isHidden = false
                    }else {
                        picker.isHidden = true
                    }
                }
            //}
            //
            
        }else{
            if groupsAcdmic.count == 0 {
                picker.isHidden = true
                warningLb.text = "no group in this grade"
            }else{
                pikType = 4
                picker.dataSource = self
                picker.delegate = self
                
                if picker.isHidden == true {
                    picker.isHidden = false
                }else {
                    picker.isHidden = true
                }
            }
        }
    }
    var type = "AcademicStudents"
    @IBOutlet weak var onlineBtn: UIButton!
    @IBOutlet weak var arrow1: UIImageView!
    @IBOutlet weak var arrow2: UIImageView!
    @IBOutlet weak var cityBtn: UIButton!
    @IBOutlet weak var countryBtn: UIButton!
    @IBOutlet weak var academyBtn: UIButton!
    @IBAction func online(_ sender: Any) {
        type = "online"
        warningLb.isHidden = true
        warningLb.text     = ""
        onlineBtn.backgroundColor = UIColor(red: 140 / 255, green: 199 / 255, blue: 181 / 255, alpha: 1.0)
        academyBtn.backgroundColor = UIColor(red: 224 / 255, green: 225 / 255, blue: 226 / 255, alpha: 1.0)
        onlineBtn.setTitleColor(UIColor.white, for: .normal)
        academyBtn.setTitleColor(UIColor.darkGray, for: .normal)
        //
        country.isHidden = true
        city.isHidden = true
        arrow1.isHidden = true
        arrow2.isHidden = true
        cityBtn.isHidden = true
        countryBtn.isHidden = true
        heightConst.constant = 20
        //
        pinLocation.isEnabled = false
        
        picker.isHidden = true
        grades.removeAll()
        self.picker.reloadAllComponents()
        grade.text = ""
        gradeId    = 0
        group.text = ""
        groupId    = 0
        getGrades()
        
    }
    @IBOutlet weak var heightConst: NSLayoutConstraint!
    @IBAction func academy(_ sender: Any) {
        type = "AcademicStudents"
        warningLb.isHidden = true
        warningLb.text     = ""
        academyBtn.backgroundColor = UIColor(red: 140 / 255, green: 199 / 255, blue: 181 / 255, alpha: 1.0)
        onlineBtn.backgroundColor  = UIColor(red: 224 / 255, green: 225 / 255, blue: 226 / 255, alpha: 1.0)
        academyBtn.setTitleColor(UIColor.white, for: .normal)
        onlineBtn.setTitleColor(UIColor.darkGray, for: .normal)
        country.isHidden = false
        city.isHidden = false
        arrow1.isHidden = false
        arrow2.isHidden = false
        cityBtn.isHidden = false
        countryBtn.isHidden = false
        heightConst.constant = 140
        pinLocation.isEnabled = true
        
        picker.isHidden = true
        grades.removeAll()
        self.picker.reloadAllComponents()
        country.text = ""
        countryId  = 0
        city.text  = ""
        cityId     = 0
        grade.text = ""
        gradeId    = 0
        group.text = ""
        groupId    = 0
    }
//    func alertmessage (Message : String) {
//        DispatchQueue.main.async {
//            let alert = UIAlertController(title: "check", message: Message, preferredStyle: UIAlertController.Style.alert)
//            alert.addAction(UIAlertAction(title: "ok", style: .default, handler: { action in
////                //navigation to next step
//                let storyBoard : UIStoryboard = UIStoryboard(name: "Auth", bundle:nil)
//                let nextViewController = storyBoard.instantiateViewController(withIdentifier: "registerComplete") as! registerComplete
//                self.show( nextViewController, sender: nil)
//                self.navigationController?.pushViewController(nextViewController, animated:
//                    true)
//                switch action.style{
//                case .default:
//                    break
//                //                                print("default")
//                case .cancel: break
//                //                                print("cancel")
//                case .destructive: break
//                    //                                print("destructive")
//                }}))
//            self.present(alert, animated: true, completion: nil)
//
//        }
//    }
    @objc func ViewTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        // Your action
        viewBg.isHidden = true
        mapK.isHidden = true
    }
}

