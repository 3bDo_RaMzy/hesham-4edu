//
//  loginVC.swift
//  LMS
//
//  Created by Abdo on 11/24/19.
//  Copyright © 2019 eaglez-group. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import JWTDecode
import SwiftMessages
import SwiftMessageBar
import JWTDecode
import AuthenticationServices

@available(iOS 13.0, *)
class loginVC: UIViewController ,UITextFieldDelegate, ASAuthorizationControllerDelegate, ASAuthorizationControllerPresentationContextProviding{
    
    
    @IBOutlet weak var logo: UIImageView!
    @IBOutlet weak var scroll: UIScrollView!
    @IBOutlet weak var pass: UITextField!
    @IBOutlet weak var phone: UITextField!
    @IBOutlet weak var loginBtn: UIButton!
    
    @IBOutlet weak var appleLoginBtn: ASAuthorizationAppleIDButton!
    
    
    @IBOutlet weak var fullName: DesignableTextField!
    @IBOutlet weak var phoneNumber: DesignableTextField!
    
    var Spinner :UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //getSocail()
        //setUpSignInAppleButton()
        hideKeyboardWhenTappedAround()
        logo.roundedImage()
        pass.delegate = self
        phone.delegate = self
        fullName.delegate = self
        phoneNumber.delegate = self
        let tapGesture2 = UITapGestureRecognizer(target: self, action: #selector(ViewTapped(tapGestureRecognizer:)))
        viewBg.isUserInteractionEnabled = true
        viewBg.addGestureRecognizer(tapGesture2)
        //scroll step 1
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(didTapView(gesture:)))
        view.addGestureRecognizer(tapGesture)
        
        let contentInset = UIEdgeInsets(top: 0, left: 0, bottom:50, right: 0)
        scroll.contentInset = contentInset
    }//-------- End Of View Did Load -------------
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
//    func setUpSignInAppleButton() {
//
//      let authorizationButton = ASAuthorizationAppleIDButton()
//        authorizationButton.addTarget(self, action: #selector(handleAppleIdRequest), for: .touchUpInside)
//         // authorizationButton.cornerRadius = 10.0
//        authorizationButton.frame.size = self.appleLoginBtn.frame.size
//
//      //Add button on some view or stack
//      self.appleLoginBtn.addSubview(authorizationButton)
//    }
    
    @IBAction func signInAppleAction(_ sender: Any) {

        let appleIDProvider = ASAuthorizationAppleIDProvider()
        let request =  appleIDProvider.createRequest()
            request.requestedScopes = [.fullName, .email]
        let authorizationController = ASAuthorizationController(authorizationRequests: [request])
            authorizationController.delegate = self
            authorizationController.presentationContextProvider = self
            authorizationController.performRequests()

    }
    
//    @objc func handleAppleIdRequest() {
//        let appleIDProvider = ASAuthorizationAppleIDProvider()
//        let request =  appleIDProvider.createRequest()
//            request.requestedScopes = [.fullName, .email]
//        let authorizationController = ASAuthorizationController(authorizationRequests: [request])
//            authorizationController.delegate = self
//            authorizationController.presentationContextProvider = self
//            authorizationController.performRequests()
//    }
    
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        
        if let appleIDCredential = authorization.credential as?  ASAuthorizationAppleIDCredential {
        let userIdentifier = appleIDCredential.user
            let fullName = (appleIDCredential.fullName?.givenName ?? "visitor") + " " + ( appleIDCredential.fullName?.familyName ?? "with apple account")
        let email = appleIDCredential.email
            
            if userIdentifier != "" || fullName != "" || email != "" {
                Spinner = UIViewController.displaySpinner(onView: self.view)
                LogVisitWithApple(fullName: fullName, Email: email ?? "visitor@gmail.com")
            }else{
                alertmessage(Message: "Your account data is not correct")
            }
        print("User id is \(userIdentifier) \n Full Name is \(String(describing: fullName)) \n Email id is \(String(describing: email))") }
    }
    
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
    // Handle error.
        print("\n Sign In With Apple Error: \(error)")
        alertmessage(Message: "Your Apple ID is not available  ")
    }
    
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return self.view.window!
    }
    
    //----------------------------------------------------------------------------------------------
    //step2 add method to handle tap event
    @objc func didTapView(gesture:UIGestureRecognizer){
        view.endEditing(true)
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.phone {
            self.pass.becomeFirstResponder()
        }else if textField == self.pass {
            view.endEditing(true)
        }
        if textField == self.fullName {
            self.phoneNumber.becomeFirstResponder()
        }else if textField == self.phoneNumber {
            view.endEditing(true)
        }
        return true
    }
    
    
    
    @IBAction func login(_ sender: Any) {
        if phone.text != "" && pass.text != "" {
            Spinner = UIViewController.displaySpinner(onView: self.view)
            LogIn()
        }else{
            alertmessage(Message: "complete empty data")
        }
    }
    
    
    @IBOutlet weak var accountBtn: UIButton!
    @IBOutlet weak var visitorBtn: UIButton!
    @IBAction func account(_ sender: Any) {
        accountBtn.backgroundColor = UIColor(hexString: "00b4ab") //green
        visitorBtn.backgroundColor = UIColor(red: 224 / 255, green: 225 / 255, blue: 226 / 255, alpha: 1.0)
        accountBtn.setTitleColor(UIColor.white, for: .normal)
        visitorBtn.setTitleColor(UIColor.darkGray, for: .normal)
    }
    @IBAction func visitor(_ sender: Any) {
        viewBg.isHidden = false
        visitView.isHidden = false
        visitorBtn.backgroundColor = UIColor(hexString: "00b4ab") //green
        accountBtn.backgroundColor = UIColor(red: 224 / 255, green: 225 / 255, blue: 226 / 255, alpha: 1.0)
        visitorBtn.setTitleColor(UIColor.white, for: .normal)
        accountBtn.setTitleColor(UIColor.darkGray, for: .normal)
    }
    
    
    @IBOutlet weak var viewBg: UIView!
    @IBOutlet weak var visitView: UIView!
    @objc func ViewTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        // Your action
        viewBg.isHidden = true
        visitView.isHidden = true
        signView.isHidden = true
        accountBtn.backgroundColor = UIColor(hexString: "00b4ab") //green
        visitorBtn.backgroundColor = UIColor(red: 224 / 255, green: 225 / 255, blue: 226 / 255, alpha: 1.0)
        accountBtn.setTitleColor(UIColor.white, for: .normal)
        visitorBtn.setTitleColor(UIColor.darkGray, for: .normal)
        
    }
    ////
    @IBAction func next(_ sender: Any) {
        if fullName.text != "" || phoneNumber.text != "" {
            Spinner = UIViewController.displaySpinner(onView: self.view)
            LogVisit()
        }else{
            alertmessage(Message: "the fullname and mobile are required!")
        }
    }
    var social :SocialNetwork!
    func getSocail(){
        //self.showLoading()
        Spinner = UIViewController.displaySpinner(onView: self.view)
        //Downloading article  data for TableView
        AF.request("\(URLs.BaseUrl)api/SocialReferences/GetAll").responseJSON { response in
            //self.hideLoading()
            UIViewController.removeSpinner(spinner: self.Spinner)
            let result = response.result
            print(result)
            switch response.result {
            case .success(let value):
                //print(String(data: value as! Data, encoding: .utf8)!)
               if let dict =  value as? Dictionary<String, AnyObject> {
                   self.social = SocialNetwork(social: dict)
               }
            case .failure(let error):
                print(error)
                
            }
            
            
        }
    }
    func LogVisit() {
        
        let parameters = [
            "Mobile": phoneNumber.text!, //email
            "FullName": fullName.text!, //pass34
        ]
        let header : HTTPHeaders = [
            "Content-Type":"application/json"
        ]
        AF.request("\(URLs.BaseUrl)api/VisitorUsers/LogStatus", method: .post, parameters: parameters as Parameters
            , encoding: JSONEncoding.default , headers : header)
            .responseJSON { response in

                switch response.result {
                case .success(let value):
                    //print(String(data: value as! Data, encoding: .utf8)!)
                  UIViewController.removeSpinner(spinner: self.Spinner)
                                   //print(response.result.value as Any)   // result of response serialization
                                   let x =  value as! Int
                                   if x == 201
                                   {
                                       //login to MainView
                                       print("success")
                   //                    if let viewController = self.storyboard?.instantiateViewController(withIdentifier: "MainView") {
                   //                        UIApplication.shared.keyWindow?.rootViewController = viewController
                   //                        self.dismiss(animated: true, completion: nil)
                   //                    }

                           
                                       let storyBoard : UIStoryboard = UIStoryboard(name: "Visitor", bundle:nil)
                                       let nextViewController = storyBoard.instantiateViewController(withIdentifier: "visitRootController") as! visitRootViewController
                                           nextViewController.modalPresentationStyle = .fullScreen
                                       //self.show(nextViewController, sender: nil)
                                       self.present(nextViewController, animated: true, completion: nil)
                           
                           
                    }
                case .failure(let error):
                    print(error)
                    
                }
                
                

        }
    }
    
    func LogVisitWithApple(fullName: String, Email: String){
        
        let parameters = [
                    "Mobile": Email, //email
                    "FullName": fullName
                ]
                let header : HTTPHeaders = [
                    "Content-Type":"application/json"
                ]
                AF.request("\(URLs.BaseUrl)api/VisitorUsers/LogStatus", method: .post, parameters: parameters as Parameters
                    , encoding: JSONEncoding.default , headers : header)
                    .responseJSON { response in

                        switch response.result {
                        case .success(let value):
                            //print(String(data: value as! Data, encoding: .utf8)!)
                           UIViewController.removeSpinner(spinner: self.Spinner)
                                           //print( value as Any)   // result of response serialization
                                           let x =  value as! Int
                                           if x == 201
                                           {
                                               //login to MainView
                                               print("success")
                           //                    if let viewController = self.storyboard?.instantiateViewController(withIdentifier: "MainView") {
                           //                        UIApplication.shared.keyWindow?.rootViewController = viewController
                           //                        self.dismiss(animated: true, completion: nil)
                           //                    }

                                   
                                               let storyBoard : UIStoryboard = UIStoryboard(name: "Visitor", bundle:nil)
                                               let nextViewController = storyBoard.instantiateViewController(withIdentifier: "visitContentViewController"/*"visitRootController"*/) as! AnimatedTabBarController //visitRootViewController
                                                   nextViewController.modalPresentationStyle = .fullScreen
                                               //self.show(nextViewController, sender: nil)
                                               self.present(nextViewController, animated: true, completion: nil)
                                   
                                   
                                           }
                            
                        case .failure(let error):
                            print(error)
                            
                        }
                        
                        

                }
        
    }
    
    
    func LogIn() {
        let parameters = [
            "LoginName": phone.text!, //email
            "Password":  pass.text!, //pass
            "FireBaseToken": helper.getUserFCMToken() ?? "can not get token from iphone",
        ]
        let header : HTTPHeaders = [
            "Content-Type":"application/json"
        ]
        AF.request("\(URLs.BaseUrl)api/Account/Auth", method: .post, parameters: parameters as Parameters
        //AF.request("\(URLs.BaseUrl)api/Account/Auth", method: .post, parameters: parameters as Parameters
            , encoding: JSONEncoding.default , headers : header)
            .responseJSON { response in
                
                print("\n\n response: \(response)")
                
                UIViewController.removeSpinner(spinner: self.Spinner)
                               switch response.result {
                                   
                                                        case .failure(let error):
                                                            //vc.hideLoading()
                                                            print("\n\n Login faild: \(error)")
                                                            self.alertmessage(Message: " Login request faild from server ")
                                                            return
                                                            
                                                        case .success(let value):
                                                         
                                                             let json = JSON(value)
                                                             
                                                             print("\n JSON: \(json)")
                                                             
                                                             if let jsonMsg = json["Message"].string {
                                                               
                                                               self.alertmessage(Message: jsonMsg)
                                                               return
                                                                
                                                             } else {
                                                               
                                                                    // -------- go to Home
                                                                    //save User Token
                                                                    print("\n\n User Token: \(json["Token"])")
                                                                    helper.saveUserToken(Token:          json["Token"].string  ?? "")
                                                                    helper.saveStudentGradeId(STGradID:  json["GradeId"].int   ?? 0)
                                                                    helper.saveStudentGroupId(STGroupID: json["GroupId"].int   ?? 0)
                                                                    helper.saveStudentId(St_Id:          json["StudentId"].int ?? 0)
                                                                    helper.saveStudentTermSystem(ST_TermSys: json["UsedTermSystem"].bool ?? true)
                                                                
                                                                    if  json["Token"].string != "" {
                                                                        
                                                                        do{
                                                                            let jwt = try decode(jwt: json["Token"].string!)
                                                                            //print("\n\n JWT: \(jwt)")
                                                                            let jwtBodyJson = JSON(jwt.body)
                                                                            
                                                                            if let jwtBodyUser = jwtBodyJson["UserType"].string {
                                                                              print("\n\n jwtBodyUser: \(jwtBodyUser)")
                                                                                
                                                                                if jwtBodyUser == "Studnet" {
                                                                                    
                                                                                    let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                                                                                    let nextViewController = storyBoard.instantiateViewController(withIdentifier: "contentViewController"/*"rootController"*/) as! AnimatedTabBarController //RootViewController
                                                                                        nextViewController.modalPresentationStyle = .fullScreen
                                                                                    self.present(nextViewController, animated: true, completion: nil)
                                                                                    //self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                                    
                                                                                } else if jwtBodyUser == "Parent" {
                                                                                    
                                                                                    let storyBoard : UIStoryboard = UIStoryboard(name: "Parent", bundle:nil)
                                                                                    let nextViewController = storyBoard.instantiateViewController(withIdentifier: "parentContentViewController"/*"parentRootController"*/) as! AnimatedTabBarController //parentRootViewController
                                                                                        nextViewController.modalPresentationStyle = .fullScreen
                                                                                    self.present(nextViewController, animated: true, completion: nil)
                                                                                    //self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                                    
                                                                                } else if jwtBodyUser == "Teacher" {
                                                                                    
                                                                                    let storyBoard : UIStoryboard = UIStoryboard(name: "Teacher", bundle:nil)
                                                                                    let nextViewController = storyBoard.instantiateViewController(withIdentifier: "teacherContentViewController"/*"teacherRootController"*/) as! AnimatedTabBarController //teacherRootViewController
                                                                                        nextViewController.modalPresentationStyle = .fullScreen
                                                                                    self.present(nextViewController, animated: true, completion: nil)
                                                                                    //self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                                    
                                                                                } else { //Teacher
                                                                                    
                                                                                    self.alertmessage(Message: "Sorry!.. You do not have permission to login here")

                                                                                }
                                                                                
                                                                            }
                                                                            
                                                                        }catch let error as NSError {
                                                                            self.alertmessage(Message: "JWT Error: \(error)")
                                                                            return
                                                                        }
                                                                        
                                                                    }
                                                                   
                                                                 }
                                   
                                                    }
                
                
                
                
//                print(response.result.value as Any)   // result of response serialization
//                let dictResponse = response.result.value as? Dictionary<String ,AnyObject>
//                if dictResponse == nil {
//                    UIViewController.removeSpinner(spinner: self.Spinner)
//                    return
//                }
//                var token = ""
//                let dictData = dictResponse!["Message"] as? String
//                token = dictResponse!["Token"] as? String ?? ""
//                print(token)
//                if dictData != "" && dictData != nil{
//                    print("message : \(dictData)")
//                     self.alertmessage(Message: dictData ?? "")
//                }
//                UIViewController.removeSpinner(spinner: self.Spinner)
//                //
//                if token != ""{
//                    do {
//                        UserDefaults.standard.set(token, forKey: "token")
//                        let jwt = try decode(jwt: token)
//                        print(jwt.body["FullName"] ?? "")
//                        // login to MainView
//                        // go to main vc
//                        if let viewController = self.storyboard?.instantiateViewController(withIdentifier: "MainView") {
//                            UIApplication.shared.keyWindow?.rootViewController = viewController
//                            self.dismiss(animated: true, completion: nil)
//                        }
//                    }catch{
//                        print("error")
//                    }
//                }
        }
    }
    
    @IBAction func student(_ sender: Any) {
        //navigation to next step
        let storyBoard : UIStoryboard = UIStoryboard(name: "Auth", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "regFirstStep") as! regFirstStep
        //self.show(nextViewController, sender: nil)
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    @IBAction func parent(_ sender: Any) {
        //navigation to next step
        let storyBoard : UIStoryboard = UIStoryboard(name: "Auth", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "parentReg") as! parentReg
        //self.show(nextViewController, sender: nil)
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    @IBAction func showPass(_ sender: UIButton) {
        if pass.isSecureTextEntry == true{
            sender.setImage(UIImage(named: "view"), for: .normal)
            pass.isSecureTextEntry = false
        }else{
          sender.setImage(UIImage(named: "hide"), for: .normal)
            pass.isSecureTextEntry = true
        }
    }
    @IBOutlet weak var signView: UIView!
    @IBAction func signUp(_ sender: Any) {
        viewBg.isHidden = false
        signView.isHidden = false
    }
    @IBAction func forgotPas(_ sender: Any) {
        //navigation to next step
        let storyBoard : UIStoryboard = UIStoryboard(name: "Auth", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "ForgotPassword") as! ForgotPassword
            
//        if helper.getSMSOption() == true {
//             nextViewController.optionUsed = 2 // 1: email   |  2: SMS code
//        }else {
             nextViewController.optionUsed = 1 // 1: email   |  2: SMS code
//        }
 
        //self.show(nextViewController, sender: nil)
        self.navigationController?.pushViewController(nextViewController, animated:    true)
    }
    func alertmessage (Message : String) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Attention!", message: Message, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                switch action.style{
                case .default:
                    break
                //                                print("default")
                case .cancel: break
                //                                print("cancel")
                case .destructive: break
                    //                                print("destructive")
                }}))
            self.present(alert, animated: true, completion: nil)
            
        }
    }
    @IBAction func facebook(_ sender: Any) {
        if social == nil {
            //alertmessage(Message: "link not avaliable")
            self.getSocail()
        }else{
            if let url = NSURL(string: social._FaceBookUrl){ UIApplication.shared.open(url as URL, options: [:], completionHandler: nil) }
        }
    }
    @IBAction func googlePlus(_ sender: Any) {
        if social == nil {
            //alertmessage(Message: "link not avaliable")
            self.getSocail()
        }else{
            if let url = NSURL(string: social._GoogleUrl){ UIApplication.shared.open(url as URL, options: [:], completionHandler: nil) }
        }
    }
    @IBAction func twitter(_ sender: Any) {
        if social == nil {
            //alertmessage(Message: "link not avaliable")
            self.getSocail()
        }else{
            if let url = NSURL(string: social._TwitterUrl){ UIApplication.shared.open(url as URL, options: [:], completionHandler: nil) }
        }
    }
}
