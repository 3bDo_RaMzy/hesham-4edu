//
//  registerComplete.swift
//  LMS
//
//  Created by Abdo on 11/25/19.
//  Copyright © 2019 eaglez-group. All rights reserved.
//

import UIKit

class registerComplete: UIViewController {

    @IBOutlet weak var feedbackLbl: UITextView!
    
    var feedbackTxt = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.setNavigationBarHidden(true, animated: true)
        self.feedbackLbl.text = feedbackTxt
        
    }
    @available(iOS 13.0, *)
    @IBAction func login(_ sender: Any) {
        
        
        dismiss(animated: true) {
            
            let storyBoard : UIStoryboard = UIStoryboard(name: "Auth", bundle:nil)
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "loginVC") as! loginVC
                nextViewController.modalPresentationStyle = .fullScreen
            nextViewController.modalTransitionStyle       = .flipHorizontal
            self.present(nextViewController, animated: true, completion: nil)
            
        }
        
        
        //self.navigationController?.pushViewController(nextViewController, animated: true)
        //self.navigationController?.popToViewController(nextViewController, animated: true)
        //self.navigationController?.popToRootViewController(animated: true)
    }
}
