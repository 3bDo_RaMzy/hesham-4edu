//
//  PasswordFinishVC.swift
//  LMS
//
//  Created by MacBook on 6/10/20.
//  Copyright © 2020 eaglez-group. All rights reserved.
//

import UIKit

class PasswordFinishVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @available(iOS 13.0, *)
    @IBAction func PasswordFinishAction(_ sender: UIButton) {
        
        //self.navigationController?.popToRootViewController(animated: true)
        
        dismiss(animated: true) {
            
            helper.resetUserToken()
            
            let storyBoard : UIStoryboard = UIStoryboard(name: "Auth", bundle:nil)
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "loginVC") as! loginVC
                nextViewController.modalPresentationStyle = .fullScreen
                nextViewController.modalTransitionStyle       = .flipHorizontal
            self.present(nextViewController, animated: true, completion: nil)
            
        }
        
    }
    

}
