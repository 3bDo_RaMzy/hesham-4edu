//
//  NewPasswordVC.swift
//  LMS
//
//  Created by MacBook on 6/10/20.
//  Copyright © 2020 eaglez-group. All rights reserved.
//

import UIKit

class NewPasswordVC: BaseVC {

    var MobileOrEmail = ""
    
    @IBOutlet weak var textPassword: UITextField!
    @IBOutlet weak var textConfPassword: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        hideKeyboardWhenTappedAround()
        self.textPassword.becomeFirstResponder()
    }
    

    @IBAction func newPasswordAction(_ sender: Any) {
        
        if textPassword.text == "" {
            
            self.alertmessage(Message: "Password text is empty")
            return
            
        } else if textConfPassword.text == "" {
            
            self.alertmessage(Message: "Confirm Password text is empty")
            return
            
        } else if textPassword.text != textConfPassword.text {
            
            self.alertmessage(Message: "Confirm Password doesn't match")
            return
            
        } else {
            
            
            API.ResetPassword(vc: self, MobileOrEmail: self.MobileOrEmail,
                              NewPassword: self.textConfPassword.text!)
            { (error, status, message) in
                                
                if error != nil {
                    
                    self.alertmessage(Message: "Sorry! Can not send request")
                
                }else if status == false {
                    
                    self.alertmessage(Message: "Error! .. \(message)")
                    
                }else if status == true{
                    
                        let storyboard  = UIStoryboard(name: "Auth", bundle: nil)
                        let vc          = storyboard.instantiateViewController(withIdentifier: "PasswordFinishVC")
                                          as! PasswordFinishVC
                            vc.modalPresentationStyle = .overFullScreen
                            vc.modalTransitionStyle   = .crossDissolve
                        
                        //self.present(vc, animated: true, completion: nil)
                        //self.show(vc, sender: nil)
                        self.navigationController?.pushViewController(vc, animated: true)
                    
                }
                
                
            }
            
             
            
        }
        
        
    }
    

}
