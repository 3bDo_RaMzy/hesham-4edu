//
//  ForgotPassword.swift
//  LMS
//
//  Created by Abdo on 11/25/19.
//  Copyright © 2019 eaglez-group. All rights reserved.
//

import UIKit

class ForgotPassword: BaseVC {

    var optionUsed = 1       // 1: email   |  2: SMS code
    
    @IBOutlet weak var viewEmail: UIView!
    @IBOutlet weak var viewPhone: UIView!
    
    @IBOutlet weak var textEmail: UITextField!
    @IBOutlet weak var textPhone: UITextField!
    
    @IBOutlet weak var btnSendEmail: UIButton!
    @IBOutlet weak var btnSendPhone: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        hideKeyboardWhenTappedAround()
        navigationController?.setNavigationBarHidden(false, animated: true)
        
//         if optionUsed == 1 {
//
//            self.viewEmail.isHidden = false
//            self.viewPhone.isHidden = true
//            self.textEmail.becomeFirstResponder()
//            self.btnSendEmail.tag   = 1
//
//        } else {
            
            self.viewEmail.isHidden = true
            self.viewPhone.isHidden = false
            self.textPhone.becomeFirstResponder()
            self.btnSendPhone.tag   = 2
//        }
        
    }//------ End Of viewDidLoad --------
    
    
    @IBAction func sendRequestAction(_ sender: UIButton) {
        
//        if sender.tag == 1 {
//
//            print("\n\n send code by email")
//            if textEmail.text == "" {
//
//                self.alertmessage(Message: "Email text is empty")
//                return
//            }else {
//
//                API.forgetPassword(vc: self, MobileOrEmail: textPhone.text!)
//                { (error, status, code) in
//
//                    if error != nil {
//
//                        self.alertmessage(Message: "Sorry! Can not send request")
//
//                    }else if status == false {
//
//                        self.alertmessage(Message: "Error! .. \(code)")
//
//                    }else if status == true{
//
//
//                        if code != ""{
//
//                            let storyboard  = UIStoryboard(name: "Auth", bundle: nil)
//                            let vc          = storyboard.instantiateViewController(withIdentifier: "ActivationCodeVC")
//                                              as! ActivationCodeVC
//                                vc.modalPresentationStyle = .overFullScreen
//                                vc.modalTransitionStyle   = .crossDissolve
//                                vc.SMSCode = code
//                                vc.MobileOrEmail = self.textEmail.text!
//                            //self.present(vc, animated: true, completion: nil)
//                            //self.show(vc, sender: nil)
//                            self.navigationController?.pushViewController(vc, animated: true)
//
//                        } else {
//
//                            self.alertmessage(Message: "Sorry!.. Can not recieve Email Code")
//                        }
//
//
//
//                    }
//
//
//
//                }
//
//            }
//
//
//
//        } else {
            
            print("\n\n send code by SMS")
            if textPhone.text == "" {
                
                self.alertmessage(Message: "Phone number or Email text is empty")
                return
            }else {
                
                API.forgetPassword(vc: self, MobileOrEmail: textPhone.text!)
                { (error, status, code, provider) in
                
                    if error != nil {
                        
                        self.alertmessage(Message: "Sorry! Can not send request")
                    
                    }else if status == false {
                        
                        self.alertmessage(Message: "Error! .. \(code)")
                        
                    }else if status == true{
                        
                        
                        if code != ""{
                            
                            let storyboard  = UIStoryboard(name: "Auth", bundle: nil)
                            let vc          = storyboard.instantiateViewController(withIdentifier: "ActivationCodeVC")
                                              as! ActivationCodeVC
                                vc.modalPresentationStyle = .overFullScreen
                                vc.modalTransitionStyle   = .crossDissolve
                                vc.SMSCode = code
                                vc.MobileOrEmail = self.textPhone.text!
                                vc.Provider = provider
                            //self.present(vc, animated: true, completion: nil)
                            //self.show(vc, sender: nil)
                            self.navigationController?.pushViewController(vc, animated: true)
                            
                        } else {
                            
                            self.alertmessage(Message: "Sorry!.. Can not recieve SMS Code")
                        }
                         
                    }
                    
                }
                
            }
            
            
            
            
//        }
        
    }
      
      
}//----- End Of class --------
