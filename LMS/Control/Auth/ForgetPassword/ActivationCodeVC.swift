//
//  ActivationCodeVC.swift
//  LMS
//
//  Created by MacBook on 6/10/20.
//  Copyright © 2020 eaglez-group. All rights reserved.
//

import UIKit

class ActivationCodeVC: BaseVC {

    var SMSCode       = ""
    var MobileOrEmail = ""
    var Provider      = ""
    
    
    @IBOutlet weak var textInstructionCode: UITextView!
    @IBOutlet weak var textActiveCode: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        hideKeyboardWhenTappedAround()
        
        print("\n\n SMSCode: \(SMSCode)  | Provider: \(Provider) ")
        
        if Provider == "Sms"{
            
            textInstructionCode.text = "Enter activation code sent to your SMS Box to reset your password ."
        } else {
            
            textInstructionCode.text = "Enter activation code sent to your Email inbox to reset your password ."
        }
        
    }
    
    
    
    
    @IBAction func ActivationCodeAction(_ sender: UIButton) {
        
        if textActiveCode.text == "" {
            
            self.alertmessage(Message: "Activation code text is empty")
            return
            
        }else if textActiveCode.text != SMSCode {
            
            self.alertmessage(Message: "Activation code is not correct")
            return
            
        } else if textActiveCode.text == SMSCode {
            
            let storyboard  = UIStoryboard(name: "Auth", bundle: nil)
            let vc          = storyboard.instantiateViewController(withIdentifier: "NewPasswordVC")
                              as! NewPasswordVC
                vc.modalPresentationStyle = .overFullScreen
                vc.modalTransitionStyle   = .crossDissolve
                vc.MobileOrEmail = self.MobileOrEmail
            
              //self.present(vc, animated: true, completion: nil)
              //self.show(vc, sender: nil)
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
        
        
    }
    
    
    
    
    
    

}
