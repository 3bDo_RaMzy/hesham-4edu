//
//  AnimatedTabBarController.swift
//  LMS
//
//  Created by Abdo on 12/2/19.
//  Copyright © 2019 eaglez-group. All rights reserved.
//

import UIKit

class AnimatedTabBarController: UITabBarController, UITabBarControllerDelegate {
    
    var titles                          = [String]()
    var newlyTapped                     = true
    var window                          : UIWindow?
    public static var goDirection       : String     = ""
    public static var goCustomDirection : String     = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //print("goDirection: \(AnimatedTabBarController.goDirection)")
        
        guard let items     = tabBar.items else { return }
        
//        for i in 0..<items.count {
//
//            titles.append(items[i].title!.localiz())
//            items[i].title = items[i].title!.localiz()
//
//        }
        
        selectedIndex = 0
    }//---> End Of ViewDidLoad --------------------------------
    
    
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        
        tabBar.selectionIndicatorImage = UIImage().createSelectionIndicator(color: UIColor(hexString: "00B4AB"), size: CGSize(width: tabBar.frame.width/CGFloat(tabBar.items!.count), height:  tabBar.frame.height), lineWidth: 2.0)
        
    }
    
    
}


extension UIImage {
    func createSelectionIndicator(color: UIColor, size: CGSize, lineWidth: CGFloat) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        color.setFill()
        UIRectFill(CGRect(x: 0, y: /*size.height - */lineWidth, width: size.width, height: lineWidth))
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
}


