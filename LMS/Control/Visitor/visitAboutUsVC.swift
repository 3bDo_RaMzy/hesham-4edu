//
//  visitAboutUsVC.swift
//  LMS
//
//  Created by Abdo on 11/28/19.
//  Copyright © 2019 eaglez-group. All rights reserved.
//

import UIKit

class visitAboutUsVC: BaseVC {

    
    @IBOutlet weak var aboutUSTable: UITableView!
    
    var aboutUSViewModel = [AboutUSViewModel]()
    var Spinner :UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.aboutUSTable.dataSource     = self
        self.aboutUSTable.layoutMargins  = UIEdgeInsets.zero
        self.aboutUSTable.separatorInset = UIEdgeInsets.zero
        self.aboutUSTable.rowHeight      = 100
        self.getaboutUSData()
     
        
    }//-------> End Of ViewDidLoad ----------

    
    
    
    private func getaboutUSData(){
        
        //self.showLoading()
        Spinner = UIViewController.displaySpinner(onView: self.view)
        
        API.getAboutUs(vc: self ) { ( err, aboutUS ) in
                                                   
                   UIViewController.removeSpinner(spinner: self.Spinner)
                   if let err = err {
                           print("Failed to fetch courses:", err)
                           return
                    }
                                                   
                    self.aboutUSViewModel = aboutUS?.map({return AboutUSViewModel(about: $0)}) ?? []
                    self.aboutUSTable.reloadData()
       }
        
        
    }

}//----- End of class --------------------------------------------------

extension visitAboutUsVC: UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.aboutUSViewModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = UITableViewCell()
        
        if (aboutUSViewModel[indexPath.row].Key != "RightIcon" && aboutUSViewModel[indexPath.row].Key != "LeftIcon") {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "visitAboutUSCell", for: indexPath) as! visitAboutUSCell
            
            cell.aboutKey.text = aboutUSViewModel[indexPath.row].Key
            cell.aboutValue.text = aboutUSViewModel[indexPath.row].Value
            return cell
            
        } else if ( aboutUSViewModel[indexPath.row].Key == "RightIcon" ){
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "visitAboutUSCellRight", for: indexPath) as! visitAboutUSCell
                  
            cell.aboutValue.text = aboutUSViewModel[indexPath.row].Value
            return cell
            
        } else if ( aboutUSViewModel[indexPath.row].Key == "LeftIcon" ){
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "visitAboutUSCellLeft", for: indexPath) as! visitAboutUSCell
                  
            cell.aboutValue.text = aboutUSViewModel[indexPath.row].Value
            return cell
            
        }
    
        return cell
    }
    
}
