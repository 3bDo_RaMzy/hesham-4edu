//
//  visitVideoPlayVC.swift
//  LMS
//
//  Created by Abdo on 12/1/19.
//  Copyright © 2019 eaglez-group. All rights reserved.
//

import UIKit

class visitVideoPlayVC: BaseVC, UITableViewDataSource, UITableViewDelegate {

            //--> variables
            var playlistID = ""
            var Spinner :UIView!
    
            @IBOutlet weak var VideoTable: UITableView!
            var videoViewModel = [VideoViewModel]()
            
            override func viewDidLoad() {
                super.viewDidLoad()
                // Do any additional setup after loading the view.
                self.title = "Play List"
                                
                self.VideoTable.dataSource     = self
                //self.VideoTable.delegate       = self
                self.VideoTable.layoutMargins  = UIEdgeInsets.zero
                self.VideoTable.separatorInset = UIEdgeInsets.zero
                self.VideoTable.rowHeight      = 100
                self.fetchData()
                
            }//------------ End Of ViewDidLoad ---------------------
                
            
             func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

                    return videoViewModel.count
                }
            
             func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
                
                    let cell = tableView.dequeueReusableCell(withIdentifier: "visitVideoPlayCell", for: indexPath) as! visitVideoCell
                cell.videoViewModel = videoViewModel[indexPath.row]
                cell.ChanalBtn.tag  = indexPath.row
                cell.ChanalBtn.addTarget(self, action: #selector(openVideo(sender:)), for: .touchUpInside)

                 return cell
                }
            
            @objc func openVideo(sender: UIButton){
                
                let storyboard            = UIStoryboard(name: "Visitor", bundle: nil)
                let vc                    = storyboard.instantiateViewController(withIdentifier: "visitVideoPalyerVC") as! visitVideoPalyerVC
                    vc.modalPresentationStyle = .overFullScreen
                    vc.modalTransitionStyle   = .crossDissolve
                    vc.VideoID  = self.videoViewModel[sender.tag].videoId
                self.present(vc, animated: true, completion: nil)
                //self.show(vc, sender: nil)
            }
            
         
                //------------------------------
                fileprivate func fetchData() {
                    
                    Spinner = UIViewController.displaySpinner(onView: self.view)
                    
                    API.getPlaylistVideos(vc: self, playlistId: self.playlistID ) { ( err, videos ) in
                        
                        UIViewController.removeSpinner(spinner: self.Spinner)
                        if let err = err {
                                print("Failed to fetch courses:", err)
                                return
                        }
                        
                        self.videoViewModel = videos?.map({return VideoViewModel(video: $0)}) ?? []
                        self.VideoTable.reloadData()
                    }
                    
                }//---> End Of fetchData method -------

    
    
}//---> End Of Class -----------------

