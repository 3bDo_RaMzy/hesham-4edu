//
//  visitMaterialTypeVC.swift
//  LMS
//
//  Created by Abdo on 11/27/19.
//  Copyright © 2019 eaglez-group. All rights reserved.
//

import UIKit

class visitMaterialTypeVC: UIViewController {

    
    @IBOutlet var backViewRecognizer: UITapGestureRecognizer!
    @IBOutlet var PDFRecognizer: UITapGestureRecognizer!
    @IBOutlet var VideoRecognizer: UITapGestureRecognizer!
    @IBOutlet var PrivatePDFRecognizer: UITapGestureRecognizer!
    
    @IBOutlet weak var ChoosePDF: UILabel!
    @IBOutlet weak var ChooseVideo: UILabel!
    @IBOutlet weak var ChoosePrivateVidoe: UILabel!
    
    @IBOutlet weak var visitMaterialTypeBackView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        visitMaterialTypeBackView.backgroundColor = UIColor(hexString: "AAAAAA", alpha: 0.8)

    }
    

    @IBAction func PDFRecognizerAction(_ sender: UITapGestureRecognizer) {
        
        
      NotificationCenter.default.post(name: NSNotification.Name(rawValue: "openPDF"), object: nil)
      self.dismiss(animated: true, completion: nil)
        
    }//--------------------------------------------------
    @IBAction func VideoRecognizer(_ sender: UITapGestureRecognizer) {
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "openVideo"), object: nil)
        self.dismiss(animated: true, completion: nil)
           
    }//-------------------------------------------------- 
    @IBAction func PrivatePDFRecognizer(_ sender: UITapGestureRecognizer) {
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "visitPrivateVideoVC"), object: nil)
        self.dismiss(animated: true, completion: nil)
              
    }//--------------------------------------------------
    
    @IBAction func backViewRecognizerAction(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
 

}//------------->  End Of Class
