//
//  visitVideoVC.swift
//  LMS
//
//  Created by Abdo on 11/29/19.
//  Copyright © 2019 eaglez-group. All rights reserved.
//

import UIKit

class visitVideoVC: BaseVC, UITableViewDataSource, UITableViewDelegate {

            //--> variables 
            var gradeID = 0
            var termSys = 0
            var selectedTerm = ""
            var Spinner :UIView!
    
            @IBOutlet weak var ChanalHeaderHieght: NSLayoutConstraint!
            @IBOutlet weak var ChanalHeaderView: UIView!
            @IBOutlet weak var ChanalTable: UITableView!
            var chanalViewModel = [ChanalViewModel]()
            //var fileViewModel = [FileViewModel]()
            
            override func viewDidLoad() {
                super.viewDidLoad()
                // Do any additional setup after loading the view.
                self.title = "Youtube Channel"
                
                if self.termSys == 0 {
                    self.ChanalHeaderView.isHidden = true
                    self.ChanalHeaderHieght.constant = 0
                } else {
                    self.selectedTerm = "First"
                }
                
                self.ChanalTable.dataSource     = self
                self.ChanalTable.delegate       = self
                self.ChanalTable.layoutMargins  = UIEdgeInsets.zero
                self.ChanalTable.separatorInset = UIEdgeInsets.zero
                self.ChanalTable.rowHeight      = 100
                self.fetchData()
               
            }//------------ End Of ViewDidLoad ---------------------
                
            
             func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

                    return chanalViewModel.count
                }
            
             func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
                
                    let cell = tableView.dequeueReusableCell(withIdentifier: "visitVideoCell", for: indexPath) as! visitVideoCell
                cell.chanalViewModel = chanalViewModel[indexPath.row]
                 return cell
                }
            
            
            func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
                      
                
                
                let storyboard            = UIStoryboard(name: "Visitor", bundle: nil)
                let vc                    = storyboard.instantiateViewController(withIdentifier: "visitVideoPlayVC") as! visitVideoPlayVC
                vc.modalPresentationStyle = .overFullScreen
                vc.modalTransitionStyle   = .crossDissolve
                vc.playlistID = chanalViewModel[indexPath.row].YouTubeListId
                
                //self.present(vc, animated: true, completion: nil)
                self.show(vc, sender: nil)
                
        }
                //------------------------------
                fileprivate func fetchData() {
                    
                    Spinner = UIViewController.displaySpinner(onView: self.view)
                    if self.termSys == 0 {
                        
                                        API.getGradeChannels(vc: self, Grade_id: self.gradeID, TermSystem: "Unique" ) { ( err, unites ) in
                                            
                                            UIViewController.removeSpinner(spinner: self.Spinner)
                                            if let err = err {
                                                    print("Failed to fetch courses:", err)
                                                    return
                                            }
                                            
                                            self.chanalViewModel = unites?.map({return ChanalViewModel(chanal: $0)}) ?? []
                                            self.ChanalTable.reloadData()
                                        }
                    } else {
                        
                                        API.getGradeChannels(vc: self, Grade_id: self.gradeID, TermSystem: self.selectedTerm ) { ( err, unites ) in
                                                   
                                            UIViewController.removeSpinner(spinner: self.Spinner)
                                            if let err = err {
                                                    print("Failed to fetch courses:", err)
                                                    return
                                            }
                                                   
                                            self.chanalViewModel = unites?.map({return ChanalViewModel(chanal: $0)}) ?? []
                                            self.ChanalTable.reloadData()
                                        }
                    }
                    
                    
                }//---> End Of fetchData method -------
         
    
    @IBOutlet weak var firstBtn: UIButton!
    @IBOutlet weak var secondBtn: UIButton!
    @IBAction func firstAction(_ sender: UIButton) {
        self.selectedTerm = "First"
        firstBtn.backgroundColor = UIColor(red: 140 / 255, green: 199 / 255, blue: 181 / 255, alpha: 1.0)
        secondBtn.backgroundColor = UIColor(red: 224 / 255, green: 225 / 255, blue: 226 / 255, alpha: 1.0)
        firstBtn.setTitleColor(UIColor.white, for: .normal)
        secondBtn.setTitleColor(UIColor.darkGray, for: .normal)
        self.fetchData()
    }
    
    @IBAction func secondAction(_ sender: UIButton) {
        self.selectedTerm = "Second"
        secondBtn.backgroundColor = UIColor(red: 140 / 255, green: 199 / 255, blue: 181 / 255, alpha: 1.0)
        firstBtn.backgroundColor = UIColor(red: 224 / 255, green: 225 / 255, blue: 226 / 255, alpha: 1.0)
        secondBtn.setTitleColor(UIColor.white, for: .normal)
        firstBtn.setTitleColor(UIColor.darkGray, for: .normal)
        self.fetchData()
    }

    
    
    
}//---> End Of Class -----------------

