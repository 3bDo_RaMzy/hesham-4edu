//
//  visitTimeTableVC.swift
//  LMS
//
//  Created by Abdo on 11/27/19.
//  Copyright © 2019 eaglez-group. All rights reserved.
//

import UIKit
import Alamofire

class visitStudyVC: BaseVC {

    
    @IBOutlet weak var visitGradeTable: UITableView!
    var Spinner :UIView!
    var selectedGrade   = 0
    var selectedTermSys = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.visitGradeTable.dataSource     = self
        self.visitGradeTable.delegate       = self
        self.visitGradeTable.layoutMargins  = UIEdgeInsets.zero
        self.visitGradeTable.separatorInset = UIEdgeInsets.zero
        self.visitGradeTable.rowHeight      = 160
        getallGrades()
        
        NotificationCenter.default.addObserver(self, selector: #selector(openPDFAction(_:)),
                                               name: Notification.Name("openPDF"),
                                               object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(openVideoAction(_:)),
                                               name: Notification.Name("openVideo"),
                                               object: nil)//visitPrivateVideoVC
        NotificationCenter.default.addObserver(self, selector: #selector(openPrivateVideoAction(_:)),
                                                name: Notification.Name("visitPrivateVideoVC"),
                                                object: nil)
    }
    

    @objc func openPDFAction(_ notification:Notification) {

               // Do something now
               let storyboard            = UIStoryboard(name: "Visitor", bundle: nil)
               let vc                    = storyboard.instantiateViewController(withIdentifier: "visitPDFVC") as! visitPDFVC
               vc.modalPresentationStyle = .overFullScreen
               vc.modalTransitionStyle   = .crossDissolve
               vc.gradeID = self.selectedGrade
               vc.termSys = self.selectedTermSys
               //self.present(vc, animated: true, completion: nil)
               self.show(vc, sender: nil)
       }
    
    @objc func openVideoAction(_ notification:Notification) {

            // Do something now
            let storyboard            = UIStoryboard(name: "Visitor", bundle: nil)
            let vc                    = storyboard.instantiateViewController(withIdentifier: "visitVideoVC") as! visitVideoVC
            vc.modalPresentationStyle = .overFullScreen
            vc.modalTransitionStyle   = .crossDissolve
            vc.gradeID = self.selectedGrade
            vc.termSys = self.selectedTermSys
            //self.present(vc, animated: true, completion: nil)
            self.show(vc, sender: nil)
    }
    
    @objc func openPrivateVideoAction(_ notification:Notification) {

            // Do something now
            let storyboard            = UIStoryboard(name: "Visitor", bundle: nil)
            let vc                    = storyboard.instantiateViewController(withIdentifier: "visitPrivateVideoVC") as! visitPrivateVideoVC
            vc.modalPresentationStyle = .overFullScreen
            vc.modalTransitionStyle   = .crossDissolve
        
            //self.present(vc, animated: true, completion: nil)
            self.show(vc, sender: nil)
    }
    
    
    var allGrades = [Grade]()
    private func getallGrades(){
        
        //self.showLoading()
        Spinner = UIViewController.displaySpinner(onView: self.view)
        //Downloading article  data for TableView //api/Grades/GetAll/filter/  groups or all  /ar
        
        let headerParams : HTTPHeaders = ["Lang": "en"]
        AF.request("\(URLs.BaseUrl)api/Grades/GetAll/filter/all", method: .get, parameters: nil, encoding: URLEncoding.default, headers: headerParams).responseJSON{ response in
        
            let result = response.result
            //self.hideLoading()
            UIViewController.removeSpinner(spinner: self.Spinner)
            print(response)
            switch response.result {
            case .success(let value):
                //print(String(data: value as! Data, encoding: .utf8)!)
                if let dict =  value as? [Dictionary<String, AnyObject>] {
                    for obj in dict {
                        let result = Grade(country: obj)
                        
                        if helper.getStudentGradeId() != nil  {
                            
                            if result._Id == helper.getStudentGradeId() {
                                 
                                 self.allGrades.append(result)
                                 //print(result._Name)
                            }
                            
                        } else {
                            
                            self.allGrades.append(result)
                            //print(result._Name)
                        }
                        
                        
                    }
                    self.visitGradeTable.reloadData()
                }
                
            case .failure(let error):
                print(error)
                
            }
            
            
        }
    }

}//----- End of class --------------------------------------------------

extension visitStudyVC: UITableViewDataSource, UITableViewDelegate {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.allGrades.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "visitStudyCell", for: indexPath) as! visitStudyCell
              
        cell.gradeName.text = allGrades[indexPath.row]._Name
        return cell
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        // Do something now
        self.selectedGrade = allGrades[indexPath.row]._Id
        if allGrades[indexPath.row]._UsedTermSystem == 1 {
            self.selectedTermSys = 1
        } else {
            self.selectedTermSys = 0
        }
        let storyboard            = UIStoryboard(name: "Visitor", bundle: nil)
        let vc                    = storyboard.instantiateViewController(withIdentifier: "visitMaterialTypeVC") as! visitMaterialTypeVC
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle   = .crossDissolve
        
        self.present(vc, animated: true, completion: nil)
        //self.show(vc, sender: nil)
    }
       
    
}
