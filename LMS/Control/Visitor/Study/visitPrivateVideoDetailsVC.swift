//
//  visitPrivateVideoDetailsVC.swift
//  LMS
//
//  Created by MacBook on 11/21/20.
//  Copyright © 2020 eaglez-group. All rights reserved.
//

import UIKit //materialsViewModel

class visitPrivateVideoDetailsVC: BaseVC, UITableViewDataSource, UITableViewDelegate {

    
    @IBOutlet weak var P_VideoDetailsTable: UITableView!
    
    var FolderName = ""
    var P_VideoMaterials = [PrivateVideosMaterials]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.title = "\(FolderName)"
        
        self.P_VideoDetailsTable.dataSource     = self
        self.P_VideoDetailsTable.delegate       = self
        self.P_VideoDetailsTable.layoutMargins  = UIEdgeInsets.zero
        self.P_VideoDetailsTable.separatorInset = UIEdgeInsets.zero
        self.P_VideoDetailsTable.rowHeight      = 100
        self.P_VideoDetailsTable.reloadData()
    }
    

         func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

                return P_VideoMaterials.count
            }
        
         func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            
                let cell = tableView.dequeueReusableCell(withIdentifier: "privateVideoTableCell", for: indexPath) as! privateVideoTableCell
            cell.materialsViewModel = P_VideoMaterials[indexPath.row]
             return cell
            }
        
        
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
                  
            
            
            let storyboard            = UIStoryboard(name: "Visitor", bundle: nil)
            let vc                    = storyboard.instantiateViewController(withIdentifier: "CustomPrivateVideoPlayerVC") as! CustomPrivateVideoPlayerVC
            vc.modalPresentationStyle = .overFullScreen
            vc.modalTransitionStyle   = .crossDissolve
            vc.VideoName = P_VideoMaterials[indexPath.row].Name ?? ""
            vc.VideoUrl  = P_VideoMaterials[indexPath.row].Path ?? ""

            //self.present(vc, animated: true, completion: nil)
            self.show(vc, sender: nil)
            
    }
    
    

}
