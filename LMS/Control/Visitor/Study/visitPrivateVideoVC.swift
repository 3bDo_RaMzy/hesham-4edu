//
//  visitPrivateVideoVC.swift
//  LMS
//
//  Created by MacBook on 11/19/20.
//  Copyright © 2020 eaglez-group. All rights reserved.
//

import UIKit

class visitPrivateVideoVC: BaseVC, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate {

    
    //--> variables
    var Spinner :UIView!
    
    @IBOutlet weak var P_VideoHeaderWidth: NSLayoutConstraint!
    @IBOutlet weak var P_VideoFilterBtn:   UIButton!
    @IBOutlet weak var P_VideoTable:       UITableView!
    @IBOutlet weak var P_VideoTableSearch: UISearchBar!
    
    var P_VideoViewModel = [PrivateVideoViewModel]()
    var filteredData     = [PrivateVideoViewModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.title = "Private Videos"
        
        //privateVideoTableCell
        if !(helper.getStudentTermSystem()!) {
            self.P_VideoFilterBtn.isHidden = true
            self.P_VideoHeaderWidth.constant = 0
            self.fetchData(Term: 0)
            
        } else {
            self.P_VideoFilterBtn.isHidden = false
            self.P_VideoHeaderWidth.constant = 100
            self.fetchData(Term: 1)
        }
        
        self.P_VideoTable.dataSource     = self
        self.P_VideoTable.delegate       = self
        self.P_VideoTableSearch.delegate = self
        self.P_VideoTable.layoutMargins  = UIEdgeInsets.zero
        self.P_VideoTable.separatorInset = UIEdgeInsets.zero
        self.P_VideoTable.rowHeight      = 100
        
    }
    

         func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

                return filteredData.count
            }
        
         func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            
                let cell = tableView.dequeueReusableCell(withIdentifier: "privateVideoTableCell", for: indexPath) as! privateVideoTableCell
            cell.privateVideoViewModel = filteredData[indexPath.row]
             return cell
            }
        
        
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
                  
            
            
            let storyboard            = UIStoryboard(name: "Visitor", bundle: nil)
            let vc                    = storyboard.instantiateViewController(withIdentifier: "visitPrivateVideoDetailsVC") as! visitPrivateVideoDetailsVC
            vc.modalPresentationStyle = .overFullScreen
            vc.modalTransitionStyle   = .crossDissolve
            vc.FolderName = filteredData[indexPath.row].FileName
            vc.P_VideoMaterials = filteredData[indexPath.row].Marials

            //self.present(vc, animated: true, completion: nil)
            self.show(vc, sender: nil)
            
        }
        
        // This method updates filteredData based on the text in the Search Box
        func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
            
            guard !searchText.isEmpty  else {
                filteredData = self.P_VideoViewModel;
                self.P_VideoTable.reloadData()
                return 
            }

                filteredData = self.P_VideoViewModel.filter({ video -> Bool in
                    return video.FileName.lowercased().contains(searchText.lowercased())
            })
            self.P_VideoTable.reloadData()
                
        }
    
    
    
    
//------------------------------------------------------------------
    fileprivate func fetchData(Term: Int) {
        
        Spinner = UIViewController.displaySpinner(onView: self.view)
        
        API.getMaterialVideos(vc: self, Token: helper.getUserToken()!,
                              StudentId: helper.getStudentId()!,
                              TermSystem: Term) {
        (err, TimeOut, PM_Videos) in
                                        
                UIViewController.removeSpinner(spinner: self.Spinner)
                if let err = err {
                    if TimeOut! {
                        let storyboard = UIStoryboard(name: "Auth", bundle: nil)
                        let vc         = storyboard.instantiateViewController(withIdentifier: "SessionExpireVC")
                            vc.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency
                        self.present(vc, animated: true, completion: nil)
                       // self.navigationController?.pushViewController(vc, animated: true)
                        return
                        
                    } else {
                        self.alertmessage(Message: "Failed to fetch private material videos:\n \(err) \n\n Try Again Later.." )
                        return
                    }
                }
                                                                              
                self.P_VideoViewModel = PM_Videos?.map({return  PrivateVideoViewModel(video: $0)}) ?? []
                self.filteredData = self.P_VideoViewModel
                self.P_VideoTable.reloadData()
        }
        
    }  //---> End Of fetchData method -------
    
    
//----------------------------------------------------------------
    
    
    @IBAction func filterActionSheet(_ sender: UIButton) {
        
        
        let alert = UIAlertController(title: "Filter Videos", message: "Please Select Your Semester", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "First Semester", style: .default , handler:{ (UIAlertAction)in
            self.fetchData(Term: 1)
        }))
        
        alert.addAction(UIAlertAction(title: "Second Semester", style: .default , handler:{ (UIAlertAction)in
            self.fetchData(Term: 2)
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler:nil))

        //uncomment for iPad Support
        alert.popoverPresentationController?.sourceView = self.view

        self.present(alert, animated: true, completion: {
            print("completion block")
        })
        
    }
    
    
    

}
