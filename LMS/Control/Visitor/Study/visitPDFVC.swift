//
//  visitPDFVC.swift
//  LMS
//
//  Created by Abdo on 11/29/19.
//  Copyright © 2019 eaglez-group. All rights reserved.
//

import UIKit

class visitPDFVC: BaseVC, UITableViewDataSource, UITableViewDelegate {

            //--> variables
            let documentInteractionController = UIDocumentInteractionController()
            var gradeID = 0
            var termSys = 0
            var selectedTerm = ""
            var Spinner :UIView!
    
    @IBOutlet weak var UnitesHeaderHieght: NSLayoutConstraint!
    @IBOutlet weak var UnitesHeaderView: UIView!
            @IBOutlet weak var UnitesTable: UITableView!
            var unitViewModel = [UnitViewModel]()
            var fileViewModel = [FileViewModel]()
            
            override func viewDidLoad() {
                super.viewDidLoad()
                // Do any additional setup after loading the view.
                self.title = "PDF Files"
                
                if self.termSys == 0 {
                    self.UnitesHeaderView.isHidden = true
                    self.UnitesHeaderHieght.constant = 0
                } else {
                    self.selectedTerm = "First"
                }
                
                documentInteractionController.delegate = self
                self.UnitesTable.dataSource     = self
                self.UnitesTable.delegate       = self
                self.UnitesTable.layoutMargins  = UIEdgeInsets.zero
                self.UnitesTable.separatorInset = UIEdgeInsets.zero
                self.UnitesTable.rowHeight      = 100
                self.fetchData()
                
                //self.tabBarController?.tabBar.isHidden = false
            }//------------ End Of ViewDidLoad ---------------------
                
            
             func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

                    return unitViewModel.count
                }
            
             func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
                
                    let cell = tableView.dequeueReusableCell(withIdentifier: "visitPDFCell", for: indexPath) as! visitPDFCell
                        cell.unitViewModel = unitViewModel[indexPath.row]
                 return cell
                }
            
            
            func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
                      
                
                Spinner = UIViewController.displaySpinner(onView: self.view)
                   API.getUnitePDF(vc: self, Unite_id: unitViewModel[indexPath.row].Id) { ( err, files ) in
                                       
                        UIViewController.removeSpinner(spinner: self.Spinner)
                        if let err = err {
                                print("Failed to fetch files:", err)
                                return
                        }
                        self.fileViewModel = files?.map({return FileViewModel(file: $0)}) ?? []
                    if self.fileViewModel.count > 0 {
                        
                        print("\n PDF url: \(URLs.ImageDomain+self.fileViewModel[0].Path) ")
                        let fileUrl   = URLs.ImageDomain+self.fileViewModel[0].Path
                        let urlString = fileUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!

                        // Passing the remote URL of the file, to be stored and then opted with mutliple actions for the user to perform
                        self.storeAndShare(withURLString: "\(urlString)")
                    } else {
                        
                        self.alertmessage(Message: "No PDF Files Available!")
                    }
                    
                }
        }
                //------------------------------
                fileprivate func fetchData() {
                    
                    Spinner = UIViewController.displaySpinner(onView: self.view)
                    if self.termSys == 0 {
                        
                                        API.getGradeUnites(vc: self, Grade_id: self.gradeID, TermSystem: "Unique" ) { ( err, unites ) in
                                            
                                            UIViewController.removeSpinner(spinner: self.Spinner)
                                            if let err = err {
                                                print("Failed to fetch courses:", err)
                                                return
                                            }
                                            
                                            self.unitViewModel = unites?.map({return UnitViewModel(unit: $0)}) ?? []
                                            self.UnitesTable.reloadData()
                                        }
                    } else {
                        
                                        API.getGradeUnites(vc: self, Grade_id: self.gradeID, TermSystem: self.selectedTerm ) { ( err, unites ) in
                                                   
                                            UIViewController.removeSpinner(spinner: self.Spinner)
                                            if let err = err {
                                                        print("Failed to fetch courses:", err)
                                                        return
                                            }
                                                   
                                            self.unitViewModel = unites?.map({return UnitViewModel(unit: $0)}) ?? []
                                            self.UnitesTable.reloadData()
                                        }
                    }
                    
                    
                }//---> End Of fetchData method -------
         
    
    @IBOutlet weak var firstBtn: UIButton!
    @IBOutlet weak var secondBtn: UIButton!
    @IBAction func firstAction(_ sender: UIButton) {
        self.selectedTerm = "First"
        firstBtn.backgroundColor = UIColor(red: 140 / 255, green: 199 / 255, blue: 181 / 255, alpha: 1.0)
        secondBtn.backgroundColor = UIColor(red: 224 / 255, green: 225 / 255, blue: 226 / 255, alpha: 1.0)
        firstBtn.setTitleColor(UIColor.white, for: .normal)
        secondBtn.setTitleColor(UIColor.darkGray, for: .normal)
        self.fetchData()
    }
    
    @IBAction func secondAction(_ sender: UIButton) {
        self.selectedTerm = "Second"
        secondBtn.backgroundColor = UIColor(red: 140 / 255, green: 199 / 255, blue: 181 / 255, alpha: 1.0)
        firstBtn.backgroundColor = UIColor(red: 224 / 255, green: 225 / 255, blue: 226 / 255, alpha: 1.0)
        secondBtn.setTitleColor(UIColor.white, for: .normal)
        firstBtn.setTitleColor(UIColor.darkGray, for: .normal)
        self.fetchData()
    }

    
    
    
    }//---> End Of Class -----------------

//----------------------------------------
//---> Extentions
extension visitPDFVC: UIDocumentInteractionControllerDelegate {
    
    // If presenting atop a navigation stack, provide the navigation controller in order to animate in a manner consistent with the rest of the platform
    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController {
        guard let navVC = self.navigationController else {
            return self
        }
        return navVC
    }

    // This function will set all the required properties, and then provide a preview for the document
    func share(url: URL) {
        
        self.documentInteractionController.url = url
//        self.documentInteractionController.uti = url.typeIdentifier ?? "public.data, public.content"
//        self.documentInteractionController.name = url.localizedName ?? url.lastPathComponent
        self.documentInteractionController.presentPreview(animated: true)
        
    }
    
    // This function will store your document to some temporary URL and then provide sharing, copying, printing, saving options to the user
    func storeAndShare(withURLString: String) {
        
        Spinner = UIViewController.displaySpinner(onView: self.view)
        guard let url = URL(string: withURLString) else { return }
        /// START YOUR ACTIVITY INDICATOR HERE
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard let data = data, error == nil else { return }
            
            let tmpURL = FileManager.default.temporaryDirectory
                .appendingPathComponent(response?.suggestedFilename ?? "fileName.png")
            do {
                try data.write(to: tmpURL)
                UIViewController.removeSpinner(spinner: self.Spinner)
            } catch {
                print(error)
            }
            
            DispatchQueue.main.async {
                /// STOP YOUR ACTIVITY INDICATOR HERE
                self.share(url: tmpURL)
            }
            
            }.resume()
        //self.dismiss(animated: true, completion: nil)
    }
}
