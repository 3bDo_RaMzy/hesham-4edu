//
//  visitContactUsVC.swift
//  LMS
//
//  Created by Abdo on 11/28/19.
//  Copyright © 2019 eaglez-group. All rights reserved.
//

import UIKit
import Alamofire
import SwiftMessages
import SwiftMessageBar
import SVProgressHUD

class visitContactUsVC: BaseVC {

    
    @IBOutlet weak var txtFullName:    DesignableTextField!
    @IBOutlet weak var txtEmail:       DesignableTextField!
    @IBOutlet weak var txtPhoneNumber: DesignableTextField!
    @IBOutlet weak var txtSubject:     DesignableTextField!
    @IBOutlet weak var txtMessage:     UITextView!

    var Spinner :UIView!
    var social :SocialNetwork!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        //--------------
        self.getSocail()
        
        
        
    }//---> End Of ViewDidLoad ---
    
    
    
 //-------------------------------------------------------
    @IBAction func SendvisitContactUsAction(_ sender: Any) {
    
        guard txtFullName.text != ""  else {
            alertmessage(Message: "Your full name is empty")
            return
        }
        guard txtEmail.text != ""  else {
            alertmessage(Message: "Your email is empty")
            return
        }
        guard txtPhoneNumber.text != ""  else {
            alertmessage(Message: "Your phone number is empty")
            return
        }
        guard txtSubject.text != ""  else {
            alertmessage(Message: "Your subject is empty")
            return
        }
        guard txtMessage.text != ""  else {
            alertmessage(Message: "Your message is empty")
            return
        }
        
        //self.showLoading()
        Spinner = UIViewController.displaySpinner(onView: self.view)
        API.sendVisitContactUs(vc: self, FullName: txtFullName.text!, PhoneNumber: txtPhoneNumber.text!,
                                Message: txtMessage.text!, Subject: txtSubject.text!, Email: txtEmail.text!,
                                ContactUs: ""){ (error, status) in
                                    
                                //self.hideLoading()
                                UIViewController.removeSpinner(spinner: self.Spinner)
                                if error != nil {
                                    
                                    self.alertmessage(Message: "Error Server Not Response")
                                    
                                } else if status == "Done"{
                                    
                                           self.resetData()
                                           self.alertmessage(Message: "Successful sending your message")
//                                           let success =      MessageView.viewFromNib(layout: .cardView)
//                                                         success.configureTheme(.success)
//                                                         success.configureDropShadow()
//                                                         success.configureContent(title: "Successful", body: "Sending Your Message ")
//                                                         success.button?.isHidden = false
//                                                         success.button?.titleLabel?.text = "Gallery"
//                                                         success.buttonTapHandler = { (sender) -> Void in
//
//                                           var successConfig = SwiftMessages.defaultConfig
//                                                         successConfig.presentationStyle = .center
//                                                         successConfig.presentationContext = .window(windowLevel: UIWindow.Level.normal)
//                                           SwiftMessages.show(config: successConfig, view: success)
//
//                                          }
                                } else {
                                            self.alertmessage(Message: "Faild, Can't send your message")
//                                          let success =      MessageView.viewFromNib(layout: .cardView)
//                                                       success.configureTheme(.error)
//                                                       success.configureDropShadow()
//                                                       success.configureContent(title: "Faild", body: "Can't send your message")
//                                                       success.button?.isHidden = false
//                                                       success.button?.titleLabel?.text = "Gallery"
//                                                       success.buttonTapHandler = { (sender) -> Void in
//
//                                           var successConfig = SwiftMessages.defaultConfig
//                                                       successConfig.presentationStyle = .center
//                                                       successConfig.presentationContext = .window(windowLevel: UIWindow.Level.normal)
//                                           SwiftMessages.show(config: successConfig, view: success)
//
//                                            }
                                    }
                                    
              }// -----    End Of Api Request ------
        
    }
    
    private func resetData(){
        
        self.txtFullName.text    = ""
        self.txtEmail.text       = ""
        self.txtPhoneNumber.text = ""
        self.txtSubject.text     = ""
        self.txtMessage.text     = ""
    }
//--------------------------------------------------------
    func getSocail(){
        //self.showLoading()
        Spinner = UIViewController.displaySpinner(onView: self.view)
        //Downloading article  data for TableView
        AF.request("\(URLs.BaseUrl)api/SocialReferences/GetAll").responseJSON { response in
            //self.hideLoading()
            UIViewController.removeSpinner(spinner: self.Spinner)
            let result = response.result
            print(result)
            switch response.result {
            case .success(let value):
                //print(String(data: value as! Data, encoding: .utf8)!)
                if let dict = value as? Dictionary<String, AnyObject> {
                    self.social = SocialNetwork(social: dict)
                }
                
            case .failure(let error):
                print(error)
                
            }
            
            
        }
    }
    
    @IBAction func facebook(_ sender: Any) {
       if social == nil {
            alertmessage(Message: "link not avaliable")
        }else{
        if let url = NSURL(string: social._FaceBookUrl){ UIApplication.shared.open(url as URL, options: [:], completionHandler: nil) }
        }
    }
    @IBAction func googlePlus(_ sender: Any) {
        if social == nil {
            alertmessage(Message: "link not avaliable")
        }else{
            if let url = NSURL(string: social._GoogleUrl){ UIApplication.shared.open(url as URL, options: [:], completionHandler: nil) }
        }
    }
    @IBAction func twitter(_ sender: Any) {
        if social == nil {
            alertmessage(Message: "link not avaliable")
        }else{
            if let url = NSURL(string: social._TwitterUrl){ UIApplication.shared.open(url as URL, options: [:], completionHandler: nil) }
        }
    }
    
}//---> End Of Class --------------------------
