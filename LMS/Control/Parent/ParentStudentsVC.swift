//
//  ParentStudentsVC.swift
//  LMS
//
//  Created by Abdo on 12/12/19.
//  Copyright © 2019 eaglez-group. All rights reserved.
//

import UIKit

class ParentStudentsVC: BaseVC, UITableViewDataSource, UITableViewDelegate {

    
        @IBOutlet weak var StudentsTable: UITableView!
        
        public static var StudentId : Int = 0
        var parentStudentsViewModel = [ParentStudentsViewModel]()
        var Spinner :UIView!
    
    
    override func viewDidLoad() {
            super.viewDidLoad()
            // Do any additional setup after loading the view.
        
        self.title = "My Sons"
                                   
        self.StudentsTable.dataSource     = self
        self.StudentsTable.delegate       = self
        self.StudentsTable.layoutMargins  = UIEdgeInsets.zero
        self.StudentsTable.separatorInset = UIEdgeInsets.zero
        self.StudentsTable.rowHeight      = 180
        self.fetchData()
                
            }//------------ End Of ViewDidLoad ---------------------
                
            
             func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

                    return parentStudentsViewModel.count
                }
            
             func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
                
                    let cell = tableView.dequeueReusableCell(withIdentifier: "ParentStudentsCell", for: indexPath) as! ParentStudentsCell
                        cell.parentStudentsViewModel = parentStudentsViewModel[indexPath.row]
                 return cell
                }
            
            
            func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
                     
                ParentStudentsVC.StudentId = self.parentStudentsViewModel[indexPath.row].StudentId
                
                // Do something now
                let storyboard            = UIStoryboard(name: "Parent", bundle: nil)
                let vc                    = storyboard.instantiateViewController(withIdentifier: "ParentChoiseVC") as! ParentChoiseVC
                    vc.modalPresentationStyle  = .overFullScreen
                    vc.modalTransitionStyle    = .crossDissolve
                    vc.parentStudentsViewModel = self.parentStudentsViewModel[indexPath.row]

                //self.present(vc, animated: true, completion: nil)
                self.show(vc, sender: nil)
                }
    
            //------------------------------
                fileprivate func fetchData() {
                    
                    Spinner = UIViewController.displaySpinner(onView: self.view)
                    API.getParentStudentsList(vc: self, Token: helper.getUserToken()!)
                    { ( err,TimeOut, students ) in
                        
                        UIViewController.removeSpinner(spinner: self.Spinner)
                       
                        if let err = err {
                            if TimeOut! {
                                let storyboard = UIStoryboard(name: "Auth", bundle: nil)
                                let vc         = storyboard.instantiateViewController(withIdentifier: "SessionExpireVC")
                                    vc.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency
                                self.present(vc, animated: true, completion: nil)
                               // self.navigationController?.pushViewController(vc, animated: true)
                                return
                                
                            } else {
                                self.alertmessage(Message: "Failed to fetch parent students:\n \(err) \n\n Try Again Later.." )
                                return
                            }
                        }
                        
                        self.parentStudentsViewModel = students?.map({return ParentStudentsViewModel(parentST: $0)}) ?? []
                        self.StudentsTable.reloadData()
                    }
                    
                }//---> End Of fetchData method -------
}
