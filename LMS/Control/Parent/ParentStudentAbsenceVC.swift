//
//  ParentStudentAbsenceVC.swift
//  LMS
//
//  Created by Abdo on 12/16/19.
//  Copyright © 2019 eaglez-group. All rights reserved.
//

import UIKit

class ParentStudentAbsenceVC: BaseVC {

    //------------------------
    
    //--> Date Choise
    @IBOutlet weak var dateFromButton: UIButton!
    @IBOutlet weak var dateToButton:   UIButton!
    @IBOutlet var      dateFromView:   UIView!
    @IBOutlet var      dateToView:     UIView!
    @IBOutlet weak var dateFromPicker: UIDatePicker!
    @IBOutlet weak var dateToPicker:   UIDatePicker!
    @IBOutlet weak var dateFromTxt:    UILabel!
    @IBOutlet weak var dateToTxt:      UILabel!
    
    @IBOutlet weak var bgView: UIView!
    
    @IBOutlet weak var QuizResultsTable: UITableView!
    var quizResultViewModel = [QuizResultViewModel]()
    
    var dateFrom             = ""
    var dateTo               = ""
    let dateFormatterView    = DateFormatter()
    let dateFormatterData    = DateFormatter()
    
    override func viewDidLoad() {
        super.viewDidLoad()

             self.QuizResultsTable.dataSource     = self
             self.QuizResultsTable.layoutMargins  = UIEdgeInsets.zero
             self.QuizResultsTable.separatorInset = UIEdgeInsets.zero
             self.QuizResultsTable.rowHeight      = 100

            //-------------------------------
            //---> For DatePicker / Date
            dateFromPicker.date      = NSDate.init(timeIntervalSinceNow: 0) as Date
            dateFormatterView.dateFormat = "dd/MM/yyyy"
            dateFromTxt.text = String(dateFormatterView.string(from: dateFromPicker.date))
            dateFrom = String(dateFormatterView.string(from: dateFromPicker.date))
            
            dateToPicker.date      = NSDate.init(timeIntervalSinceNow: 0) as Date
            dateFormatterView.dateFormat = "dd/MM/yyyy"
            dateToTxt.text = String(dateFormatterView.string(from: dateToPicker.date))
            dateTo = String(dateFormatterView.string(from: dateToPicker.date))
        
         }//------------ End Of ViewDidLoad ---------------------
    
    @IBAction func showDateFromView(_ sender: Any) {
        
        inAnimateShowDateFrom()
    }
    @IBAction func showDateToView(_ sender: Any) {
        
        inAnimateShowDateTo()
    }
    @IBAction func removeDateFromView(_ sender: Any) {
        
        dateFormatterData.dateFormat = "MM/dd/yyyy"
        dateFromTxt.text = String(dateFormatterView.string(from: dateFromPicker.date))
        dateFrom = String(dateFormatterData.string(from: dateFromPicker.date))
        
        outAnimateShowDateFrom()
    }
    @IBAction func removeDateToView(_ sender: Any) {
        
        dateFormatterData.dateFormat = "MM/dd/yyyy"
        dateToTxt.text = String(dateFormatterView.string(from: dateToPicker.date))
        dateTo = String(dateFormatterData.string(from: dateToPicker.date))
        
        outAnimateShowDateTo()
    }
    
    //--------------------------------------------------------------------------------
    func inAnimateShowDateFrom() {
        self.bgView.isUserInteractionEnabled = false
        self.dateToView.removeFromSuperview()
        self.view.addSubview(dateFromView)
        dateFromView.center    = self.view.center
        dateFromView.transform = CGAffineTransform.init(scaleX: 1.3, y: 1.3)
        dateFromView.alpha     = 0
        UIView.animate(withDuration: 0.4) {
            self.dateFromView.alpha     = 1
            self.dateFromView.transform = CGAffineTransform.identity
        }
    }
    func outAnimateShowDateFrom() {
        self.bgView.isUserInteractionEnabled = true
        UIView.animate(withDuration: 0.3, animations: {
            self.dateFromView.transform = CGAffineTransform.init(scaleX: 1.3, y: 1.3)
            self.dateFromView.alpha     = 0
        }) { (success:Bool) in
            self.dateFromView.removeFromSuperview()
        }
    }//--------------------------------------------
    func inAnimateShowDateTo() {
        self.bgView.isUserInteractionEnabled = false
        self.dateFromView.removeFromSuperview()
        self.view.addSubview(dateToView)
        dateToView.center    = self.view.center
        dateToView.transform = CGAffineTransform.init(scaleX: 1.3, y: 1.3)
        dateToView.alpha     = 0
        UIView.animate(withDuration: 0.4) {
            self.dateToView.alpha     = 1
            self.dateToView.transform = CGAffineTransform.identity
        }
    }
    func outAnimateShowDateTo() {
        self.bgView.isUserInteractionEnabled = true
        UIView.animate(withDuration: 0.3, animations: {
            self.dateToView.transform = CGAffineTransform.init(scaleX: 1.3, y: 1.3)
            self.dateToView.alpha     = 0
        }) { (success:Bool) in
            self.dateToView.removeFromSuperview()
        }
    }//--------------------------------------------
    

    @IBAction func showResultAction(_ sender: Any) {
        
        if dateFrom == "" || dateTo == "" {
            
            self.alertmessage(Message: "Please, choose your search dates")
            
        }else {
        
                API.getParentStudentAbsence(vc: self, Token:  helper.getUserToken() ?? "",
                                                from: dateFrom, to: dateTo, studentId: ParentStudentsVC.StudentId   )
                { ( err, TimeOut, results ) in
                                                       
                        
                        if let err = err {
                            if TimeOut! {
                                let storyboard = UIStoryboard(name: "Auth", bundle: nil)
                                let vc         = storyboard.instantiateViewController(withIdentifier: "SessionExpireVC")
                                    vc.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency
                                self.present(vc, animated: true, completion: nil)
                               // self.navigationController?.pushViewController(vc, animated: true)
                                return
                                
                            } else {
                                self.alertmessage(Message: "Failed to fetch student result:\n \(err) \n\n Try Again Later.." )
                                return
                            }
                        }
                                                       
                        self.quizResultViewModel = results?.map({return QuizResultViewModel(q_result: $0)}) ?? []
                        self.QuizResultsTable.reloadData()
                }
            
            
            
        }
        
        
    }
    
 
}//---- End Of Class ------------------------------------

extension ParentStudentAbsenceVC: UITableViewDataSource {
    
    
    
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

            return quizResultViewModel.count
        }
    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
            let cell = tableView.dequeueReusableCell(withIdentifier: "ParentStudentAbsenceCell", for: indexPath) as! ParentStudentAbsenceCell
        cell.absenceReportViewModel = quizResultViewModel[indexPath.row]
         return cell
        }
}
