//
//  ParentChoiseVC.swift
//  LMS
//
//  Created by Abdo on 12/12/19.
//  Copyright © 2019 eaglez-group. All rights reserved.
//

import UIKit

class ParentChoiseVC: UIViewController {
 
    
    @IBOutlet weak var sonName:  UILabel!
    @IBOutlet weak var sonGrade: UILabel!
    @IBOutlet weak var sonGroup: UILabel!
    @IBOutlet weak var sonImage: UIImageView!
    
    
    
    var activityIndicator       = UIActivityIndicatorView()
    var parentStudentsViewModel = ParentStudentsViewModel()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        sonName?.text  = parentStudentsViewModel.StundetName
        sonGrade?.text = parentStudentsViewModel.Grade
        sonGroup?.text = parentStudentsViewModel.GroupName
        
        sonImage.addSubview(self.activityIndicator)
        activityIndicator.startAnimating()
        if parentStudentsViewModel  == nil {
            
            self.sonImage.image = UIImage(named: "student.png")
        } else if parentStudentsViewModel.ImageUrl == "" {
            
            self.sonImage.image = UIImage(named: "student.png")
        } else {
            
            self.sonImage.sd_setImage(with: URL(string:  URLs.ImageDomain + parentStudentsViewModel.ImageUrl.dropFirst(1)), placeholderImage: UIImage(named: "student.png"))
        }
        self.activityIndicator.stopAnimating()

        
    }
    


    

}
