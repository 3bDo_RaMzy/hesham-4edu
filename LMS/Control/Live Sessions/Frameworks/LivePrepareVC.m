//
//  LiveListVC.m
//  LMS
//
//  Created by Abdo on 4/2/20.
//  Copyright © 2020 eaglez-group. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "LivePrepareVC.h"
#import "ConferenceViewController.h"

@interface LivePrepareVC ()

@end

@implementation LivePrepareVC

- (void)viewDidLoad {
    [super viewDidLoad];

        [self performSelector:@selector(showLiveScreen) withObject:nil afterDelay:0.1];
        //self.room = nil;
    
}
 


- (void)showLiveScreen {
    [self performSegueWithIdentifier:@"joinConference" sender:self];
}


- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    // Don't present the ConferenceViewController if no room was specified.
    if ([identifier isEqualToString:@"joinConference"]) {
        self.room = self.textField.text;
        if (self.room == nil || [self.room length] == 0) {
            return NO;
        }
    }
    
    return YES;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"joinConference"]) {
        // Attach the room to the new controller.
        ConferenceViewController *vc = [segue destinationViewController];
        vc.room =  self.room;
    }
}

@end
