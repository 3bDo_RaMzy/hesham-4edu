//
//  ConferenceViewController.h
//  LMS
//
//  Created by Abdo on 4/1/20.
//  Copyright © 2020 eaglez-group. All rights reserved.
//

//#ifndef ConferenceViewController_h
//#define ConferenceViewController_h
//
//
//#endif /* ConferenceViewController_h */

#import <UIKit/UIKit.h>

@import JitsiMeet;


@interface ConferenceViewController : UIViewController<JitsiMeetViewDelegate>

@property (nonatomic, weak) NSString *room;

@end
