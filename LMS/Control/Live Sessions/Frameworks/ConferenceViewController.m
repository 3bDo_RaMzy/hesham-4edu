//
//  ConferenceViewController.m
//  LMS
//
//  Created by Abdo on 4/1/20.
//  Copyright © 2020 eaglez-group. All rights reserved.
//

//#import <Foundation/Foundation.h>

#import "ConferenceViewController.h"

@interface ConferenceViewController ()

@end

@implementation ConferenceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (self.room == nil) {
        NSLog(@"Room is nul!");

        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self dismissViewControllerAnimated:YES completion:nil];
        });

        return;
    }
       
    // Attach this controller as the delegate.
    JitsiMeetView *jitsiView = (JitsiMeetView*)self.view;
    jitsiView.delegate = self;
    
    // Join the room.
    JitsiMeetConferenceOptions *options
        = [JitsiMeetConferenceOptions fromBuilder:^(JitsiMeetConferenceOptionsBuilder *builder) {
            builder.room = self.room;
            builder.videoMuted = true;
        }];
    [jitsiView join:options];
}



- (void)conferenceWillJoin:(NSDictionary *)data {
    NSLog(@"About to join conference %@", self.room);
}

- (void)conferenceJoined:(NSDictionary *)data {
    NSLog(@"Conference %@ joined", self.room);
}

- (void)conferenceTerminated:(NSDictionary *)data {
    NSLog(@"Conference %@ terminated", self.room);
   
    //--- DismissCurrentController
    //[self dismissViewControllerAnimated:YES completion:nil];
    
    //--- DismissAllControllers
    [[[self presentingViewController]presentingViewController]dismissModalViewControllerAnimated:YES];
    
}

@end
