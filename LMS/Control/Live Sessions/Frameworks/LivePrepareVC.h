//
//  LiveListVC.h
//  LMS
//
//  Created by Abdo on 4/2/20.
//  Copyright © 2020 eaglez-group. All rights reserved.
//

#ifndef LiveListVC_h
#define LiveListVC_h


#endif /* LiveListVC_h */

#import <UIKit/UIKit.h>

@interface LivePrepareVC : UIViewController

@property (nonatomic, weak) IBOutlet UITextField *textField;
@property (nonatomic, weak) IBOutlet UIButton *button;
@property (nonatomic, copy) NSString *room;

@end
