//
//  LiveListVC.swift
//  LMS
//
//  Created by Abdo on 4/5/20.
//  Copyright © 2020 eaglez-group. All rights reserved.
//

import UIKit

class LiveListVC: BaseVC {

    
    @IBOutlet weak var LiveTableView: UITableView!
    var liveViewModel = [Live]()
    var Spinner :UIView!
    private let refreshControl = UIRefreshControl()
    
    let dateFormatter = DateFormatter()
    let date          = Date()
    
    override func viewDidLoad() {
        super.viewDidLoad()

         dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
         // Do any additional setup after loading the view.
         self.title = "Live Sessions"
         
         // set up the refresh control
        if #available(iOS 10.0, *) {
             LiveTableView.refreshControl = refreshControl
         } else {
             LiveTableView.addSubview(refreshControl)
         }
          refreshControl.tintColor = UIColor(red:0.25, green:0.72, blue:0.85, alpha:1.0)
          refreshControl.attributedTitle = NSAttributedString(string: "Refresh Data...", attributes: nil)
          refreshControl.addTarget(self, action: #selector(refreshData(_:)), for: .valueChanged)
                                     
          self.LiveTableView.dataSource     = self
          self.LiveTableView.delegate       = self
          self.LiveTableView.layoutMargins  = UIEdgeInsets.zero
          self.LiveTableView.separatorInset = UIEdgeInsets.zero
          self.LiveTableView.rowHeight      = 80
          self.fetchData()
         
    } //-----> End of ViewDidLoad --------
    

    
    @objc private func refreshData(_ sender: Any) {
        // Fetch Weather Data
        self.refreshData()
    }

    //--------------------------------------------------------------------
    fileprivate func fetchData() {
                        
        Spinner = UIViewController.displaySpinner(onView: self.view)
        
        API.getLiveList(vc: self, Token: helper.getUserToken()!)
        { ( err, TimeOut, lives ) in
                                        
                    UIViewController.removeSpinner(spinner: self.Spinner)

                      if let err = err {
                          if TimeOut! {
                              let storyboard = UIStoryboard(name: "Auth", bundle: nil)
                              let vc         = storyboard.instantiateViewController(withIdentifier: "SessionExpireVC")
                                  vc.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency
                              self.present(vc, animated: true, completion: nil)
                             // self.navigationController?.pushViewController(vc, animated: true)
                              return
                              
                          } else {
                              self.alertmessage(Message: "Failed to fetch live sessions:\n \(err) \n\n Try Again Later.." )
                              return
                          }
                      }
                            
                    self.liveViewModel = lives?.map({return Live(live:  $0)}) ?? []
                    if self.liveViewModel.count <= 0 {
                        self.showToast(message: "No Live Sessions Available Now !!", vc: self)
                    }
                    self.LiveTableView.reloadData()
            }
                        
     }//---> End Of fetchData method -------
    fileprivate func refreshData() {
                       
        self.refreshControl.beginRefreshing()
        API.getLiveList(vc: self, Token: helper.getUserToken()!)
        { ( err, TimeOut, lives ) in
                           
                   self.refreshControl.endRefreshing()
                         
                   if let err = err {
                        if TimeOut! {
                                let storyboard = UIStoryboard(name: "Auth", bundle: nil)
                                let vc         = storyboard.instantiateViewController(withIdentifier: "SessionExpireVC")
                                    vc.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency
                                self.present(vc, animated: true, completion: nil)
                                // self.navigationController?.pushViewController(vc, animated: true)
                                return
                                 
                        } else {
                                self.alertmessage(Message: "Failed to fetch live sessions:\n \(err) \n\n Try Again Later.." )
                                return
                        }
                    }
            
                           
                   self.liveViewModel = lives?.map({return Live(live:  $0)}) ?? []
                   if self.liveViewModel.count <= 0 {
                      self.showToast(message: "No Live Sessions Available Now !!", vc: self)
                   }
                   self.LiveTableView.reloadData()
           }
                       
    }//---> End Of fetchData method -------
    
    //--------------------------------------------------------------------
        @objc func FuncRecordAttendance(_ sender: UIButton){
                    
            
            Spinner = UIViewController.displaySpinner(onView: self.view)
            API.sendStudentAttendance(vc: self, StudentId: helper.getStudentId()!,
                                      LiveVideoId: liveViewModel[sender.tag].Id, LiveTime: dateFormatter.string(from: date) ){
                (error,TimeOut, status) in
                               
                
                                UIViewController.removeSpinner(spinner: self.Spinner)
                
                                 if let err = error {
                                      if TimeOut! {
                                              let storyboard = UIStoryboard(name: "Auth", bundle: nil)
                                              let vc         = storyboard.instantiateViewController(withIdentifier: "SessionExpireVC")
                                                  vc.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency
                                              self.present(vc, animated: true, completion: nil)
                                              // self.navigationController?.pushViewController(vc, animated: true)
                                              return
                                               
                                      } else {
                                              self.alertmessage(Message: "Failed to fetch live sessions:\n \(err) \n\n Try Again Later.." )
                                              return
                                      }
                                  }
                

                                let storyboard   = UIStoryboard(name: "Main", bundle: nil)
                                let vc           = storyboard.instantiateViewController(withIdentifier: "LivePrepareVC") as! LivePrepareVC
                                              vc.modalPresentationStyle = .overFullScreen
                                              vc.modalTransitionStyle   = .crossDissolve
                                              vc.room =  self.liveViewModel[sender.tag].Name
                                self.present(vc, animated: true, completion: nil)
                                                      
                
            }
        }
    
        
    

}//-------- End Of Class ------------------------------------------------

extension LiveListVC: UITableViewDataSource, UITableViewDelegate{
    

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
 
            return liveViewModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
            let cell = tableView.dequeueReusableCell(withIdentifier: "LiveCell", for: indexPath) as! LiveCell
                cell.liveViewModel = liveViewModel[indexPath.row]
                cell.LiveJoin.tag = indexPath.row
                cell.LiveJoin.addTarget(self, action: #selector(FuncRecordAttendance(_:)), for: .touchUpInside)
         return cell
    }

                
    
}
